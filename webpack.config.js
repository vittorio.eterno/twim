var Encore = require('@symfony/webpack-encore');

Encore
// the project directory where all compiled assets will be stored
    .setOutputPath('web/build/')

    // the public path used by the web server to access the previous directory
    .setPublicPath('/build')

    // allow legacy applications to use $/jQuery as a global variable
    .autoProvidejQuery()

    // enable source maps during development
    .enableSourceMaps(!Encore.isProduction())

    // empty the outputPath dir before each build
    .cleanupOutputBeforeBuild()

    // show OS notifications when builds finish/fail
    .enableBuildNotifications()

    // create hashed filenames (e.g. app.abc123.css)
    // .enableVersioning()

    // allow sass/scss files to be processed
    .enableSassLoader()


    // will create web/build/app.js and web/build/app.css
    .createSharedEntry('js/common', ['jquery'])

    .addEntry('js/app', './assets/js/app.js')
    .addStyleEntry('css/app', './assets/scss/app.scss')

    // Add Entry JS and SCSS for Admin
    .addEntry('js/admin', './assets/js/admin.js')
    .addStyleEntry('css/admin', './assets/scss/admin.scss')
    .addEntry('js/sb-admin', './assets/vendor/sb-admin/js/sb-admin.js')
    .addStyleEntry('css/sb-admin', './assets/vendor/sb-admin/scss/sb-admin.scss')

    // Add Entry JS and SCSS for PayPal
    .addEntry('js/paypal_admin', './assets/js/paypal_admin.js')
    .addEntry('js/paypal', './assets/js/paypal.js')

    // Add Entry JS and SCSS for Signup && Login
    .addEntry('js/signup_login', './assets/js/signup_login.js')

    .addEntry('js/setting', './assets/js/setting.js')
    .addEntry('js/purchasing_transaction', './assets/js/purchasing_transaction.js')
    .addEntry('js/addresses', './assets/js/addresses.js')

    .addEntry('js/share', './assets/js/share.js')
;

// export the final configuration
module.exports = Encore.getWebpackConfig();