Symfony Standard Edition
========================

**WARNING**: This distribution is only a cleaned project with the main Symfony feature already configured.
So that you are ready to coding, deploy and make rich yourself!!!

What's inside?
--------------

The Symfony Standard Edition is configured with the following defaults:

  * An EmailBundle with main service of mailing and queue;

  * An ImageBundle to define main utils for croping resizing all your uploaded images

  * An UserBundle that inherit FOSUserBundle

  * Twig as the only configured template engine;

  * Doctrine ORM/DBAL;

  * Swiftmailer;

  * Annotations enabled for everything.

It comes pre-configured with the following bundles:

  * **FrameworkBundle** - The core Symfony framework bundle

  * [**SensioFrameworkExtraBundle**][6] - Adds several enhancements, including
    template and routing annotation capability

  * [**DoctrineBundle**][7] - Adds support for the Doctrine ORM

  * [**TwigBundle**][8] - Adds support for the Twig templating engine

  * [**SecurityBundle**][9] - Adds security by integrating Symfony's security
    component

  * [**SwiftmailerBundle**][10] - Adds support for Swiftmailer, a library for
    sending emails

  * [**MonologBundle**][11] - Adds support for Monolog, a logging library

  * **WebProfilerBundle** (in dev/test env) - Adds profiling functionality and
    the web debug toolbar

  * **SensioDistributionBundle** (in dev/test env) - Adds functionality for
    configuring and working with Symfony distributions

  * [**SensioGeneratorBundle**][13] (in dev env) - Adds code generation
    capabilities

  * [**WebServerBundle**][14] (in dev env) - Adds commands for running applications
    using the PHP built-in web server

  * **DebugBundle** (in dev/test env) - Adds Debug and VarDumper component
    integration

All libraries and bundles included in the Symfony Standard Edition are
released under the MIT or BSD license.

Enjoy!

[1]:  https://symfony.com/doc/3.4/setup.html
[6]:  https://symfony.com/doc/current/bundles/SensioFrameworkExtraBundle/index.html
[7]:  https://symfony.com/doc/3.4/doctrine.html
[8]:  https://symfony.com/doc/3.4/templating.html
[9]:  https://symfony.com/doc/3.4/security.html
[10]: https://symfony.com/doc/3.4/email.html
[11]: https://symfony.com/doc/3.4/logging.html
[13]: https://symfony.com/doc/current/bundles/SensioGeneratorBundle/index.html
[14]: https://symfony.com/doc/current/setup/built_in_web_server.html
[15]: https://symfony.com/doc/current/setup.html
[16]: https://symfony.com/doc/3.4/frontend/encore/installation.html
[17]: https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-redis-on-ubuntu-18-04

What i must install in my local machine?
--------------

  * [**WebPackEncore**][16] It's all installed already! But you need to install **Node JS** 
  and **Yarn** extension

  * [**Redis**][17] Follow instructions to install Redis on Ubuntu 18.04
  * **Crontab** 
  

  
Symfony Utilities
--------------


#### Container

    php bin/console debug:container



#### Route

    php bin/console debug:router



#### Doctrine

    php bin/console doctrine:database:drop --force
    php bin/console doctrine:database:create

    php bin/console doctrine:generate:entity
    
    php bin/console doctrine:schema:validate
    
    php bin/console doctrine:schema:update --force


    
#### Cache

    php bin/console doctrine:cache:clear-metadata
    
    php bin/console cache:clear
    
    php bin/console redis:flushdb
    
    
#### Webpack encore
    
compile assets once    
    
    yarn encore dev

or, recompile assets automatically when files change

    yarn encore dev --watch

On deploy, create a production build

    yarn encore production

  
Technical Utilities
--------------


    php bin/console import:countries
    php bin/console import:regions
    php bin/console import:provinces
    php bin/console import:cities
 
    php bin/console doctrine:query:sql "$(< src/ResourceBundle/Data/calendar_timezones.sql)"
    php bin/console doctrine:query:sql "$(< src/ResourceBundle/Data/settings.sql)"
    php bin/console doctrine:query:sql "$(< src/ResourceBundle/Data/translations.sql)"
    php bin/console doctrine:query:sql "$(< src/ResourceBundle/Data/optionals_structure.sql)"
    php bin/console doctrine:query:sql "$(< src/ResourceBundle/Data/optionals.sql)"
    php bin/console doctrine:query:sql "$(< src/ResourceBundle/Data/picture_frames.sql)"
  
  
Directories on the app
--------------

 * path/to/openssl
 * image_base_path