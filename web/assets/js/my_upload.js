function UploadFiles(tagId, url){
    $("#"+tagId).fileinput({
        uploadUrl: url, 
        maxFileSize: 1000 //1000 = 1Mb
    });
}