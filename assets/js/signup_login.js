const $ = require('jquery');


var timeout;

$(document).ready(function() {


    $('#btn-form-signin').on('click', function(e){
        e.preventDefault();
        var formSignin = $('#modal-form-signin');

        $.ajax({
            type        : formSignin.attr( 'method' ),
            url         : formSignin.attr( 'action' ),
            data        : formSignin.serialize()
        })
            .done(function(data, status, object){
                if (data.success === false) {
                    $("#dv-mdl-form-signin-error").show().html("").append(createBootstrapErrorSpan(data.message))
                } else {
                    window.location.href = data.targetUrl;
                }
            })
        ;
    });

    $('#btn-signup-step-1').on('click', function(e){
        e.preventDefault();

        var url      = $('#rw-signup-step-1').data('url');
        var email    = $('#signup_email').val();
        var password = $('#signup_plainPassword').val();
        var token    = $('#hdn-token-step-1').val();

        $.ajax({
            type        : "POST",
            url         : url,
            data        : {
                "email"     : email,
                "password"  : password,
                "token"     : token
            }
        })
            .done(function(data, status, object){
                $('.bsp-err-spn').remove();
                $(".is-invalid").removeClass("is-invalid");
                $('#rw-signup-step-1').hide();
                $('#rw-signup-step-2').show();
                $('#btn_signup_previous').show();
                $('#signup_username').trigger("focus");
            })
            .fail(function (jqXHR, textStatus, errorThrown){

                $('.bsp-err-spn').remove();
                $(".is-invalid").removeClass("is-invalid");

                var errors = jqXHR.responseJSON.errors;
                for (var key in errors) {
                    var selector = "#signup_" + key;
                    var input = $(selector);
                    $(input).addClass("is-invalid");

                    if (errors.hasOwnProperty(key)) {
                        var errorSpn = createBootstrapErrorSpan(errors[key]);
                        // var labelFor = $(input).closest("div").find("label");
                        var labelFor = $("label[for='signup_"+key+"']");
                        $(labelFor).append(errorSpn);
                    }
                }

            })
        ;
    });

    $('#signupModal').on('shown.bs.modal', function () {
        $('#signup_email').trigger('focus')
    });

    $("#spn_login_modal").on("click", function() {
        $('#signupModal').modal('hide');
        $('#loginModal').modal('show');
    });

    $("#spn_signup_modal").on("click", function() {
        $('#loginModal').modal('hide');
        $('#signupModal').modal('show');
    });


    $(".login_on_focus").on("focus", function() {
        $('#loginModal').modal('show');
    });

    $("#btn_signup_previous").on("click", function() {
        $('#rw-signup-step-1').show();
        $('#rw-signup-step-2').hide();
        $(this).hide();
    });


    $('#btn-signup-step-2').on('click', function(e){
        e.preventDefault();

        $(".is-invalid").removeClass("is-invalid");

        var count_errors = 0;

        if ($("#signup_genre").val() === '' || $("#signup_genre").val() === null) {
            $("#signup_genre").addClass("is-invalid");
            count_errors++;
        }

        if ($("#signup_timezone").val() === '' || $("#signup_timezone").val() === null) {
            $("#signup_timezone").addClass("is-invalid");
            count_errors++;
        }

        var response = grecaptcha.getResponse();

        if(response.length == 0){
            $("#dv-g-recaptcha-container").addClass("border border-danger");
            count_errors++;
        } else {
            $("#dv-g-recaptcha-container").removeClass("border border-danger");
        }

        if (!$("#signup_terms").is(":checked")) {
            $("#signup_terms").addClass("is-invalid");
            count_errors++;
        }

        if (!$("#signup_privacy").is(":checked")) {
            $("#signup_privacy").addClass("is-invalid");
            count_errors++;
        }

        if (count_errors > 0) {
            return;
        }

        var form = $('#modal-form-signup');

        $.ajax({
            type        : form.attr("method"),
            url         : form.attr("action"),
            data        : form.serialize()
        })
            .done(function(data, status, object){
                if (data.result && data.result.targetUrl) {
                    window.location.href = data.result.targetUrl;
                }
            })
            .fail(function (jqXHR, textStatus, errorThrown){

                $('.bsp-err-spn').remove();
                $(".is-invalid").removeClass("is-invalid");

                var errors = jqXHR.responseJSON.errors;
                for (var key in errors) {
                    if(key === "dv-g-recaptcha-container") {
                        $("#dv-g-recaptcha-container").addClass("border border-danger");
                        if (errors.hasOwnProperty(key)) {
                            $("#dv-g-recaptcha-container").before(createBootstrapErrorSpan(errors[key]));
                        }
                    } else if (key === "signup_terms") {
                        var selector = "#" + key;
                        var input = $(selector);
                        $(input).addClass("is-invalid");

                        if (errors.hasOwnProperty(key)) {
                            $('#err-lab-signup-terms').append(createBootstrapErrorSpan(errors[key]));
                        }
                    } else if (key === "signup_privacy") {
                        var selector = "#" + key;
                        var input = $(selector);
                        $(input).addClass("is-invalid");

                        if (errors.hasOwnProperty(key)) {
                            $('#err-lab-signup-privacy').append(createBootstrapErrorSpan(errors[key]));
                        }
                    } else {
                        var selector = "#" + key;
                        var input = $(selector);
                        $(input).addClass("is-invalid");

                        if (errors.hasOwnProperty(key)) {
                            var errorSpn = createBootstrapErrorSpan(errors[key]);
                            var labelFor = $(input).closest("div").find("label");
                            $(labelFor).append(errorSpn);
                        }
                    }
                }

            })
        ;

    });

});




function createBootstrapErrorSpan (msg) {
    return "<span class=\"bsp-err-spn invalid-feedback d-block\">\n" +
        "        <span class=\"d-block\">\n" +
        "            <span class=\"form-error-icon badge badge-danger text-uppercase\">Error</span>\n" +
        "            <span class=\"form-error-message\">"+msg+"</span>\n" +
        "        </span>\n" +
        "    </span>"
        ;
}