
// require('../scss/app.scss');

const $ = require('jquery');


var canvas = document.getElementById('canvas');
var ctx = canvas.getContext('2d');

// load image from data url
var imageObj = new Image();
imageObj.onload = function() {
    ctx.drawImage(this, 0, 0);
};

imageObj.src = $(canvas).data("urlImg");

$(document).ready(function() {


    $('#shareFB').click(function () {
        var data = $('#canvas')[0].toDataURL("image/png");
        try {
            blob = dataURItoBlob(data);
        } catch (e) {
            console.log(e);
        }
        FB.getLoginStatus(function (response) {
            console.log(response);
            if (response.status === "connected") {
                postImageToFacebook(response.authResponse.accessToken, "Canvas to Facebook/Twitter", "image/png", blob, window.location.href);
            } else if (response.status === "not_authorized") {
                FB.login(function (response) {
                    postImageToFacebook(response.authResponse.accessToken, "Canvas to Facebook/Twitter", "image/png", blob, window.location.href);
                }, {scope: "publish_actions"});
            } else {
                FB.login(function (response) {
                    postImageToFacebook(response.authResponse.accessToken, "Canvas to Facebook/Twitter", "image/png", blob, window.location.href);
                }, {scope: "publish_actions"});
            }
        });
    });


    // Twitter oauth handler
    $.oauthpopup = function (options) {
        if (!options || !options.path) {
            throw new Error("options.path must not be empty");
        }
        options = $.extend({
            windowName: 'ConnectWithOAuth' // should not include space for IE
            , windowOptions: 'location=0,status=0,width=800,height=400'
            , callback: function () {
                debugger;
                //window.location.reload();
            }
        }, options);

        var oauthWindow = window.open(options.path, options.windowName, options.windowOptions);
        var oauthInterval = window.setInterval(function () {
            if (oauthWindow.closed) {
                window.clearInterval(oauthInterval);
                options.callback();
            }
        }, 1000);
    };
// END Twitter oauth handler

//bind to element and pop oauth when clicked
    $.fn.oauthpopup = function (options) {
        $this = $(this);
        $this.click($.oauthpopup.bind(this, options));
    };

    $('#shareTW').click(function () {
        var dataURL = $('#canvas')[0].toDataURL("image/png");
        $.oauthpopup({
            path: '/auth/twitter.php',
            callback: function () {
                console.log(window.twit);
                var data = new FormData();
                // Tweet text
                data.append('status', "Look at the cute panda! " + window.location.href + " @jerezb31");
                // Binary image
                data.append('image', dataURL);
                // oAuth Data
                data.append('oauth_token', window.twit.oauth_token);
                data.append('oauth_token_secret', window.twit.oauth_token_secret);
                // Post to Twitter as an update with

                return $.ajax({
                    url: '/auth/share-on-twitter.php',
                    type: 'POST',
                    data: data,
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        console.log('Posted to Twitter.');
                        console.log(data);
                    }
                });
            }
        });
    });


});


// require_once '../vendor/abraham/twitteroauth/autoload.php';
// use Abraham\TwitterOAuth\TwitterOAuth;
//
// session_start();
//
// define('CONSUMER_KEY', '*************');
// define('CONSUMER_SECRET', '************');
// define('OAUTH_CALLBACK', '*****************');
//
// //echo '<pre>'.print_r($_REQUEST).'</pre>';
// //exit;
//
// $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $_REQUEST['oauth_token'], $_REQUEST['oauth_token_secret']);
// $twitterUser = $connection->get("account/verify_credentials");
//
// $media1 = $connection->upload('media/upload', ['media' => $_REQUEST['image']]);
// $parameters = [
//     'status' => $_REQUEST['status'],
//     'media_ids' => implode(',', [$media1->media_id_string]),
// ];
// $result = $connection->post('statuses/update', $parameters);
// echo json_encode($result);


function dataURItoBlob(dataURI) {
    var byteString = atob(dataURI.split(',')[1]);
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ab], {type: 'image/png'});
}


function postImageToFacebook(token, filename, mimeType, imageData, message) {
    var fd = new FormData();
    fd.append("access_token", token);
    fd.append("source", imageData);
    fd.append("no_story", true);

    // Upload image to facebook without story(post to feed)
    $.ajax({
        url: "https://graph.facebook.com/me/photos?access_token=" + token,
        type: "POST",
        data: fd,
        processData: false,
        contentType: false,
        cache: false,
        success: function (data) {
            console.log("success: ", data);

            // Get image source url
            FB.api(
                "/" + data.id + "?fields=images",
                function (response) {
                    if (response && !response.error) {
                        //console.log(response.images[0].source);

                        // Create facebook post using image
                        FB.api(
                            "/me/feed",
                            "POST",
                            {
                                "message": "",
                                "picture": response.images[0].source,
                                "link": window.location.href,
                                "name": 'Look at the cute panda!',
                                "description": message,
                                "privacy": {
                                    value: 'SELF'
                                }
                            },
                            function (response) {
                                if (response && !response.error) {
                                    /* handle the result */
                                    console.log("Posted story to facebook");
                                    console.log(response);
                                }
                            }
                        );
                    }
                }
            );
        },
        error: function (shr, status, data) {
            console.log("error " + data + " Status " + shr.status);
        },
        complete: function (data) {
            //console.log('Post to facebook Complete');
        }
    });
}
