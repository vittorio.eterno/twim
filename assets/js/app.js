// require('../scss/app.scss');

const $ = require('jquery');
// JS is equivalent to the normal "bootstrap" package
// no need to set this to a variable, just require it
require('bootstrap');

// or you can include specific pieces
// require('bootstrap/js/dist/tooltip');
// require('bootstrap/js/dist/popover');


var timeout;

$(document).ready(function() {

    // $('body').on('hidden.bs.modal', function () {
    //     if($('.modal .show').length > 0) {
    //         $('body').addClass('modal-open');
    //     }
    // });

    // Asynch load images
    $('.lazyload').each(function() {
        //* set the img src from data-src
        $(this).attr('src', $(this).attr('data-src'));
    });


    $('[data-toggle="popover"]').popover();


    $('.cls_word_name_inpt').on('keyup', function() {

        if (timeout) {
            clearTimeout(timeout);
            timeout = null;
        }

        var inputWord = $(this);
        var form = $(this).closest('form');
        var labelFor = $(this).closest('label');

        timeout = setTimeout(function() {

            $.ajax({
                    url: form.data('url'),
                    type: "POST",
                    data: form.serialize()
                })
                .done(function(response, textStatus, jqXHR) {
                    // Log a message to the console
                    // console.log("Hooray, it worked!");

                    $('.bsp-err-spn').remove();
                    $(inputWord).removeClass("is-invalid");
                })
                .fail(function(jqXHR, textStatus, errorThrown) {
                    // Log the error to the console
                    // console.error(
                    //     "The following error occurred: "+
                    //     textStatus, errorThrown
                    // );

                    $('.bsp-err-spn').remove();

                    var msg = jqXHR.responseJSON.message;
                    if (msg.name) {
                        msg = msg.name;
                    }

                    var errorSpn = createBootstrapErrorSpan(msg);
                    // var labelFor = $(form).find('label[for="word_name"]');
                    $(labelFor).append(errorSpn);
                    $(inputWord).addClass("is-invalid");
                })
                .always(function() {
                    // console.log("Alwaysss!");
                });


        }, 1000, form);

    });



    $('.hm_cls_word_name_inpt').on('keyup', function() {

        if (timeout) {
            clearTimeout(timeout);
            timeout = null;
        }


        if ($(this).val().length > 0) {

            $("#spnn-buy-new-word").show();

            timeout = setTimeout(function() {

                $.ajax({
                        url: $("#hm_input_word").data('checkUrl'),
                        type: "POST",
                        data: {
                            'word': $("#hm_input_word").val(),
                            'token': $("#hdn_input_hm_word").val()
                        }
                    })
                    .done(function(response, textStatus, jqXHR) {
                        $('.bsp-err-spn').remove();
                        $("#hm_input_word").removeClass("is-invalid");
                        $('#btn-create-word-on-success').prop("disabled", false);
                        $("#spnn-buy-new-word").hide();
                    })
                    .fail(function(jqXHR, textStatus, errorThrown) {
                        $('.bsp-err-spn').remove();
                        $(".is-invalid").removeClass("is-invalid");
                        $("#btn-create-word-on-success").prop("disabled", true);
                        $("#spnn-buy-new-word").hide();

                        var errors = jqXHR.responseJSON.errors;
                        for (var key in errors) {
                            $("#hm_input_word").addClass("is-invalid");

                            if (errors.hasOwnProperty(key)) {
                                var errorSpn = createBootstrapErrorSpan(errors[key]);
                                $("label[for='hm_input_word']").append(errorSpn);
                            }
                        }
                    })

            }, 1000);

        } else {
            $("#btn-create-word-on-success").prop("disabled", true);
            $("#spnn-buy-new-word").hide();
        }

    });


    // $('#btn-create-word-on-success').on('click', function(e){
    //     e.preventDefault();
    //     window.location.href = $(this).data('buyWord');
    // });


    $('#spn-resent-confirmation-email').on('click', function(e) {
        e.preventDefault();

        var hdn_email = $('#hdn_email').val();
        var url = $('#hdn_email').data('url');

        console.log(hdn_email, url);

        $.ajax({
                type: "POST",
                url: url,
                data: { email: hdn_email }
            })
            .done(function(data, status, object) {

                if (data.result && data.result.message_body) {
                    $("#resend-message-body").html(data.result.message_body);
                }
                if (data.result && data.result.message_footer) {
                    $("#resend-message-footer").html(data.result.message_footer);
                }

                $("#resendConfirmationEmailModal").modal("show");


            })
            .fail(function(jqXHR, textStatus, errorThrown) {

                var message = jqXHR.responseJSON.message;
                alert(message);
            });

    });



    //TODO: Aggiungere var contatore che dopo tot chiamate imposta il timeout a 15 min
    //TODO: Aggiungere anche alla parola

    $(".check-al-exist").on('keyup', function() {

        if (timeout) {
            clearTimeout(timeout);
            timeout = null;
        }

        var input = $(this);
        var url = $(this).data('usernameValidation');
        var idError = $(this).data('idError');
        var labelFor = $(this).closest('label');

        if (input.val().length >= 1) {
            timeout = setTimeout(function() {

                $.ajax({
                        url: url,
                        type: "POST",
                        data: { "username": input.val(), "key": idError }
                    })
                    .done(function(response, textStatus, jqXHR) {
                        $('.bsp-err-spn').remove();
                        $(input).removeClass("is-invalid");
                    })
                    .fail(function(jqXHR, textStatus, errorThrown) {
                        $('.bsp-err-spn').remove();

                        var errors = jqXHR.responseJSON.errors;
                        for (var key in errors) {
                            var selector = "#" + key;
                            input = $(selector);
                            $(input).addClass("is-invalid");

                            if (errors.hasOwnProperty(key)) {
                                var errorSpn = createBootstrapErrorSpan(errors[key]);
                                var labelFor = $(input).closest("div").find("label");
                                $(labelFor).append(errorSpn);
                            }
                        }

                    })


            }, 1100, [input, url, labelFor]);
        }

    });



    $(".toggle-password").on("click", function() {
        var input_id = $(this).parent().find(".inp-cls-pwd").prop('id');
        showPassword(input_id);

    });


    $(".href-sharing-social-url").on("click", function() {
        var url = $(this).data("sharingUrl");
        window.open(url, 'sharer', 'toolbar=0,status=0,width=648,height=395');
        return true;
    });

});


function createBootstrapErrorSpan(msg) {
    return "<span class=\"bsp-err-spn invalid-feedback d-block\">\n" +
        "        <span class=\"d-block\">\n" +
        //"            <span class=\"form-error-icon badge badge-danger text-uppercase\">Error</span>\n" +
        "            <span class=\"form-error-message\">" + msg + "</span>\n" +
        "        </span>\n" +
        "    </span>";
}



function showPassword(input_id) {
    var x = document.getElementById(input_id);
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}


require('../images/user_placeholder.png');