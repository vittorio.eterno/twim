const $ = require('jquery');
const $paypal = require('../vendor/paypal/js/checkout.js');

$(document).ready(function() {

    var paypalButton        = document.getElementById("paypal-button-container");
    var upgradePaypalButton = document.getElementById("upgrade-paypal-button-container");

    if (paypalButton) {

        // Render the Create PayPal button
        paypal.Button.render({
            // Set your environment
            env: 'sandbox', // sandbox | production

            // Specify the style of the button
            style: {
                layout: 'vertical',  // horizontal | vertical
                size: 'medium',    // medium | large | responsive
                shape: 'rect',      // pill | rect
                color: 'gold'       // gold | blue | silver | white | black
            },

            // Specify allowed and disallowed funding sources
            //
            // Options:
            // - paypal.FUNDING.CARD
            // - paypal.FUNDING.CREDIT
            // - paypal.FUNDING.ELV
            funding: {
                allowed: [
                    paypal.FUNDING.CARD,
                    paypal.FUNDING.CREDIT
                ],
                disallowed: []
            },

            // PayPal Client IDs - replace with your own
            // Create a PayPal app: https://developer.paypal.com/developer/applications/create
            //         client: {
            //             sandbox: 'AZDxjDScFpQtjWTOUtWKbyN_bDt4OgqaF4eYXlewfBP4-8aqX3PiV8e1GWU6liB2CUXlkA59kJXE7M6R',
            //             production: '<insert production client id>'
            //         },

            payment: function (data, actions) {
                return new paypal.Promise(function (resolve, reject) {
                    $.ajax('/api/admin/transaction/create-payment/', {
                        crossDomain: true,
                        data: $("#form_transaction_create").serialize(),
                        headers: {
                            'Accept': 'application/paypal-json-token',
                        },
                        method: 'POST',
                        success: function (response) {
                            if (!response || !response.result || !response.result.id) {
                                throw new Error('There was an error fetching the PayPal token');
                            }
                            return resolve(response.result.id);
                        },
                        error: function (err) {

                            $('.bsp-err-spn').remove();
                            $(".is-invalid").removeClass("is-invalid");

                            var errors = err.responseJSON.errors;
                            for (var key in errors) {
                                var selector = "#" + key;
                                var input = $(selector);
                                $(input).addClass("is-invalid");

                                if (errors.hasOwnProperty(key)) {
                                    var errorSpn = createBootstrapErrorSpan(errors[key]);
                                    var labelFor = $(input).closest("div").find("label");
                                    $(labelFor).append(errorSpn);
                                }
                            }

                            return reject(new Error('Create payment XHR failed'));
                        }
                    });
                });
            },

            onAuthorize: function (data, actions) {

                var dataPayment = "&paymentID="+data.paymentID+"&payerID="+data.payerID;

                return new paypal.Promise(function (resolve, reject) {
                    $.ajax('/api/admin/transaction/execute-payment/', {
                        crossDomain: true,
                        data: $("#form_transaction_create").serialize() + dataPayment,
                        headers: {
                            'Accept': 'application/paypal-json-token',
                        },
                        method: 'POST',
                        success: function (res) {
                            if (res.errors.length > 0) {
                                return actions.restart();
                            }
                            if (res.result && res.result.url) {
                                window.location.href = res.result.url;
                            }
                        },
                        error: function (err) {

                            if (err.error === 'INSTRUMENT_DECLINED') {
                                return actions.restart();
                            }
                            
                            $('.bsp-err-spn').remove();
                            $(".is-invalid").removeClass("is-invalid");

                            var errors = err.responseJSON.errors;
                            for (var key in errors) {
                                var selector = "#" + key;
                                var input = $(selector);
                                $(input).addClass("is-invalid");

                                if (errors.hasOwnProperty(key)) {
                                    var errorSpn = createBootstrapErrorSpan(errors[key]);
                                    var labelFor = $(input).closest("div").find("label");
                                    $(labelFor).append(errorSpn);
                                }
                            }

                            return reject(new Error('Create payment XHR failed'));
                        }
                    });
                });

                // return actions.request.post('/api/transaction/execute-payment/', {
                //     paymentID: data.paymentID,
                //     payerID: data.payerID
                // })
                //     .then(function (res) {
                //         if (res.errors.length > 0) {
                //             return actions.restart();
                //         }
                //         if (res.result && res.result.url) {
                //             window.location.href = res.result.url;
                //         }
                //         // 3. Show the buyer a confirmation message.
                //     });
            },

            onError: function (err) {
                // console.log("herewwww",err);
                // Show an error page here, when an error occurs
            }
        }, '#paypal-button-container');

    }


    if (upgradePaypalButton) {

        // Render the Upgrade PayPal button
        paypal.Button.render({
            // Set your environment
            env: 'sandbox', // sandbox | production

            // Specify the style of the button
            style: {
                layout: 'vertical',  // horizontal | vertical
                size: 'medium',    // medium | large | responsive
                shape: 'rect',      // pill | rect
                color: 'gold'       // gold | blue | silver | white | black
            },

            funding: {
                allowed: [
                    paypal.FUNDING.CARD,
                    paypal.FUNDING.CREDIT
                ],
                disallowed: []
            },

            payment: function (data, actions) {
                var form = $("#form_transaction_edit");
                return new paypal.Promise(function (resolve, reject) {
                    $.ajax('/api/admin/transaction/upgrade-payment/'+form.data('word'), {
                        crossDomain: true,
                        data: form.serialize(),
                        headers: {
                            'Accept': 'application/paypal-json-token',
                        },
                        method: 'POST',
                        success: function (response) {
                            if (!response || !response.result || !response.result.id) {
                                throw new Error('There was an error fetching the PayPal token');
                            }
                            return resolve(response.result.id);
                        },
                        error: function (err) {

                            $('.bsp-err-spn').remove();
                            $(".is-invalid").removeClass("is-invalid");

                            var errors = err.responseJSON.errors;

                            if (errors.url) {
                                window.location.href = errors.url;
                            } else {

                                for (var key in errors) {

                                    var selector = "#" + key;
                                    var input = $(selector);
                                    $(input).addClass("is-invalid");

                                    if (errors.hasOwnProperty(key)) {
                                        var errorSpn = createBootstrapErrorSpan(errors[key]);
                                        var labelFor = $(input).closest("div").find("label");
                                        $(labelFor).append(errorSpn);
                                    }
                                }

                            }

                            return reject(new Error('Create payment XHR failed'));
                        }
                    });
                });
            },

            onAuthorize: function (data, actions) {

                var form = $("#form_transaction_edit");
                var dataPayment = "&paymentID="+data.paymentID+"&payerID="+data.payerID;

                return new paypal.Promise(function (resolve, reject) {
                    $.ajax('/api/admin/transaction/execute-upgrade-payment/'+form.data('word'), {
                        crossDomain: true,
                        data: form.serialize() + dataPayment,
                        headers: {
                            'Accept': 'application/paypal-json-token',
                        },
                        method: 'POST',
                        success: function (res) {
                            if (res.errors.length > 0) {
                                return actions.restart();
                            }
                            if (res.result && res.result.url) {
                                window.location.href = res.result.url;
                            }
                        },
                        error: function (err) {
                            if (err.error === 'INSTRUMENT_DECLINED') {
                                return actions.restart();
                            }

                            $('.bsp-err-spn').remove();
                            $(".is-invalid").removeClass("is-invalid");

                            var errors = err.responseJSON.errors;
                            for (var key in errors) {
                                var selector = "#" + key;
                                var input = $(selector);
                                $(input).addClass("is-invalid");

                                if (errors.hasOwnProperty(key)) {
                                    var errorSpn = createBootstrapErrorSpan(errors[key]);
                                    var labelFor = $(input).closest("div").find("label");
                                    $(labelFor).append(errorSpn);
                                }
                            }

                            return reject(new Error('Create payment XHR failed'));
                        }
                    });
                });
            },

            onError: function (err) {
                // console.log("herewwww",err);
                // Show an error page here, when an error occurs
            }
        }, '#upgrade-paypal-button-container');

    }

});


function createBootstrapErrorSpan (msg) {
    return "<span class=\"bsp-err-spn invalid-feedback d-block\">\n" +
        "        <span class=\"d-block\">\n" +
        "            <span class=\"form-error-icon badge badge-danger text-uppercase\">Error</span>\n" +
        "            <span class=\"form-error-message\">"+msg+"</span>\n" +
        "        </span>\n" +
        "    </span>"
        ;
}