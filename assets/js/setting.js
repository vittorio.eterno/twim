const $ = require('jquery');

require('../scss/setting.scss');


$(document).on('click', '#close-preview', function(){
    $('.image-preview').popover('hide');
    // Hover befor close the preview
    $('.image-preview').hover(
        function () {
            $('.image-preview').popover('show');
        },
        function () {
            $('.image-preview').popover('hide');
        }
    );
});

$(document).ready(function() {


    $(".inp-preview").on('keyup', function () {

        var val = $(this).val();
        var linkType = $(this).data('linkType');
        var idTarget = "#preview_"+linkType;

        $(idTarget).html(val);
    });

    $('#changePwdModal').on('shown.bs.modal', function () {
        $('#cp_oldPlainPassword').trigger('focus');
    });

    $('#sub-change-password-form').on('click', function(e){
        e.preventDefault();

        var url          = $(this).data('url');
        var old_password = $('#cp_oldPlainPassword').val();
        var new_password = $('#cp_newPlainPassword').val();
        var token        = $('#hdn-token-cp').val();

        $.ajax({
            type        : "POST",
            url         : url,
            data        : {
                "old_password"  : old_password,
                "new_password"  : new_password,
                "token"         : token
            }
        })
            .done(function(data, status, object){
                $('.bsp-err-spn').remove();
                $(".is-invalid").removeClass("is-invalid");

                if (data.result && data.result.targetUrl) {
                    window.location.href = data.result.targetUrl;
                }

            })
            .fail(function (jqXHR, textStatus, errorThrown){
                $('.bsp-err-spn').remove();
                $(".is-invalid").removeClass("is-invalid");

                var errors = jqXHR.responseJSON.errors;
                for (var key in errors) {
                    var selector = "#" + key;
                    var input = $(selector);
                    $(input).addClass("is-invalid");

                    if (errors.hasOwnProperty(key)) {
                        var errorSpn = createBootstrapErrorSpan(errors[key]);
                        var labelFor = $("label[for='"+key+"']");
                        $(labelFor).append(errorSpn);
                    }
                }

            })
        ;
    });


    $('#sub-upload-pic-form').on('click', function(e){
        e.preventDefault();

        var formUpload  = $("#form-upload-picture");

        var file_data = $('#inp-upload-pic').prop('files')[0];
        var form_data = new FormData();
        form_data.append('image', file_data);
        form_data.append('token', $("#hdn-token-upload-pic").val());
        // var formData = new FormData(formUpload);

        $.ajax({
            type        : "POST",
            async: false,
            cache: false,
            processData: false,
            contentType: false,
            url         : formUpload.attr( 'action' ),
            enctype     : formUpload.attr('enctype'),
            data        : form_data
        })
            .done(function(data, status, object){
                $('.bsp-err-spn').remove();
                $(".is-invalid").removeClass("is-invalid");

                if (data.result && data.result.targetUrl) {
                    window.location.href = data.result.targetUrl;
                }

            })
            .fail(function (jqXHR, textStatus, errorThrown){
                $('.bsp-err-spn').remove();

                var errors = jqXHR.responseJSON.errors;
                for (var key in errors) {
                    var selector = "#" + key;
                    if (errors.hasOwnProperty(key)) {
                        var errorSpn = createBootstrapErrorSpan(errors[key]);
                        $(selector).append(errorSpn);
                    }
                }

            })
        ;
    });



    // Create the close button
    var closebtn = $('<button/>', {
        type:"button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");
    // Set the popover default content
    $('.image-preview').popover({
        trigger:'manual',
        html:true,
        title: "<strong>"+$("#modal-upload-translations-label").data("previewLabel")+"</strong>"+$(closebtn)[0].outerHTML,
        content: "There's no image",
        placement:'bottom'
    });
    // Clear event
    $('.image-preview-clear').click(function(){
        $('.image-preview').attr("data-content","").popover('hide');
        $('.image-preview-filename').text("");
        $('.image-preview-clear').hide();
        $('.image-preview-input input:file').val("");
        $(".image-preview-input-title").text($("#modal-upload-translations-label").data("browseLabel"));
    });
    // Create the preview image
    $(".image-preview-input input:file").change(function (){
        var img = $('<img/>', {
            id: 'dynamic',
            width:200//,
            // height:200
        });
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".image-preview-input-title").text($("#modal-upload-translations-label").data("changeLabel"));
            $(".image-preview-clear").show();
            $(".image-preview-filename").text(file.name);
            img.attr('src', e.target.result);
            $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
        }
        reader.readAsDataURL(file);
    });


    $('#btn-delete-profile-pic').on('click', function(e){
        e.preventDefault();

        $.ajax({
            type        : "DELETE",
            url         : $(this).data( 'deleteUrl' )
        })
            .done(function(data, status, object){
                if (data.result && data.result.targetUrl) {
                    window.location.href = data.result.targetUrl;
                }
            })
            .fail(function (jqXHR, textStatus, errorThrown){
                $('.bsp-err-spn').remove();

                var errors = jqXHR.responseJSON.errors;
                for (var key in errors) {
                    var selector = "#" + key;
                    if (errors.hasOwnProperty(key)) {
                        var errorSpn = createBootstrapErrorSpan(errors[key]);
                        $(selector).append(errorSpn);
                    }
                }

            })
        ;
    });

    ///////////////////////////////////////////////////////////////
    //    DELETE ACCOUNT
    ///////////////////////////////////////////////////////////////

    $('#deleteAccountModal').on('shown.bs.modal', function () {
        var btn = $("#btn-modal-delete-account");
        if (btn.data('isSocial') !== 'y') {
            $('#da_password').trigger('focus');
        }
    });

    $('#btn-da-step-1').on('click', function(e){
        e.preventDefault();

        var url      = $('#rw-da-step-1').data('url');
        var password = $('#da_password').val();
        var token    = $('#hdn-token-da').val();

        $.ajax({
            type        : "POST",
            url         : url,
            data        : {
                "password"  : password,
                "token"     : token
            }
        })
            .done(function(data, status, object){
                $('.bsp-err-spn').remove();
                $(".is-invalid").removeClass("is-invalid");
                $('#rw-da-step-1').hide();
                $('#rw-da-step-2').show();
            })
            .fail(function (jqXHR, textStatus, errorThrown){

                $('.bsp-err-spn').remove();
                $(".is-invalid").removeClass("is-invalid");

                var errors = jqXHR.responseJSON.errors;
                for (var key in errors) {
                    var selector = "#" + key;
                    var input = $(selector);
                    $(input).addClass("is-invalid");

                    if (errors.hasOwnProperty(key)) {
                        var errorSpn = createBootstrapErrorSpan(errors[key]);
                        var labelFor = $("label[for='"+key+"']");
                        $(labelFor).append(errorSpn);
                    }
                }

            })
        ;
    });


    $('#btn-da-step-2').on('click', function(e){
        e.preventDefault();

        var url      = $('#rw-da-step-2').data('url');
        var password = $('#da_password').val();
        var token    = $('#hdn-token-da-confirmation').val();

        $.ajax({
            type        : "DELETE",
            url         : url,
            data        : {
                "password"  : password,
                "token"     : token
            }
        })
            .done(function(data, status, object){
                $('.bsp-err-spn').remove();
                $(".is-invalid").removeClass("is-invalid");
                $('#rw-da-step-2').hide();
                $('#rw-da-step-3').show();

                if (data.result && data.result.targetUrl) {
                    window.setTimeout(function() {
                        window.location.href = data.result.targetUrl;
                    }, 3000);
                }
            })
            .fail(function (jqXHR, textStatus, errorThrown){

                $('.bsp-err-spn').remove();
                $(".is-invalid").removeClass("is-invalid");

                var errors = jqXHR.responseJSON.errors;
                for (var key in errors) {
                    var selector = "#" + key;
                    var input = $(selector);
                    $(input).addClass("is-invalid");

                    if (errors.hasOwnProperty(key)) {
                        var errorSpn = createBootstrapErrorSpan(errors[key]);
                        var labelFor = $("label[for='"+key+"']");
                        $(labelFor).append(errorSpn);
                    }
                }

            })
        ;
    });

});


function createBootstrapErrorSpan (msg) {
    return "<span class=\"bsp-err-spn invalid-feedback d-block\">\n" +
        "        <span class=\"d-block\">\n" +
        "            <span class=\"form-error-icon badge badge-danger text-uppercase\">Error</span>\n" +
        "            <span class=\"form-error-message\">"+msg+"</span>\n" +
        "        </span>\n" +
        "    </span>"
        ;
}