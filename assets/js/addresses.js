const $ = require('jquery');

var invoiceErrors = {
    firstName: 0,
    lastName: 0,
    companyName: 0,
    companyAddressInfo: 0,
    streetAddress: 0,
    addressDetail: 0,
    city: 0,
    stateProvince: 0,
    postalCode: 0,
    country: 0,
    email: 0,
    phone: 0,
    general: 0
};


$(document).ready(function() {


    $('#manageAddressesModal').on('hidden.bs.modal', function() {
        if ($('#saveAddressModal').hasClass('show')) {
            $('body').addClass('modal-open');
        }
    });

    $('#changeAddressesModal').on('hidden.bs.modal', function() {
        if ($('#saveAddressModal').hasClass('show')) {
            $('body').addClass('modal-open');
        }
        if ($('#manageAddressesModal').hasClass('show')) {
            $('body').addClass('modal-open');
        }
    });




    $('.link-create-address-modal').on('click', function(e) {
        e.preventDefault();

        var hdnTransUtils = $("#hdn-translation-utils");

        $("#saveAddressModalLabel").text($(hdnTransUtils).data('createAddressTitle'));
        $("#saveAddressModalSubtitle").text($(hdnTransUtils).data('createAddressSubtitle'));
    });

    $('#manageAddressesModal').on('click', '.link-edit-address-modal', function(e) {
        e.preventDefault();

        var hdnTransUtils = $("#hdn-translation-utils");

        $("#saveAddressModalLabel").text($(hdnTransUtils).data('editAddressTitle'));
        $("#saveAddressModalSubtitle").text($(hdnTransUtils).data('editAddressSubtitle'));

        var addressId = $(this).data('addressId');
        $('#htn-form-invoice-id').val(addressId);

        $('#fid_first_name').val($(this).parent().find('.h_cls_first_name').val());
        $('#fid_last_name').val($(this).parent().find('.h_cls_last_name').val());

        var h_company_name = $(this).parent().find('.h_cls_company_name').val();
        if (h_company_name && h_company_name != 'null') {
            $('#fid_company_name').val(h_company_name);
        }
        var h_company_address_info = $(this).parent().find('.h_cls_company_address_info').val();
        if (h_company_address_info && h_company_address_info != 'null') {
            $('#fid_company_address_info').val(h_company_address_info);
        }
        var h_phone = $(this).parent().find('.h_cls_phone').val();
        if (h_phone && h_phone != 'null') {
            $('#fid_phone').val(h_phone);
        }

        $('#fid_street_address').val($(this).parent().find('.h_cls_street_address').val());
        $('#fid_address_detail').val($(this).parent().find('.h_cls_address_detail').val());
        $('#fid_city').val($(this).parent().find('.h_cls_city').val());
        $('#fid_state_province').val($(this).parent().find('.h_cls_state_province').val());
        $('#fid_postal_code').val($(this).parent().find('.h_cls_postal_code').val());
        $('#fid_country').val($(this).parent().find('.h_cls_country').val());
        $('#fid_email').val($(this).parent().find('.h_cls_email').val());

        $(".cls-manage-addresses-modal").modal('hide');
        $("#link-delete-address").show();
    });

    $('#manageAddressesModal').on('click', '.link-create-address-modal', function(e) {
        e.preventDefault();
        $(".cls-manage-addresses-modal").modal('hide');
    });

    $('#changeAddressesModal').on('click', '.link-create-address-modal', function(e) {
        e.preventDefault();
        $("#changeAddressesModal").modal('hide');
    });

    $('#saveAddressModal').on('shown.bs.modal', function() {
        $('.cls_fid_input').prop("disabled", false);
    });

    $('#saveAddressModal').on('hide.bs.modal', function() {
        $('.cls_fid_input').prop("disabled", true);
        $('.cls_field_to_empty').val('');
        $('.cls_field_to_0').val(0);
        $("#link-delete-address").hide();
    });

    $('#link-delete-address').on('click', function(e) {
        e.preventDefault();

        var addressId = $("#htn-form-invoice-id").val();
        $("#btn-delete-shipping-address").data('address', addressId);

        $('.cls-save-address-modal').modal('hide');
        $(".cls-delete-address-modal").modal('show');
    });

    $("#btn-delete-shipping-address").on('click', function(e) {
        e.preventDefault();

        var dataAddress = {
            addressId: $(this).data('address')
        }


        $.ajax({
                url: $(this).data('deleteUrl'),
                type: "DELETE",
                data: dataAddress
            })
            .done(function(dataReturned, status, object) {

                var idAddress = 0;

                //check on checkout
                if ($('#dv_preview_invoice_address').length) {
                    if ($('#dv_preview_invoice_address').html().length > 0) {
                        var inputAddress = $(".cls_slc_modal_change_address_cer:checked");
                        idAddress = inputAddress.val();
                    }
                }


                $("#g_error_delete_address").html('');
                $('.cls-delete-address-modal').modal('hide');

                var clsElement = '.cls_address_' + dataAddress.addressId;
                $(clsElement).remove();

                /*
                if ($('.cls_addresses_on_cer').length > 0) {

                    $("#add-address-on-certificate").hide();
                    $("#dv-text-shipping-address-required").hide();
                    $("#manage-addresses-on-certificate").show();

                    if ($('.cls_slc_address_cer:checked').length === 0) {
                        $('.cls_slc_address_cer:first-child').prop('checked', true);
                    }

                } else {
                    $("#add-address-on-certificate").show();
                    $("#dv-text-shipping-address-required").show();
                    $("#manage-addresses-on-certificate").hide();
                }*/

                // On change modal
                if ($(".cls_slc_modal_change_address_cer").length > 10) {
                    $("#add-address-on-change-modal").hide();
                    $("#manage-addresses-on-change-modal").show();
                } else {
                    $("#add-address-on-change-modal").show();
                    $("#manage-addresses-on-change-modal").hide();
                }

                //check on checkout
                if ($('#dv_preview_invoice_address').length) {
                    if ($('#dv_preview_invoice_address').html().length > 0) {
                        if (dataAddress.addressId == idAddress) {
                            $(".cls_slc_modal_change_address_cer:first").attr('checked', true);
                            var inputNewAddress = $(".cls_slc_modal_change_address_cer:checked");
                            var newAddressHtml = inputNewAddress.parent().find('.cls_address_shipping_body').html();
                            var newIdAddress = inputNewAddress.val();
                            $("#dv_preview_invoice_address").html(newAddressHtml).append("<input type='hidden' id='hdn_invoice_address' name='hdn_invoice_address' value='" + newIdAddress + "'>");
                        }
                    }
                }

            })
            .fail(function(jqXHR, textStatus, errorThrown) {

                var errors = jqXHR.responseJSON.errors;

                for (var key in errors) {
                    if (errors.hasOwnProperty(key)) {
                        var errorSpn = createBootstrapErrorSpan(errors[key], true);
                        $("#g_error_delete_address").append(errorSpn);
                    }
                }

            });

    });


    $('.link-change-address-modal').on('click', function(e) {
        e.preventDefault();

        var idAddress = $("#hdn_invoice_address").val();

        if (idAddress && idAddress !== "undefined" && $.isNumeric(idAddress)) {
            $("#modal_change_ch_address_" + idAddress).prop('checked', true);
        }

    });


    $("#btn-change-addresses").on('click', function(e) {
        e.preventDefault();

        var inputAddress = $(".cls_slc_modal_change_address_cer:checked");
        var addressHtml = inputAddress.parent().find('.cls_address_shipping_body').html();

        var idAddress = inputAddress.val();
        $("#dv_preview_invoice_address").html(addressHtml).append("<input type='hidden' id='hdn_invoice_address' name='hdn_invoice_address' value='" + idAddress + "'>");

        $('#changeAddressesModal').modal('hide');
    });


    $('#changeAddressesModal').on('shown.bs.modal', function() {
        $('.cls_slc_modal_change_address_cer').prop("disabled", false);
    });

    $('#changeAddressesModal').on('hide.bs.modal', function() {
        $('.cls_slc_modal_change_address_cer').prop("disabled", true);
    });


    ////////////////////////////////////////////////////////
    // START INVOICE EVENTS
    ////////////////////////////////////////////////////////

    $('#fid_first_name').on('blur', function() {
        // if ($('#chb_require_invoice').is(':checked')) {
        var data = {
            'token': $("#htn-form-invoice-token").val(),
            'first_name': $(this).val()
        };
        checkInvoice(data, 'firstName', $(this).attr('id'));
        // }
    });

    $('#fid_last_name').on('blur', function() {
        // if ($('#chb_require_invoice').is(':checked')) {
        var data = {
            'token': $("#htn-form-invoice-token").val(),
            'last_name': $(this).val()
        };
        checkInvoice(data, 'lastName', $(this).attr('id'));
        // }
    });

    $('#fid_company_name').on('blur', function() {
        // if ($('#chb_require_invoice').is(':checked')) {
        var data = {
            'token': $("#htn-form-invoice-token").val(),
            'company_name': $(this).val()
        };
        checkInvoice(data, 'companyName', $(this).attr('id'));
        // }
    });

    $('#fid_company_address_info').on('blur', function() {
        if ($('#chb_require_invoice').is(':checked')) {
            var data = {
                'token': $("#htn-form-invoice-token").val(),
                'company_address_info': $(this).val()
            };
            checkInvoice(data, 'companyAddressInfo', $(this).attr('id'));
        }
    });

    $('#fid_street_address').on('blur', function() {
        // if ($('#chb_require_invoice').is(':checked')) {
        var data = {
            'token': $("#htn-form-invoice-token").val(),
            'street_address': $(this).val()
        };
        checkInvoice(data, 'streetAddress', $(this).attr('id'));
        // }
    });

    $('#fid_address_detail').on('blur', function() {
        // if ($('#chb_require_invoice').is(':checked')) {
        var data = {
            'token': $("#htn-form-invoice-token").val(),
            'address_detail': $(this).val()
        };
        // checkInvoice(data);
        checkInvoice(data, 'addressDetail', $(this).attr('id'));
        // }
    });

    $('#fid_city').on('blur', function() {
        // if ($('#chb_require_invoice').is(':checked')) {
        var data = {
            'token': $("#htn-form-invoice-token").val(),
            'city': $(this).val()
        };
        // checkInvoice(data);
        checkInvoice(data, 'city', $(this).attr('id'));
        // }
    });

    $('#fid_state_province').on('blur', function() {
        // if ($('#chb_require_invoice').is(':checked')) {
        var data = {
            'token': $("#htn-form-invoice-token").val(),
            'state_province': $(this).val()
        };
        checkInvoice(data, 'stateProvince', $(this).attr('id'));
        // }
    });

    $('#fid_postal_code').on('blur', function() {
        // if ($('#chb_require_invoice').is(':checked')) {
        var data = {
            'token': $("#htn-form-invoice-token").val(),
            'postal_code': $(this).val()
        };
        checkInvoice(data, 'postalCode', $(this).attr('id'));
        // }
    });

    $('#fid_country').on('change', function() {
        // if ($('#chb_require_invoice').is(':checked')) {
        var data = {
            'token': $("#htn-form-invoice-token").val(),
            'country': $(this).val()
        };
        checkInvoice(data, 'country', $(this).attr('id'));
        // }
    });

    $('#fid_email').on('blur', function() {
        // if ($('#chb_require_invoice').is(':checked')) {
        var data = {
            'token': $("#htn-form-invoice-token").val(),
            'email': $(this).val()
        };
        checkInvoice(data, 'email', $(this).attr('id'));
        // }
    });

    $('#fid_phone').on('blur', function() {
        // if ($('#chb_require_invoice').is(':checked')) {
        var data = {
            'token': $("#htn-form-invoice-token").val(),
            'phone': $(this).val()
        };
        checkInvoice(data, 'phone', $(this).attr('id'));
        // }
    });


    $('#btn-save-addresses').on('click', function(e) {
        e.preventDefault();

        $.each(invoiceErrors, function(k, v) {
            if (v === 1) {
                return false;
            }
        });


        var addressData = {};

        $('.cls_fid_input').each(function(k, v) {
            addressData[$(v).attr('name')] = $(v).val();
        });

        $.ajax({
                url: $(this).data('urlSave'),
                type: "POST",
                data: addressData
            })
            .done(function(dataReturned, status, object) {

                if (dataReturned.result) {

                    var htmlBodyAddress = "";
                    var htmlToCertificate = "";
                    var htmlToManage = "";
                    var htmlToChangeAddress = "";

                    var transEditOrRemove = $("#hdn-translation-utils").data('editRemoveLabel');

                    var ci = 0;

                    $.each(dataReturned.result, function(k, v) {

                        var companyName = "";
                        if (v.companyName) {
                            companyName = "(" + v.companyName + ")";
                        }

                        htmlBodyAddress = "<div class='text-muted'>" +
                            "<div>" + v.firstName + " " + v.lastName + " " + companyName + "</div>" +
                            "<div>" + v.streetAddress + " " + v.addressDetail + ", " + v.postalCode + ", " + v.city + " (" + v.stateProvince + ")</div>" +
                            "</div>";

                        var checkedRadio = "";
                        if (ci === 0) {
                            checkedRadio = "checked='checked'";
                        }

                        var displayNone = "";
                        if (ci > 0) {
                            displayNone = "style='display:none;'"
                        }

                        /*htmlToCertificate += "<div class='row border cls_row_bordered cls_addresses_on_cer mb-3 cls_address_"+v.id+"'>" +
                            "<div class='col-10'>" +
                                "<div class='custom-control custom-radio'>" +
                                    "<input type='radio' id='ch_address_"+v.id+"' name='choose_address' class='custom-control-input cls_slc_address_cer' "+checkedRadio+" value='"+v.id+"'>" +
                                    "<label class='custom-control-label' for='ch_address_"+v.id+"'><div class='ml-3'>"+htmlBodyAddress+"</div></label>" +
                                "</div>" +
                            "</div>" +
                            "<div class='col-2 text-right align-middle'>" +
                                "<div class='h4 mt-2 dv_address_delivery_charges' "+displayNone+"> $ <span class='cls_address_delivery_charges'>"+v.country.deliveryCharges+"</span></div>" +
                            "</div>" +
                            "</div>";*/


                        htmlToChangeAddress += "<div class='border cls_row_bordered mb-3 cls_address_" + v.id + "'>" +
                            "<div class='custom-control custom-radio'>" +
                            "<input type='radio' id='modal_change_ch_address_" + v.id + "' name='modal-change-invoice-address' class='custom-control-input cls_slc_modal_change_address_cer' " + checkedRadio + " value='" + v.id + "'>" +
                            "<label class='custom-control-label' for='modal_change_ch_address_" + v.id + "'><div class='ml-3 cls_address_shipping_body'>" + htmlBodyAddress + "</div></label>" +
                            "</div>" +
                            "</div>";


                        htmlToManage += "<div class='border cls_row_bordered mb-3 cls_address_" + v.id + "'>" +
                            htmlBodyAddress +
                            "<div class='mt-1'>" +
                            "<input type='hidden' class='h_cls_first_name' value='" + v.firstName + "'>" +
                            "<input type='hidden' class='h_cls_last_name' value='" + v.lastName + "'>" +
                            "<input type='hidden' class='h_cls_company_name' value='" + v.companyName + "'>" +
                            "<input type='hidden' class='h_cls_company_address_info' value='" + v.companyAddressInfo + "'>" +
                            "<input type='hidden' class='h_cls_street_address' value='" + v.streetAddress + "'>" +
                            "<input type='hidden' class='h_cls_address_detail' value='" + v.addressDetail + "'>" +
                            "<input type='hidden' class='h_cls_city' value='" + v.city + "'>" +
                            "<input type='hidden' class='h_cls_state_province' value='" + v.stateProvince + "'>" +
                            "<input type='hidden' class='h_cls_postal_code' value='" + v.postalCode + "'>" +
                            "<input type='hidden' class='h_cls_country' value='" + v.country.id + "'>" +
                            "<input type='hidden' class='h_cls_email' value='" + v.email + "'>" +
                            "<input type='hidden' class='h_cls_phone' value='" + v.phone + "'>" +
                            "<a class='text-primary cls-pointer-effect link-edit-address-modal' data-address-id='" + v.id + "' data-toggle='modal' data-target='#saveAddressModal'>" + transEditOrRemove + "</a>" +
                            "</div>" +
                            "</div>";

                        ci++;

                    });

                    //$("#dv-body-step-certificate-address").addClass('mt-3').html(htmlToCertificate);
                    $("#dv-body-manage-address").addClass('mt-3').html(htmlToManage);
                    $("#dv-body-change-address").addClass('mt-3').html(htmlToChangeAddress);

                    // On change modal
                    if ($(".cls_slc_modal_change_address_cer").length > 10) {
                        $("#add-address-on-change-modal").hide();
                        $("#manage-addresses-on-change-modal").show();
                    } else {
                        $("#add-address-on-change-modal").show();
                        $("#manage-addresses-on-change-modal").hide();
                    }


                } else {
                    //refresh page;
                    location.reload();
                }

                /*if ($('.cls_addresses_on_cer').length > 0) {
                    $("#add-address-on-certificate").hide();
                    $("#manage-addresses-on-certificate").show();
                }*/

                if ($('#dv_preview_invoice_address').length) {
                    if ($('#dv_preview_invoice_address').html().length > 0) {
                        var inputAddress = $(".cls_slc_modal_change_address_cer:checked");
                        var addressHtml = inputAddress.parent().find('.cls_address_shipping_body').html();
                        var idAddress = inputAddress.val();
                        $("#dv_preview_invoice_address").html(addressHtml).append("<input type='hidden' id='hdn_invoice_address' name='hdn_invoice_address' value='" + idAddress + "'>");
                    }
                }

                $('.cls-save-address-modal').modal('hide');

            })
            .fail(function(jqXHR, textStatus, errorThrown) {

                var errors = jqXHR.responseJSON.errors;

                for (var key in errors) {
                    var selector = "#" + key;
                    var input = $(selector);
                    $(input).addClass("is-invalid");

                    if (errors.hasOwnProperty(key)) {
                        var errorSpn = createBootstrapErrorSpan(errors[key], true);
                        var labelFor = $("label[for='" + key + "']");
                        $(labelFor).append(errorSpn);
                    }
                }

            });

    });


    ////////////////////////////////////////////////////////
    // END INVOICE EVENTS
    ////////////////////////////////////////////////////////


});

function checkInvoice(data, keyError, idInput) {

    $.ajax({
            url: $("#form-invoice-data").data('urlValidate'),
            type: "POST",
            data: data
        })
        .done(function(dataReturned, status, object) {

            if (invoiceErrors[keyError] && invoiceErrors[keyError] === 1) {
                $("label[for='" + idInput + "'] .bsp-err-spn").remove();
                $("#" + idInput).removeClass("is-invalid");
            }

        })
        .fail(function(jqXHR, textStatus, errorThrown) {

            var errors = jqXHR.responseJSON.errors;

            if (invoiceErrors[keyError] === 0) {
                invoiceErrors[keyError] = 1;
                for (var key in errors) {
                    var selector = "#" + key;
                    var input = $(selector);
                    $(input).addClass("is-invalid");

                    if (errors.hasOwnProperty(key)) {
                        var errorSpn = createBootstrapErrorSpan(errors[key], true);
                        var labelFor = $("label[for='" + key + "']");
                        $(labelFor).append(errorSpn);
                    }
                }
            }

        });
}

function createBootstrapErrorSpan(msg, isInline = false) {
    var clsInline = isInline ? 'd-inline' : 'd-block';

    return "<span class=\"bsp-err-spn invalid-feedback " + clsInline + " \"><small>" +
        "        <span class=\" " + clsInline + " \">" +
        "            <span class=\"form-error-message\">" + msg + "</span>" +
        "        </span></small>" +
        "    </span>";
}