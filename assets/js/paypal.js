const $ = require('jquery');
const $paypal = require('../vendor/paypal/js/checkout.js');

$(document).ready(function() {

    var paypalButton        = document.getElementById("paypal-button-container");
    var upgradePaypalButton = document.getElementById("upgrade-paypal-button-container");

    if (paypalButton) {

        // Render the Create PayPal button
        paypal.Button.render({
            // Set your environment
            env: 'sandbox', // sandbox | production

            // Specify the style of the button
            style: {
                layout: 'vertical',  // horizontal | vertical
                size: 'medium',    // medium | large | responsive
                shape: 'rect',      // pill | rect
                color: 'gold'       // gold | blue | silver | white | black
            },

            // Specify allowed and disallowed funding sources
            //
            // Options:
            // - paypal.FUNDING.CARD
            // - paypal.FUNDING.CREDIT
            // - paypal.FUNDING.ELV
            funding: {
                allowed: [
                    paypal.FUNDING.CARD,
                    paypal.FUNDING.CREDIT
                ],
                disallowed: []
            },

            payment: function (data, actions) {
                return new paypal.Promise(function (resolve, reject) {
                    $.ajax($('#dv-empt-pp-urls').data('ppCreate'), {
                        crossDomain: true,
                        data: $("#form-buy-word").serialize(),
                        headers: {
                            'Accept': 'application/paypal-json-token',
                        },
                        method: 'POST',
                        success: function (response) {
                            $('.cls_bw_g_err').html('');
                            if (!response || !response.result || !response.result.id) {
                                throw new Error('There was an error fetching the PayPal token');
                            }
                            return resolve(response.result.id);
                        },
                        error: function (err) {
                            $('.cls_bw_g_err').html('');
                            $('.bsp-err-spn').remove();
                            $(".is-invalid").removeClass("is-invalid");

                            var errors = err.responseJSON.errors;
                            for (var key in errors) {
                                var selector = "#" + key;
                                var input = $(selector);
                                $(input).addClass("is-invalid");

                                if (errors.hasOwnProperty(key)) {
                                    var labelFor = $("label[for='"+key+"']");
                                    if ($(labelFor).length) {
                                        var errorSpn = createBootstrapErrorSpan(errors[key],true);
                                        $(labelFor).append(errorSpn);
                                    } else {
                                        var errorSpn = createBootstrapErrorSpan(errors[key]);
                                        $('.cls_bw_g_err').append(errorSpn);
                                    }
                                }
                            }

                            return reject(new Error('Create payment XHR failed'));
                        }
                    });
                });
            },

            onAuthorize: function (data, actions) {

                var dataPayment = "&paymentID="+data.paymentID+"&payerID="+data.payerID;

                return new paypal.Promise(function (resolve, reject) {
                    $.ajax($('#dv-empt-pp-urls').data('ppExecute'), {
                        crossDomain: true,
                        data: $("#form-buy-word").serialize() + dataPayment,
                        headers: {
                            'Accept': 'application/paypal-json-token',
                        },
                        method: 'POST',
                        success: function (res) {
                            $('.cls_bw_g_err').html('');
                            if (res.errors.length > 0) {
                                return actions.restart();
                            }
                            if (res.result && res.result.url) {
                                window.location.href = res.result.url;
                            }
                        },
                        error: function (err) {
                            $('.cls_bw_g_err').html('');

                            if (err.error === 'INSTRUMENT_DECLINED') {
                                return actions.restart();
                            }
                            
                            $('.bsp-err-spn').remove();
                            $(".is-invalid").removeClass("is-invalid");

                            var errors = err.responseJSON.errors;
                            for (var key in errors) {
                                var selector = "#" + key;
                                var input = $(selector);
                                $(input).addClass("is-invalid");

                                if (errors.hasOwnProperty(key)) {
                                    var labelFor = $("label[for='"+key+"']");
                                    if ($(labelFor).length) {
                                        var errorSpn = createBootstrapErrorSpan(errors[key],true);
                                        $(labelFor).append(errorSpn);
                                    } else {
                                        var errorSpn = createBootstrapErrorSpan(errors[key]);
                                        $('.cls_bw_g_err').append(errorSpn);
                                    }
                                }
                            }

                            return reject(new Error('Create payment XHR failed'));
                        }
                    });
                });

            },

            onError: function (err) {
                // console.log("herewwww",err);
                // Show an error page here, when an error occurs
            }
        }, '#paypal-button-container');

    }


    if (upgradePaypalButton) {

        // Render the Upgrade PayPal button
        paypal.Button.render({
            // Set your environment
            env: 'sandbox', // sandbox | production

            // Specify the style of the button
            style: {
                layout: 'vertical',  // horizontal | vertical
                size: 'medium',    // medium | large | responsive
                shape: 'rect',      // pill | rect
                color: 'gold'       // gold | blue | silver | white | black
            },

            funding: {
                allowed: [
                    paypal.FUNDING.CARD,
                    paypal.FUNDING.CREDIT
                ],
                disallowed: []
            },

            payment: function (data, actions) {
                var form = $("#form_transaction_edit");
                return new paypal.Promise(function (resolve, reject) {
                    $.ajax('/api/transaction/upgrade-payment/'+form.data('word'), {
                        crossDomain: true,
                        data: form.serialize(),
                        headers: {
                            'Accept': 'application/paypal-json-token',
                        },
                        method: 'POST',
                        success: function (response) {
                            if (!response || !response.result || !response.result.id) {
                                throw new Error('There was an error fetching the PayPal token');
                            }
                            return resolve(response.result.id);
                        },
                        error: function (err) {

                            $('.bsp-err-spn').remove();
                            $(".is-invalid").removeClass("is-invalid");

                            var errors = err.responseJSON.errors;

                            if (errors.url) {
                                window.location.href = errors.url;
                            } else {

                                for (var key in errors) {

                                    var selector = "#" + key;
                                    var input = $(selector);
                                    $(input).addClass("is-invalid");

                                    if (errors.hasOwnProperty(key)) {
                                        var errorSpn = createBootstrapErrorSpan(errors[key]);
                                        var labelFor = $(input).closest("div").find("label");
                                        $(labelFor).append(errorSpn);
                                    }
                                }

                            }

                            return reject(new Error('Create payment XHR failed'));
                        }
                    });
                });
            },

            onAuthorize: function (data, actions) {

                var form = $("#form_transaction_edit");
                var dataPayment = "&paymentID="+data.paymentID+"&payerID="+data.payerID;

                return new paypal.Promise(function (resolve, reject) {
                    $.ajax('/api/transaction/execute-upgrade-payment/'+form.data('word'), {
                        crossDomain: true,
                        data: form.serialize() + dataPayment,
                        headers: {
                            'Accept': 'application/paypal-json-token',
                        },
                        method: 'POST',
                        success: function (res) {
                            if (res.errors.length > 0) {
                                return actions.restart();
                            }
                            if (res.result && res.result.url) {
                                window.location.href = res.result.url;
                            }
                        },
                        error: function (err) {
                            if (err.error === 'INSTRUMENT_DECLINED') {
                                return actions.restart();
                            }

                            $('.bsp-err-spn').remove();
                            $(".is-invalid").removeClass("is-invalid");

                            var errors = err.responseJSON.errors;
                            for (var key in errors) {
                                var selector = "#" + key;
                                var input = $(selector);
                                $(input).addClass("is-invalid");

                                if (errors.hasOwnProperty(key)) {
                                    var errorSpn = createBootstrapErrorSpan(errors[key]);
                                    var labelFor = $(input).closest("div").find("label");
                                    $(labelFor).append(errorSpn);
                                }
                            }

                            return reject(new Error('Create payment XHR failed'));
                        }
                    });
                });
            },

            onError: function (err) {
                // console.log("herewwww",err);
                // Show an error page here, when an error occurs
            }
        }, '#upgrade-paypal-button-container');

    }

});


function createBootstrapErrorSpan (msg, isInline=false) {
    var clsInline = isInline ? 'd-inline' : 'd-block';

    return "<span class=\"bsp-err-spn invalid-feedback "+clsInline+" \">\n" +
        "        <span class=\" "+clsInline+" \">\n" +
        "            <span class=\"form-error-icon badge badge-danger text-uppercase\">Error</span>\n" +
        "            <span class=\"form-error-message\">"+msg+"</span>\n" +
        "        </span>\n" +
        "    </span>"
        ;
}