const $ = require('jquery');


require('../images/e90c0cbba7a963349546c1be9da9e87e.jpg');


require('../scss/purchasing_transaction.scss');

var timeoutTransaction;
var isOptionalFlow = false;
var isPackageFlow = false;

var domErrors = {
    wordName: 0,
    wordValue: 0,
    optionals: 0,
    certificates: 0,
};

var showPreviewCheckout = {
    optionals: 0
};

var counterCertificate = 0;
var isUpdateCertificate = 0;
var objUpdateCertificate = null;

var certificatesErrors = '';

var isTotalDiscounted = false;
var discountPercentage = 0;
var discountSubtracted = 0;

$(document).ready(function() {

    $('.cls-check-validate-word').on('keyup', function() {

        if (timeoutTransaction) {
            clearTimeout(timeoutTransaction);
            timeoutTransaction = null;
        }

        var input = $(this);

        if ($(this).val().length > 0) {

            timeoutTransaction = setTimeout(function() {

                $.ajax({
                        url: input.data('urlValidate'),
                        type: "POST",
                        data: {
                            'word': $("#bw_word_name").val(),
                            'token': $("#bw-hdn-token-1").val(),
                            'errorKey': input.data('idError')
                        }
                    })
                    .done(function(response, textStatus, jqXHR) {
                        $('.bsp-err-spn').remove();
                        $(".is-invalid").removeClass("is-invalid");
                        $('.rsp-txt-validate-word').show();
                        $('.cls_preview_word_name').text($("#bw_word_name").val().toUpperCase());

                        domErrors.wordName = 0;
                    })
                    .fail(function(jqXHR, textStatus, errorThrown) {
                        $('.bsp-err-spn').remove();
                        $(".is-invalid").removeClass("is-invalid");
                        $('.rsp-txt-validate-word').hide();

                        var errors = jqXHR.responseJSON.errors;
                        for (var key in errors) {
                            var selector = "#" + key;
                            var input = $(selector);
                            $(input).addClass("is-invalid");

                            if (errors.hasOwnProperty(key)) {
                                var errorSpn = createBootstrapErrorSpan(errors[key]);
                                var labelFor = $("label[for='" + key + "']");
                                $(labelFor).append(errorSpn);
                            }
                        }

                        domErrors.wordName = 1;
                    })

            }, 800, input);

        } else {
            $(input).addClass("is-invalid");
            $('.rsp-txt-validate-word').hide();
            domErrors.wordName = 1;
        }

    });


    $('.cls-check-validate-word-value').on('keyup', function() {

        if (timeoutTransaction) {
            clearTimeout(timeoutTransaction);
            timeoutTransaction = null;
        }

        checkWordValue();

    });

    $('.cls-check-validate-word-value').on('blur', function() {

        if (timeoutTransaction) {
            clearTimeout(timeoutTransaction);
            timeoutTransaction = null;
        }

        checkWordValue();

    });

    ////////////////////////////////////////////////////////
    // START FLOW EVENTS
    ////////////////////////////////////////////////////////

    //OPTIONALS
    $("#btn-go-to-options").on("click", function(e) {
        e.preventDefault();

        if (domErrors.wordName > 0 || domErrors.wordValue > 0) {
            alert($("#hdn-js-alert").val());
            return;
        }

        //TODO: annullare valore del pacchetto selezionato se è stato selezionato

        $("#bw-step-choose-word").hide();
        $("#dv-flow-optionals").show();
        $("#bw-step-opt-1").show();
        isOptionalFlow = true;

        $("#spn_bw_choose_opts").removeClass("text-muted");
        $("#spn_bw_choose_opts").addClass("text-primary");
    });

    //back
    $("#btn-back-to-choose-word").on("click", function(e) {
        e.preventDefault();
        $("#dv-flow-optionals").hide();
        $("#bw-step-opt-1").hide();
        $("#bw-step-choose-word").show();
        isOptionalFlow = false;

        // $(".chb_category_optional").prop("checked",false);
        // $(".slc_optional_effect").hide();
        // checkOptionals();

        $("#spn_bw_choose_opts").removeClass("text-primary");
        $("#spn_bw_choose_opts").addClass("text-muted");
    });

    //certificate
    /*$("#btn-go-to-certificate").on("click", function(e) {
        e.preventDefault();

        if (domErrors.optionals > 0) {
            alert($("#hdn-js-alert").val());
            return;
        }

        //TODO: annullare opts selezionati

        $("#bw-step-opt-1").hide();
        $("#bw-step-opt-2").show();

        $("#spn_bw_certificate").removeClass("text-muted");
        $("#spn_bw_certificate").addClass("text-primary");

    });

    //back from certificate
    $("#btn-back-from-certificate").on("click", function(e) {
        e.preventDefault();
        $("#bw-step-opt-2").hide();
        $("#bw-step-opt-1").show();

        $("#spn_bw_certificate").removeClass("text-primary");
        $("#spn_bw_certificate").addClass("text-muted");
    });*/


    //checkout
    $("#form-buy-word").on("click", ".btn-go-to-checkout", function(e) {
        e.preventDefault();

        if (domErrors.wordName > 0 || domErrors.wordValue > 0) {
            alert($("#hdn-js-alert").val());
            return;
        }

        $("#dv_row_shipping_address").hide();
        $("#bw-step-opt-1").hide();
        //$("#dv_preview_checkout_certificates").hide();

        if (isOptionalFlow) {

            /*if ($('.cls_row_preview_certificate_recap').length > 0) {

                if ($('.cls_slc_address_cer:checked').length === 0) {
                    alert($("#hdn_alert_address_is_required").val());
                    return;
                }

                checkCertificates();
                if (domErrors.certificates > 0) {
                    alert(certificatesErrors);
                    return;
                }

                // UPDATE TOTAL WITH CERTIFICATE
                checkDiscountToRemove();
                var certificateAmount = getPreviewAmountCertificate();
                var previousTotal = $("#preview_checkout_total_price").text();

                if (certificateAmount > 0) {
                    var updateTotal = certificateAmount + parseFloat(previousTotal);
                    $("#preview_checkout_total_price").text(updateTotal);
                }
                recalculateDiscount();

                var htmlShippingAddress = $('.cls_slc_address_cer:checked').parent().find('.cls_address_shipping_body').html();
                $("#dv_preview_shipping_address").html(htmlShippingAddress);
                $("#dv_row_shipping_address").show();

                var idAddress = $('.cls_slc_address_cer:checked').val();
                var isFirstInvoiceAddress = $("#hdn_invoice_address_default").val();
                if (htmlShippingAddress !== "" && htmlShippingAddress !== "undefined" && isFirstInvoiceAddress === "y") {
                    $("#dv_preview_invoice_address").html(htmlShippingAddress).append("<input type='hidden' id='hdn_invoice_address' name='hdn_invoice_address' value='" + idAddress + "'>");
                }

                setRecapTotalOrderCertificate();

            }*/

            $("#spn_bw_checkout_opt").removeClass("text-muted");
            $("#spn_bw_checkout_opt").addClass("text-primary");
        } else if (isPackageFlow) {
            $("#spn_bw_checkout_pack").removeClass("text-muted");
            $("#spn_bw_checkout_pack").addClass("text-primary");
        }

        $("#bw-step-choose-word").hide();
        $("#bw-step-opt-2").hide();
        $("#bw-step-pack-1").hide();

        var wordValue = $("#bw_word_value").val();

        var html_preview_word_checkout = $("#bw_word_name").val().toUpperCase() + " ($" + wordValue + ")";

        $("#preview_checkout_word_name").text(html_preview_word_checkout);


        if (showPreviewCheckout.optionals === 1) {
            $("#dv_preview_checkout_optionals").show();
        } else {
            $("#dv_preview_checkout_optionals").hide();
        }

        $("#bw-step-checkout").show();



    });


    $("#form-buy-word").on("change", "input[name='choose_address']", function(e) {
        e.preventDefault();

        if ($('.dv_address_delivery_charges').is(':visible')) {
            $('.dv_address_delivery_charges:visible').hide();
        }

        $(this).closest('div.cls_addresses_on_cer').find('.dv_address_delivery_charges').show();

        //setPreviewAmountCertificate();
    });


    //back from checkout
    $("#form-buy-word").on("click", ".cls-btn-back-from-checkout", function(e) {
        e.preventDefault();
        $("#bw-step-checkout").hide();

        if (isOptionalFlow) {
            //$("#bw-step-opt-2").show();
            $("#bw-step-opt-1").show();

            $("#spn_bw_checkout_opt").removeClass("text-primary");
            $("#spn_bw_checkout_opt").addClass("text-muted");

            checkDiscountToRemove();
            /*var certificateAmount = getPreviewAmountCertificate();
            var previousTotal = $("#preview_checkout_total_price").text();
            var updateTotal = parseFloat(previousTotal) - certificateAmount;
            $("#preview_checkout_total_price").text(updateTotal);*/
            recalculateDiscount();

        } else if (isPackageFlow) {
            $("#bw-step-pack-1").show();

            $("#spn_bw_checkout_pack").removeClass("text-primary");
            $("#spn_bw_checkout_pack").addClass("text-muted");
        } else {
            $("#bw-step-choose-word").show();
        }
    });

    //PACKAGES
    //back
    /*$("#btn-back-to-word-from-pack").on("click", function (e) {
        e.preventDefault();
        $("#dv-flow-packages").hide();
        $("#bw-step-pack-1").hide();
        $("#bw-step-choose-word").show();
        isPackageFlow = false;

        $("#spn_bw_choose_packs").removeClass("text-primary");
        $("#spn_bw_choose_packs").addClass("text-muted");
    });

    //packages
    $("#btn-go-to-packages").on("click", function (e) {
        e.preventDefault();

        if (domErrors.wordName > 0 || domErrors.wordValue > 0) {
            alert($("#hdn-js-alert").val());
            return;
        }

        $("#bw-step-choose-word").hide();
        $("#dv-flow-packages").show();
        $("#bw-step-pack-1").show();
        isPackageFlow = true;

        $("#spn_bw_choose_packs").removeClass("text-muted");
        $("#spn_bw_choose_packs").addClass("text-primary");
    });*/

    ////////////////////////////////////////////////////////
    // END FLOW EVENTS
    ////////////////////////////////////////////////////////


    ////////////////////////////////////////////////////////
    // START OPTIONAL EVENTS
    ////////////////////////////////////////////////////////
    $(".chb_category_optional").on("click", function(e) {

        var categorySource = $(this).data('categorySource');
        var selector = "slc_category_optional_" + categorySource;

        $("." + selector).toggle("fast", function() {
            // Animation complete.
            // ADD WORD EFFECT PREVIEW
            if ($("." + selector).is(":visible")) {
                $("." + selector).prop("disabled", false);
                var specialEffect = $("." + selector).find(":selected").data('specialEffect');
                $('.cls_preview_word_name').addClass(specialEffect);
            } else {
                $("." + selector).prop("disabled", true);
                $("." + selector + " option").each(function(k, option) {
                    var spe = $(option).data('specialEffect');
                    if ($('.cls_preview_word_name').hasClass(spe)) {
                        $('.cls_preview_word_name').removeClass(spe);
                    }
                });
            }
            checkOptionals();
        });

    });

    $('.slc_optional_effect').on('change', function() {

        var specialEffect = $(this).find(":selected").data('specialEffect');

        $(this).find("option").each(function(k, option) {
            var spe = $(option).data('specialEffect');
            if ($('.cls_preview_word_name').hasClass(spe)) {
                $('.cls_preview_word_name').removeClass(spe);
            }
        });

        $('.cls_preview_word_name').addClass(specialEffect);

        checkOptionals();

    });

    ////////////////////////////////////////////////////////
    // END OPTIONAL EVENTS
    ////////////////////////////////////////////////////////



    ////////////////////////////////////////////////////////
    // START CHECKOUT EVENTS
    ////////////////////////////////////////////////////////
    $("#btn-validate-coupon").on("click", function(e) {

        var url = $(this).data('urlValidate');
        var input = $("#bw_coupon").val();

        if (input.length > 0) {

            $.ajax({
                    url: url,
                    type: "POST",
                    data: {
                        'coupon': input
                    }
                })
                .done(function(data, status, object) {
                    $('.bsp-err-spn').remove();
                    $(".is-invalid").removeClass("is-invalid");
                    $(".rsp-txt-validate-coupon").show();

                    checkDiscountToRemove();

                    if (data.result && data.result.discountPercentage) {
                        isTotalDiscounted = true;
                        discountPercentage = data.result.discountPercentage;
                        if (data.result.couponCode) {
                            var htmlCoupon = data.result.couponCode + " (-" + data.result.discountPercentage + "%)";
                            $("#preview_checkout_coupon").html(htmlCoupon);
                            $("#dv_preview_checkout_coupon").show();
                        }

                    }

                    recalculateDiscount();

                })
                .fail(function(jqXHR, textStatus, errorThrown) {
                    $('.bsp-err-spn').remove();
                    $(".is-invalid").removeClass("is-invalid");
                    $(".rsp-txt-validate-coupon").hide();

                    checkDiscountToRemove();

                    isTotalDiscounted = false;
                    discountPercentage = 0;
                    discountSubtracted = 0;

                    $("#dv_preview_checkout_coupon").hide();

                    var errors = jqXHR.responseJSON.errors;
                    for (var key in errors) {
                        $("#bw_coupon").addClass("is-invalid");

                        if (errors.hasOwnProperty(key)) {
                            var errorSpn = createBootstrapErrorSpan(errors[key]);
                            $("#bw_coupon_error").append(errorSpn);
                        }
                    }


                });

        } else {
            $("#bw_coupon").addClass("is-invalid");
        }

    });

    // By default they are disabled!
    $('.cls_fid_input').prop("disabled", true);


    $("#chb_require_invoice").on("click", function(e) {

        var invoicePrice = parseInt($(this).data('invoicePrice'));
        var totalPrice = parseFloat($("#preview_checkout_total_price").text());


        checkDiscountToRemove();

        if ($(this).is(':checked')) {

            totalPrice = totalPrice + invoicePrice;
            // $('.cls_fid_input').prop("disabled", false);
            // $("#form-invoice-data").show();
            $("#preview_checkout_invoice").text("($" + invoicePrice + ")");
            $("#dv_preview_checkout_invoice").show();

            $("#dv_row_invoice_address").show();
            $("#hdn_invoice_address").prop("disabled", false);

        } else {

            totalPrice = totalPrice - invoicePrice;
            // $('.cls_fid_input').prop("disabled", true);
            // $("#form-invoice-data").hide();
            $("#dv_preview_checkout_invoice").hide();

            $("#dv_row_invoice_address").hide();
            $("#hdn_invoice_address").prop("disabled", true);
        }

        $("#preview_checkout_total_price").text(totalPrice);

        recalculateDiscount();

    });

    ////////////////////////////////////////////////////////
    // END CHECKOUT EVENTS
    ////////////////////////////////////////////////////////


    ////////////////////////////////////////////////////////
    // START CERTIFICATE EVENTS
    ////////////////////////////////////////////////////////
    /*
        $('.certificate-thumb-imgs').on('click', function() {
            $(".certificate-thumb-imgs").removeClass("thumb-active");
            $(this).addClass("thumb-active");

            var imgURL = $(this).attr("src");
            $("#img-holder").find('img').attr("src", imgURL);
        });

        $('#addCertificateModal').on('shown.bs.modal', function() {
            $('.cer_fields').prop('disabled', false);
        });

        $('#addCertificateModal').on('hide.bs.modal', function() {
            $('.cer_fields').prop('disabled', true);
            $('#cer_require_dedication').prop('checked', false);
            $('.cer_dedication_fields').prop('disabled', true);
            $('.cer_dedication_fields').val('');
        });

        $("#cer_require_dedication").on("click", function(e) {

            if ($(this).is(':checked')) {
                $('.cer_dedication_fields').prop("disabled", false);
            } else {
                $('.cer_dedication_fields').prop("disabled", true);
            }

        });

        $("#hrf-add-new-certificate").on("click", function(e) {
            isUpdateCertificate = 0;
        });

        $("#btn-add-certificate").on("click", function(e) {

            var dataCertificate = {
                'cer_require_dedication': '0',
                'cer_dedicated_to': null,
                'cer_dedication_line_1': null,
                'cer_dedication_line_2': null,
                'slc_graphics_certificate': $("#slc_graphics_certificate").val()
            };

            $('.cls_slc_paper_opt').each(function(k, v) {
                dataCertificate[$(v).attr('id')] = $(v).val();
            });

            if ($("#cer_require_dedication").is(':checked')) {
                dataCertificate["cer_require_dedication"] = '1';
                dataCertificate["cer_dedicated_to"] = $("#cer_dedicated_to").val();
                dataCertificate["cer_dedication_line_1"] = $("#cer_dedication_line_1").val();
                dataCertificate["cer_dedication_line_2"] = $("#cer_dedication_line_2").val();
            }

            $.ajax({
                    url: $(this).data('urlValidate'),
                    type: "POST",
                    data: dataCertificate
                })
                .done(function(data, status, object) {

                    $('.bsp-err-spn').remove();
                    $(".is-invalid").removeClass("is-invalid");

                    var htmlRecapCertificateBody = "";
                    if (data.result && data.result.recap_certificate) {
                        $.each(data.result.recap_certificate, function(key, value) {
                            htmlRecapCertificateBody += "<div class='text-muted'><small>" + value + "</small></div>";
                        });
                    }

                    var certificateValue = 0;
                    if (data.result && data.result.certificate_value) {
                        certificateValue = data.result.certificate_value;
                    }

                    var certificateTitle = "";
                    if (data.result && data.result.certificate_title) {
                        certificateTitle = "<div><strong>" + data.result.certificate_title + "</strong></div>";
                    }

                    var borderAdd = '';
                    if ($('.cls_row_preview_certificate_recap').length > 0) {
                        borderAdd = 'mt-3';
                        // borderAdd = 'border-top-0';
                    }

                    var graphicsSource = "";
                    if (data.result && data.result.graphics_source) {
                        graphicsSource = data.result.graphics_source;
                    }

                    var graphicsAlt = "";
                    if (data.result && data.result.graphics_alt) {
                        graphicsAlt = data.result.graphics_alt;
                    }

                    if (isUpdateCertificate) {

                        $(objUpdateCertificate).find('img').attr('src', graphicsSource).attr('alt', graphicsAlt);
                        $(objUpdateCertificate).find('.cls_recap_certificate_body').html(htmlRecapCertificateBody);
                        $(objUpdateCertificate).find('.cls_recap_certificate_single_value').text(certificateValue);
                        $(objUpdateCertificate).find('.hdn_field_format').val(dataCertificate["slc_paper_opt_printformatlabel"]);
                        $(objUpdateCertificate).find('.hdn_field_frame').val(dataCertificate["slc_paper_opt_framelabel"]);
                        $(objUpdateCertificate).find('.hdn_field_graphics').val(dataCertificate["slc_graphics_certificate"]);
                        $(objUpdateCertificate).find('.hdn_field_is_required').val(dataCertificate["cer_require_dedication"]);
                        $(objUpdateCertificate).find('.hdn_field_ded_to').val(dataCertificate["cer_dedicated_to"]);
                        $(objUpdateCertificate).find('.hdn_field_ded_line_1').val(dataCertificate["cer_dedication_line_1"]);
                        $(objUpdateCertificate).find('.hdn_field_ded_line_2').val(dataCertificate["cer_dedication_line_2"]);

                        isUpdateCertificate = 0;
                        objUpdateCertificate = null;

                    } else {

                        var htmlEdit = createManageDropdownCertificate();

                        var certificateHTML = "<div class='border cls_row_preview_certificate_recap " + borderAdd + "'>" +
                            "<div class='row'>" +
                            "<div class='col-3'><img src='" + graphicsSource + "' alt='" + graphicsAlt + "' class='img-fluid' width='150' height='125'></div>" +
                            "<div class='col-7'>" + certificateTitle + "<div class='cls_recap_certificate_body'>" + htmlRecapCertificateBody + "</div></div>" +
                            "<div class='col-2 text-right align-middle'>" + htmlEdit +
                            // "<div class='text-muted'><span class='manage-single-certificate cls-pointer-effect'><i class='fas fa-ellipsis-h'></i></span></div>" +
                            "<div class='h4 mt-4'>$ <span class='cls_recap_certificate_single_value'>" + certificateValue + "</span></div>" +
                            "</div>" +
                            "</div>" +
                            "<input type='hidden' class='hdn_fields_certicate hdn_field_format' name='cer[" + counterCertificate + "][format]' value='" + dataCertificate["slc_paper_opt_printformatlabel"] + "'>" +
                            "<input type='hidden' class='hdn_fields_certicate hdn_field_frame' name='cer[" + counterCertificate + "][frame]' value='" + dataCertificate["slc_paper_opt_framelabel"] + "'>" +
                            "<input type='hidden' class='hdn_fields_certicate hdn_field_graphics' name='cer[" + counterCertificate + "][graphics]' value='" + dataCertificate["slc_graphics_certificate"] + "'>" +
                            "<input type='hidden' class='hdn_fields_certicate hdn_field_is_required' name='cer[" + counterCertificate + "][is_dedication_required]' value='" + dataCertificate["cer_require_dedication"] + "'>" +
                            "<input type='hidden' class='hdn_fields_certicate hdn_field_ded_to' name='cer[" + counterCertificate + "][dedicated_to]' value='" + dataCertificate["cer_dedicated_to"] + "'>" +
                            "<input type='hidden' class='hdn_fields_certicate hdn_field_ded_line_1' name='cer[" + counterCertificate + "][dedication_line_1]' value='" + dataCertificate["cer_dedication_line_1"] + "'>" +
                            "<input type='hidden' class='hdn_fields_certicate hdn_field_ded_line_2' name='cer[" + counterCertificate + "][dedication_line_2]' value='" + dataCertificate["cer_dedication_line_2"] + "'>" +
                            "</div>";

                        $("#dv-recap-certification").append(certificateHTML).show();
                        counterCertificate++;

                    }

                    $("#addCertificateModal").modal('hide');
                    $("#cer-manage-shipping-address").show();

                    // SHOW only if is present an address
                    setPreviewAmountCertificate();

                })
                .fail(function(jqXHR, textStatus, errorThrown) {

                    $("#dv-recap-certification").hide();
                    var errors = jqXHR.responseJSON.errors;

                    for (var key in errors) {
                        var selector = "#" + key;
                        var input = $(selector);
                        $(input).addClass("is-invalid");

                        if (errors.hasOwnProperty(key)) {
                            var errorSpn = createBootstrapErrorSpan(errors[key], true);
                            var labelFor = $("label[for='" + key + "']");
                            $(labelFor).append(errorSpn);
                        }
                    }

                    if ($('.cls_row_preview_certificate_recap').length === 0) {
                        $("#cer-manage-shipping-address").hide();
                    }

                });

        });

        $("#bw-step-opt-2").on("click", ".cls-cer-edit", function() {
            var rowRecap = $(this).closest("div.cls_row_preview_certificate_recap");

            var format = $(rowRecap).find('.hdn_field_format').val();
            var frame = $(rowRecap).find('.hdn_field_frame').val();
            var graphics = $(rowRecap).find('.hdn_field_graphics').val();
            var isRequired = $(rowRecap).find('.hdn_field_is_required').val();
            var dedTo = $(rowRecap).find('.hdn_field_ded_to').val();
            var dedLine1 = $(rowRecap).find('.hdn_field_ded_line_1').val();
            var dedLine2 = $(rowRecap).find('.hdn_field_ded_line_2').val();

            $("#slc_paper_opt_printformatlabel[value='" + format + "']").prop("selected", true);
            $("#slc_paper_opt_framelabel[value='" + frame + "']").prop("selected", true);
            $("#slc_graphics_certificate[value='" + graphics + "']").prop("selected", true);

            if (isRequired === 1 || isRequired === '1') {
                $('#cer_require_dedication').prop('checked', true);
                $('.cer_dedication_fields').prop('disabled', false);
                $("#cer_dedicated_to").val(dedTo);
                $("#cer_dedication_line_1").val(dedLine1);
                $("#cer_dedication_line_2").val(dedLine2);
            } else {
                $('#cer_require_dedication').prop('checked', false);
                $('.cer_dedication_fields').prop('disabled', true);
                $("#cer_dedicated_to").val('');
                $("#cer_dedication_line_1").val('');
                $("#cer_dedication_line_2").val('');
            }

            objUpdateCertificate = rowRecap;
            isUpdateCertificate = 1;

            $("#addCertificateModal").modal('show');
        });

        $("#bw-step-opt-2").on("click", ".cls-cer-delete", function() {
            $(this).closest("div.cls_row_preview_certificate_recap").remove();
            // counterCertificate--;

            if ($('.cls_row_preview_certificate_recap').length === 0) {
                $("#cer-manage-shipping-address").hide();
                $("#row_preview_certificate_value").hide();
            }

            setPreviewAmountCertificate();
        });
    */
    ////////////////////////////////////////////////////////
    // END CERTIFICATE EVENTS
    ////////////////////////////////////////////////////////

});


function checkDiscountToRemove() {
    if (isTotalDiscounted) {
        var totalPrice = parseFloat($("#preview_checkout_total_price").text());
        var totalDiscounted = totalPrice + discountSubtracted;
        $("#preview_checkout_total_price").text(totalDiscounted);
    }
}

function recalculateDiscount() {
    if (isTotalDiscounted) {
        var totalPrice = parseFloat($("#preview_checkout_total_price").text());
        var totalDiscount = totalPrice * (discountPercentage / 100);
        totalDiscount = round(totalDiscount, 2);
        var totalDiscounted = totalPrice - totalDiscount;
        $("#preview_checkout_total_price").text(totalDiscounted);

        discountSubtracted = totalDiscount;
    }
}

/*
function setRecapTotalOrderCertificate() {

    var htmlToAppend = "";

    $('.cls_row_preview_certificate_recap').each(function() {

        var certificateAmount = parseInt($(this).find('.cls_recap_certificate_single_value').text());
        var hasDedication = $(this).find('.hdn_field_is_required').val();

        if (hasDedication === "1") {
            htmlToAppend += "<div>" + $("#utils_trans_p_certificate_w_dedication").val() + " (" + certificateAmount + " $)</div>";
        } else {
            htmlToAppend += "<div>" + $("#utils_trans_p_certificate").val() + " (" + certificateAmount + " $)</div>";
        }

    });

    var deliveryCharges = parseInt($("#form-buy-word input[name='choose_address']:checked").closest('div.cls_addresses_on_cer').find('.cls_address_delivery_charges').text());

    htmlToAppend += "<div>" + $("#utils_trans_delivery_charges").val() + " (" + deliveryCharges + " $)</div>";

    $("#preview_checkout_certificates").html(htmlToAppend);
    $("#dv_preview_checkout_certificates").show();
}

function setPreviewAmountCertificate() {
    if ($('.cls_row_preview_certificate_recap').length > 0) {
        var certificateAmount = parseInt($("#form-buy-word input[name='choose_address']:checked").closest('div.cls_addresses_on_cer').find('.cls_address_delivery_charges').text());

        $('.cls_recap_certificate_single_value').each(function() {
            certificateAmount += parseInt($(this).text());
        });

        $(".cls_preview_certificate_value").text(certificateAmount);
        $("#row_preview_certificate_value").show();
    }
}

function getPreviewAmountCertificate() {
    var certificateAmount = 0;

    if ($('.cls_row_preview_certificate_recap').length > 0) {
        certificateAmount = parseInt($("#form-buy-word input[name='choose_address']:checked").closest('div.cls_addresses_on_cer').find('.cls_address_delivery_charges').text());

        $('.cls_recap_certificate_single_value').each(function() {
            certificateAmount += parseInt($(this).text());
        });
    }

    return certificateAmount;
}*/

function checkOptionals() {

    var data = { 'opt_categories[]': [], 'optionals[]': [], 'wordValue': $("#bw_word_value").val() };

    $('.chb_category_optional:checked').each(function() {
        data['opt_categories[]'].push($(this).val());
    });

    $('.slc_optional_effect:visible').each(function() {
        data['optionals[]'].push($(this).find(":selected").val());
    });


    $.ajax({
            url: $("#bw-step-opt-1").data('urlValidate'),
            type: "POST",
            data: data
        })
        .done(function(data, status, object) {
            $('.bsp-err-spn').remove();
            $(".is-invalid").removeClass("is-invalid");
            $("#form_opt_step_1_general_error").parent().hide();

            if (data.result && data.result.expectedRank) {
                $('.cls_preview_word_rank').text(data.result.expectedRank);
            }
            if (data.result && data.result.newWordValue) {
                $('.cls_preview_word_value').text(data.result.newWordValue);
            }

            checkDiscountToRemove();

            $("#preview_checkout_total_price").text(data.result.newWordValue);

            recalculateDiscount();

            if (data.result && data.result.optionalsPreview && data.result.optionalsPreview.length > 0) {
                var optToAppend = "";
                $.each(data.result.optionalsPreview, function(k, opt) {
                    optToAppend += "<div>" + opt + "</div>";
                });
                $("#preview_checkout_optionals").html(optToAppend);
                showPreviewCheckout.optionals = 1;
            } else {
                showPreviewCheckout.optionals = 0;
            }

            domErrors.optionals = 0;
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
            $('.bsp-err-spn').remove();
            $(".is-invalid").removeClass("is-invalid");
            $("#form_opt_step_1_general_error").parent().show();

            var errors = jqXHR.responseJSON.errors;
            for (var key in errors) {
                $("input[type='checkbox']").addClass("is-invalid");

                if (errors.hasOwnProperty(key)) {
                    var errorSpn = createBootstrapErrorSpan(errors[key]);
                    $("#form_opt_step_1_general_error").append(errorSpn);
                }
            }

            domErrors.optionals = 1;

        });
}

/*
function checkCertificates() {

    var data = {};

    $('.hdn_fields_certicate').each(function(k, v) {
        data[$(v).attr('name')] = $(v).val();
    });


    $.ajax({
            url: $("#btn-validate-certificates").data('urlValidate'),
            type: "POST",
            data: data
        })
        .done(function(data, status, object) {
            certificatesErrors = '';
            domErrors.certificates = 0;
        })
        .fail(function(jqXHR, textStatus, errorThrown) {

            var errors = jqXHR.responseJSON.errors;
            for (var key in errors) {

                if (errors.hasOwnProperty(key)) {
                    var errorSpn = createBootstrapErrorSpan(errors[key]);
                    certificatesErrors += errorSpn + "<br>";
                }
            }

            domErrors.certificates = 1;

        });
}*/

function checkWordValue() {

    var input = $('.cls-check-validate-word-value');

    if ($('.cls-check-validate-word-value').val().length > 0 && !isNaN($('.cls-check-validate-word-value').val())) {

        timeoutTransaction = setTimeout(function() {

            $.ajax({
                    url: input.data('urlValidate'),
                    type: "POST",
                    data: {
                        'value': $("#bw_word_value").val(),
                        'token': $("#bw-hdn-token-1").val(),
                        'errorKey': input.data('idError')
                    }
                })
                .done(function(data, status, object) {
                    $('.bsp-err-spn').remove();
                    $(".is-invalid").removeClass("is-invalid");

                    if (data.result && data.result.expectedRank) {
                        $('.cls_preview_word_rank').text(data.result.expectedRank);
                    }
                    if (data.result && data.result.newWordValue) {
                        $('.cls_preview_word_value').text(data.result.newWordValue);
                    }

                    checkDiscountToRemove();
                    $("#preview_checkout_total_price").text(data.result.newWordValue);
                    recalculateDiscount();

                    domErrors.wordValue = 0;

                })
                .fail(function(jqXHR, textStatus, errorThrown) {
                    $('.bsp-err-spn').remove();
                    $(".is-invalid").removeClass("is-invalid");

                    var errors = jqXHR.responseJSON.errors;
                    for (var key in errors) {
                        var selector = "#" + key;
                        var input = $(selector);
                        $(input).addClass("is-invalid");

                        if (errors.hasOwnProperty(key)) {
                            var errorSpn = createBootstrapErrorSpan(errors[key]);
                            var labelFor = $("label[for='" + key + "']");
                            $(labelFor).append(errorSpn);
                        }
                    }

                    domErrors.wordValue = 1;

                })

        }, 500, input);

    } else {
        $(input).addClass("is-invalid");
        domErrors.wordValue = 1;
    }
}

function createBootstrapErrorSpan(msg, isInline = false) {
    var clsInline = isInline ? 'd-inline' : 'd-block';

    return "<span class=\"bsp-err-spn invalid-feedback " + clsInline + " \">\n" +
        "        <span class=\" " + clsInline + " \">\n" +
        "            <span class=\"form-error-icon badge badge-danger text-uppercase\">Error</span>\n" +
        "            <span class=\"form-error-message\">" + msg + "</span>\n" +
        "        </span>\n" +
        "    </span>";
}

/*
function createManageDropdownCertificate() {
    var hdnInput = $("#hdn-translation-utils");
    var editLabel = $(hdnInput).data('edit');
    var deleteLabel = $(hdnInput).data('delete');

    return "<div class='dropdown'>" +
        "  <a class='dropdown text-muted cls-pointer-effect' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>" +
        "  <i class='fas fa-ellipsis-h'></i></a>" +
        "  <div class='dropdown-menu'>" +
        "    <div class='dropdown-item cls-cer-edit'><i class='fas fa-pen'></i> " + editLabel + "</div>" +
        "    <div class='dropdown-item cls-cer-delete'><i class='fas fa-trash'></i> " + deleteLabel + "</div>" +
        "  </div>" +
        "</div>";
}*/

function round(value, decimals) {
    return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
}