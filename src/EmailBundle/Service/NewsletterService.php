<?php

namespace EmailBundle\Service;

use DateInterval;
use DateTime;
use EmailBundle\Repository\NewsletterRepository;
use Psr\Log\LoggerInterface;
use Schema\Entity;
use Schema\EntityService;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class NewsletterService extends EntityService {

    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * NewsletterService constructor.
     * @param NewsletterRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param LoggerInterface $logger
     */
    public function __construct(NewsletterRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                LoggerInterface $logger
    ) {
        parent::__construct($repository, $authorizationChecker, $logger);
        $this->repository = $repository;
    }

    /**
     * get newsletter not queuing
     * @return array|null
     * @throws \Exception
     */
    public function getNotQueuing() {
        $dateTimeBefore = new DateTime();
        $interval = new DateInterval("PT15M");
        $interval->invert = 1;
        $dateTimeBefore->sub($interval);
        $dateTimeBefore->setTimestamp(time());

        $dateTimeAfter = new DateTime();
        $interval = new DateInterval("PT15M");
        $interval->invert = 1;
        $dateTimeAfter->add($interval);
        $dateTimeAfter->setTimestamp(time());


        $list = $this->repository->findNotQueuing($dateTimeBefore, $dateTimeAfter);

        if(count($list) > 0) {
            return $list;
        }

        return null;
    }
}