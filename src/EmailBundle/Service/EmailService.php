<?php

namespace EmailBundle\Service;

use EmailBundle\Entity\Email;
use EmailBundle\Repository\EmailRepository;
use Psr\Log\LoggerInterface;
use Schema\EntityService;
use Swift_Mailer;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use DateTime;
use Exception;
use UserBundle\Entity\User;
use Utils\StaticUtil\LogUtils;

class EmailService extends EntityService {

    private $administration_address;

    private $swiftMailer;

    private $sender_name;

    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * EmailService constructor.
     * @param EmailRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param string $mailer_user
     * @param string $sender_name
     * @param Swift_Mailer $swift_mailer
     * @param LoggerInterface $logger
     */
    public function __construct(EmailRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                string $mailer_user,
                                string $sender_name,
                                Swift_Mailer $swift_mailer,
                                LoggerInterface $logger
    ) {
        $this->repository = $repository;
        $this->administration_address = $mailer_user;
        $this->swiftMailer = $swift_mailer;
        $this->sender_name = $sender_name;
        parent::__construct($repository, $authorizationChecker, $logger);
    }

    /**
     * @param null $recipient
     * @param $subject
     * @param $body
     * @param null $user
     * @param null $when
     * @param null $type
     * @param null $sendAt
     * @return bool|object
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function emailEnqueue($recipient = null, $subject, $body, $user = null, $when = null, $type = null, $sendAt = null) {
        if(is_null($when)) {
            $creation = new DateTime();
            $creation->setTimestamp(time());
        } else {
            $creation = $when;
        }

        $queueMail = new Email();
        $queueMail->setRecipient(is_null($recipient)?$this->administration_address:$recipient);
        $queueMail->setUser($user);
        $queueMail->setSubject($subject);
        $queueMail->setBody($body);
        $queueMail->setQueueAt($creation);
        $queueMail->setSendAt($sendAt);
        $queueMail->setStatus(false);
        $queueMail->setType($type);

        return $this->save($queueMail);
    }

    /**
     * get Queue Email not sent
     * @return array|null
     */
    public function getNotSent() {
        $date = new DateTime();
        $date->setTimestamp(time());
        $list = $this->repository->findNotSent($date);

        if(count($list) > 0) {
            return $list;
        }

        return null;
    }

    public function processQueueEmail(string $subject, string $email, string $body) {

        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom([$this->administration_address => $this->sender_name])
            ->setTo($email)
            ->setContentType('text/html')
            ->setBody($body)
        ;

        try {
            if(!$this->swiftMailer->send($message)) {
                return false;
            }
        } catch (Exception $e) {
            //TODO: add log
//            $exception = LogUtils::getFormattedExceptions($e);
            return false;
        }

        return true;
    }

    /**
     * Get just sent confirmation
     * @param User $user
     * @return bool
     */
    public function getJustSentConfirmation(User $user){
        $current_date = new DateTime();
        $current_date->setTimestamp(time());
        $day_less_date = (new \DateTime())->modify('-2 hours');
        $list = $this->repository->findJustSentConfirmation($user, $current_date, $day_less_date);

        if(count($list) > 1) {
            return true;
        }

        return false;
    }


    /**
     * Get just sent reset
     * @param User $user
     * @return bool
     */
    public function getJustSentReset(User $user){
        $current_date = new DateTime();
        $current_date->setTimestamp(time());
        $day_less_date = (new \DateTime())->modify('-2 hours');
        $list = $this->repository->findJustSentReset($user, $current_date, $day_less_date);

        if(count($list) > 0) {
            return true;
        }

        return false;
    }

}