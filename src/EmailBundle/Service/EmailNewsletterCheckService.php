<?php

namespace EmailBundle\Service;

use DateTime;
use EmailBundle\Entity\EmailNewsletterCheck;
use EmailBundle\Repository\EmailNewsletterCheckRepository;
use Psr\Log\LoggerInterface;
use Schema\EntityService;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class EmailNewsletterCheckService extends EntityService {

    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * EmailNewsletterCheckService constructor.
     * @param EmailNewsletterCheckRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param LoggerInterface $logger
     */
    public function __construct(EmailNewsletterCheckRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                LoggerInterface $logger
    ) {
        parent::__construct($repository, $authorizationChecker, $logger);
    }

//    /**
//     * @param Profile $profile
//     * @param bool $manualFlush
//     * @param EmailNewsletterCheck|null $emailNewsletterCheck
//     * @return EmailNewsletterCheck|null
//     * @throws \Doctrine\DBAL\ConnectionException
//     */
//    public function saveEmailNewsletterCheck(Profile $profile, $manualFlush = false, EmailNewsletterCheck $emailNewsletterCheck = null) {
//        $creation = new DateTime();
//        $creation->setTimestamp(time());
//
//        if(is_null($emailNewsletterCheck)) {
//            $emailNewsletterCheck = new EmailNewsletterCheck();
//            $emailNewsletterCheck->setCreatedOn($creation);
//        }
//        $emailNewsletterCheck->setIdProfile($profile->getId());
//        $emailNewsletterCheck->setEmail($profile->getEmail());
//        $emailNewsletterCheck->setIsBadged($profile->serializedIsBadged());
//
//        if(!$manualFlush) {
//            if(!$this->save($emailNewsletterCheck)) {
//                return null;
//            }
//        }
//
//        return $emailNewsletterCheck;
//    }
}