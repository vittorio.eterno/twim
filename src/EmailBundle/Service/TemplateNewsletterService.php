<?php

namespace EmailBundle\Service;

use EmailBundle\Repository\TemplateNewsletterRepository;
use Psr\Log\LoggerInterface;
use Schema\Entity;
use Schema\EntityService;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class TemplateNewsletterService extends EntityService {

    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * TemplateNewsletterService constructor.
     * @param TemplateNewsletterRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param LoggerInterface $logger
     */
    public function __construct(TemplateNewsletterRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                LoggerInterface $logger
    ) {
        parent::__construct($repository, $authorizationChecker, $logger);
    }
    
}