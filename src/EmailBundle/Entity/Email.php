<?php

namespace EmailBundle\Entity;

use DateTime;
use Schema\Entity;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;
use UserBundle\Entity\User;

/**
 * @ORM\Table(name="`emails`")
 * @ORM\Entity(repositoryClass="EmailBundle\Repository\EmailRepository")
 */
class Email extends Entity {

    const TYPE_REGISTRATION_CONFIRM         = 0;
    const TYPE_REGISTRATION_SUCCESS         = 1;
    const TYPE_PASSWORD_RESETTING           = 2;
    const TYPE_PASSWORD_RESETTING_SUCCESS   = 3;
    const TYPE_LOGMONITOR_CHECK             = 4;
    const TYPE_REMINDER_THERAPY             = 5;
    const TYPE_INVITE_TO_REGISTRATION       = 6;
    const TYPE_EXPIRATION_TRIAL_PREMIUM     = 7;
    const TYPE_GENERIC_NEWSLETTER           = 8;
    const TYPE_CHANGE_EMAIL                 = 9;
    const TYPE_NOTICE_TO_ADMIN              = 10;

    const STATUS_PENDING    = 0;
    const STATUS_SENDED     = 1;
    const STATUS_FAILED     = 2;

    /**
     * @ORM\Column(name="`id`", type="bigint", options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer $id
     */
    private $id;

    /**
     * @ORM\Column(name="`recipient`", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "require.email")
     * @Assert\Email(
     *     message = "invalid.email",
     *     checkMX = true,
     *     checkHost = true
     * )
     */
    private $recipient = null;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="emails")
     * @ORM\JoinColumn(name="id_user", referencedColumnName="id", nullable=true)
     * @var User $user
     */
    private $user;

    /**
     * @ORM\Column(name="`type`", type="integer", nullable=false, options={"default":0})
     */
    private $type;

    /**
     * @ORM\Column(name="`subject`", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "require.subject")
     * @Assert\Regex(
     *     pattern="/^[^<>]*$/",
     *     message="invalid.subject"
     * )
     * @Assert\Length(
     *      min = 0,
     *      max = 255,
     *      maxMessage = "max.length.subject"
     * )
     */
    private $subject = null;

    /**
     * @ORM\Column(name="`body`", type="text", nullable=false)
     * @Assert\NotBlank(message="require.body")
     * @Assert\Regex(
     *     pattern="/^[^<>]*$/",
     *     message="invalid.body"
     * )
     */
    private $body = null;

    /**
     * @ORM\Column(name="`queue_at`", type="datetime", nullable=false)
     * @var DateTime $queueAt
     */
    private $queueAt;

    /**
     * @ORM\Column(name="`send_at`", type="datetime", nullable=true, options={"default":NULL})
     * @var DateTime $sendAt
     */
    private $sendAt;

    /**
     * @ORM\Column(name="`failed_at`", type="datetime", nullable=true, options={"default":NULL})
     * @var DateTime $failedAt
     */
    private $failedAt;

    /**
     * @ORM\Column(name="`status`", type="integer", nullable=true, options={"default":0})
     */
    private $status;

    ################################################# SERIALIZER FUNCTIONS

    /**
     * Returns the array of fields to serialize in entity administration view.
     * @return array
     */
    public function adminSerializer()
    {
        $view_vars = $this->viewSerializer();

        $admin_vars = array();

        return array_merge($view_vars, $admin_vars);
    }

    /**
     * Returns the array of fields to serialize in entity view.
     * @return array
     */
    public function viewSerializer()
    {
        $list_vars = $this->listSerializer();

        $view_vars = array(
        );

        return array_merge($list_vars, $view_vars);
    }

    /**
     * Returns the array of fields to serialize in a list of this entity.
     * @return array
     */
    public function listSerializer()
    {
        $list_vars = array(
        );
        return $list_vars;
    }

    /**
     * Returns the hash code unique identifier of the entity.
     * @return string
     */
    public function hashCode()
    {
        // TODO: Implement hashCode() method.
    }

    ################################################# SERIALIZED FUNCTIONS



    ################################################# GETTERS AND SETTERS FUNCTIONS


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set recipient.
     *
     * @param string $recipient
     *
     * @return Email
     */
    public function setRecipient($recipient)
    {
        $this->recipient = $recipient;

        return $this;
    }

    /**
     * Get recipient.
     *
     * @return string
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * Set subject.
     *
     * @param string $subject
     *
     * @return Email
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject.
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set body.
     *
     * @param string $body
     *
     * @return Email
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body.
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set queueAt.
     *
     * @param \DateTime $queueAt
     *
     * @return Email
     */
    public function setQueueAt($queueAt)
    {
        $this->queueAt = $queueAt;

        return $this;
    }

    /**
     * Get queueAt.
     *
     * @return \DateTime
     */
    public function getQueueAt()
    {
        return $this->queueAt;
    }

    /**
     * Set sendAt.
     *
     * @param \DateTime|null $sendAt
     *
     * @return Email
     */
    public function setSendAt($sendAt = null)
    {
        $this->sendAt = $sendAt;

        return $this;
    }

    /**
     * Get sendAt.
     *
     * @return \DateTime|null
     */
    public function getSendAt()
    {
        return $this->sendAt;
    }

    /**
     * Set failedAt.
     *
     * @param \DateTime|null $failedAt
     *
     * @return Email
     */
    public function setFailedAt($failedAt = null)
    {
        $this->failedAt = $failedAt;

        return $this;
    }

    /**
     * Get failedAt.
     *
     * @return \DateTime|null
     */
    public function getFailedAt()
    {
        return $this->failedAt;
    }

    /**
     * Set status.
     *
     * @param integer $status
     *
     * @return Email
     */
    public function setStatus($status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return integer|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set user.
     *
     * @param \UserBundle\Entity\User|null $user
     *
     * @return Email
     */
    public function setUser(\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \UserBundle\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set type.
     *
     * @param bool|null $type
     *
     * @return Email
     */
    public function setType($type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return bool|null
     */
    public function getType()
    {
        return $this->type;
    }
}
