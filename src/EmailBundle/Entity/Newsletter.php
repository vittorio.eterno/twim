<?php

namespace EmailBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Schema\Entity;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="`newsletters`")
 * @ORM\Entity(repositoryClass="EmailBundle\Repository\NewsletterRepository")
 */
class Newsletter extends Entity {

    const MAILING_LIST_ALL              = 0;
    const MAILING_LIST_PATIENTS         = 1;
    const MAILING_LIST_BADGES           = 2;
    const MAILING_LIST_SPECIFIC_BADGE   = 3;

    /**
     * @ORM\Column(name="`id`", type="bigint", options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer $id
     */
    private $id;

    /**
     * @ORM\Column(name="`summary`", type="string", length=255, nullable=true)
     * @Assert\Regex(
     *     pattern="/^[^<>]*$/",
     *     message="invalid.summary"
     * )
     * @Assert\Length(
     *      min = 0,
     *      max = 255,
     *      maxMessage = "max.length.summary"
     * )
     */
    private $summary = null;

    /**
     * @ORM\Column(name="`is_draft`", type="boolean", nullable=false, options={"default":0})
     */
    private $isDraft = false;

    /**
     * @ORM\Column(name="`send_now`", type="boolean", nullable=true, options={"default":0})
     */
    private $sendNow = false;

    /**
     * @ORM\Column(name="`mailing_list_type`", type="integer", nullable=false)
     * @Assert\NotBlank(message = "require.mailing.list.type")
     */
    private $mailingListType;

    /**
     * @ORM\Column(name="`id_badge_structure_mailing_list`", type="integer", nullable=true)
     */
    private $idBadgeStructureMailingList;

    /**
     * @ORM\Column(name="`send_at`", type="datetime", nullable=true, options={"default":NULL})
     * @var DateTime $sendAt
     */
    private $sendAt;

    /**
     * @ORM\Column(name="`created_by`", type="string", length=22, nullable=false)
     */
    private $createdBy;

    /**
     * @ORM\Column(name="`created_at`", type="datetime", nullable=false)
     * @var DateTime $createdAt
     */
    private $createdAt;

    /**
     * @ORM\Column(name="`edited_by`", type="string", length=22, nullable=true)
     */
    private $editedBy;

    /**
     * @ORM\Column(name="`edited_at`", type="datetime", nullable=true)
     * @var DateTime $editedAt
     */
    private $editedAt;

    /**
     * @ORM\Column(name="`disabled_by`", type="string", length=22, nullable=true)
     */
    private $disabledBy;

    /**
     * @ORM\Column(name="`disabled_at`", type="datetime", nullable=true)
     * @var DateTime $disabledAt
     */
    private $disabledAt;

    /**
     * @ORM\Column(name="`is_disabled`", type="boolean", nullable=false, options={"default":0})
     */
    private $isDisabled = false;

    /**
     * @ORM\Column(name="`queuing`", type="boolean", nullable=false, options={"default":0})
     */
    private $queuing = false;

    /**
     * @ORM\ManyToOne(targetEntity="TemplateNewsletter", inversedBy="newsletter")
     * @ORM\JoinColumn(name="id_template_newsletters", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank(message = "require.template.newsletter")
     */
    private $templateNewsletter;


    ################################################# SERIALIZER FUNCTIONS

    /**
     * Returns the array of fields to serialize in entity administration view.
     * @return array
     */
    public function adminSerializer()
    {
        $view_vars = $this->viewSerializer();

        $admin_vars = array(
            'id'        => $this->serializedId(),
            'summary'   => $this->serializedSummary(),
            'sendAt'    => $this->serializedSendAt(),
            'isDraft'   => $this->serializedIsDraft()
        );

        return array_merge($view_vars, $admin_vars);
    }

    /**
     * Returns the array of fields to serialize in entity view.
     * @return array
     */
    public function viewSerializer()
    {
        $list_vars = $this->listSerializer();

        $view_vars = array(
        );

        return array_merge($list_vars, $view_vars);
    }

    /**
     * Returns the array of fields to serialize in a list of this entity.
     * @return array
     */
    public function listSerializer()
    {
        $list_vars = array(
        );
        return $list_vars;
    }

    /**
     * Returns the hash code unique identifier of the entity.
     * @return string
     */
    public function hashCode()
    {
        // TODO: Implement hashCode() method.
    }

    ################################################# SERIALIZED FUNCTIONS

    /**
     * Newsletter id
     * @JMS\VirtualProperty
     * @JMS\SerializedName("id")
     * @JMS\Type("string")
     * @JMS\Groups({"view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedId() {
        return (is_null($this->id)?null:$this->id);
    }

    /**
     * Newsletter summary
     * @JMS\VirtualProperty
     * @JMS\SerializedName("summary")
     * @JMS\Type("string")
     * @JMS\Groups({"view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedSummary() {
        return (is_null($this->summary)?null:$this->summary);
    }

    /**
     * Newsletter isDraft
     * @JMS\VirtualProperty
     * @JMS\SerializedName("isDraft")
     * @JMS\Type("string")
     * @JMS\Groups({"view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedIsDraft() {
        return (is_null($this->isDraft)?null:$this->isDraft);
    }

    /**
     * Newsletter sendAt
     * @JMS\VirtualProperty
     * @JMS\SerializedName("createdAt")
     * @JMS\Type("string")
     * @JMS\Groups({"view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedSendAt() {
        return (is_null($this->sendAt)?null:strftime('%Y-%m-%d %H:%M',$this->sendAt->getTimestamp()));
    }

    ################################################# GETTERS AND SETTERS FUNCTIONS


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set summary.
     *
     * @param string|null $summary
     *
     * @return Newsletter
     */
    public function setSummary($summary = null)
    {
        $this->summary = $summary;

        return $this;
    }

    /**
     * Get summary.
     *
     * @return string|null
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /**
     * Set sendAt.
     *
     * @param \DateTime|null $sendAt
     *
     * @return Newsletter
     */
    public function setSendAt($sendAt = null)
    {
        $this->sendAt = $sendAt;

        return $this;
    }

    /**
     * Get sendAt.
     *
     * @return \DateTime|null
     */
    public function getSendAt()
    {
        return $this->sendAt;
    }

    /**
     * Set templateNewsletter.
     *
     * @param TemplateNewsletter|null $templateNewsletter
     *
     * @return Newsletter
     */
    public function setTemplateNewsletter(TemplateNewsletter $templateNewsletter = null)
    {
        $this->templateNewsletter = $templateNewsletter;

        return $this;
    }

    /**
     * Get templateNewsletter.
     *
     * @return TemplateNewsletter|null
     */
    public function getTemplateNewsletter()
    {
        return $this->templateNewsletter;
    }

    /**
     * Set createdBy.
     *
     * @param string $createdBy
     *
     * @return Newsletter
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Newsletter
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set editedBy.
     *
     * @param string|null $editedBy
     *
     * @return Newsletter
     */
    public function setEditedBy($editedBy = null)
    {
        $this->editedBy = $editedBy;

        return $this;
    }

    /**
     * Get editedBy.
     *
     * @return string|null
     */
    public function getEditedBy()
    {
        return $this->editedBy;
    }

    /**
     * Set editedAt.
     *
     * @param \DateTime|null $editedAt
     *
     * @return Newsletter
     */
    public function setEditedAt($editedAt = null)
    {
        $this->editedAt = $editedAt;

        return $this;
    }

    /**
     * Get editedAt.
     *
     * @return \DateTime|null
     */
    public function getEditedAt()
    {
        return $this->editedAt;
    }

    /**
     * Set disabledBy.
     *
     * @param string|null $disabledBy
     *
     * @return Newsletter
     */
    public function setDisabledBy($disabledBy = null)
    {
        $this->disabledBy = $disabledBy;

        return $this;
    }

    /**
     * Get disabledBy.
     *
     * @return string|null
     */
    public function getDisabledBy()
    {
        return $this->disabledBy;
    }

    /**
     * Set disabledAt.
     *
     * @param \DateTime|null $disabledAt
     *
     * @return Newsletter
     */
    public function setDisabledAt($disabledAt = null)
    {
        $this->disabledAt = $disabledAt;

        return $this;
    }

    /**
     * Get disabledAt.
     *
     * @return \DateTime|null
     */
    public function getDisabledAt()
    {
        return $this->disabledAt;
    }

    /**
     * @return mixed
     */
    public function getIsDisabled()
    {
        return $this->isDisabled;
    }

    /**
     * @param mixed $isDisabled
     * @return Newsletter
     */
    public function setIsDisabled($isDisabled)
    {
        $this->isDisabled = $isDisabled;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsDraft()
    {
        return $this->isDraft;
    }

    /**
     * @param mixed $isDraft
     * @return Newsletter
     */
    public function setIsDraft($isDraft)
    {
        $this->isDraft = $isDraft;

        return $this;
    }

    /**
     * Set mailingListType.
     *
     * @param int $mailingListType
     *
     * @return Newsletter
     */
    public function setMailingListType($mailingListType)
    {
        $this->mailingListType = $mailingListType;

        return $this;
    }

    /**
     * Get mailingListType.
     *
     * @return int
     */
    public function getMailingListType()
    {
        return $this->mailingListType;
    }

    /**
     * Set idBadgeStructureMailingList.
     *
     * @param int|null $idBadgeStructureMailingList
     *
     * @return Newsletter
     */
    public function setIdBadgeStructureMailingList($idBadgeStructureMailingList = null)
    {
        $this->idBadgeStructureMailingList = $idBadgeStructureMailingList;

        return $this;
    }

    /**
     * Get idBadgeStructureMailingList.
     *
     * @return int|null
     */
    public function getIdBadgeStructureMailingList()
    {
        return $this->idBadgeStructureMailingList;
    }

    /**
     * Set sendNow.
     *
     * @param bool|null $sendNow
     *
     * @return Newsletter
     */
    public function setSendNow($sendNow = null)
    {
        $this->sendNow = $sendNow;

        return $this;
    }

    /**
     * Get sendNow.
     *
     * @return bool|null
     */
    public function getSendNow()
    {
        return $this->sendNow;
    }

    /**
     * Set queuing.
     *
     * @param bool $queuing
     *
     * @return Newsletter
     */
    public function setQueuing($queuing)
    {
        $this->queuing = $queuing;

        return $this;
    }

    /**
     * Get queuing.
     *
     * @return bool
     */
    public function getQueuing()
    {
        return $this->queuing;
    }
}
