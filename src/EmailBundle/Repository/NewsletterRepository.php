<?php

namespace EmailBundle\Repository;

use DateTime;
use EmailBundle\Entity\Newsletter;
use Schema\SchemaEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Schema\Entity;
use Utils\StaticUtil\LogUtils;

class NewsletterRepository extends SchemaEntityRepository {

    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * NewsletterRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Newsletter::class);
    }

    /**
     * get Queue Email not sent
     * @param DateTime $dateBefore
     * @param DateTime $dateAfter
     * @return array
     */
    public function findNotQueuing($dateBefore, $dateAfter) {
        $qb = $this->createQueryBuilder('newsletter')
            ->orWhere('newsletter.sendNow = 1 OR (newsletter.sendAt >= :date_before AND newsletter.sendAt <= :date_after)')
            ->andWhere('newsletter.queuing = 0')
            ->andWhere('newsletter.isDraft = 0')
            ->setParameter('date_before' , date('Y-m-d H:i:s', $dateBefore->getTimestamp()))
            ->setParameter('date_after' ,  date('Y-m-d H:i:s', $dateAfter->getTimestamp()))
            ->orderBy('newsletter.sendNow');

        $list = array();

        try {
            $list = $qb->getQuery()->getResult();
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            // TODO: add log
//            $this->logMonitorService->trace(LogMonitor::LOG_CHANNEL_QUERY, 500, "query.error", $exception);
        }

        return $list;
    }
}