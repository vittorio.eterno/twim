<?php

namespace EmailBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use EmailBundle\Entity\TemplateNewsletter;
use EmailBundle\Form\TemplateNewsletterType;
use EmailBundle\Service\TemplateNewsletterService;
use Exceptions\ValidationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Utils\Service\ResponseUtils;
use Utils\StaticUtil\IntegerUtils;

class TemplateNewsletterController extends Controller {

    /**
     * Get a list of all template newsletter.
     *
     * @Route("/admin/template-newsletter/list")
     *
     */
    public function listAction(TemplateNewsletterService $templateNewsletterService) {
        $response = new ResponseUtils($this->get("translator"));

        $templateNewsletters = $templateNewsletterService->getList('adminSerializer', array("isDisable"=>'0'));

        return $response->getListResponse($templateNewsletters);
    }

    /**
     * Action used to get a TemplateNewsletter.
     *
     * @Route("/admin/template-newsletter/{id}")
     *
     */
    public function showAction(TemplateNewsletter $templateNewsletter=null, $id){
        $response = new ResponseUtils($this->get("translator"));

        if(!IntegerUtils::checkId($id)){
            return $response->getResponse(array(), "parameter.id.invalid",400);
        }

        if(is_null($templateNewsletter)){
            return $response->getResponse(array(), "data.not.found.404",404);
        }

        return $response->getResponse($templateNewsletter->adminSerializer());
    }

    /**
     * Create a templateNewsletter.
     *
     * @Route("super/template-newsletter")
     *
     */
    public function createAction(Request $request, TemplateNewsletterService $templateNewsletterService, ValidationException $validationException) {
        $response  = new ResponseUtils($this->get("translator"));

        $templateNewsletter    = new TemplateNewsletter();
        $validator = $this->get('validator');

        $form = $this->createForm(TemplateNewsletterType::class, $templateNewsletter);
        $data = json_decode($request->getContent(), true);

        $form->submit($data);
        $errors = $validator->validate($templateNewsletter);
        if(count($errors) > 0) {
            $formattedErrors = $validationException->getFormattedExceptions($errors);
            return $response->getResponse($formattedErrors, "parameters.invalid",400);
        }

        if(!$templateNewsletterService->save($templateNewsletter, $this->getUser())) {
            return $response->getResponse(array(), "error.save", 500);
        }

        return $response->getResponse($templateNewsletter->adminSerializer(), "success.save", 201);
    }

    /**
     * Update a templateNewsletter.
     *
     * @Route("/super/template-newsletter/{id}")
     *
     */
    public function updateAction(TemplateNewsletter $templateNewsletter=null, $id, Request $request, TemplateNewsletterService $templateNewsletterService, ValidationException $validationException) {
        $response  = new ResponseUtils($this->get("translator"));

        if(!IntegerUtils::checkId($id)){
            return $response->getResponse(array(), "parameter.id.invalid",400);
        }

        if(is_null($templateNewsletter)){
            return $response->getResponse(array(), "data.not.found.404",404);
        }

        $validator = $this->get('validator');

        $form = $this->createForm(TemplateNewsletterType::class, $templateNewsletter);
        $data = json_decode($request->getContent(), true);

        $form->submit($data);
        $errors = $validator->validate($templateNewsletter);

        if(count($errors) > 0) {
            $formattedErrors = $validationException->getFormattedExceptions($errors);
            return $response->getResponse($formattedErrors, "parameters.invalid",400);
        }

        if(!$templateNewsletterService->save($templateNewsletter)) {
            return $response->getResponse(array(), "error.save",500);
        }

        return $response->getResponse($templateNewsletter->adminSerializer(), "success.save");
    }

    /**
     * Delete a templateNewsletter.
     *
     * @Route("/super/template-newsletter/{id}")
     *
     */
    public function deleteAction(TemplateNewsletter $templateNewsletter=null, $id, TemplateNewsletterService $templateNewsletterService) {
        $response  = new ResponseUtils($this->get("translator"));

        if(!IntegerUtils::checkId($id)){
            return $response->getResponse(array(), "parameter.id.invalid",400);
        }

        if(is_null($templateNewsletter)){
            return $response->getResponse(array(), "data.not.found.404",404);
        }

        $user = $this->getUser();
        if(!$user) {
            return $response->getResponse(array(), "user.inexistent", 400);
        }

        if(!$templateNewsletterService->delete($templateNewsletter, $user)) {
            return $response->getResponse(array(), "error.save",500);
        }

        return $response->getResponse(array(),"success.delete");
    }
}
