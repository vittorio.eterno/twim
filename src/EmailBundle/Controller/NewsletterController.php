<?php

namespace EmailBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use EmailBundle\Entity\Newsletter;
use EmailBundle\Form\NewsletterType;
use EmailBundle\Service\NewsletterService;
use Exceptions\ValidationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Utils\Service\ResponseUtils;
use Utils\StaticUtil\IntegerUtils;

class NewsletterController extends Controller {

    /**
     * Get a list of all articles.
     *
     * @Route("/admin/newsletter/list")
     *
     */
    public function listAction(NewsletterService $newsletterService) {
        $response = new ResponseUtils($this->get("translator"));

        $newsletters = $newsletterService->getList('adminSerializer', array("isDisable"=>'0'));

        return $response->getListResponse($newsletters);
    }

    /**
     * Action used to get a Newsletter.
     *
     * @Route("/admin/newsletter/{id}")
     */
    public function showAction(Newsletter $newsletter=null, $id){
        $response = new ResponseUtils($this->get("translator"));

        if(!IntegerUtils::checkId($id)){
            return $response->getResponse(array(), "parameter.id.invalid",400);
        }

        if(is_null($newsletter)){
            return $response->getResponse(array(), "data.not.found.404",404);
        }

        return $response->getResponse($newsletter->adminSerializer());
    }

    /**
     * Create a newsletter.
     *
     * @Route("super/newsletter")
     *
     */
    public function createAction(Request $request, NewsletterService $newsletterService, ValidationException $validationException) {
        $response  = new ResponseUtils($this->get("translator"));

        $newsletter    = new Newsletter();
        $validator = $this->get('validator');

        $form = $this->createForm(NewsletterType::class, $newsletter);
        $data = json_decode($request->getContent(), true);

        $form->submit($data);
        $errors = $validator->validate($newsletter);
        if(count($errors) > 0) {
            $formattedErrors = $validationException->getFormattedExceptions($errors);
            return $response->getResponse($formattedErrors, "parameters.invalid",400);
        }

        //Control for mailing list on specific badge structure
        if($newsletter->getMailingListType() === Newsletter::MAILING_LIST_SPECIFIC_BADGE) {
            if(is_null($newsletter->getIdBadgeStructureMailingList())) {
                return $response->getResponse(array(), "parameters.invalid",400);
            }

//            /** @var BadgeStructure $badgeStructure */
//            $badgeStructure = $badgeStructureService->getById($newsletter->getIdBadgeStructureMailingList());
//            if(is_null($badgeStructure)){
//                return $response->getResponse(array(), "parameters.invalid",400);
//            }
        }

        if(!$newsletterService->save($newsletter, $this->getUser())) {
            return $response->getResponse(array(), "error.save", 500);
        }

        return $response->getResponse($newsletter->adminSerializer(), "success.save", 201);
    }

    /**
     * Update a newsletter.
     *
     * @Route("/super/newsletter/{id}")
     *
     */
    public function updateAction(Newsletter $newsletter=null, $id, Request $request, NewsletterService $newsletterService, ValidationException $validationException) {
        $response  = new ResponseUtils($this->get("translator"));

        if(!IntegerUtils::checkId($id)){
            return $response->getResponse(array(), "parameter.id.invalid",400);
        }

        if(is_null($newsletter)){
            return $response->getResponse(array(), "data.not.found.404",404);
        }

        $validator = $this->get('validator');

        $form = $this->createForm(NewsletterType::class, $newsletter);
        $data = json_decode($request->getContent(), true);

        $form->submit($data);
        $errors = $validator->validate($newsletter);

        if(count($errors) > 0) {
            $formattedErrors = $validationException->getFormattedExceptions($errors);
            return $response->getResponse($formattedErrors, "parameters.invalid",400);
        }

        //Control for mailing list on specific badge structure
        if($newsletter->getMailingListType() === Newsletter::MAILING_LIST_SPECIFIC_BADGE) {
            if(is_null($newsletter->getIdBadgeStructureMailingList())) {
                return $response->getResponse(array(), "parameters.invalid",400);
            }

//            /** @var BadgeStructure $badgeStructure */
//            $badgeStructure = $badgeStructureService->getById($newsletter->getIdBadgeStructureMailingList());
//            if(is_null($badgeStructure)){
//                return $response->getResponse(array(), "parameters.invalid",400);
//            }
        }

        if(!$newsletterService->save($newsletter)) {
            return $response->getResponse(array(), "error.save",500);
        }

        return $response->getResponse($newsletter->adminSerializer(), "success.save");
    }

    /**
     * Delete a newsletter.
     *
     * @Route("/super/newsletter/{id}")
     *
     */
    public function deleteAction(Newsletter $newsletter=null, $id, NewsletterService $newsletterService) {
        $response  = new ResponseUtils($this->get("translator"));

        if(!IntegerUtils::checkId($id)){
            return $response->getResponse(array(), "parameter.id.invalid",400);
        }

        if(is_null($newsletter)){
            return $response->getResponse(array(), "data.not.found.404",404);
        }

        $user = $this->getUser();
        if(!$user) {
            return $response->getResponse(array(), "user.inexistent", 400);
        }

        if(!$newsletterService->delete($newsletter, $user)) {
            return $response->getResponse(array(), "error.save",500);
        }

        return $response->getResponse(array(),"success.delete");
    }
}
