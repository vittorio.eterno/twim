<?php

namespace EmailBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use EmailBundle\Entity\EmailNewsletterCheck;
use EmailBundle\Service\EmailNewsletterCheckService;
use DateTime;
use ProfileBundle\Document\Profile;
use ProfileBundle\Entity\Setting;
use ProfileBundle\Service\ProfileService;
use ProfileBundle\Service\ProfileSettingService;
use ProfileBundle\Service\SettingService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EmailNewsletterScanningCommand extends ContainerAwareCommand {
//    private $settingService;
//    private $profileService;
//    private $emailNewsletterCheckService;
//    private $profileSettingService;
//    /** @var EntityManagerInterface $entityManager */
//    private $entityManager;
//
//    public function __construct(SettingService $settingService,
//                                ProfileService $profileService,
//                                EmailNewsletterCheckService $emailNewsletterCheckService,
//                                ProfileSettingService $profileSettingService,
//                                EntityManagerInterface $entityManager) {
//        $this->settingService               = $settingService;
//        $this->profileService               = $profileService;
//        $this->profileSettingService        = $profileSettingService;
//        $this->emailNewsletterCheckService  = $emailNewsletterCheckService;
//        $this->entityManager        = $entityManager;
//        parent::__construct();
//    }
//
    protected function configure() {
        $this
            ->setName('email-newsletter:scanning')
            ->setDescription('Start to scanning emails newsletter.')
            ->setHelp('Start to scanning emails newsletter.')
        ;
    }
//
//    protected function execute(InputInterface $input, OutputInterface $output) {
//        $log_dir = $this->getContainer()->getParameter('log_dir');
//
//        $log_file = $log_dir.'reports/email-newsletter-scanning.log';
//
//        if (!file_exists($log_dir.'reports')) {
//            mkdir($log_dir.'reports', 0777, true);
//        }
//
//        $handle = fopen($log_file, "a+");
//        flock($handle, LOCK_EX);
//        fwrite($handle, strftime('%Y-%m-%d %H:%M:%S')." - START COMMAND\n");
//        flock($handle, LOCK_UN);
//        fclose($handle);
//        set_time_limit(0);
//        $output->writeln("START");
//
//        $emailNewsletterChecks = $this->emailNewsletterCheckService->getListByAttributes();
//
//        if(empty($emailNewsletterChecks)) {
//            $output->writeln("DONE");
//            return;
//        }
//
//        $idProfiles = array();
//        /** @var EmailNewsletterCheck $emailNewsletterCheck */
//        foreach($emailNewsletterChecks as $emailNewsletterCheck) {
//            $idProfiles[] = $emailNewsletterCheck->getIdProfile();
//        }
//
//        $newProfiles = $this->profileService->getAllNotIdsEscaped($idProfiles);
//
//        $creation = new DateTime();
//        $creation->setTimestamp(time());
//        $i = 0;
//        $batchSize = 200;
//
//        $allows_email_source = "email.update";
//        /** @var Setting $setting */
//        $setting = $this->settingService->getOneByAttributes(array('isDisable' => 0, 'source' => $allows_email_source));
//        if(is_null($setting)) {
//            return;
//        }
//
//        if(empty($newProfiles)) {
//            $output->writeln("DONE");
//            return;
//        }
//
//        //ADD EMAIL
//        /** @var Profile $profile */
//        foreach($newProfiles as $profile) {
//            $i++;
//            $profileSettings = $profile->getProfileSettings()->getValues();
//
//            if($this->profileSettingService->settingAllowsBySource($allows_email_source, $profileSettings, $setting)) {
//                try {
//                    $emailNewsletterCheck = $this->emailNewsletterCheckService->saveEmailNewsletterCheck($profile, true);
//                } catch (\Exception $e) {
//                    $handle = fopen($log_file, "a+");
//                    flock($handle, LOCK_EX);
//                    fwrite($handle, strftime('%Y-%m-%d %H:%M:%S')." - EMAIL NEWSLETTER CHECK CREATE ERROR\n");
//                    flock($handle, LOCK_UN);
//                    fclose($handle);
//                    $output->writeln("NOT CREATE");
//                }
//
//                $this->entityManager->merge($emailNewsletterCheck);
//
//                //if (($i % 20) === 0) {
//                    $this->entityManager->flush();
//                    $this->entityManager->clear();
//                //}
//            }
//        }
//
//        $oldProfiles = $this->profileService->getAllByIds($idProfiles);
//
//        if(empty($oldProfiles)) {
//            $output->writeln("DONE");
//            return;
//        }
//
//        //REMOVE EMAIL
//        $i = 0;
//        /** @var Profile $profile */
//        foreach($oldProfiles as $profile) {
//            $profileSettings = $profile->getProfileSettings()->getValues();
//
//            if(!$this->profileSettingService->settingAllowsBySource($allows_email_source, $profileSettings, $setting)) {
//                /** @var EmailNewsletterCheck $emailNewsletterCheck */
//                foreach($emailNewsletterChecks as $emailNewsletterCheck) {
//                    if($emailNewsletterCheck->getIdProfile() === $profile->getId()) {
//                        $i++;
//                        try {
//                            if (($i % $batchSize) === 0) {
//                                $this->emailNewsletterCheckService->delete($emailNewsletterCheck);
//                            }
//                        } catch (\Exception $e) {
//                            $handle = fopen($log_file, "a+");
//                            flock($handle, LOCK_EX);
//                            fwrite($handle, strftime('%Y-%m-%d %H:%M:%S')." - EMAIL NEWSLETTER CHECK ERROR\n");
//                            flock($handle, LOCK_UN);
//                            fclose($handle);
//                            $output->writeln("NOT DELETE");
//                        }
//                    }
//                }
//            }
//        }
//
//
//        $handle = fopen($log_file, "a+");
//        flock($handle, LOCK_EX);
//        fwrite($handle, strftime('%Y-%m-%d %H:%M:%S')." - STOP COMMAND\n");
//        flock($handle, LOCK_UN);
//        fclose($handle);
//
//        $output->writeln("DONE");
//    }
}