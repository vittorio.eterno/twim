<?php

namespace EmailBundle\Command;

use EmailBundle\Entity\Email;
use EmailBundle\Service\EmailService;
use DateTime;
use Exception;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Utils\Service\Encryption;

class EmailQueueScanningCommand extends ContainerAwareCommand {

    /** @var EmailService $emailService */
    private $emailService;

    /** @var Encryption $encryption */
    private $encryption;

    /**
     * EmailQueueScanningCommand constructor.
     * @param EmailService $emailService
     * @param Encryption $encryption
     */
    public function __construct(EmailService $emailService, Encryption $encryption) {
        $this->emailService = $emailService;
        $this->encryption   = $encryption;
        parent::__construct();
    }

    protected function configure() {
        $this
            ->setName('email:queue-scanning')
            ->setDescription('Start to process email queue.')
            ->setHelp('Start to process email queue.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
//        $log_dir = $this->getContainer()->getParameter('log_dir');
//
//        $report_file = $log_dir.'reports/report.log';
//        $log_file = $log_dir.'reports/email-queue_process.log';
//        $log_error = $log_dir.'reports/email-errors.log';


//        $handle = fopen($log_file, "a+");
//        flock($handle, LOCK_EX);
//        fwrite($handle, strftime('%Y-%m-%d %H:%M:%S')." - START COMMAND\n");
//        flock($handle, LOCK_UN);
//        fclose($handle);
//        set_time_limit(0);
        $output->writeln("START");

        try {
            $queue_mail_list = $this->emailService->getNotSent();

            if(is_null($queue_mail_list)) {
//                $handle = fopen($log_file, "a+");
//                flock($handle, LOCK_EX);
//                fwrite($handle, strftime('%Y-%m-%d %H:%M:%S')." - NO MAILS FOUND\n");
//                flock($handle, LOCK_UN);
//                fclose($handle);
                $output->writeln("NO MAILS FOUND");
                return;
            }

            $mail_error_counter = 0;
            $db_error_counter = 0;
            $sent_counter = 0;
            $counter = 0;

            /** @var Email $queueMail */
            foreach ($queue_mail_list as $queueMail) {
                $counter++;
//                $handle = fopen($log_file, "a+");
//                flock($handle, LOCK_EX);
//                fwrite($handle, strftime('%Y-%m-%d %H:%M:%S')." - MAIL IN CHARGE ".$queueMail->getRecipient()." (ID: ".$queueMail->getId().")\n");
//                flock($handle, LOCK_UN);
//                fclose($handle);

                $output->writeln("MAIL IN CHARGE");

                $recipient = $this->encryption->decrypting($queueMail->getRecipient());

                $sent = $this->emailService->processQueueEmail($queueMail->getSubject(), $recipient, $queueMail->getBody());
                $creation = new DateTime();
                $creation->setTimestamp(time());
                if($sent) {
                    $sent_counter++;
//                    $handle = fopen($log_file, "a+");
//                    flock($handle, LOCK_EX);
//                    fwrite($handle, strftime('%Y-%m-%d %H:%M:%S', $creation->getTimestamp())." - SENT MAIL TO ".$queueMail->getRecipient()." (ID: ".$queueMail->getId().")\n");
//                    flock($handle, LOCK_UN);
//                    fclose($handle);
                    $queueMail->setSendAt($creation);
                    $queueMail->setStatus($queueMail::STATUS_SENDED);
                    $output->writeln("SENT");
                } else {
                    $mail_error_counter++;
//                    $handle = fopen($log_file, "a+");
//                    flock($handle, LOCK_EX);
//                    fwrite($handle, strftime('%Y-%m-%d %H:%M:%S')." - SENDING ERROR\n");
//                    flock($handle, LOCK_UN);
//                    fclose($handle);
                    $queueMail->setFailedAt($creation);
                    $queueMail->setStatus($queueMail::STATUS_FAILED);
                    $output->writeln("NOT SENT");
                }

                if(!$this->emailService->save($queueMail)) {
                    $db_error_counter++;
//                    $handle = fopen($log_file, "a+");
//                    flock($handle, LOCK_EX);
//                    fwrite($handle, strftime('%Y-%m-%d %H:%M:%S')." - DB NOT UPDATED CORRECTLY (ID: ".$queueMail->getId().")\n");
//                    flock($handle, LOCK_UN);
//                    fclose($handle);
                    $output->writeln("ERROR");
                }
                sleep(1);
                if($counter >= 25)
                    break;
            }

//            $handle = fopen($report_file, "a+");
//            flock($handle, LOCK_EX);
//            fwrite($handle, strftime('%Y-%m-%d %H:%M:%S')." - Email Queue Process Command\n");
//            fwrite($handle, strftime('%Y-%m-%d %H:%M:%S')." - Number of email to process: ".count($queue_mail_list)."\n");
//            fwrite($handle, strftime('%Y-%m-%d %H:%M:%S')." - Number of email processed: $counter\n");
//            fwrite($handle, strftime('%Y-%m-%d %H:%M:%S')." - Number of email sent: $sent_counter\n");
//            fwrite($handle, strftime('%Y-%m-%d %H:%M:%S')." - Number of sending errors: $mail_error_counter\n");
//            fwrite($handle, strftime('%Y-%m-%d %H:%M:%S')." - Number of database errors: $db_error_counter\n");
//            fwrite($handle, "\n");
//            flock($handle, LOCK_UN);
//            fclose($handle);

//            $handle = fopen($log_file, "a+");
//            flock($handle, LOCK_EX);
//            fwrite($handle, strftime('%Y-%m-%d %H:%M:%S')." - STOP COMMAND\n");
//            flock($handle, LOCK_UN);
//            fclose($handle);
        } catch (Exception $e) {
//            $handle = fopen($report_file, "a+");
//            flock($handle, LOCK_EX);
//            fwrite($handle, strftime('%Y-%m-%d %H:%M:%S')." - EMAIL FATAL ERROR: ".json_encode($e)."\n");
//            flock($handle, LOCK_UN);
//            fclose($handle);
        }

        $output->writeln("DONE");

    }
}