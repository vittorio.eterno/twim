
SET FOREIGN_KEY_CHECKS = 0;
TRUNCATE TABLE `regions`;
SET FOREIGN_KEY_CHECKS = 1;

INSERT INTO `regions` (`id`, `code`, `name`, `hash`) VALUES (1, 'PIE', 'piemonte', 'ea702ba4205cb37a88cc84851690a7a5');
INSERT INTO `regions` (`id`, `code`, `name`, `hash`) VALUES (2, 'VDA', 'valle d\'aosta', '2e84018fc8b4484b94e89aae212fe615');
INSERT INTO `regions` (`id`, `code`, `name`, `hash`) VALUES (3, 'LOM', 'lombardia', '8493a4e8e4662fce2ac8e3b103fcefb0');
INSERT INTO `regions` (`id`, `code`, `name`, `hash`) VALUES (4, 'TAA', 'trentino-alto adige', '02ab934e877888838bb756b18e8f0e4c');
INSERT INTO `regions` (`id`, `code`, `name`, `hash`) VALUES (5, 'VEN', 'veneto', '1c2a56fe5565117328923543da4eb3b9');
INSERT INTO `regions` (`id`, `code`, `name`, `hash`) VALUES (6, 'FVG', 'friuli-venezia giulia', '221a4353f6caf897caa84af5d56682c0');
INSERT INTO `regions` (`id`, `code`, `name`, `hash`) VALUES (7, 'LIG', 'liguria', '583b6abaf6139cbd83855e21b41e8b11');
INSERT INTO `regions` (`id`, `code`, `name`, `hash`) VALUES (8, 'EMR', 'emilia-romagna', '98b84a80080b49716cdf31b29e11dbb4');
INSERT INTO `regions` (`id`, `code`, `name`, `hash`) VALUES (9, 'TOS', 'toscana', '227f82daef18fd97b0ed9538ce418761');
INSERT INTO `regions` (`id`, `code`, `name`, `hash`) VALUES (10, 'UMB', 'umbria', 'fa71b968cd9d68525579f0d496ae4533');
INSERT INTO `regions` (`id`, `code`, `name`, `hash`) VALUES (11, 'MAR', 'marche', '5fa9db2e335ef69a4eeb9fe7974d61f4');
INSERT INTO `regions` (`id`, `code`, `name`, `hash`) VALUES (12, 'LAZ', 'lazio', '156d2223c533866221ac3501ad1b1a37');
INSERT INTO `regions` (`id`, `code`, `name`, `hash`) VALUES (13, 'ABR', 'abruzzo', 'fc1e82b9a18586b50eb40c3867a61bc9');
INSERT INTO `regions` (`id`, `code`, `name`, `hash`) VALUES (14, 'MOL', 'molise', '4fcb130eb3b47f0d3916d1cd92a5b4be');
INSERT INTO `regions` (`id`, `code`, `name`, `hash`) VALUES (15, 'CAM', 'campania', 'ad017e9d6654bd0ccd978958fa1b6da6');
INSERT INTO `regions` (`id`, `code`, `name`, `hash`) VALUES (16, 'PUG', 'puglia', '714ab9fbdad5c5da1b5d34fe1a093b79');
INSERT INTO `regions` (`id`, `code`, `name`, `hash`) VALUES (17, 'BAS', 'basilicata', 'f6e660ced42e946f69a41cc473d923cc');
INSERT INTO `regions` (`id`, `code`, `name`, `hash`) VALUES (18, 'CAL', 'calabria', '912b498a2f2f5913c1d0a8c913ac8b01');
INSERT INTO `regions` (`id`, `code`, `name`, `hash`) VALUES (19, 'SIC', 'sicilia', '82f5c1c9be89c68344d5c6bcf404c804');
INSERT INTO `regions` (`id`, `code`, `name`, `hash`) VALUES (20, 'SAR', 'sardegna', '74101aa14a1d883badc954c4f462e365');