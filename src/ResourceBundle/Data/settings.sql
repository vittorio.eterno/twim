/*
-- Query: SELECT * FROM meedox.settings
LIMIT 0, 1000

-- Date: 2018-08-02 12:12
*/
SET FOREIGN_KEY_CHECKS = 0;
TRUNCATE TABLE `settings`;
SET FOREIGN_KEY_CHECKS = 1;

INSERT INTO `settings` (`id_parent`,`id`,`source`,`name`,`default_value`,`created_by`,`created_on`,`edited_by`,`edited_on`,`disabled_by`,`disabled_on`,`is_disable`) VALUES
(NULL,1,'language','Language',2,1,'2018-07-24 10:08:29',NULL,NULL,NULL,NULL,0),
(1,2,'language.italiano','Italiano',1,1,'2018-07-24 10:08:29',NULL,NULL,NULL,NULL,0),
(1,3,'language.english','English',0,1,'2018-07-24 10:08:29',NULL,NULL,NULL,NULL,0),
(1,4,'language.french','French',0,1,'2018-07-24 10:08:29',NULL,NULL,NULL,NULL,0),
(NULL,5,'notifications','Notifications',2,1,'2018-07-24 10:08:29',NULL,NULL,NULL,NULL,0),
(5,6,'notify.message','Receive a notification every time you receive a message from a user',1,1,'2018-07-24 10:08:29',NULL,NULL,NULL,NULL,0),
(5,7,'notify.like.comment','Receive a notification every time a user puts a like on your comment',1,1,'2018-07-24 10:08:29',NULL,NULL,NULL,NULL,0),
(5,8,'notify.answer.comment','Get notified when someone answers your comment',1,1,'2018-07-24 10:08:29',NULL,NULL,NULL,NULL,0),
(5,9,'meedox.update','Get updates on meedox',1,1,'2018-07-24 10:08:29',NULL,NULL,NULL,NULL,0),
(5,10,'email.update','Receive email updates',1,1,'2018-07-24 10:08:29',NULL,NULL,NULL,NULL,0),
(5,11,'app.notification','Activate notifications on the app',1,1,'2018-07-24 10:08:29',NULL,NULL,NULL,NULL,0);
