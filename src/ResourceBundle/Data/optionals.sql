/*
-- Query: SELECT * FROM twim.optionals
LIMIT 0, 1000

-- Date: 2018-11-09 16:02
*/
SET FOREIGN_KEY_CHECKS = 0;
TRUNCATE TABLE `optionals`;
SET FOREIGN_KEY_CHECKS = 1;

INSERT INTO `optionals` (`id`,`source`,`description`,`value`,`price`,`type`,`created_at`,`is_disabled`,`optional_structure_id`) VALUES
(1,'size.medium.label','medium size','20px',20,0,'2018-11-01 08:00:00',0,1),
(2,'size.big.label','big size','30px',30,0,'2018-11-01 08:00:00',0,1),
(3,'size.super.label','super size','40px',40,0,'2018-11-01 08:00:00',0,1),
(4,'color.red.label','red color','red',50,0,'2018-11-01 08:00:00',0,2),
(5,'color.yellow.label','yellow color','yellow',70,0,'2018-11-01 08:00:00',0,2),
(6,'a4.label','paper format A4','a4',12,1,'2018-11-01 08:00:00',0,3),
(7,'basic.black.label','frame basic black','frame',12,1,'2018-11-01 08:00:00',0,4)
;
