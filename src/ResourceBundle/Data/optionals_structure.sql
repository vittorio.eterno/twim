/*
-- Query: SELECT * FROM twim.optionals_structure
LIMIT 0, 1000

-- Date: 2018-11-09 16:03
*/
SET FOREIGN_KEY_CHECKS = 0;
TRUNCATE TABLE `optionals_structure`;
SET FOREIGN_KEY_CHECKS = 1;

INSERT INTO `optionals_structure` (`id`,`source`,`created_at`,`disabled_at`,`disabled_by`,`is_disabled`) VALUES
(1,'sizelabel','2018-11-01 09:00:00',NULL,NULL,0),
(2,'colorlabel','2018-11-01 09:00:00',NULL,NULL,0),
(3,'printformatlabel','2018-11-01 09:00:00',NULL,NULL,0),
(4,'framelabel','2018-11-01 09:00:00',NULL,NULL,0)
;
