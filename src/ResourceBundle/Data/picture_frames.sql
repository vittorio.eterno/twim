/*
-- Query: SELECT * FROM twim.optionals_structure
LIMIT 0, 1000

-- Date: 2018-11-09 16:03
*/
SET FOREIGN_KEY_CHECKS = 0;
TRUNCATE TABLE `picture_frames`;
SET FOREIGN_KEY_CHECKS = 1;


INSERT INTO `picture_frames` (`id`,`source`,`name`,`created_at`,`disabled_at`,`disabled_by`,`is_disabled`,`hash`,`type`,`related_pictures`,`extension`,`is_default`) VALUES
(1,'default.frame.label','Default','2018-12-12 12:12:12',NULL,NULL,0,'e90c0cbba7a963349546c1be9da9e87e',1,'[\"e90c0cbba7a963349546c1be9da9e87e.jpg\",\"e90c0cbba7a963349546c1be9da9e87e.jpg\",\"e90c0cbba7a963349546c1be9da9e87e.jpg\"]','jpg',1)
;
