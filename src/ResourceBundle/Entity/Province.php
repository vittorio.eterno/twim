<?php

namespace ResourceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Schema\Entity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Province
 *
 * @ORM\Table("`provinces`")
 * @ORM\Entity(repositoryClass="ResourceBundle\Repository\ProvinceRepository")
 * @UniqueEntity(
 *     fields="hash",
 *     message="exist.province"
 * )
 */
class Province extends Entity
{

    /**
     * @var UniqueEntity $p1
     * @var ORM\ $p2
     * @var JMS\ $p3
     * @var Assert\ $p4
     */

    /**
     * @ORM\Column(name="`id`", type="integer", options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer $id
     */
    private $id;

    /**
     * @ORM\Column(name="`source`", type="string", length=255, nullable=true)
     */
    private $source = null;

    /**
     * @ORM\Column(name="`code`", type="string", length=2, nullable=false)
     * @Assert\NotBlank(message = "require.province.code")
     * @Assert\Regex(
     *     pattern="/^[A-Z]*$/",
     *     message="invalid.province.code"
     * )
     * @Assert\Length(
     *      min = 2,
     *      max = 2,
     *      minMessage = "min.length.province.code",
     *      maxMessage = "max.length.province.code"
     * )
     */
    private $code = null;

    /**
     * @ORM\Column(name="`name`", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "require.province")
     * @Assert\Regex(
     *     pattern="/^[^<>]*$/",
     *     message="invalid.province"
     * )
     * @Assert\Length(
     *      min = 2,
     *      max = 255,
     *      minMessage = "min.length.province",
     *      maxMessage = "max.length.province"
     * )
     */
    private $name = null;

    /**
     * @ORM\ManyToOne(targetEntity="Region", inversedBy="provinces")
     * @ORM\JoinColumn(name="id_region", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank(message = "require.region.reference")
     * @var Region $region
     */
    private $region;

    /**
     * @ORM\Column(name="`hash`", type="string", length=40, nullable=false, unique=true)
     */
    private $hash = null;

    /**
     * @ORM\OneToMany(targetEntity="City", mappedBy="province")
     */
    private $cities;

//    /**
//     * @ORM\ManyToOne(targetEntity="Country", inversedBy="provinces")
//     * @ORM\JoinColumn(name="id_country", referencedColumnName="id", nullable=false)
//     * @Assert\NotBlank(message = "require.country.reference")
//     * @var Country $country
//     */
//    private $country;


    ################################################# SERIALIZER FUNCTIONS

    /**
     * Returns the array of fields to serialize in entity administration view.
     * @return array
     */
    public function adminSerializer()
    {
        $view_vars = $this->viewSerializer();

        $admin_vars = array();

        return array_merge($view_vars, $admin_vars);
    }

    /**
     * Returns the array of fields to serialize in entity view.
     * @return array
     */
    public function viewSerializer()
    {
        $list_vars = $this->listSerializer();

        $view_vars = array(
        );

        return array_merge($list_vars, $view_vars);
    }

    /**
     * Returns the array of fields to serialize in a list of this entity.
     * @return array
     */
    public function listSerializer()
    {
        $list_vars = array(
            'id'            => $this->serializedId(),
            'code'          => $this->serializedCode(),
            'source'        => $this->serializedSource(),
        );
        return $list_vars;
    }

    /**
     * Returns the hash code unique identifier of the entity.
     * @return string
     */
    public function hashCode()
    {
        // TODO: Implement hashCode() method.
    }

    ################################################# SERIALIZED FUNCTIONS

    /**
     * id
     * @JMS\VirtualProperty
     * @JMS\SerializedName("id")
     * @JMS\Type("integer")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedId() {
        return (is_null($this->id)?null:$this->id);
    }

    /**
     * source
     * @JMS\VirtualProperty
     * @JMS\SerializedName("source")
     * @JMS\Type("string")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedSource() {
        return (is_null($this->source)?null:$this->source);
    }

    /**
     * code
     * @JMS\VirtualProperty
     * @JMS\SerializedName("code")
     * @JMS\Type("string")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedCode() {
        return (is_null($this->code)?null:$this->code);
    }

    /**
     * countryCode
     * @JMS\VirtualProperty
     * @JMS\SerializedName("countryCode")
     * @JMS\Type("string")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedCountryCode() {
        return (!is_null($this->country) && $this->country instanceof Country ? $this->country->serializedSource():null);
    }

    ################################################# GETTERS AND SETTERS

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Set source.
     *
     * @param string $source
     *
     * @return Province
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source.
     *
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Region
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param Region $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

//    /**
//     * @return Country
//     */
//    public function getCountry()
//    {
//        return $this->country;
//    }
//
//    /**
//     * @param Country $country
//     */
//    public function setCountry($country)
//    {
//        $this->country = $country;
//    }

    /**
     * @return mixed
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param mixed $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    /**
     * @return mixed
     */
    public function getCities()
    {
        return $this->cities;
    }

    /**
     * @param mixed $cities
     */
    public function setCities($cities)
    {
        $this->cities = $cities;
    }

}
