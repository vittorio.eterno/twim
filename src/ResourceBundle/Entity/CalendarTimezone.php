<?php

namespace ResourceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Schema\Entity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Country
 *
 * @ORM\Table("`calendar_timezones`")
 * @ORM\Entity(repositoryClass="ResourceBundle\Repository\CalendarTimezoneRepository")
 */
class CalendarTimezone extends Entity
{

    /**
     * @var UniqueEntity $p1
     * @var ORM\ $p2
     * @var JMS\ $p3
     * @var Assert\ $p4
     */

    /**
     * @ORM\Column(name="`id`", type="integer", options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer $id
     */
    private $id;

    /**
     * @ORM\Column(name="`country_code`", type="string", length=2, nullable=false)
     */
    private $countryCode;

    /**
     * @ORM\Column(name="`coordinates`", type="string", length=15, nullable=false)
     */
    private $coordinates;

    /**
     * @ORM\Column(name="`timezone`", type="string", length=32, nullable=false)
     */
    private $timezone;

    /**
     * @ORM\Column(name="`comments`", type="string", length=85, nullable=false)
     */
    private $comments;

    /**
     * @ORM\Column(name="`notes`", type="string", length=79, nullable=false)
     */
    private $notes;

    /**
     * @ORM\Column(name="`UTC_offset`", type="string", length=85, nullable=false)
     */
    private $UTCOffset;

    /**
     * @ORM\Column(name="`UTC_DST_offset`", type="string", length=79, nullable=true)
     */
    private $UTCDSTOffset;


    ################################################# SERIALIZER FUNCTIONS

    /**
     * Returns the array of fields to serialize in entity administration view.
     * @return array
     */
    public function adminSerializer()
    {
        $view_vars = $this->viewSerializer();

        $admin_vars = array();

        return array_merge($view_vars, $admin_vars);
    }

    /**
     * Returns the array of fields to serialize in entity view.
     * @return array
     */
    public function viewSerializer()
    {
        $list_vars = $this->listSerializer();

        $view_vars = array(
        );

        return array_merge($list_vars, $view_vars);
    }

    /**
     * Returns the array of fields to serialize in a list of this entity.
     * @return array
     */
    public function listSerializer()
    {
        $list_vars = array(
            'id'                => $this->serializedId(),
            'countryCode'       => $this->serializedCountryCode(),
            'coordinates '      => $this->serializedCoordinates(),
            'timezone'          => $this->serializedTimezone(),
            'UTCOffset'         => $this->serializedUTCOffset(),
        );
        return $list_vars;
    }

    /**
     * Returns the hash code unique identifier of the entity.
     * @return string
     */
    public function hashCode()
    {
        // TODO: Implement hashCode() method.
    }

    ################################################# SERIALIZED FUNCTIONS

    /**
     * CalendarTimezone id
     * @JMS\VirtualProperty
     * @JMS\SerializedName("id")
     * @JMS\Type("string")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedId() {
        return (is_null($this->id)?null:$this->id);
    }

    /**
     * CalendarTimezone countryCode
     * @JMS\VirtualProperty
     * @JMS\SerializedName("countryCode")
     * @JMS\Type("string")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedCountryCode() {
        return (is_null($this->countryCode)?null:$this->countryCode);
    }

    /**
     * CalendarTimezone coordinates
     * @JMS\VirtualProperty
     * @JMS\SerializedName("coordinates")
     * @JMS\Type("string")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedCoordinates() {
        return (is_null($this->coordinates)?null:$this->coordinates);
    }

    /**
     * CalendarTimezone timezone
     * @JMS\VirtualProperty
     * @JMS\SerializedName("timezone")
     * @JMS\Type("string")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedTimezone() {
        return (is_null($this->timezone)?null:$this->timezone);
    }

    /**
     * CalendarTimezone UTCOffset
     * @JMS\VirtualProperty
     * @JMS\SerializedName("UTCOffset")
     * @JMS\Type("string")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedUTCOffset() {
        return (is_null($this->UTCOffset)?null:$this->UTCOffset);
    }

    ################################################# GETTERS AND SETTERS FUNCTIONS

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set countryCode.
     *
     * @param string $countryCode
     *
     * @return CalendarTimezone
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;

        return $this;
    }

    /**
     * Get countryCode.
     *
     * @return string
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * Set coordinates.
     *
     * @param string $coordinates
     *
     * @return CalendarTimezone
     */
    public function setCoordinates($coordinates)
    {
        $this->coordinates = $coordinates;

        return $this;
    }

    /**
     * Get coordinates.
     *
     * @return string
     */
    public function getCoordinates()
    {
        return $this->coordinates;
    }

    /**
     * Set timezone.
     *
     * @param string $timezone
     *
     * @return CalendarTimezone
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;

        return $this;
    }

    /**
     * Get timezone.
     *
     * @return string
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * Set comments.
     *
     * @param string $comments
     *
     * @return CalendarTimezone
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments.
     *
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set notes.
     *
     * @param string $notes
     *
     * @return CalendarTimezone
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes.
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set uTCOffset.
     *
     * @param string $uTCOffset
     *
     * @return CalendarTimezone
     */
    public function setUTCOffset($uTCOffset)
    {
        $this->UTCOffset = $uTCOffset;

        return $this;
    }

    /**
     * Get uTCOffset.
     *
     * @return string
     */
    public function getUTCOffset()
    {
        return $this->UTCOffset;
    }

    /**
     * Set uTCDSTOffset.
     *
     * @param string|null $uTCDSTOffset
     *
     * @return CalendarTimezone
     */
    public function setUTCDSTOffset($uTCDSTOffset = null)
    {
        $this->UTCDSTOffset = $uTCDSTOffset;

        return $this;
    }

    /**
     * Get uTCDSTOffset.
     *
     * @return string|null
     */
    public function getUTCDSTOffset()
    {
        return $this->UTCDSTOffset;
    }
}
