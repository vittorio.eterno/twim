<?php

namespace ResourceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Schema\Entity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Country
 *
 * @ORM\Table("`countries`")
 * @ORM\Entity(repositoryClass="ResourceBundle\Repository\CountryRepository")
 * @UniqueEntity(
 *     fields="hash",
 *     message="exist.country"
 * )
 */
class Country extends Entity
{

    /**
     * @var UniqueEntity $p1
     * @var ORM\ $p2
     * @var JMS\ $p3
     * @var Assert\ $p4
     */

    /**
     * @ORM\Column(name="`id`", type="integer", options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer $id
     */
    private $id;

    /**
    * @ORM\Column(name="`source`", type="string", length=255, nullable=true)
    */
    private $source = null;

    /**
     * @ORM\Column(name="`name`", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "require.country")
     * @Assert\Regex(
     *     pattern="/^[^<>]*$/",
     *     message="invalid.country"
     * )
     * @Assert\Length(
     *      min = 2,
     *      max = 255,
     *      minMessage = "min.length.country",
     *      maxMessage = "max.length.country"
     * )
     */
    private $name = null;

    /**
     * @ORM\Column(name="`code`", type="string", length=10, nullable=false)
     * @Assert\NotBlank(message = "require.country.code")
     * @Assert\Regex(
     *     pattern="/^[A-Z0-9]*$/",
     *     message="invalid.country.code"
     * )
     * @Assert\Length(
     *      min = 4,
     *      max = 10,
     *      minMessage = "min.length.country.code",
     *      maxMessage = "max.length.country.code"
     * )
     */
    private $code = null;

    /**
     * @ORM\Column(name="`continent`", type="string", length=100, nullable=true)
     * @Assert\NotBlank(message = "require.continent")
     * @Assert\Regex(
     *     pattern="/^[a-zA-Z]+[a-zA-Z.,\-' òàùèéì]*$/",
     *     message="invalid.continent"
     * )
     * @Assert\Length(
     *      min = 2,
     *      max = 100,
     *      minMessage = "min.length.continent",
     *      maxMessage = "max.length.continent"
     * )
     */
    private $continent = null;

    /**
     * @ORM\Column(name="`hash`", type="string", length=40, nullable=false, unique=true)
     */
    private $hash = null;

    /**
     * @var float|null
     *
     * @ORM\Column(name="delivery_charges", type="float", nullable=true, options={"default":0})
     */
    private $deliveryCharges;

//    /**
//     * @ORM\OneToMany(targetEntity="City", mappedBy="country")
//     */
//    private $cities;
//
//    /**
//     * @ORM\OneToMany(targetEntity="Province", mappedBy="country")
//     */
//    private $provinces;


    ################################################# SERIALIZER FUNCTIONS

    /**
     * Returns the array of fields to serialize in entity administration view.
     * @return array
     */
    public function adminSerializer()
    {
        $view_vars = $this->viewSerializer();

        $admin_vars = array();

        return array_merge($view_vars, $admin_vars);
    }

    /**
     * Returns the array of fields to serialize in entity view.
     * @return array
     */
    public function viewSerializer()
    {
        $list_vars = $this->listSerializer();

        $view_vars = array(
        );

        return array_merge($list_vars, $view_vars);
    }

    /**
     * Returns the array of fields to serialize in a list of this entity.
     * @return array
     */
    public function listSerializer()
    {
        $list_vars = array(
            'id'                => $this->serializedId(),
            'code'              => $this->serializedCode(),
            'source'            => $this->serializedSource(),
            'deliveryCharges'   => $this->serializedDeliveryCharges()
        );
        return $list_vars;
    }

    /**
     * Returns the hash code unique identifier of the entity.
     * @return string
     */
    public function hashCode()
    {
        // TODO: Implement hashCode() method.
    }

    ################################################# SERIALIZED FUNCTIONS

    /**
     * id
     * @JMS\VirtualProperty
     * @JMS\SerializedName("id")
     * @JMS\Type("integer")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedId() {
        return (is_null($this->id)?null:$this->id);
    }

    /**
     * source
     * @JMS\VirtualProperty
     * @JMS\SerializedName("source")
     * @JMS\Type("string")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedSource() {
        return (is_null($this->source)?null:$this->source);
    }

    /**
     * code
     * @JMS\VirtualProperty
     * @JMS\SerializedName("code")
     * @JMS\Type("string")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedCode() {
        return (is_null($this->code)?null:$this->code);
    }

    /**
     * deliveryCharges
     * @JMS\VirtualProperty
     * @JMS\SerializedName("deliveryCharges")
     * @JMS\Type("string")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedDeliveryCharges() {
        return (is_null($this->deliveryCharges)?null:$this->deliveryCharges);
    }

    ################################################# GETTERS AND SETTERS

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Set source.
     *
     * @param string $source
     *
     * @return Country
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source.
     *
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getContinent()
    {
        return $this->continent;
    }

    /**
     * @param mixed $continent
     */
    public function setContinent($continent)
    {
        $this->continent = $continent;
    }

    /**
     * @return mixed
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param mixed $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    /**
     * @return float|null
     */
    public function getDeliveryCharges(): ?float
    {
        return $this->deliveryCharges;
    }

    /**
     * @param float|null $deliveryCharges
     */
    public function setDeliveryCharges(?float $deliveryCharges): void
    {
        $this->deliveryCharges = $deliveryCharges;
    }

//    /**
//     * @return mixed
//     */
//    public function getCities()
//    {
//        return $this->cities;
//    }
//
//    /**
//     * @param mixed $cities
//     */
//    public function setCities($cities)
//    {
//        $this->cities = $cities;
//    }
//
//    /**
//     * @return mixed
//     */
//    public function getProvinces()
//    {
//        return $this->provinces;
//    }
//
//    /**
//     * @param mixed $provinces
//     */
//    public function setProvinces($provinces)
//    {
//        $this->provinces = $provinces;
//    }

}
