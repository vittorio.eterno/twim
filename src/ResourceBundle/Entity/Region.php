<?php

namespace ResourceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Schema\Entity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Region
 *
 * @ORM\Table("`regions`")
 * @ORM\Entity(repositoryClass="ResourceBundle\Repository\RegionRepository")
 * @UniqueEntity(
 *     fields="hash",
 *     message="exist.region"
 * )
 */
class Region extends Entity {

    /**
     * @var UniqueEntity $p1
     * @var ORM\ $p2
     * @var JMS\ $p3
     * @var Assert\ $p4
     */

    /**
     * @ORM\Column(name="`id`", type="integer", options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer $id
     */
    private $id;

    /**
     * @ORM\Column(name="`code`", type="string", length=3, nullable=false)
     * @Assert\NotBlank(message = "require.region.code")
     * @Assert\Regex(
     *     pattern="/^[A-Z]*$/",
     *     message="invalid.region.code"
     * )
     * @Assert\Length(
     *      min = 3,
     *      max = 3,
     *      minMessage = "min.length.region.code",
     *      maxMessage = "max.length.region.code"
     * )
     */
    private $code = null;

    /**
     * @ORM\Column(name="`source`", type="string", length=255, nullable=true)
     */
    private $source = null;

    /**
     * @ORM\Column(name="`name`", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "require.region")
     * @Assert\Regex(
     *     pattern="/^[^<>]*$/",
     *     message="invalid.region"
     * )
     * @Assert\Length(
     *      min = 2,
     *      max = 255,
     *      minMessage = "min.length.region",
     *      maxMessage = "max.length.region"
     * )
     */
    private $name = null;

    /**
     * @ORM\Column(name="`hash`", type="string", length=40, nullable=false, unique=true)
     */
    private $hash = null;

    /**
     * @ORM\OneToMany(targetEntity="Province", mappedBy="region")
     */
    private $provinces;

    /**
     * @ORM\OneToMany(targetEntity="City", mappedBy="region")
     */
    private $cities;

    ################################################# SERIALIZER FUNCTIONS

    /**
     * Returns the array of fields to serialize in entity administration view.
     * @return array
     */
    public function adminSerializer()
    {
        $view_vars = $this->viewSerializer();

        $admin_vars = array();

        return array_merge($view_vars, $admin_vars);
    }

    /**
     * Returns the array of fields to serialize in entity view.
     * @return array
     */
    public function viewSerializer()
    {
        $list_vars = $this->listSerializer();

        $view_vars = array(
        );

        return array_merge($list_vars, $view_vars);
    }

    /**
     * Returns the array of fields to serialize in a list of this entity.
     * @return array
     */
    public function listSerializer()
    {
        $list_vars = array(
        );
        return $list_vars;
    }

    /**
     * Returns the hash code unique identifier of the entity.
     * @return string
     */
    public function hashCode()
    {
        // TODO: Implement hashCode() method.
    }

    ################################################# SERIALIZED FUNCTIONS

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * Set source.
     *
     * @param string $source
     *
     * @return Region
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source.
     *
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param mixed $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    /**
     * @return mixed
     */
    public function getProvinces()
    {
        return $this->provinces;
    }

    /**
     * @param mixed $provinces
     */
    public function setProvinces($provinces)
    {
        $this->provinces = $provinces;
    }

    /**
     * @return mixed
     */
    public function getCities()
    {
        return $this->cities;
    }

    /**
     * @param mixed $cities
     */
    public function setCities($cities)
    {
        $this->cities = $cities;
    }

}
