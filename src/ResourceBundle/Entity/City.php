<?php

namespace ResourceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Schema\Entity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * City
 *
 * @ORM\Table("`cities`")
 * @ORM\Entity(repositoryClass="ResourceBundle\Repository\CityRepository")
 * @UniqueEntity(
 *     fields="hash",
 *     message="exist.city"
 * )
 */
class City extends Entity
{

    /**
     * @var UniqueEntity $p1
     * @var ORM\ $p2
     * @var JMS\ $p3
     * @var Assert\ $p4
     */

    /**
     * @ORM\Column(name="`id`", type="bigint", options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer $id
     */
    private $id;

    /**
     * @ORM\Column(name="`source`", type="string", length=255, nullable=true)
     */
    private $source = null;

    /**
     * @ORM\Column(name="`name`", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "require.city")
     * @Assert\Regex(
     *     pattern="/^[^<>]*$/",
     *     message="invalid.city"
     * )
     * @Assert\Length(
     *      min = 2,
     *      max = 255,
     *      minMessage = "min.length.city",
     *      maxMessage = "max.length.city"
     * )
     */
    private $name = null;

    /**
     * @ORM\Column(name="`code`", type="string", length=10, nullable=false)
     * @Assert\NotBlank(message = "require.city.code")
     * @Assert\Regex(
     *     pattern="/^[A-Z0-9]*$/",
     *     message="invalid.city.code"
     * )
     * @Assert\Length(
     *      min = 2,
     *      max = 10,
     *      minMessage = "min.length.city.code",
     *      maxMessage = "max.length.city.code"
     * )
     */
    private $code = null;

    /**
     * @ORM\ManyToOne(targetEntity="Province", inversedBy="cities")
     * @ORM\JoinColumn(name="id_province", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank(message = "require.province.reference")
     * @var Province $province
     */
    private $province;

    /**
     * @ORM\ManyToOne(targetEntity="Region", inversedBy="cities")
     * @ORM\JoinColumn(name="id_region", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank(message = "require.region.reference")
     * @var Region $region
     */
    private $region;

//    /**
//     * @ORM\ManyToOne(targetEntity="Country", inversedBy="cities")
//     * @ORM\JoinColumn(name="id_country", referencedColumnName="id", nullable=false)
//     * @Assert\NotBlank(message = "require.country.reference")
//     * @var Country $country
//     */
//    private $country;

    /**
     * @ORM\Column(name="`hash`", type="string", length=40, nullable=false, unique=true)
     */
    private $hash = null;


    ################################################# SERIALIZER FUNCTIONS

    /**
     * Returns the array of fields to serialize in entity administration view.
     * @return array
     */
    public function adminSerializer()
    {
        $view_vars = $this->viewSerializer();

        $admin_vars = array();

        return array_merge($view_vars, $admin_vars);
    }

    /**
     * Returns the array of fields to serialize in entity view.
     * @return array
     */
    public function viewSerializer()
    {
        $list_vars = $this->listSerializer();

        $view_vars = array(
        );

        return array_merge($list_vars, $view_vars);
    }

    /**
     * Returns the array of fields to serialize in a list of this entity.
     * @return array
     */
    public function listSerializer()
    {
        $list_vars = array(
        );
        return $list_vars;
    }

    /**
     * Returns the hash code unique identifier of the entity.
     * @return string
     */
    public function hashCode()
    {
        // TODO: Implement hashCode() method.
    }

    ################################################# SERIALIZED FUNCTIONS


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Set source.
     *
     * @param string $source
     *
     * @return City
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source.
     *
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return Province
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * @param Province $province
     */
    public function setProvince($province)
    {
        $this->province = $province;
    }

    /**
     * @return Region
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param Region $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

//    /**
//     * @return Country
//     */
//    public function getCountry()
//    {
//        return $this->country;
//    }
//
//    /**
//     * @param Country $country
//     */
//    public function setCountry($country)
//    {
//        $this->country = $country;
//    }

    /**
     * @return mixed
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param mixed $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

}
