<?php

namespace ResourceBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use ResourceBundle\Entity\Country;
use ResourceBundle\Service\CountryService;
use Symfony\Component\HttpFoundation\JsonResponse;
use TranslationBundle\Service\TranslationService;
use Utils\Service\ResponseUtils;

class CountryController extends Controller {

    /**
     * Get list of countries.
     *
     * @Route("/public/country/list/")
     *
     */
    public function listAction(CountryService $countryService, TranslationService $translationService) {
        $response = new ResponseUtils($this->get("translator"));

        $countries = $countryService->getListByAttributes();

        $sources = array();
        /** @var Country $country */
        foreach($countries as $country) {
            $sources[] = $country->getSource();
        }

        $translations = array();
        if ($translationService->isToBeTranslated()) {
            $translations = $translationService->getBySources($sources);
        }

        $responseList = $countryService->getResponseList($countries, $translations);

        return $response->getListResponse($responseList, 'success');
    }
}
