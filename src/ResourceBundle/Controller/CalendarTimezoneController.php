<?php

namespace ResourceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use ResourceBundle\Service\CalendarTimezoneService;
use Symfony\Component\HttpFoundation\Request;
use ResourceBundle\Entity\CalendarTimezone;
use Utils\Service\ResponseUtils;
use Utils\StaticUtil\IntegerUtils;
use Utils\StaticUtil\StringUtils;

class CalendarTimezoneController extends Controller {

    /**
     * Get a list of all calendar timezones.
     *
     * @Route("/public/calendar-timezone/list/")
     * @Route("/public/calendar-timezone/list/{suggest}")
     *
     */
    public function listAction($suggest = '', Request $request, CalendarTimezoneService $calendarTimezoneService) {
        $response = new ResponseUtils($this->get("translator"));

        $numElement = $request->headers->get("TOTAL_ITEMS",10);
        if(!IntegerUtils::checkNum($numElement)){
            $numElement = 10;
        }

        $offset = 0;
        $currentPage = $request->headers->get("CURRENT_PAGE",0);
        if(!IntegerUtils::checkNum($currentPage)){
            $currentPage = 0;
        }
        else {
            if($currentPage > 1 ){
                $offset = ($currentPage - 1) * $numElement;
            }
        }

        $suggest = urldecode($suggest);

        if($suggest != '') {
            if(!StringUtils::checkSuggestString($suggest)) {
                return $response->getResponse(array(), "parameters.invalid",400);
            }

            $calendarTimezones = $calendarTimezoneService->getListBySuggest(null, array('timezone' => $suggest), $currentPage, $numElement, $offset);
        }
        else {
            $calendarTimezones = $calendarTimezoneService->getList(null, array(), $currentPage, $numElement, $offset);
        }

        return $response->getListResponse($calendarTimezones, 'success');
    }

}