<?php

namespace ResourceBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use ResourceBundle\Entity\Country;
use ResourceBundle\Entity\Province;
use ResourceBundle\Service\CountryService;
use ResourceBundle\Service\ProvinceService;
use Symfony\Component\HttpFoundation\JsonResponse;
use TranslationBundle\Service\TranslationService;
use Utils\Service\ResponseUtils;

class ProvinceController extends Controller {

    /**
     * Get list of provinces given source country.
     *
     * @Route("/public/province/list/{source_country}")
     *
     */
    public function listAction($source_country, CountryService $countryService, ProvinceService $provinceService, TranslationService $translationService) {
        $response = new ResponseUtils($this->get("translator"));

        /** @var Country $country */
        $country = $countryService->getOneByAttributes(array('source' => $source_country));
        if(is_null($country)) {
            return $response->getResponse(array(), "data.not.found.404",404);
        }

        $provinces = $provinceService->getListByAttributes(array('country' => $country));

        $sources = array();
        /** @var Province $province */
        foreach($provinces as $province) {
            $sources[] = $province->getSource();
        }

        $translations = array();
        if ($translationService->isToBeTranslated()) {
            $translations = $translationService->getBySources($sources);
        }

        $responseList = $provinceService->getResponseList($provinces, $translations);

        return $response->getListResponse($responseList, 'success');
    }
}
