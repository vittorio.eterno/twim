<?php

namespace ResourceBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use ResourceBundle\Entity\City;
use ResourceBundle\Entity\Province;
use ResourceBundle\Service\CityService;
use ResourceBundle\Service\CountryService;
use ResourceBundle\Service\ProvinceService;
use Symfony\Component\HttpFoundation\JsonResponse;
use TranslationBundle\Service\TranslationService;
use Utils\Service\ResponseUtils;

class CityController extends Controller {

    /**
     * Get list of cities given source province.
     *
     * @Route("/public/city/list/{source_province}")
     *
     */
    public function listAction($source_province, ProvinceService $provinceService, CityService $cityService, TranslationService $translationService) {
        $response = new ResponseUtils($this->get("translator"));

        /** @var Province $province */
        $province = $provinceService->getOneByAttributes(array('source' => $source_province));
        if(is_null($province)) {
            return $response->getResponse(array(), "data.not.found.404",404);
        }

        $cities = $cityService->getListByAttributes(array('province' => $province));

        $sources = array();
        /** @var City $city */
        foreach($cities as $city) {
            $sources[] = $city->getSource();
        }

        $translations = array();
        if ($translationService->isToBeTranslated()) {
            $translations = $translationService->getBySources($sources);
        }

        $responseList = $cityService->getResponseList($cities, $translations);

        return $response->getListResponse($responseList, 'success');
    }
}
