<?php

namespace ResourceBundle\Service;


use Psr\Log\LoggerInterface;
use ResourceBundle\Entity\Country;
use ResourceBundle\Entity\Province;
use ResourceBundle\Repository\ProvinceRepository;
use Schema\EntityService;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Utils\Service\CacheService;

class ProvinceService extends EntityService {

    /** @var CacheService $cacheService */
    private $cacheService;

    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * ProvinceService constructor.
     * @param ProvinceRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param LoggerInterface $logger
     * @param CacheService $cacheService
     */
    public function __construct(ProvinceRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                LoggerInterface $logger,
                                CacheService $cacheService
    ) {

        $this->repository = $repository;
        parent::__construct($repository, $authorizationChecker, $logger);

        $this->cacheService = $cacheService;
    }


    /**
     * @return array|mixed
     */
    public function getAllProvinces () {
        return $this->repository->findAllProvinces();
    }

    /**
     * @return array|mixed|null
     */
    public function getCachedProvinces () {

        $formatted = $this->cacheService->get("list_public_formatted_provinces");
        if (is_null($formatted)) {
            $formatted  = $this->getFormattedProvinces();

            // Expire after 1 year
            $expire = 60*60*24*365;
            $this->cacheService->set("list_public_formatted_provinces", $formatted, $expire);
        }

        return $formatted;
    }

    /**
     * @return array
     */
    private function getFormattedProvinces () {
        $formatted = array();
        $provinces = $this->getAllProvinces();
        if (!empty($provinces) && count($provinces)>0) {
            /** @var Province $province */
            foreach ($provinces as $province) {
                $formatted[$province->serializedCountryCode()] = $province->listSerializer();
            }
        }
        return $formatted;
    }


    /**
     * @param Country $country
     * @return mixed|null
     */
    public function getProvincesByCountry (Country &$country) {
        return $this->repository->findProvincesByCountry($country);
    }

    /**
     * @param Country $country
     * @return array|mixed|null
     */
    public function getCachedProvincesByCountry (Country &$country) {

        $formatted = $this->cacheService->get("list_public_formatted_provinces_by_country");
        if (is_null($formatted)) {
            $formatted  = $this->getFormattedProvincesByCountry($country);

            // Expire after 1 year
            $expire = 60*60*24*365;
            $this->cacheService->set("list_public_formatted_provinces_by_country", $formatted, $expire);
        }

        return $formatted;
    }

    /**
     * @param Country $country
     * @return array
     */
    private function getFormattedProvincesByCountry (Country &$country) {
        $formatted = array();
        $provinces = $this->getProvincesByCountry($country);
        if (!empty($provinces) && count($provinces)>0) {
            /** @var Province $province */
            foreach ($provinces as $province) {
                $formatted[$country->getCode()] = $province->listSerializer();
            }
        }
        return $formatted;
    }


    /**
     * @param array $provinces
     * @param array $translations
     * @return mixed
     */
    public function getResponseList($provinces, $translations = array()) {
        $results = array();
        $page = 0;
        $i = 0;
        /** @var Province $province */
        foreach($provinces as $province) {
            $results[$i] = array(
                'id' => $province->getId(),
                'source' => $province->getSource()
            );

            if(count($translations) == 0) {
                $results[$i]['name'] = $province->getName();
            }
            else {
                foreach ($translations as $translation_source => $translation_value) {
                    if ($results[$i]['source'] == $translation_source) {
                        $results[$i]['name'] = $translation_value;
                    }
                }
            }
            $i++;
        }

        $response['list'] = array();

        if(!empty($results)){
            $response['list'] = array_values($results);
        }

        $response['total_items'] = count($results);

        $pagination = $this->pagination;
        if($page == 0) {
            $pagination = count($results);
            $response['current_page'] = 1;
            $response['total_pages'] = 1;
            return $response;
        }

        $response['current_page'] = $page;
        $response['total_pages'] = ceil($response['records'] / $pagination);

        return $response;
    }
}