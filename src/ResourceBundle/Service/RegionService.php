<?php

namespace ResourceBundle\Service;


use Psr\Log\LoggerInterface;
use ResourceBundle\Repository\RegionRepository;
use Schema\EntityService;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class RegionService extends EntityService {

    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * RegionService constructor.
     * @param RegionRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param LoggerInterface $logger
     */
    public function __construct(RegionRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                LoggerInterface $logger
    ) {

        $this->repository = $repository;
        parent::__construct($repository, $authorizationChecker, $logger);
    }

}