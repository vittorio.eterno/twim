<?php

namespace ResourceBundle\Service;


use Psr\Log\LoggerInterface;
use ResourceBundle\Entity\Country;
use ResourceBundle\Repository\CountryRepository;
use Schema\Entity;
use Schema\EntityService;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Utils\Service\CacheService;

class CountryService extends EntityService {

    /** @var CacheService $cacheService */
    private $cacheService;

    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * CountryService constructor.
     * @param CountryRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param LoggerInterface $logger
     * @param CacheService $cacheService
     */
    public function __construct(CountryRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                LoggerInterface $logger,
                                CacheService $cacheService
    ) {
        $this->repository = $repository;
        parent::__construct($repository, $authorizationChecker, $logger);

        $this->cacheService = $cacheService;
    }

    /**
     * @param $code
     * @return array|mixed
     */
    public function getByCode ($code) {
        return $this->repository->findByCode($code);
    }

    /**
     * @return array|mixed
     */
    public function getAllCountries () {
        return $this->repository->findAllCountries();
    }

    /**
     * @return array|mixed|null
     */
    public function getCachedCountries () {

        $formatted = $this->cacheService->get("list_public_formatted_countries");
        if (is_null($formatted)) {
            $formatted  = $this->getFormattedCountries();

            // Expire after 1 year
            $expire = 60*60*24*365;
            $this->cacheService->set("list_public_formatted_countries", $formatted, $expire);
        }

        return $formatted;
    }

    /**
     * @return array
     */
    private function getFormattedCountries () {
        $formatted = array();
        $countries = $this->getAllCountries();
        if (!empty($countries) && count($countries)>0) {
            /** @var Country $country */
            foreach ($countries as $country) {
                $formatted[] = $country->listSerializer();
            }
        }
        return $formatted;
    }


    /**
     * @param array $countries
     * @param array $translations
     * @return mixed
     */
    public function getResponseList($countries, $translations = array()) {
        $results = array();
        $page = 0;
        $i = 0;
        /** @var Country $country */
        foreach($countries as $country) {
            $results[$i] = array(
                'id' => $country->getId(),
                'source' => $country->getSource()
            );

            if(count($translations) == 0) {
                $results[$i]['name'] = $country->getName();
            }
            else {
                foreach ($translations as $translation_source => $translation_value) {
                    if ($results[$i]['source'] == $translation_source) {
                        $results[$i]['name'] = $translation_value;
                    }
                }
            }
            $i++;
        }

        $response['list'] = array();

        if(!empty($results)){
            $response['list'] = array_values($results);
        }

        $response['total_items'] = count($results);

        $pagination = $this->pagination;
        if($page == 0) {
            $pagination = count($results);
            $response['current_page'] = 1;
            $response['total_pages'] = 1;
            return $response;
        }

        $response['current_page'] = $page;
        $response['total_pages'] = ceil($response['records'] / $pagination);

        return $response;
    }
}