<?php

namespace ResourceBundle\Service;

use Psr\Log\LoggerInterface;
use ResourceBundle\Entity\CalendarTimezone;
use ResourceBundle\Repository\CalendarTimezoneRepository;
use Schema\EntityService;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Utils\Service\CacheService;

class CalendarTimezoneService extends EntityService {

    /** @var CacheService $cacheService */
    private $cacheService;

    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * CalendarTimezoneService constructor.
     * @param CalendarTimezoneRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param LoggerInterface $logger
     * @param CacheService $cacheService
     */
    public function __construct(CalendarTimezoneRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                LoggerInterface $logger,
                                CacheService $cacheService
    ) {

        $this->repository = $repository;
        parent::__construct($repository, $authorizationChecker, $logger);

        $this->cacheService = $cacheService;

    }

    /**
     * @return array
     */
    public function getFormattedListTimezone () {
        $formatted = $this->cacheService->get("list_timezone");

        if (is_null($formatted)) {
            $allTimezone = $this->getAllTimezone();

            if (!empty($allTimezone) && count($allTimezone) > 0) {
                /** @var CalendarTimezone $timezone */
                foreach ($allTimezone as $timezone) {
                    $formatted[$timezone->getId()] = $timezone->getUTCOffset() . " UTC " . $timezone->getTimezone();
                }
            }

            // Expire after 6 month
            $expire = 60*60*24*30*6;
            $this->cacheService->set("list_timezone", $formatted, $expire);
        }

        return $formatted;
    }

    /**
     * @return array|mixed
     */
    public function getAllTimezone () {
        return $this->repository->findAllTimezone();
    }

}