<?php

namespace ResourceBundle\Service;


use Psr\Log\LoggerInterface;
use ResourceBundle\Entity\City;
use ResourceBundle\Repository\CityRepository;
use Schema\Entity;
use Schema\EntityService;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class CityService extends EntityService {

    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * CityService constructor.
     * @param CityRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param LoggerInterface $logger
     */
    public function __construct(CityRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                LoggerInterface $logger
    ) {

        $this->repository = $repository;
        parent::__construct($repository, $authorizationChecker, $logger);
    }

    /**
     * @param array $cities
     * @param array $translations
     * @return mixed
     */
    public function getResponseList($cities, $translations = array()) {
        $results = array();
        $page = 0;
        $i = 0;
        /** @var City $city */
        foreach($cities as $city) {
            $results[$i] = array(
                'id' => $city->getId(),
                'source' => $city->getSource()
            );

            if(count($translations) == 0) {
                $results[$i]['name'] = $city->getName();
            }
            else {
                foreach ($translations as $translation_source => $translation_value) {
                    if ($results[$i]['source'] == $translation_source) {
                        $results[$i]['name'] = $translation_value;
                    }
                }
            }
            $i++;
        }

        $response['list'] = array();

        if(!empty($results)){
            $response['list'] = array_values($results);
        }

        $response['total_items'] = count($results);

        $pagination = $this->pagination;
        if($page == 0) {
            $pagination = count($results);
            $response['current_page'] = 1;
            $response['total_pages'] = 1;
            return $response;
        }

        $response['current_page'] = $page;
        $response['total_pages'] = ceil($response['records'] / $pagination);

        return $response;
    }

}