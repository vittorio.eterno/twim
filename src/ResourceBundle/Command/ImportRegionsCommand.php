<?php

namespace ResourceBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TranslationBundle\Entity\Translation;
use TranslationBundle\Service\TranslationService;

class ImportRegionsCommand extends ContainerAwareCommand {

    /** @var TranslationService $translationService */
    private $translationService;

    /** @var EntityManagerInterface $entityManager */
    private $entityManager;


    public function __construct(TranslationService $translationService,
                                EntityManagerInterface $entityManager) {

        $this->translationService   = $translationService;
        $this->entityManager        = $entityManager;

        parent::__construct();

    }


    protected function configure() {
        $this
            ->setName('import:regions')
            ->setDescription('Read file csv with all regions formatted and save them on db')
            ->setHelp('Use this command to import all regions on the database');
    }


    protected function execute(InputInterface $input, OutputInterface $output) {
        $output->writeln("START");

        $em = $this->entityManager;
        $connection = $em->getConnection();

        $filePath = realpath(__DIR__."/../Data/02_regions.sql");

        $sql = file_get_contents($filePath);

        $statement = $connection->prepare($sql);
        $statement->execute();

        $statement = $connection->prepare("SELECT id, name FROM regions");
        $statement->execute();
        $results = $statement->fetchAll();

        $filePath = __DIR__."/../Data/source_regions.csv";

        $file = fopen($filePath,"r");

        $csvParsed = array();

        while(! feof($file)) {
            $csvParsed[] = fgetcsv($file,0,';');
        }

        fclose($file);

        $statement = $connection->prepare("SELECT source FROM translations");
        $statement->execute();
        $translations_sources_array = $statement->fetchAll();

        $translations_sources = array();
        foreach($translations_sources_array as $translation_source) {
            $translations_sources[] = $translation_source['source'];
        }

        $sql = ' ';
        $i = 0;
        $batchSize = 200;
        // Format CSV
        foreach($csvParsed as $key=>$value) {
            $i++;
            $source = $value[0];
            $output->writeln("DONE - ". $source);
            $sql .= "UPDATE regions SET source = '".$source."' WHERE id = ".$i."; ";

            if(!in_array($source, $translations_sources)) {
                $Translation = new Translation();
                $index = array_search($i, array_column($results, 'id'));

                $Translation->setSource($source);
                $Translation->setValue($results[$index]['name']);
                $Translation->setLang('it_IT');
                $this->entityManager->merge($Translation);

                $Translation = new Translation();
                $index = array_search($i, array_column($results, 'id'));

                $Translation->setSource($source);
                $Translation->setValue($results[$index]['name']);
                $Translation->setLang('en_GB');
                $this->entityManager->merge($Translation);

                if (($i % $batchSize) === 0) {
                    $em->flush(); // Executes all updates.
                    $em->clear(); // Detaches all objects from Doctrine!
                }
            }
        }

        $this->entityManager->flush();
        $this->entityManager->clear();

        $statement = $connection->prepare($sql);
        $statement->execute();
        $output->writeln("DONE");
    }
}