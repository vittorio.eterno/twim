<?php

namespace ResourceBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TranslationBundle\Entity\Translation;
use TranslationBundle\Service\TranslationService;

class ImportCitiesCommand extends ContainerAwareCommand {

    /** @var TranslationService $translationService */
    private $translationService;

    /** @var EntityManagerInterface $entityManager */
    private $entityManager;


    public function __construct(TranslationService $translationService,
                                EntityManagerInterface $entityManager) {

        $this->translationService   = $translationService;
        $this->entityManager        = $entityManager;

        parent::__construct();

    }


    protected function configure() {
        $this
            ->setName('import:cities')
            ->setDescription('Read file csv with all cities formatted and save them on db')
            ->setHelp('Use this command to import all cities on the database');
    }


    protected function execute(InputInterface $input, OutputInterface $output) {
        $output->writeln("START");

        $connection = $this->entityManager->getConnection();

        $filePath = realpath(__DIR__."/../Data/04_cities.sql");

        $sql = file_get_contents($filePath);
        $time_start = microtime(true);
        $statement = $connection->prepare($sql);
        $statement->execute();
        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start)/60;
        $output->writeln("Total Execution Time SQL CITIES: ".$execution_time." Mins");


        $time_start = microtime(true);
        $statement = $connection->prepare("SELECT id, name FROM cities");
        $statement->execute();
        $results = $statement->fetchAll();
        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start)/60;
        $output->writeln("Total Execution Time RESULTS SQL GET CITIES: ".$execution_time." Mins");


        $time_start = microtime(true);
        $filePath = __DIR__."/../Data/source_cities.csv";

        $file = fopen($filePath,"r");

        $csvParsed = array();

        while(! feof($file)) {
            $csvParsed[] = fgetcsv($file,0,';');
        }

        fclose($file);
        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start)/60;
        $output->writeln("Total Execution Time parse csv: ".$execution_time." Mins");

        $time_start = microtime(true);
        $statement = $connection->prepare("SELECT source FROM translations");
        $statement->execute();
        $translations_sources_array = $statement->fetchAll();


        $translations_sources = array();
        foreach($translations_sources_array as $translation_source) {
            $key_source = $translation_source['source'];
            $translations_sources[$key_source] = $translation_source['source'];
        }
        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start)/60;
        $output->writeln("Total Execution Time RESULTS SQL GET TRANSLATIONS: ".$execution_time." Mins");

        $sql = ' ';
        $i = 0;
        $batchSize = 200;
        // Format CSV
        $time_start = microtime(true);
        foreach($csvParsed as $key=>$value) {
            $i++;
            $source = $value[0];
            //$output->writeln("DONE - ". $source);
            //$sql .= "UPDATE cities SET source = '".$source."' WHERE id = ".$i."; ";
            if(!array_key_exists($source, $translations_sources)) {
                $Translation = new Translation();
                $index = array_search($i, array_column($results, 'id'));

                $Translation->setSource($source);
                $Translation->setValue($results[$index]['name']);
                $Translation->setLang('it_IT');
                $this->entityManager->merge($Translation);

                $Translation = new Translation();
                $index = array_search($i, array_column($results, 'id'));

                $Translation->setSource($source);
                $Translation->setValue($results[$index]['name']);
                $Translation->setLang('en_GB');
                $this->entityManager->merge($Translation);

                if (($i % $batchSize) === 0) {
                    $this->entityManager->flush(); // Executes all updates.
                    $this->entityManager->clear(); // Detaches all objects from Doctrine!
                }

            }
        }
        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start)/60;
        $output->writeln("Total Execution Time translations TRANSLATIONS: ".$execution_time." Mins");

        $this->entityManager->flush();
        $this->entityManager->clear();
        $time_start = microtime(true);
        //$statement = $connection->prepare($sql);
        //$statement->execute();
        $connection->close();

        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start)/60;
        $output->writeln("Total Execution Time translations UPDATE SOURCES: ".$execution_time." Mins");
        $output->writeln("DONE");
    }
}