<?php

namespace ResourceBundle\Repository;

use ResourceBundle\Entity\City;
use Schema\SchemaEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * CityRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CityRepository extends SchemaEntityRepository {

    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, City::class);
    }
}
