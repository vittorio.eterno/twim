<?php

namespace ResourceBundle\Repository;

use Psr\Log\LoggerInterface;
use ResourceBundle\Entity\Country;
use Schema\SchemaEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Utils\StaticUtil\LogUtils;

/**
 * Class CountryRepository
 * @package ResourceBundle\Repository
 */
class CountryRepository extends SchemaEntityRepository {


    /** @var LoggerInterface $logger */
    private $logger;

    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * CountryRepository constructor.
     * @param ManagerRegistry $registry
     * @param LoggerInterface $logger
     */
    public function __construct(ManagerRegistry $registry, LoggerInterface $logger) {
        parent::__construct($registry, Country::class);

        $this->logger = $logger;
    }

    /**
     * @return array|mixed
     */
    public function findAllCountries() {

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('c')
            ->from('ResourceBundle:Country', 'c')
        ;

        $results = array();

        try {
            $results = $qb->getQuery()->getResult();
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            $this->logger->error("Failed get all countries", $exception);
        }

        return $results;
    }

    /**
     * @param $code
     * @return mixed|null
     */
    public function findByCode($code) {

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('c')
            ->from('ResourceBundle:Country', 'c')
            ->where('c.code = :code')
            ->setParameter('code', $code)
        ;

        $result = null;

        try {
            $result = $qb->setMaxResults(1)->getQuery()->getOneOrNullResult();
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            $this->logger->error("Failed get country by code: ".$code, $exception);
        }

        return $result;
    }

}
