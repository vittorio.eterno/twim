<?php

namespace EventListener;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Security;
use Utils\Service\DeviceIdentifier;
use Utils\Service\EnvironmentIdentifier;
use Utils\Service\MaintenanceIdentifier;

use UserBundle\Entity\User;
use UserBundle\Service\UserService;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Description of RequestListener
 */
class RequestListener {

    /** @var LoggerInterface $logger */
    private $logger;

    /** @var EnvironmentIdentifier $environmentIdentifier */
    private $environmentIdentifier;

    /** @var DeviceIdentifier $deviceIdentifier */
    private $deviceIdentifier;

    /** @var MaintenanceIdentifier $maintenanceIdentifier */
    private $maintenanceIdentifier;

    /** @var Security $security */
    private $security;

    /** @var UserService $userService */
    private $userService;

    /** @var RouterInterface $router */
    private $router;

    /**
     * RequestListener constructor.
     * @param LoggerInterface $logger
     * @param EnvironmentIdentifier $environmentIdentifier
     * @param DeviceIdentifier $deviceIdentifier
     * @param MaintenanceIdentifier $maintenanceIdentifier
     * @param Security $security
     * @param UserService $userService
     * @param RouterInterface $router
     */
    public function __construct(LoggerInterface $logger,
                                EnvironmentIdentifier $environmentIdentifier,
                                DeviceIdentifier $deviceIdentifier,
                                MaintenanceIdentifier $maintenanceIdentifier,
                                Security $security,
                                UserService $userService,
                                RouterInterface $router
    ) {
        $this->logger                   = $logger;
        $this->environmentIdentifier    = $environmentIdentifier;
        $this->deviceIdentifier         = $deviceIdentifier;
        $this->maintenanceIdentifier    = $maintenanceIdentifier;
        $this->security                 = $security;
        $this->userService              = $userService;
        $this->router                   = $router;
    }


    public function onKernelRequest(GetResponseEvent $event) {

        if (!$event->isMasterRequest()) {
            // don't do anything if it's not the master request
            // usefull for embedded controllers
            return;
        }

        $request = $event->getRequest();
        $user = $this->security->getUser();

        if (!is_null($user) && $user instanceof User) {

            if (!$user->getIsTermsPrivacyAccepted()) {

                $token  = $this->userService->getSocialToken($user);

                $url    = $this->router->generate(
                    'private_registration_social_user',
                    array('user_id' => $user->getId(), 'token' => $token),
                    UrlGeneratorInterface::ABSOLUTE_URL
                );

                if ($request->isMethod('GET') && $request->getUri() !== $url) {
                    $response =  new RedirectResponse($url);
                    return $event->setResponse($response);
                }

            }

        }


//        $ip = $request->getClientIp();
//        if($ip == 'unknown'){
//            $ip = $_SERVER['REMOTE_ADDR'];
//        }

        $this->environmentIdentifier->detect();
        $this->deviceIdentifier->detect();
        $this->maintenanceIdentifier->detect($event);
        
    }
    
    
}
