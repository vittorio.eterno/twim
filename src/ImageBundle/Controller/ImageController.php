<?php

namespace ImageBundle\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use ImageBundle\Entity\Image;
use ImageBundle\Service\ImageService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Translation\TranslatorInterface;
use Utils\Service\ResponseUtils;
use Utils\StaticUtil\FileSystemUtils;
use Utils\StaticUtil\IntegerUtils;
use Utils\StaticUtil\StringUtils;

class ImageController extends Controller {

    /**
     * Returns a specific image content given its id and hash.
     *
     * @Route("/public/picture/{image_id}/{hash}", name="public_get_original_picture", methods={"GET"})
     *
     * @param string|null $image_id
     * @param string|null $hash
     * @param ImageService $imageService
     * @param TranslatorInterface $translator
     * @param LoggerInterface $logger
     *
     * @return JsonResponse
     */
    public function getImageAction(string $image_id=null, string $hash=null, ImageService $imageService, TranslatorInterface $translator, LoggerInterface $logger) {

        $response = new ResponseUtils($translator, $logger);

        if(!IntegerUtils::checkId($image_id)) {
            return $response->getApiResponse(array(), "parameter.id.invalid", 400, array("image_error"=>$translator->trans("parameter.id.invalid")));
        }

        if(!StringUtils::checkHashString($hash)) {
            return $response->getApiResponse(array(), "parameter.hash.invalid", 400, array("image_error"=>$translator->trans("parameter.hash.invalid")));
        }

        /** @var Image $image */
        $image = $imageService->getById($image_id);

        if(is_null($image)) {
            return $response->getApiResponse(array(), "image.not.found", 404, array("image_error"=>$translator->trans("image.not.found")));
        }

        if($image->getHash() != $hash) {
            return $response->getApiResponse(array(), "image.not.found", 404, array("image_error"=>$translator->trans("image.not.found")));
        }

        return $response->getResponseImage($imageService, $image);
    }

    /**
     * Returns a specific image with small size content given its id and hash.
     *
     * @Route("/public/picture/small/{image_id}/{hash}", name="public_get_small_picture", methods={"GET"})
     *
     * @param string|null $image_id
     * @param string|null $hash
     * @param ImageService $imageService
     * @param TranslatorInterface $translator
     * @param LoggerInterface $logger
     *
     * @return JsonResponse
     */
    public function getImageSmallAction(string $image_id=null, string $hash=null, ImageService $imageService, TranslatorInterface $translator, LoggerInterface $logger) {

        $response = new ResponseUtils($translator, $logger);

        if(!IntegerUtils::checkId($image_id)) {
            return $response->getApiResponse(array(), "parameter.id.invalid", 400, array("image_error"=>$translator->trans("parameter.id.invalid")));
        }

        if(!StringUtils::checkHashString($hash)) {
            return $response->getApiResponse(array(), "parameter.hash.invalid", 400, array("image_error"=>$translator->trans("parameter.hash.invalid")));
        }

        /** @var Image $image */
        $image = $imageService->getById($image_id);

        if(is_null($image)) {
            return $response->getApiResponse(array(), "image.not.found", 404, array("image_error"=>$translator->trans("image.not.found")));
        }

        if($image->getHash() != $hash) {
            return $response->getApiResponse(array(), "image.not.found", 404, array("image_error"=>$translator->trans("image.not.found")));
        }

        if(!FileSystemUtils::checkFile($image->getSmallThumbnailPath())) {
            if(!FileSystemUtils::createSmallThumbnail($image->getFullPath(), $image->getFileName())) {
                return $response->getApiResponse(array(), "error.small.image", 500, array("image_error"=>$translator->trans("error.small.image")));
            }
        }

        return $response->getResponseImage($imageService, $image);
    }

    /**
     * Returns a specific image with medium size content given its id and hash.
     *
     * @Route("/public/picture/medium/{image_id}/{hash}", name="public_get_medium_picture", methods={"GET"})
     *
     * @param string|null $image_id
     * @param string|null $hash
     * @param ImageService $imageService
     * @param TranslatorInterface $translator
     * @param LoggerInterface $logger
     *
     * @return JsonResponse
     */
    public function getImageMediumAction(string $image_id=null, string $hash=null, ImageService $imageService, TranslatorInterface $translator, LoggerInterface $logger) {

        $response = new ResponseUtils($translator, $logger);

        if(!IntegerUtils::checkId($image_id)) {
            return $response->getApiResponse(array(), "parameter.id.invalid", 400, array("image_error"=>$translator->trans("parameter.id.invalid")));
        }

        if(!StringUtils::checkHashString($hash)) {
            return $response->getApiResponse(array(), "parameter.hash.invalid", 400, array("image_error"=>$translator->trans("parameter.hash.invalid")));
        }

        /** @var Image $image */
        $image = $imageService->getById($image_id);

        if(is_null($image)) {
            return $response->getApiResponse(array(), "image.not.found", 404, array("image_error"=>$translator->trans("image.not.found")));
        }

        if($image->getHash() != $hash) {
            return $response->getApiResponse(array(), "image.not.found", 404, array("image_error"=>$translator->trans("image.not.found")));
        }

        if(!FileSystemUtils::checkFile($image->getMediumThumbnailPath())) {
            if(!FileSystemUtils::createMediumThumbnail($image->getFullPath(), $image->getFileName())) {
                return $response->getApiResponse(array(), "error.medium.image", 500, array("image_error"=>$translator->trans("error.medium.image")));
            }
        }

        return $response->getResponseImage($imageService,$image);
    }

    /**
     * Returns a specific image with large size content given its id and hash.
     *
     * @Route("/public/picture/large/{image_id}/{hash}", name="public_get_large_picture", methods={"GET"})
     *
     * @param string|null $image_id
     * @param string|null $hash
     * @param ImageService $imageService
     * @param TranslatorInterface $translator
     * @param LoggerInterface $logger
     *
     * @return JsonResponse
     */
    public function getImageLargeAction(string $image_id=null, string $hash=null, ImageService $imageService, TranslatorInterface $translator, LoggerInterface $logger) {

        $response = new ResponseUtils($translator, $logger);

        if(!IntegerUtils::checkId($image_id)) {
            return $response->getApiResponse(array(), "parameter.id.invalid", 400, array("image_error"=>$translator->trans("parameter.id.invalid")));
        }

        if(!StringUtils::checkHashString($hash)) {
            return $response->getApiResponse(array(), "parameter.hash.invalid", 400, array("image_error"=>$translator->trans("parameter.hash.invalid")));
        }

        /** @var Image $image */
        $image = $imageService->getById($image_id);

        if(is_null($image)) {
            return $response->getApiResponse(array(), "image.not.found", 404, array("image_error"=>$translator->trans("image.not.found")));
        }

        if($image->getHash() != $hash) {
            return $response->getApiResponse(array(), "image.not.found", 404, array("image_error"=>$translator->trans("image.not.found")));
        }

        if(!FileSystemUtils::checkFile($image->getLargeThumbnailPath())) {
            if(!FileSystemUtils::createLargeThumbnail($image->getFullPath(), $image->getFileName())) {
                return $response->getApiResponse(array(), "error.large.image", 500, array("image_error"=>$translator->trans("error.large.image")));
            }
        }

        return $response->getResponseImage($imageService, $image);
    }
}
