<?php

namespace ImageBundle\Service;


use ImageBundle\Entity\PictureFrame;
use Psr\Log\LoggerInterface;
use PurchasingBundle\Entity\Optional;
use Schema\EntityService;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Utils\Service\CacheService;
use Utils\StaticUtil\LogUtils;
use ImageBundle\Repository\PictureFrameRepository;

class PictureFrameService extends EntityService {


    /** @var CacheService $cacheService */
    private $cacheService;

    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * PictureFrameService constructor.
     * @param PictureFrameRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param LoggerInterface $logger
     * @param CacheService $cacheService
     */
    public function __construct(PictureFrameRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                LoggerInterface $logger,
                                CacheService $cacheService
    ) {
        $this->repository = $repository;
        parent::__construct($repository, $authorizationChecker, $logger);

        $this->cacheService = $cacheService;
    }

    /**
     * @param $source
     * @return string
     */
    public function generateFrameHash ($source) {
        return md5("H@sH! Fr@m3 BY s0u3c3: ".$source);
    }

    /**
     * @param int $type
     * @return array|mixed|null
     */
    public function getFormattedPictureFramesByType ($type = Optional::TYPE_DIGITAL_FORMAT) {

        $cacheKey = $type == Optional::TYPE_DIGITAL_FORMAT ? 'list_formatted_digital_pic_frames' : 'list_formatted_paper_pic_frames';

        $formatted = $this->cacheService->get($cacheKey);
        if (is_null($formatted)) {
            $picFrames = $this->getActivesByType($type);
            $formatted = $this->getFormattedResults($picFrames);

            // Expire after 30 days
            $expire = 60*60*24*30;
            $this->cacheService->set($cacheKey, $formatted, $expire);
        }

        return $formatted;

    }

    /**
     * @param int $type
     * @return array|mixed|null
     */
    public function getFormattedDefaultPictureFrameByType ($type = Optional::TYPE_DIGITAL_FORMAT) {

        $cacheKey = $type == Optional::TYPE_DIGITAL_FORMAT ? 'default_formatted_digital_pic_frame' : 'default_formatted_paper_pic_frame';

        $formatted = $this->cacheService->get($cacheKey);
        if (is_null($formatted)) {
            /** @var PictureFrame $defaultPicFrame */
            $defaultPicFrame = $this->getDefaultByType($type);
            if (!is_null($defaultPicFrame)) {
                $formatted  = $defaultPicFrame->listSerializer();

                // Expire after 30 days
                $expire = 60*60*24*30;
                $this->cacheService->set($cacheKey, $formatted, $expire);
            }
        }

        return $formatted;

    }



    /**
     * @param int $type
     * @return array|mixed
     */
    public function getActivesByType ($type = Optional::TYPE_DIGITAL_FORMAT) {
        return $this->repository->findActivesByType($type);
    }

    /**
     * @param int $type
     * @return mixed|null
     */
    public function getDefaultByType ($type = Optional::TYPE_DIGITAL_FORMAT) {
        return $this->repository->findDefaultByType($type);
    }

    /**
     * @param $picFrames
     * @return array
     */
    public function getFormattedResults (&$picFrames) {

        $formatted = array();

        if (!empty($picFrames) && count($picFrames) > 0) {

            /** @var PictureFrame $picFrame */
            foreach ($picFrames as $picFrame) {
                $formatted[] = $picFrame->listSerializer();
            }
        }

        return $formatted;
    }

}