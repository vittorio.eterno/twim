<?php

namespace ImageBundle\Service;


use ImageBundle\Repository\PictureFrameStructureRepository;
use Psr\Log\LoggerInterface;
use Schema\EntityService;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Utils\StaticUtil\LogUtils;


class PictureFrameStructureService extends EntityService {


    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * PictureFrameStructureService constructor.
     * @param PictureFrameStructureRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param LoggerInterface $logger
     */
    public function __construct(PictureFrameStructureRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                LoggerInterface $logger
    ) {
        $this->repository = $repository;
        parent::__construct($repository, $authorizationChecker, $logger);
    }

}