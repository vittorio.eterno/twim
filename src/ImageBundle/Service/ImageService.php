<?php

namespace ImageBundle\Service;

use ImageBundle\Entity\Image;
use ImageBundle\Repository\ImageRepository;
use Psr\Log\LoggerInterface;
use Schema\EntityService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\HttpFoundation\File\File as RowFile;
use UserBundle\Entity\User;
use Utils\StaticUtil\DateUtils;
use Utils\StaticUtil\FileSystemUtils;
use Utils\StaticUtil\LogUtils;
use Utils\StaticUtil\StringUtils;

class ImageService extends EntityService {

    /** @var integer $max_images_per_element */
    public $max_images_per_element = 10;

    /** @var integer $max_image_size */
    public $max_image_size = 5242880; //1024*1024*5;

    /** @var integer $max_photo_size */
    public $max_photo_size = 3145728;

    private $imageBasePath;

    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * ImageService constructor.
     * @param ImageRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param $imageBasePath
     * @param LoggerInterface $logger
     */
    public function __construct(ImageRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                $imageBasePath,
                                LoggerInterface $logger
    ) {
        $this->imageBasePath = $imageBasePath;
        parent::__construct($repository, $authorizationChecker, $logger);
    }

    /**
     * @param Request $request
     * @param User $user
     * @return Image|null
     */
    public function init(Request &$request, User &$user) {
        $image = new Image();

        /** @var RowFile $row_image */
        $row_image = $request->files->get('image');

        if(is_null($row_image)) {
            return null;
        }

        $currentTime = DateUtils::getCurrentTimeUTC();
        $image->setCreatedAt($currentTime);
        $image->setCreatedBy($user->getId());

        $imageName = uniqid('pic') . md5($user->getId() . $row_image->getFilename() . StringUtils::randomString(24));
        $image->setName($imageName);

        $info = new \SplFileInfo($row_image->getFilename());
        $image->setExtension($info->getExtension());

        if(!$row_image->getSize() || $row_image->getSize() > $this->max_image_size) {
            return null;
        }

        return $image;
    }

    /**
     * @param Image $image
     * @return bool
     */
    public function remove(Image $image) {
        try {
            $full_name = $image->getFullFilePath();
            $this->repository->remove($image);
            FileSystemUtils::deleteFile($full_name);
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            $this->logger->error("Failed remove image ID: ".$image->getId(), $exception);
            return false;
        }
        return true;
    }


    public function getBasePath() {
        return $this->imageBasePath;
    }

    public function getSpecificPath($type) {
        switch ($type) {
            case 'profile_photo':    return 'profile/photo/';
            case 'group_photo':      return 'group/photo/';
            case 'post_photo':       return 'post/photo/';
            case 'question_photo':   return 'question/photo/';
            case 'beneficial_photo': return 'beneficial/photo/';
        }
        return null;
    }

    public function getFullPath(Image &$image) {
        return  $this->getBasePath() . $image->getPath();
    }

    public function getFileName(Image &$image) {
        return  md5($image->getId().$image->getHash()) . '.' . $image->getExtension();
    }

    public function getFullFilePath(Image &$image) {
        return $this->getFullPath($image) . $this->getFileName($image);
    }

    public function getSmallThumbnailName(Image &$image) {
        return "thumb_s_" . $this->getFileName($image);
    }

    public function getMediumThumbnailName(Image &$image) {
        return "thumb_m_" . $this->getFileName($image);
    }

    public function getLargeThumbnailName(Image &$image) {
        return "thumb_l_" . $this->getFileName($image);
    }

    public function getSmallThumbnailPath(Image &$image) {
        return $this->getFullPath($image) . $this->getSmallThumbnailName($image);
    }

    public function getMediumThumbnailPath(Image &$image) {
        return $this->getFullPath($image) . $this->getMediumThumbnailName($image);
    }

    public function getLargeThumbnailPath(Image &$image) {
        return $this->getFullPath($image) . $this->getLargeThumbnailName($image);
    }

}