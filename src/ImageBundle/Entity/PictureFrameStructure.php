<?php

namespace ImageBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Schema\Entity;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * PictureFrameStructure
 *
 * @ORM\Table(name="picture_frames_structure")
 * @ORM\Entity(repositoryClass="ImageBundle\Repository\PictureFrameStructureRepository")
 */
class PictureFrameStructure extends Entity {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

//    /**
//     * One PictureFrameStructure has Many PictureFrames.
//     * @ORM\OneToMany(targetEntity="PictureFrame", mappedBy="pictureFrameStructure")
//     */
//    private $pictureFrames;

    /**
     * @var string
     *
     * @ORM\Column(name="source", type="string", length=255)
     */
    private $source;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="disabled_at", type="datetime", nullable=true)
     */
    private $disabledAt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="disabled_by", type="string", length=22, nullable=true)
     */
    private $disabledBy;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_disabled", type="boolean", nullable=true)
     */
    private $isDisabled;



    ################################################# SERIALIZER FUNCTIONS

    /**
     * Returns the array of fields to serialize in entity administration view.
     * @return array
     */
    public function adminSerializer() {
        $view_vars = $this->viewSerializer();

        $admin_vars = array();

        return array_merge($view_vars, $admin_vars);
    }

    /**
     * Returns the array of fields to serialize in entity view.
     * @return array
     */
    public function viewSerializer() {
        $list_vars = $this->listSerializer();

        $view_vars = array(
        );

        return array_merge($list_vars, $view_vars);
    }

    /**
     * Returns the array of fields to serialize in a list of this entity.
     * @return array
     */
    public function listSerializer() {
        $list_vars = array(
            'id'           => $this->serializedId()
        );
        return $list_vars;
    }

    /**
     * Returns the hash code unique identifier of the entity.
     * @return string
     */
    public function hashCode() {
        // TODO: Implement hashCode() method.
    }

    ################################################# SERIALIZED FUNCTIONS

    /**
     * Setting id
     * @JMS\VirtualProperty
     * @JMS\SerializedName("id")
     * @JMS\Type("integer")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedId() {
        return (is_null($this->id)?null:$this->id);
    }


    ################################################# GETTERS AND SETTERS FUNCTIONS

//    public function __construct() {
//        $this->pictureFrames = new ArrayCollection();
//    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

//    /**
//     * Add pictureFrame.
//     *
//     * @param PictureFrame $pictureFrame
//     * @return $this
//     */
//    public function addPictureFrame(PictureFrame $pictureFrame)
//    {
//        $this->pictureFrames[] = $pictureFrame;
//
//        return $this;
//    }
//
//    /**
//     * Remove pictureFrame.
//     *
//     * @param PictureFrame $pictureFrame
//     *
//     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
//     */
//    public function removePictureFrame(PictureFrame $pictureFrame)
//    {
//        return $this->pictureFrames->removeElement($pictureFrame);
//    }
//
//    /**
//     * Get pictureFrames.
//     *
//     * @return \Doctrine\Common\Collections\Collection
//     */
//    public function getPictureFrames()
//    {
//        return $this->pictureFrames;
//    }

    /**
     * Set source.
     *
     * @param string $source
     *
     * @return PictureFrameStructure
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source.
     *
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return PictureFrameStructure
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return PictureFrameStructure
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set disabledAt.
     *
     * @param \DateTime|null $disabledAt
     *
     * @return PictureFrameStructure
     */
    public function setDisabledAt($disabledAt = null)
    {
        $this->disabledAt = $disabledAt;

        return $this;
    }

    /**
     * Get disabledAt.
     *
     * @return \DateTime|null
     */
    public function getDisabledAt()
    {
        return $this->disabledAt;
    }

    /**
     * Set disabledBy.
     *
     * @param string|null $disabledBy
     *
     * @return PictureFrameStructure
     */
    public function setDisabledBy($disabledBy = null)
    {
        $this->disabledBy = $disabledBy;

        return $this;
    }

    /**
     * Get disabledBy.
     *
     * @return string|null
     */
    public function getDisabledBy()
    {
        return $this->disabledBy;
    }

    /**
     * Set isDisabled.
     *
     * @param bool|null $isDisabled
     *
     * @return PictureFrameStructure
     */
    public function setIsDisabled($isDisabled = null)
    {
        $this->isDisabled = $isDisabled;

        return $this;
    }

    /**
     * Get isDisabled.
     *
     * @return bool|null
     */
    public function getIsDisabled()
    {
        return $this->isDisabled;
    }
}
