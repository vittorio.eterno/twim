<?php

namespace ImageBundle\Entity;

use DateTime;
use Schema\Entity;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table("`images`")
 * @ORM\Entity(repositoryClass="ImageBundle\Repository\ImageRepository")
 */
class Image extends Entity {

    /**
     * @ORM\Column(name="`id`", type="bigint", options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer $id
     */
    private $id;

    /**
     * @ORM\Column(name="`path`", type="string", length=255, nullable=false)
     */
    private $path = null;

    /**
     * @ORM\Column(name="`hash`", type="string", length=255, nullable=false, unique=true)
     */
    private $hash = null;

    /**
     * @ORM\Column(name="`name`", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "The title is mandatory")
     * @Assert\Regex(
     *     pattern="/^[a-zA-Z0-9_\- ]*$/",
     *     message="The name is not acceptable"
     * )
     * @Assert\Length(
     *      min = 2,
     *      max = 255,
     *      minMessage = "The name must be at least {{ limit }} characters long",
     *      maxMessage = "The name cannot be longer than {{ limit }} characters"
     * )
     */
    private $name = null;

    /**
     * @ORM\Column(name="`extension`", type="string", length=10, nullable=false)
     */
    private $extension = null;

    /**
     * @ORM\Column(name="`created_by`", type="string", length=22, nullable=false)
     */
    private $createdBy;

    /**
     * @ORM\Column(name="`created_at`", type="datetime", nullable=false)
     * @var DateTime $createdAt
     */
    private $createdAt;

    /**
     * @ORM\Column(name="`edited_by`", type="string", length=22, nullable=true)
     */
    private $editedBy;

    /**
     * @ORM\Column(name="`edited_at`", type="datetime", nullable=true)
     * @var DateTime $editedAt
     */
    private $editedAt;

    /**
     * @ORM\Column(name="`disabled_by`", type="string", length=22, nullable=true)
     */
    private $disabledBy;

    /**
     * @ORM\Column(name="`disabled_at`", type="datetime", nullable=true)
     * @var DateTime $disabledAt
     */
    private $disabledAt;

    /**
     * @ORM\Column(name="`is_disabled`", type="boolean", nullable=false, options={"default":0})
     */
    private $isDisabled = false;

    ################################################# SERIALIZER FUNCTIONS

    /**
     * Returns the array of fields to serialize in entity administration view.
     * @return array
     */
    public function adminSerializer() {
        $view_vars = $this->viewSerializer();

        $admin_vars = array();

        return array_merge($view_vars, $admin_vars);
    }

    /**
     * Returns the array of fields to serialize in entity view.
     * @return array
     */
    public function viewSerializer() {
        $list_vars = $this->listSerializer();

        $view_vars = array(
        );

        return array_merge($list_vars, $view_vars);
    }

    /**
     * Returns the array of fields to serialize in a list of this entity.
     * @return array
     */
    public function listSerializer() {
        $list_vars = array(
            'id'                => $this->serializedId(),
            'hash'              => $this->serializedHash(),
            'name'              => $this->serializedName(),
        );
        return $list_vars;
    }

    /**
     * Returns the hash code unique identifier of the entity.
     * @return string
     */
    public function hashCode() {
        $str = "";
        $str .= time();
        $str .= (is_null($this->name)?"":strtolower($this->name));
        $str .= time();
        return md5($str);
    }

    ################################################# SERIALIZED FUNCTIONS

    /**
     * if the image is locked or not
     * @JMS\VirtualProperty
     * @JMS\SerializedName("locked")
     * @JMS\Type("boolean")
     * @JMS\Groups({"admin"})
     * @JMS\Since("1.0.x")
     */
    public function serializedLocked()
    {
        return $this->isLocked();
    }

    /**
     * if the image is deleted or not
     * @JMS\VirtualProperty
     * @JMS\SerializedName("deleted")
     * @JMS\Type("boolean")
     * @JMS\Groups({"admin"})
     * @JMS\Since("1.0.x")
     */
    public function serializedDeleted()
    {
        return $this->isDeleted();
    }

    /**
     * image name
     * @JMS\VirtualProperty
     * @JMS\SerializedName("name")
     * @JMS\Type("string")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedName()
    {
        return (is_null($this->name)?null:$this->name);
    }

    /**
     * image unique id
     * @JMS\VirtualProperty
     * @JMS\SerializedName("id")
     * @JMS\Type("integer")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedId()
    {
        return (is_null($this->id)?null:$this->id);
    }

    /**
     * image hash
     * @JMS\VirtualProperty
     * @JMS\SerializedName("hash")
     * @JMS\Type("string")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedHash()
    {
        return (is_null($this->hash)?null:$this->hash);
    }

    /**
     * true if it is enabled
     * @JMS\VirtualProperty
     * @JMS\SerializedName("enabled")
     * @JMS\Type("boolean")
     * @JMS\Groups({"admin"})
     * @JMS\Since("1.0.x")
     */
    public function serializedEnabled()
    {
        return $this->isEnabled();
    }



    ################################################# GETTERS AND SETTERS FUNCTIONS

    public function getBasePath() {
        return  __DIR__  . "/../../../web/images/";
    }

    public function getSpecificPath($type) {
        switch ($type) {
            case 'profile_photo':    return 'profile/photo/';
            case 'group_photo':      return 'group/photo/';
            case 'post_photo':       return 'post/photo/';
            case 'question_photo':   return 'question/photo/';
            case 'beneficial_photo': return 'beneficial/photo/';
        }
        return null;
    }

    public function getFullPath() {
        return  $this->getBasePath() . $this->getPath();
    }

    public function getFileName() {
        return  md5($this->getId().$this->getHash()) . '.' . $this->getExtension();
    }

    public function getFullFilePath() {
        return $this->getFullPath() . $this->getFileName();
    }

    public function getSmallThumbnailName() {
        return "thumb_s_" . $this->getFileName();
    }

    public function getMediumThumbnailName() {
        return "thumb_m_" . $this->getFileName();
    }

    public function getLargeThumbnailName() {
        return "thumb_l_" . $this->getFileName();
    }

    public function getSmallThumbnailPath() {
        return $this->getFullPath() . $this->getSmallThumbnailName();
    }

    public function getMediumThumbnailPath() {
        return $this->getFullPath() . $this->getMediumThumbnailName();
    }

    public function getLargeThumbnailPath() {
        return $this->getFullPath() . $this->getLargeThumbnailName();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Image
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     * @return Image
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param mixed $hash
     * @return Image
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Image
     */
    public function setName($name)
    {
        $this->name = $name;
        $this->hash = $this->hashCode();

        return $this;
    }

    /**
     * @return mixed
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * @param mixed $extension
     * @return Image
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     * @return Image
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     * @return Image
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEditedBy()
    {
        return $this->editedBy;
    }

    /**
     * @param mixed $editedBy
     * @return Image
     */
    public function setEditedBy($editedBy)
    {
        $this->editedBy = $editedBy;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getEditedAt()
    {
        return $this->editedAt;
    }

    /**
     * @param DateTime $editedAt
     * @return Image
     */
    public function setEditedAt($editedAt)
    {
        $this->editedAt = $editedAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDisabledBy()
    {
        return $this->disabledBy;
    }

    /**
     * @param mixed $disabledBy
     * @return Image
     */
    public function setDisabledBy($disabledBy)
    {
        $this->disabledBy = $disabledBy;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDisabledAt()
    {
        return $this->disabledAt;
    }

    /**
     * @param DateTime $disabledAt
     * @return Image
     */
    public function setDisabledAt($disabledAt)
    {
        $this->disabledAt = $disabledAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsDisabled()
    {
        return $this->isDisabled;
    }

    /**
     * @param mixed $isDisabled
     * @return Image
     */
    public function setIsDisabled($isDisabled)
    {
        $this->isDisabled = $isDisabled;

        return $this;
    }
}