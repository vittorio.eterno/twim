<?php

namespace ImageBundle\Entity;

use Schema\Entity;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * PictureFrame
 *
 * @ORM\Table(name="picture_frames")
 * @ORM\Entity(repositoryClass="ImageBundle\Repository\PictureFrameRepository")
 */
class PictureFrame extends Entity {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

//    /**
//     * Many PictureFrames have One PictureFrameStructure.
//     * @ORM\ManyToOne(targetEntity="PictureFrameStructure", inversedBy="pictureFrames")
//     * @ORM\JoinColumn(name="picture_frame_structure_id", referencedColumnName="id")
//     */
//    private $pictureFrameStructure;

    /**
     * @var string|null
     *
     * @ORM\Column(name="`hash`", type="string", length=45, nullable=true)
     */
    private $hash;

    /**
     * @var string|null
     *
     * @ORM\Column(name="related_pictures", type="text", nullable=true)
     */
    private $relatedPictures;

    /**
     * @var int|null
     *
     * @ORM\Column(name="type", type="smallint", nullable=true, options={"default":0})
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="source", type="string", length=255)
     */
    private $source;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="extension", type="string", length=25)
     */
    private $extension;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="disabled_at", type="datetime", nullable=true)
     */
    private $disabledAt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="disabled_by", type="string", length=22, nullable=true)
     */
    private $disabledBy;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_disabled", type="boolean", nullable=true, options={"default":0})
     */
    private $isDisabled;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_default", type="boolean", nullable=true, options={"default":0})
     */
    private $isDefault;


    ################################################# SERIALIZER FUNCTIONS

    /**
     * Returns the array of fields to serialize in entity administration view.
     * @return array
     */
    public function adminSerializer() {
        $view_vars = $this->viewSerializer();

        $admin_vars = array();

        return array_merge($view_vars, $admin_vars);
    }

    /**
     * Returns the array of fields to serialize in entity view.
     * @return array
     */
    public function viewSerializer() {
        $list_vars = $this->listSerializer();

        $view_vars = array(
        );

        return array_merge($list_vars, $view_vars);
    }

    /**
     * Returns the array of fields to serialize in a list of this entity.
     * @return array
     */
    public function listSerializer() {
        $list_vars = array(
            'id'                => $this->serializedId(),
            'source'            => $this->serializedSource(),
            'name'              => $this->serializedName(),
            'hash'              => $this->serializedHash(),
            'relatedPictures'   => $this->serializedRelatedPictures(),
            'extension'         => $this->serializedExtension(),
            'assetUrl'          => $this->serializedAssetUrl(),
        );
        return $list_vars;
    }

    /**
     * Returns the hash code unique identifier of the entity.
     * @return string
     */
    public function hashCode() {
        // TODO: Implement hashCode() method.
    }

    ################################################# SERIALIZED FUNCTIONS

    /**
     * id
     * @JMS\VirtualProperty
     * @JMS\SerializedName("id")
     * @JMS\Type("integer")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedId() {
        return (is_null($this->id)?null:$this->id);
    }

    /**
     * source
     * @JMS\VirtualProperty
     * @JMS\SerializedName("source")
     * @JMS\Type("string")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedSource() {
        return (is_null($this->source)?null:$this->source);
    }

    /**
     * name
     * @JMS\VirtualProperty
     * @JMS\SerializedName("name")
     * @JMS\Type("string")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedName() {
        return (is_null($this->name)?null:$this->name);
    }

    /**
     * hash
     * @JMS\VirtualProperty
     * @JMS\SerializedName("hash")
     * @JMS\Type("string")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedHash() {
        return (is_null($this->hash)?null:$this->hash);
    }

    /**
     * relatedPictures
     * @JMS\VirtualProperty
     * @JMS\SerializedName("relatedPictures")
     * @JMS\Type("string")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedRelatedPictures() {
        $results = array();

        if (!is_null($this->relatedPictures)) {
            $relPics = json_decode($this->relatedPictures, true);
            if (!empty($relPics) && count($relPics)>0) {
                foreach ($relPics as $relPic) {
                    $results[] = "build/images/".$relPic;
                }
            }
        }

        return $results;
    }

    /**
     * extension
     * @JMS\VirtualProperty
     * @JMS\SerializedName("extension")
     * @JMS\Type("string")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedExtension() {
        return (is_null($this->extension)?null:$this->extension);
    }

    /**
     * assetUrl
     * @JMS\VirtualProperty
     * @JMS\SerializedName("assetUrl")
     * @JMS\Type("string")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedAssetUrl() {
        return "build/images/".$this->hash.".".$this->extension;
    }

    ################################################# GETTERS AND SETTERS FUNCTIONS


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

//    /**
//     * @return PictureFrameStructure
//     */
//    public function getPictureFrameStructure()
//    {
//        return $this->pictureFrameStructure;
//    }
//
//    /**
//     * @param PictureFrameStructure $pictureFrameStructure
//     */
//    public function setPictureFrameStructure(PictureFrameStructure $pictureFrameStructure): void
//    {
//        $this->pictureFrameStructure = $pictureFrameStructure;
//    }

    /**
     * Set source.
     *
     * @param string $source
     *
     * @return PictureFrame
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source.
     *
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return PictureFrame
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return PictureFrame
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set disabledAt.
     *
     * @param \DateTime|null $disabledAt
     *
     * @return PictureFrame
     */
    public function setDisabledAt($disabledAt = null)
    {
        $this->disabledAt = $disabledAt;

        return $this;
    }

    /**
     * Get disabledAt.
     *
     * @return \DateTime|null
     */
    public function getDisabledAt()
    {
        return $this->disabledAt;
    }

    /**
     * Set disabledBy.
     *
     * @param string|null $disabledBy
     *
     * @return PictureFrame
     */
    public function setDisabledBy($disabledBy = null)
    {
        $this->disabledBy = $disabledBy;

        return $this;
    }

    /**
     * Get disabledBy.
     *
     * @return string|null
     */
    public function getDisabledBy()
    {
        return $this->disabledBy;
    }

    /**
     * Set isDisabled.
     *
     * @param bool|null $isDisabled
     *
     * @return PictureFrame
     */
    public function setIsDisabled($isDisabled = null)
    {
        $this->isDisabled = $isDisabled;

        return $this;
    }

    /**
     * Get isDisabled.
     *
     * @return bool|null
     */
    public function getIsDisabled()
    {
        return $this->isDisabled;
    }

    /**
     * @return null|string
     */
    public function getHash(): ?string
    {
        return $this->hash;
    }

    /**
     * @param null|string $hash
     */
    public function setHash(?string $hash): void
    {
        $this->hash = $hash;
    }

    /**
     * @return int|null
     */
    public function getType(): ?int
    {
        return $this->type;
    }

    /**
     * @param int|null $type
     */
    public function setType(?int $type): void
    {
        $this->type = $type;
    }

    /**
     * @return null|string
     */
    public function getRelatedPictures(): ?string
    {
        return $this->relatedPictures;
    }

    /**
     * @param null|string $relatedPictures
     */
    public function setRelatedPictures(?string $relatedPictures): void
    {
        $this->relatedPictures = $relatedPictures;
    }

    /**
     * @return string
     */
    public function getExtension(): string
    {
        return $this->extension;
    }

    /**
     * @param string $extension
     */
    public function setExtension(string $extension): void
    {
        $this->extension = $extension;
    }

    /**
     * @return bool|null
     */
    public function getIsDefault(): ?bool
    {
        return $this->isDefault;
    }

    /**
     * @param bool|null $isDefault
     */
    public function setIsDefault(?bool $isDefault): void
    {
        $this->isDefault = $isDefault;
    }

}
