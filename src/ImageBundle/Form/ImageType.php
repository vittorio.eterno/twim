<?php

namespace ImageBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ImageType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',           TextType::class,    array('required'=>true,     'description'=>'Name'))
            ->add('description',    TextType::class,    array('required'=>false,    'description'=>'Description'))
            ->add('image',          FileType::class,    array('required'=>true,     'description'=>'Image'))
        ;
    }

}