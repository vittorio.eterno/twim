<?php

namespace WordBundle\Controller\Admin;


use PurchasingBundle\Repository\TransactionRepository;
use PurchasingBundle\Service\TransactionService;
use Schema\AbstractRenderController as Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use UserBundle\Entity\User;
use Utils\Constants\AdminLimitation;
use WordBundle\Entity\Word;
use WordBundle\Repository\WordRepository;
use WordBundle\Service\WordService;

/**
 * Class WordController
 * @package WordBundle\Controller\Admin
 *
 * @Route("/admin/word")
 */
class WordController extends Controller {

    /**
     * Return a paginated list of all words
     *
     * @Route("/list", name="admin_list_words", methods="GET")
     *
     * @param Request $request
     * @param WordService $wordService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(Request $request,
                               WordService $wordService
    ) {

        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        /**
         * Is admin authorized to this Action?
         */
        if ($currentUser->isAdminLimited() &&
            !$currentUser->isGrantedLimitation(AdminLimitation::AREA_WORDS, array(AdminLimitation::PRIVILEGE_READ))
        ) {
            throw new AccessDeniedException();
        }

        $page       = $request->query->get('p'  , 1);
        $maxResults = $request->query->get('mxr', WordRepository::DEFAULT_MAX_RESULTS);
        $suggest    = $request->query->get('s');

        $formatted  = $wordService->getPaginatedList($page, $suggest, $maxResults);

        return $this->renderByAdmin('WordBundle\list.html.twig',
            array(
                "words"     => $formatted["words"],
                "paginator" => $formatted["paginator"]
            )
        );
    }

    /**
     * Return a word by id
     *
     * @Route("/{id}", name="admin_show_word", methods="GET")
     *
     *
     * @param null|Word $word
     * @param Request $request
     * @param WordService $wordService
     * @param TransactionService $transactionService
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(?Word $word,
                               Request $request,
                               WordService $wordService,
                               TransactionService $transactionService
    ) {

        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        /**
         * Is admin authorized to this Action?
         */
        if ($currentUser->isAdminLimited() &&
            !$currentUser->isGrantedLimitation(AdminLimitation::AREA_WORDS, array(AdminLimitation::PRIVILEGE_READ))
        ) {
            throw new AccessDeniedException();
        }

        $page       = $request->query->get('p'  , 1);
        $maxResults = $request->query->get('mxr', TransactionRepository::DEFAULT_MAX_RESULTS);
        $suggest    = $request->query->get('s');

        $result = array(
            "word"         => array(),
            "paginatedTransactions" => array(),
        );

        if (!is_null($word)) {
            $result["word"] = $word->adminSerializer();
            if ($currentUser->isGrantedLimitation(AdminLimitation::AREA_TRANSACTIONS, array(AdminLimitation::PRIVILEGE_READ))) {
                $result["paginatedTransactions"] = $transactionService->getPaginatedListByWord($word, $page, $suggest, $maxResults);
            }
        }

        return $this->renderByAdmin('WordBundle\show.html.twig', $result);
    }


}
