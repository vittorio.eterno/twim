<?php

namespace WordBundle\Controller;


use UserBundle\Entity\User;
use WordBundle\Entity\Word;
use Utils\Service\RequestInfo;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Utils\StaticUtil\StringUtils;
use UserBundle\Service\UserService;
use WordBundle\Service\WordService;
use PurchasingBundle\Entity\Optional;
use PurchasingBundle\Service\OptionalService;
use Symfony\Component\HttpFoundation\Request;
use PurchasingBundle\Entity\OptionalStructure;
use Symfony\Component\Routing\Annotation\Route;
use Schema\AbstractRenderController as Controller;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Class WordController
 * @package WordBundle\Controller
 *
 * @Route("/word")
 */
class WordController extends Controller {


    //@ParamConverter("word", class="WordBundle:Word", options={"mapping": {"word_name" : "name"}})
    /**
     *
     * @Route("/{word_name}", name="private_word_show", methods="GET")
     *
     * @param string $word_name
     * @param Request $request
     * @param RequestInfo $requestInfo
     * @param TranslatorInterface $translator
     * @param WordService $wordService
     * @param UserService $userService
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(string $word_name, Request $request, RequestInfo $requestInfo, TranslatorInterface $translator, WordService $wordService, UserService $userService) {

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY', null, 'Unable to access this page!');

        $deviceName = $requestInfo->get('deviceName','Desktop');

        /** @var User $currentUser */
        $currentUser = $this->getUser();
        $currentUserResponse = $currentUser->listSerializer();

        if(!StringUtils::checkNameString($word_name)) {
            return $this->renderError(array("message" => $translator->trans("parameter.hash.invalid")));
        }

        $currentUserResponse["isTheKing"] = false;
        $kingUserId = $wordService->getKingUserId();
        if (!is_null($kingUserId) && isset($kingUserId["user_id"]) && $kingUserId["user_id"]==$currentUser->getId()) {
            $currentUserResponse["isTheKing"] = true;
        }

        $wordArray = $wordService->getWordByName($word_name);
        if (is_null($wordArray) || empty($wordArray)) {
            return $this->renderError(array("message" => $translator->trans("word.not.found")));
        }

        $wordArray["editedAt"] = (isset($wordArray["editedAt"]) && !is_null($wordArray["editedAt"]) && $wordArray["editedAt"] instanceof \DateTime ? $wordArray["editedAt"]->format("d/m/Y") : null);

        if (!isset($wordArray["statusPayment"]) || $wordArray["statusPayment"] != Word::STATUS_PAYMENT_APPROVED) {
            return $this->renderError(array("message" => $translator->trans("payment.not.created")));
        }

        $wordArray["formattedOptionals"] = "";
        if (isset($wordArray["optionals"]) && !empty($wordArray["optionals"])) {

            $optionals = json_decode($wordArray["optionals"], true);
            if (isset($optionals["values"]) && count($optionals["values"]) > 0) {
                foreach ($optionals["values"] as $optional) {
                    $wordArray["formattedOptionals"] .= $optional." ";
                }
            }

        }

        $wordArray["sharingUrl"]    = urlencode($this->generateUrl('private_word_show', array("word_name"=>$wordArray["name"]), UrlGeneratorInterface::ABSOLUTE_URL));
        $wordArray["sharingTitle"]  = $translator->trans('certification.sharing.title', array("%username%"=>$currentUser->getUsername(), "%word%"=>$wordArray["name"]));

        $wordArray["isOwner"]       = false;
        if (isset($wordArray['user_id']) && $wordArray['user_id'] == $currentUser->getId()) {
            $wordArray["isOwner"] = true;
        }

        $wordArray["owner"] = array();
        if (isset($wordArray['user_id'])) {
            /** @var User $owner */
            $owner = $userService->getById($wordArray['user_id']);
            $wordArray["owner"] = $owner->listSerializer();
        }
        $wordArray["owner"]["isTheKing"] = false;

        $wordArray["isWordKing"] = false;
        $wordArray["rankPercentageToGoal"] = 0;
        $wordArray["kingValue"] = 0;

        $wordDifference = 0;
        /** @var Word $wordKing */
        $wordKing = $wordService->getKingWord();
        if (!is_null($wordKing)) {
            
            $wordArray["kingValue"] = $wordKing->getValue();

            if (isset($wordArray["id"]) && $wordKing->getId() == $wordArray["id"]) {
                $wordArray["isWordKing"] = true;
                $wordArray["rankPercentageToGoal"] = 100;
            } else {
                $wordArray["rankPercentageToGoal"] = round($wordArray['value']/$wordKing->getValue()*100);
                $wordDifference = $wordKing->getValue() - $wordArray['value'] + 1;
            }
            if($wordKing->getUser()->getId() == $wordArray['user_id']) {
                $wordArray["owner"]["isTheKing"] = true;
            }
        }

        $wordArray["upgradeValue"] = 0;
        if ($wordDifference > 0) {
            $wordArray["upgradeValue"] = $wordDifference;
        }

//        if (!isset($wordArray['user']) || !$wordService->checkCanGet($wordArray['user'], $currentUser)) {
//            return $this->renderError(array("message" => $translator->trans("user.not.rights")));
//        }

        return $this->renderByDevice($deviceName, 'WordBundle\\show.html.twig', array(
            'word' => $wordArray,
            'currentUser' => $currentUserResponse
        ));
    }


    // @ParamConverter("word", class="WordBundle:Word", options={"mapping": {"word_hash" : "hash"}})
    
    /**
     *
     * @Route("/customize/{word_hash}", name="private_word_customize", methods="GET")
     *
     * @param string $word_hash
     * @param Request $request
     * @param RequestInfo $requestInfo
     * @param TranslatorInterface $translator
     * @param WordService $wordService
     * @param UserService $userService
     * @param OptionalService $optionalService
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function customizeAction(string $word_hash, 
                                    Request $request, 
                                    RequestInfo $requestInfo, 
                                    TranslatorInterface $translator, 
                                    WordService $wordService, 
                                    UserService $userService,
                                    OptionalService $optionalService
                                    ) {

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY', null, 'Unable to access this page!');

        $deviceName = $requestInfo->get('deviceName','Desktop');

        if(!StringUtils::checkHashString($word_hash)) {
            return $this->renderError(array("message" => $translator->trans("parameter.hash.invalid")));
        }

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        $wordArray = $wordService->getWordByHash($currentUser, $word_hash);
        if (is_null($wordArray)) {
            return $this->renderError(array("message" => $translator->trans("word.not.found")));
        }

        $formattedOptionals = array();
        $wordArray["formattedOptionals"] = "";

        $wordOptionals = isset($wordArray["optionals"]) ? $wordArray["optionals"]:null;
        if (!is_null($wordOptionals)) {

            $wordOptionalsDecoded = json_decode($wordOptionals, true);
            if (isset($wordOptionalsDecoded["ids"]) && count($wordOptionalsDecoded["ids"])>0) {

                $optionalsObj = $optionalService->getByIds($wordOptionalsDecoded["ids"]);
                if (!empty($optionalsObj)) {

                    /** @var Optional $optional */
                    foreach ($optionalsObj as $optional) {

                        /** @var OptionalStructure $optionalStructure */
                        $optionalStructure = $optional->getOptionalStructure();
                        if (!is_null($optionalStructure)) {
                            
                            if (!array_key_exists($optionalStructure->getId(), $formattedOptionals)) {
                                $formattedOptionals[$optionalStructure->getId()]["structureSource"] = $translator->trans($optionalStructure->getSource(),[],'frontends');
                                $formattedOptionals[$optionalStructure->getId()]["structureId"] = $optionalStructure->getId();
                                $formattedOptionals[$optionalStructure->getId()]["isActive"] = false;
                            }

                            $optionalSerialized = $optional->listSerializer();
                            if (isset($optionalSerialized["source"])) {
                                $optionalSerialized["source"] = $translator->trans($optionalSerialized["source"],[],'frontends');
                            }

                            $optionalSerialized["isActive"] = false;
                            
                            if (isset($wordOptionalsDecoded["actives"]) && $wordOptionalsDecoded["actives"]>0) {
                                foreach ($wordOptionalsDecoded["actives"] as $optArray) {
                                    if (isset($optArray["id"]) && $optArray["id"] == $optionalSerialized["id"]) {
                                        $optionalSerialized["isActive"] = true;
                                        $wordArray["formattedOptionals"] .= isset($optArray["value"]) ? $optArray["value"]." " : ""; 
                                        $formattedOptionals[$optionalStructure->getId()]["isActive"] = true;
                                    }
                                }
                            }

                            $formattedOptionals[$optionalStructure->getId()]["optionals"][] = $optionalSerialized;

                        }
                    }
                }
            }
        } 


        return $this->renderByDevice($deviceName, 'WordBundle\\customize.html.twig', array(
            'word' => $wordArray,
            'customizeOptionals' => $formattedOptionals
        ));

    }


}
