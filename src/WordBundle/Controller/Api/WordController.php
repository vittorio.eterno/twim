<?php

namespace WordBundle\Controller\Api;

use Psr\Log\LoggerInterface;
use PurchasingBundle\Entity\Transaction;
use PurchasingBundle\Form\TransactionType;
use Schema\AbstractRenderController as Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Exceptions\ValidationException;
use Symfony\Component\Translation\TranslatorInterface;
use UserBundle\Entity\User;
use Utils\Service\ResponseUtils;
use Utils\StaticUtil\IntegerUtils;
use Utils\StaticUtil\StringUtils;
use WordBundle\Entity\Word;
use WordBundle\Form\WordType;
use WordBundle\Service\WordService;

/**
 * Class WordController
 * @package WordBundle\Controller
 *
 * @Route("/word")
 */
class WordController extends Controller {



    /**
     * This action is used in ajax call to check in real-time if word inserted it's correct and not already bought
     *
     * @Route("/api/validate", name="word_post_validation", methods="POST")
     *
     *
     * @param Request $request
     * @param TranslatorInterface $translator
     * @param ValidationException $validationException
     * @param WordService $wordService
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function validationAction(Request $request,
                                     TranslatorInterface $translator,
                                     ValidationException $validationException,
                                     WordService $wordService
    ) {

        $this->denyAccessUnlessGranted('ROLE_USER', null, 'Unable to access this page!');

//        /** @var User $currentUser */
//        $currentUser = $this->getUser();

        $status  = 200;
        $message = "";

//        $word = new Word();
//        $form = $this->createForm(WordType::class, $word);

        $transaction = new Transaction();
        $form = $this->createForm(TransactionType::class, $transaction);

        $form->handleRequest($request);

        $word = $transaction->getWord();
        $validator = $this->get('validator');
        $errors    = $validator->validate($word, null, array('ajx_check'));
        if(count($errors) > 0) {
            $status  = 400;
            $message = $validationException->getFormattedExceptions($errors);
        } else {
            if (!$wordService->isAvailable($word)) {
                $status  = 400;
                $message = $translator->trans('word.not.available');
            }
        }

        return $this->json(array("message"=>$message), $status);

    }


    /**
     * This action is used in ajax call to check in real-time if word inserted it's correct and not already bought
     *
     * @Route("/api/check/validation", name="only_word_post_validation", methods="POST")
     *
     *
     * @param Request $request
     * @param TranslatorInterface $translator
     * @param ValidationException $validationException
     * @param WordService $wordService
     * @param LoggerInterface $logger
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function checkValidationAction(Request $request,
                                          TranslatorInterface $translator,
                                          ValidationException $validationException,
                                          WordService $wordService,
                                          LoggerInterface $logger
    ) {

        $response = new ResponseUtils($translator, $logger);

        /** @var User $currentUser */
        $currentUser    = $this->getUser();

        $errorKey       = $request->request->get('errorKey',0);
        $wordName       = $request->request->get('word');
        $submittedToken = $request->request->get('token');

        if (is_null($currentUser)) {
            return $response->getApiResponse(array(), "user.not.rights", 403, array($errorKey=>$translator->trans("user.not.rights")));
        }

        if (!$this->isCsrfTokenValid('hm_word_inp_to_buy', $submittedToken)) {
            return $response->getApiResponse(array(), "parameter.token.invalid", 400, array($errorKey=>$translator->trans("parameter.token.invalid")));
        }

        if (is_null($wordName)) {
            return $response->getApiResponse(array(), "parameters.invalid", 400, array($errorKey=>$translator->trans("parameters.invalid")));
        }

        $validator = $this->get('validator');

        $word = new Word();

        $nameUtf8 = StringUtils::encodeToUtf8(strtolower($wordName));
        $word->setName($nameUtf8);
        $hash     = $wordService->getGeneratedHash($word);
        $word->setHash($hash);
        $errors = $validator->validate($word, null, array("ajx_check"));
        if(count($errors) > 0) {
            $responseError   = array();
            $formattedErrors = $validationException->getFormattedExceptions($errors);
            foreach ($formattedErrors as $formattedError) {
                $responseError[$errorKey] = $formattedError;
                break;
            }
            return $response->getApiResponse(array(), "word.invalid", 400, $responseError);
        } else {
            if (!$wordService->isAvailable($word)) {
                return $response->getApiResponse(array(), "word.not.available", 400, array($errorKey=>$translator->trans("word.not.available")));
            }
        }

        return $response->getApiResponse(array(), "OK");

    }


    /**
     * This action is used in ajax call to check in real-time if word inserted it's correct and not already bought
     *
     * @Route("/api/value/validation", name="only_word_value_validation", methods="POST")
     *
     *
     * @param Request $request
     * @param TranslatorInterface $translator
     * @param WordService $wordService
     * @param LoggerInterface $logger
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function valueValidationAction(Request $request,
                                          TranslatorInterface $translator,
                                          WordService $wordService,
                                          LoggerInterface $logger
    ) {

        $response = new ResponseUtils($translator, $logger);

        /** @var User $currentUser */
        $currentUser    = $this->getUser();

        $errorKey       = $request->request->get('errorKey',0);
        $wordValue      = $request->request->get('value');
        $submittedToken = $request->request->get('token');

        if (is_null($currentUser)) {
            return $response->getApiResponse(array(), "user.not.rights", 403, array($errorKey=>$translator->trans("user.not.rights")));
        }

        if (!$this->isCsrfTokenValid('hm_word_inp_to_buy', $submittedToken)) {
            return $response->getApiResponse(array(), "parameter.token.invalid", 400, array($errorKey=>$translator->trans("parameter.token.invalid")));
        }

        if (!IntegerUtils::checkInt($wordValue)) {
            return $response->getApiResponse(array(), "parameters.invalid", 400, array($errorKey=>$translator->trans("parameters.invalid")));
        }

        $expectedRank = $wordService->getExpectedRank($wordValue);

        return $response->getApiResponse(array(
            "expectedRank" => $expectedRank,
            "newWordValue" => $wordValue
        ), "OK");

    }

}
