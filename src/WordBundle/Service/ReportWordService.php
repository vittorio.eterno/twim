<?php

namespace WordBundle\Service;


use Psr\Log\LoggerInterface;
use Schema\EntityService;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Utils\StaticUtil\LogUtils;
use WordBundle\Repository\ReportWordRepository;

class ReportWordService extends EntityService {


    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * ReportWordService constructor.
     * @param ReportWordRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param LoggerInterface $logger
     */
    public function __construct(ReportWordRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                LoggerInterface $logger
    ) {
        $this->repository = $repository;
        parent::__construct($repository, $authorizationChecker, $logger);
    }

}