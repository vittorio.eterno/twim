<?php

namespace WordBundle\Service;


use Psr\Log\LoggerInterface;
use PurchasingBundle\Entity\Transaction;
use Schema\EntityService;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use UserBundle\Entity\User;
use Utils\Service\CacheService;
use Utils\StaticUtil\LogUtils;
use Utils\StaticUtil\PaginationUtils;
use Utils\StaticUtil\StringUtils;
use WordBundle\Entity\Word;
use WordBundle\Repository\WordRepository;

class WordService extends EntityService {

    /** @var CacheService $cacheService */
    private $cacheService;

    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * WordService constructor.
     * @param WordRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param LoggerInterface $logger
     * @param CacheService $cacheService
     */
    public function __construct(WordRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                LoggerInterface $logger,
                                CacheService $cacheService
    ) {
        $this->repository = $repository;
        parent::__construct($repository, $authorizationChecker, $logger);

        $this->cacheService = $cacheService;
    }


    /**
     * @param User $userWord
     * @param User $user
     * @return bool
     */
    public function checkCanGet (User &$userWord, User &$user) {

        if ($userWord !== $user && (!in_array("ROLE_ADMIN", $user->getRoles()) || !in_array("ROLE_SUPER_ADMIN", $user->getRoles())) ) {
            return false;
        }

        return true;
    }


    /**
     * @param Word $word
     * @return string
     */
    public function getGeneratedHash (Word &$word) {
        $salt = md5("H31 u TH3r3 : W0RD 1s M1N3 ! . - ") . md5($word->getName()) . md5(" Big S@lT! -_:return W i B1g M0n3y!; ");
        return hash("sha256",$salt);
    }

    /**
     * @param Transaction $transaction
     * @return Word
     */
    public function initByTransaction (Transaction &$transaction) {

        /** @var Word $word */
        $word     = $transaction->getWord();
        $nameUtf8 = StringUtils::encodeToUtf8(strtolower($word->getName()));

        $word->setName($nameUtf8);

        $hash     = $this->getGeneratedHash($word);
        $word->setHash($hash);

        return $word;
    }

    /**
     * @param int $page
     * @param null|string $suggest
     * @param int $maxResults
     * @return array|\Doctrine\ORM\Tools\Pagination\Paginator
     */
    public function getAllPaginatedWords ($page=1, ?string $suggest, $maxResults=WordRepository::DEFAULT_MAX_RESULTS) {
        return $this->repository->findAllPaginatedWords($page, $suggest, $maxResults);
    }

    /**
     * @param User $user
     * @param int $page
     * @param null|string $suggest
     * @param int $maxResults
     * @return array|\Doctrine\ORM\Tools\Pagination\Paginator
     */
    public function getAllPaginatedWordsByUser (User &$user, $page=1, ?string $suggest, $maxResults=WordRepository::DEFAULT_MAX_RESULTS) {
        return $this->repository->findAllPaginatedWordsByUser($user, $page, $suggest, $maxResults);
    }

    /**
     * @param int $page
     * @param null|string $suggest
     * @param int $maxResults
     * @return array
     */
    public function getPaginatedList ($page=1, ?string $suggest, $maxResults=WordRepository::DEFAULT_MAX_RESULTS) {
        $words = $this->getAllPaginatedWords($page, $suggest, $maxResults);
        return $this->getFormattedList($words, $page, $maxResults);
    }

    /**
     * @param User $user
     * @param int $page
     * @param null|string $suggest
     * @param int $maxResults
     * @return array
     */
    public function getPaginatedListByUser (User &$user, $page=1, ?string $suggest, $maxResults=WordRepository::DEFAULT_MAX_RESULTS) {
        $words = $this->getAllPaginatedWordsByUser($user, $page, $suggest, $maxResults);
        return $this->getFormattedList($words, $page, $maxResults);
    }

    /**
     * @param $words
     * @param $page
     * @param $maxResults
     * @return array
     */
    private function getFormattedList (&$words,$page,$maxResults) {

        $formatted = array(
            "paginator" => array(),
            "words"     => array());

        if (!empty($words) && count($words)>0) {

            $formatted["paginator"] = PaginationUtils::getPaginatorResponse($words,$page,$maxResults);

            $i = 0;

            foreach ($words as $word) {
                $formatted["words"][$i] = $word;
                $i++;
            }
        }

        return $formatted;

    }

    /**
     * If no result found, the word is available
     *
     * @param Word $word
     * @return bool
     */
    public function isAvailable (Word &$word) {
        $result = $this->repository->findByHash($word);
        return is_null($result);
    }

    /**
     * @param int $value
     * @return mixed|null
     */
    public function getRankByValue ($value=1) {
        return $this->repository->findRankByValue($value);
    }

    /**
     * @param int $value
     * @return int|null
     */
    public function getExpectedRank ($value=1) {

        $result = $this->getRankByValue($value);
        if (!is_null($result) && isset($result["rank"])) {
            return $result["rank"]+1;
        }

        return null;
    }

    /**
     * @return mixed|null
     */
    public function getKingWordValue () {

        $kingWordValue = $this->cacheService->get("king_word_only_value");
        if (is_null($kingWordValue)) {
            $kingWordValue  = $this->repository->findKingWordValue();

            if (isset($kingWordValue["value"])) {
                // Expire after 5 minutes
                $expire = 60*5;
                $this->cacheService->set("king_word_only_value", $kingWordValue["value"], $expire);
                $kingWordValue = $kingWordValue["value"];
            } else {
                $kingWordValue = 0;
            }
        }

        return $kingWordValue;
    }

    /**
     * @return Word|null
     */
    public function getKingWord () {

        $kingWord = $this->cacheService->get("king_word");
        if (is_null($kingWord)) {
            $kingWord  = $this->repository->findKingWord();

            if (!is_null($kingWord)) {
                // Expire after 5 minutes
                $expire = 60*5;
                $this->cacheService->set("king_word", $kingWord, $expire);
            }
        }

        return $kingWord;
    }

    /**
     * @param User $user
     * @param $hash
     * @return mixed|null
     */
    public function getWordByHash (User &$user, $hash) {
        return $this->repository->findWordByHash($user,$hash);
    }

    /**
     * @param $name
     * @return mixed|null
     */
    public function getWordByName ($name) {
        return $this->repository->findWordByName($name);
    }



    /**
     * @return mixed|null
     */
    public function getKingUserId () {

        $kingUserId = $this->cacheService->get("king_user_id");
        if (is_null($kingUserId)) {
            $kingUserId  = $this->repository->findKingUserId();

            if (!is_null($kingUserId)) {
                // Expire after 5 minutes
                $expire = 60*5;
                $this->cacheService->set("king_user_id", $kingUserId, $expire);
            }
        }

        return $kingUserId;
    }


}