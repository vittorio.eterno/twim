<?php

namespace WordBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use WordBundle\Entity\Word;


class WordType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('name', TextType::class, array(
                'required' => true,
                'attr'     => array(
                    'class' => 'cls_word_name_inpt'
                )
            ))
            ->add('value', IntegerType::class, array(
                'required' => true
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class'    => Word::class,
            'validation_groups' => array('ajx_check'),
        ));
    }

}