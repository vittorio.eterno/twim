<?php

namespace WordBundle\Entity;

use Schema\Entity;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use UserBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Word
 *
 * @ORM\Table(name="words",
 *     indexes={@ORM\Index(name="word_idx", columns={"name","hash"})},
 *     )
 * @ORM\Entity(repositoryClass="WordBundle\Repository\WordRepository")
 */
class Word extends Entity {

    //Add status create_pending
    //Add status created => pagata!

    const STATUS_CHECK_PENDING                 = 0; // to be approved from admin
    const STATUS_CHECK_APPROVED                = 1; // approved from admin
    const STATUS_CHECK_REFUSED                 = 2; // refused from admin
    const STATUS_CHECK_BANNED                  = 3; // banned from admin

    const STATUS_CHECK_PENDING_SOURCE          = "word.status.pending";
    const STATUS_CHECK_APPROVED_SOURCE         = "word.status.approved";
    const STATUS_CHECK_REFUSED_SOURCE          = "word.status.refused";
    const STATUS_CHECK_BANNED_SOURCE           = "word.status.banned";


    const STATUS_PAYMENT_PENDING               = 0;
    const STATUS_PAYMENT_APPROVED              = 1;
    const STATUS_PAYMENT_REFUSED               = 2;
    const STATUS_PAYMENT_RESERVED              = 3;

    const STATUS_PAYMENT_PENDING_SOURCE        = "word.status.pending";
    const STATUS_PAYMENT_APPROVED_SOURCE       = "word.status.approved";
    const STATUS_PAYMENT_REFUSED_SOURCE        = "word.status.refused";
    const STATUS_PAYMENT_RESERVED_SOURCE       = "word.status.reserved";


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Many Words have One User.
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=80)
     * @Assert\NotBlank(message = "require.word", groups={"word_type","ajx_check"})
     * @Assert\Regex(
     *     pattern="/^[\p{L}]+$/u",
     *     message="invalid.word",
     *     groups={"word_type","ajx_check"}
     * )
     * @Assert\Length(
     *     min = 1,
     *     max = 80,
     *     minMessage = "min.length.word",
     *     maxMessage = "max.length.word",
     *     groups={"word_type","ajx_check"}
     * )
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=200)
     */
    private $hash;

    /**
     * @var string|null
     *
     * @ORM\Column(name="optionals", type="text", nullable=true)
     */
    private $optionals;

    /**
     * @var float|null
     *
     * @ORM\Column(name="value", type="float", nullable=true, options={"default":0})
     * @Assert\NotBlank(message = "require.value", groups={"word_type","word_value_type"})
     */
    private $value;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_verified", type="boolean", nullable=true, options={"default":0})
     */
    private $isVerified;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="edited_at", type="datetime")
     */
    private $editedAt;

    /**
     * @var int|null
     *
     * @ORM\Column(name="status_check", type="smallint", nullable=true, options={"default":0})
     */
    private $statusCheck;

    /**
     * @var int|null
     *
     * @ORM\Column(name="status_payment", type="smallint", nullable=true, options={"default":0})
     */
    private $statusPayment;


    ################################################# SERIALIZER FUNCTIONS

    /**
     * Returns the array of fields to serialize in entity administration view.
     * @return array
     */
    public function adminSerializer() {
        $view_vars = $this->viewSerializer();

        $admin_vars = array(
            'user'         => $this->serializedUser()
        );

        return array_merge($view_vars, $admin_vars);
    }

    /**
     * Returns the array of fields to serialize in entity view.
     * @return array
     */
    public function viewSerializer() {
        $list_vars = $this->listSerializer();

        $view_vars = array(
        );

        return array_merge($list_vars, $view_vars);
    }

    /**
     * Returns the array of fields to serialize in a list of this entity.
     * @return array
     */
    public function listSerializer() {
        $list_vars = array(
            'id'                => $this->serializedId(),
            'name'              => $this->serializedName(),
            'value'             => $this->serializedValue(),
            'optionals'         => $this->serializedOptionals(),
            'statusCheck'       => $this->serializedStatusCheckSource(),
            'statusPayment'     => $this->serializedStatusPaymentSource(),
            'editedAt'          => $this->serializedEditedAt(),
            'isVerified'        => $this->serializedIsVerified(),
        );
        return $list_vars;
    }

    /**
     * Returns the hash code unique identifier of the entity.
     * @return string
     */
    public function hashCode() {
        // TODO: Implement hashCode() method.
    }

    ################################################# SERIALIZED FUNCTIONS

    /**
     * Word id
     * @JMS\VirtualProperty
     * @JMS\SerializedName("id")
     * @JMS\Type("integer")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedId() {
        return (is_null($this->id)?null:$this->id);
    }

    /**
     * Word name
     * @JMS\VirtualProperty
     * @JMS\SerializedName("name")
     * @JMS\Type("string")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedName() {
        return (is_null($this->name)?null:$this->name);
    }

    /**
     * Word optionals
     * @JMS\VirtualProperty
     * @JMS\SerializedName("optionals")
     * @JMS\Type("string")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedOptionals() {
        return (is_null($this->optionals)?null:$this->optionals);
    }

    /**
     * Word value
     * @JMS\VirtualProperty
     * @JMS\SerializedName("value")
     * @JMS\Type("float")
     * @JMS\Groups({"admin"})
     * @JMS\Since("1.0.x")
     */
    public function serializedValue() {
        return (is_null($this->value)?0:$this->value);
    }

    /**
     * Word isVerified
     * @JMS\VirtualProperty
     * @JMS\SerializedName("isVerified")
     * @JMS\Type("boolean")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedIsVerified()
    {
        return (is_null($this->isVerified)?false:$this->isVerified);
    }

    /**
     * Word statusCheck
     * @JMS\VirtualProperty
     * @JMS\SerializedName("statusCheck")
     * @JMS\Type("integer")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedStatusCheck() {
        return (is_null($this->statusCheck)?0:$this->statusCheck);
    }

    /**
     * Word statusCheckSource
     * @JMS\VirtualProperty
     * @JMS\SerializedName("statusCheckSource")
     * @JMS\Type("string")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedStatusCheckSource() {

        switch ($this->statusCheck) {
            case self::STATUS_CHECK_APPROVED:
                $statusCheck = self::STATUS_CHECK_APPROVED_SOURCE;
                break;
            case self::STATUS_CHECK_REFUSED:
                $statusCheck = self::STATUS_CHECK_REFUSED_SOURCE;
                break;
            case self::STATUS_CHECK_BANNED:
                $statusCheck = self::STATUS_CHECK_BANNED_SOURCE;
                break;
            default:
                $statusCheck = self::STATUS_CHECK_PENDING_SOURCE;
        }

        return $statusCheck;
    }

    /**
     * Word statusPaymentSource
     * @JMS\VirtualProperty
     * @JMS\SerializedName("statusPaymentSource")
     * @JMS\Type("string")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedStatusPaymentSource() {

        switch ($this->statusPayment) {
            case self::STATUS_PAYMENT_APPROVED:
                $statusPayment = self::STATUS_PAYMENT_APPROVED_SOURCE;
                break;
            case self::STATUS_PAYMENT_REFUSED:
                $statusPayment = self::STATUS_PAYMENT_REFUSED_SOURCE;
                break;
            case self::STATUS_PAYMENT_RESERVED:
                $statusPayment = self::STATUS_PAYMENT_RESERVED_SOURCE;
                break;
            default:
                $statusPayment = self::STATUS_PAYMENT_PENDING_SOURCE;
        }

        return $statusPayment;
    }

    /**
     * user
     * @JMS\VirtualProperty
     * @JMS\SerializedName("user")
     * @JMS\Type("array")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedUser () {
        return (!is_null($this->getUser()) && $this->getUser() instanceof User ? $this->getUser()->listSerializer() : null);
    }

    /**
     * editedAt
     * @JMS\VirtualProperty
     * @JMS\SerializedName("edited")
     * @JMS\Type("string")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedEditedAt() {
        return (!is_null($this->editedAt) && $this->editedAt instanceof \DateTime ? $this->editedAt->format("Y-m-d H:i:s") : null);
    }

    ################################################# GETTERS AND SETTERS FUNCTIONS


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Word
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getHash(): string
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash(string $hash): void
    {
        $this->hash = $hash;
    }


    /**
     * Set value.
     *
     * @param float|null $value
     *
     * @return Word
     */
    public function setValue($value = null)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value.
     *
     * @return float|null
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return null|string
     */
    public function getOptionals(): ?string
    {
        return $this->optionals;
    }

    /**
     * @param null|string $optionals
     */
    public function setOptionals(?string $optionals): void
    {
        $this->optionals = $optionals;
    }

    /**
     * @return bool|null
     */
    public function getIsVerified(): ?bool
    {
        return $this->isVerified;
    }

    /**
     * @param bool|null $isVerified
     */
    public function setIsVerified(?bool $isVerified): void
    {
        $this->isVerified = $isVerified;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Word
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set editedAt.
     *
     * @param \DateTime $editedAt
     *
     * @return Word
     */
    public function setEditedAt($editedAt)
    {
        $this->editedAt = $editedAt;

        return $this;
    }

    /**
     * Get editedAt.
     *
     * @return \DateTime
     */
    public function getEditedAt()
    {
        return $this->editedAt;
    }


    /**
     * @return int|null
     */
    public function getStatusCheck(): ?int
    {
        return $this->statusCheck;
    }

    /**
     * @param int|null $statusCheck
     */
    public function setStatusCheck(?int $statusCheck): void
    {
        $this->statusCheck = $statusCheck;
    }

    /**
     * @return int|null
     */
    public function getStatusPayment(): ?int
    {
        return $this->statusPayment;
    }

    /**
     * @param int|null $statusPayment
     */
    public function setStatusPayment(?int $statusPayment): void
    {
        $this->statusPayment = $statusPayment;
    }

}
