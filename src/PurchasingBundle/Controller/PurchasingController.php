<?php

namespace PurchasingBundle\Controller;

use Schema\AbstractRenderController as Controller;
use Symfony\Component\Routing\Annotation\Route;

class PurchasingController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->render('PurchasingBundle:Default:home.html.twig');
    }
}
