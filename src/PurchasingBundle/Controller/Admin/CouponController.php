<?php

namespace PurchasingBundle\Controller\Admin;


use PurchasingBundle\Entity\Coupon;
use PurchasingBundle\Form\CouponType;
use PurchasingBundle\Repository\CouponRepository;
use Schema\AbstractRenderController as Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use UserBundle\Entity\User;
use Utils\Constants\AdminLimitation;
use PurchasingBundle\Service\CouponService;


/**
 * Class CouponController
 * @package PurchasingBundle\Controller\Admin
 *
 * @Route("/admin/coupon")
 */
class CouponController extends Controller {

    /**
     * Return a paginated list of all coupons
     *
     * @Route("/list", name="admin_list_coupons", methods="GET")
     *
     * @param Request $request
     * @param CouponService $couponService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(Request $request,
                               CouponService $couponService
    ) {

        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        /**
         * Is admin authorized to this Action?
         */
        if ($currentUser->isAdminLimited() &&
            !$currentUser->isGrantedLimitation(AdminLimitation::AREA_COUPONS, array(AdminLimitation::PRIVILEGE_READ))
        ) {
            throw new AccessDeniedException();
        }

        $page       = $request->query->get('p'  , 1);
        $maxResults = $request->query->get('mxr', CouponRepository::DEFAULT_MAX_RESULTS);
        $suggest    = $request->query->get('s');

        $formatted  = $couponService->getPaginatedList($page, $suggest, $maxResults);

        return $this->renderByAdmin('PurchasingBundle\Coupon\list.html.twig',
            array(
                "coupons"   => $formatted["coupons"],
                "paginator" => $formatted["paginator"]
            )
        );
    }


    /**
     * Admin form to create/edit a Coupon
     *
     * @Route("/save", name="admin_create_coupon", methods={"GET","POST"})
     * @Route("/save/{id}", name="admin_edit_coupon", methods={"GET","POST"})
     *
     * @param null|Coupon $coupon
     * @param Request $request
     * @param CouponService $couponService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function saveAction(?Coupon $coupon,
                               Request $request,
                               CouponService $couponService
    ) {

        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        /**
         * Is admin authorized to this Action?
         */
        if ($currentUser->isAdminLimited() &&
            !$currentUser->isGrantedLimitation(AdminLimitation::AREA_COUPONS, array(AdminLimitation::PRIVILEGE_CREATE, AdminLimitation::PRIVILEGE_UPDATE, AdminLimitation::PRIVILEGE_DELETE))
        ) {
            throw new AccessDeniedException();
        }

        if (is_null($coupon)) {
            $coupon    = new Coupon();
        }

        $form = $this->createForm(CouponType::class, $coupon);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $result = $couponService->saveCoupon($currentUser, $coupon);
            if (is_null($result)) {
                $this->addFlash(
                    'error',
                    'Error on save!'
                );
            }

            return $this->redirectToRoute('admin_list_coupons');
        }

        return $this->renderByAdmin('PurchasingBundle\Coupon\save.html.twig',
            array(
                'form' => $form->createView()
            )
        );
    }


    /**
     * Return a coupon by id
     *
     * @Route("/{id}", name="admin_show_coupon", methods="GET")
     *
     * @param Coupon|null $coupon
     * @param CouponService $couponService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(?Coupon $coupon,
                               CouponService $couponService
    ) {

        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        /**
         * Is admin authorized to this Action?
         */
        if ($currentUser->isAdminLimited() &&
            !$currentUser->isGrantedLimitation(AdminLimitation::AREA_COUPONS, array(AdminLimitation::PRIVILEGE_READ))
        ) {
            throw new AccessDeniedException();
        }

        if (!is_null($coupon)) {
            $coupon = $coupon->adminSerializer();
        }

        return $this->renderByAdmin('PurchasingBundle\Coupon\show.html.twig', array(
            "coupon" => $coupon
        ));
    }

}
