<?php

namespace PurchasingBundle\Controller\Admin;


use Psr\Log\LoggerInterface;
use PurchasingBundle\Entity\Coupon;
use PurchasingBundle\Entity\Transaction;
use PurchasingBundle\Form\EditTransactionType;
use PurchasingBundle\Form\TransactionType;
use PurchasingBundle\Service\CouponService;
use PurchasingBundle\Service\OptionalService;
use PurchasingBundle\Service\PackageService;
use PurchasingBundle\Service\TransactionService;
use Schema\AbstractRenderController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\TranslatorInterface;
use UserBundle\Entity\User;
use WordBundle\Entity\Word;
use WordBundle\Service\WordService;


/**
 * Class TransactionController
 * @package PurchasingBundle\Controller\Admin
 *
 * @Route("/admin/transaction")
 */
class TransactionController extends Controller {

    /**
     * @Route("/create", name="admin_transaction_create", methods={"GET","POST"})
     *
     * @param Request $request
     * @param WordService $wordService
     * @param CouponService $couponService
     * @param TranslatorInterface $translator
     * @param PackageService $packageService
     * @param OptionalService $optionalService
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request,
                                 WordService $wordService,
                                 CouponService $couponService,
                                 TranslatorInterface $translator,
                                 PackageService $packageService,
                                 OptionalService $optionalService
    ) {

        //TODO: se sarà da dividere in più step, preparare diverse chiamate per ogni step, e non far procedere se non passano le validazioni, se passano salvare $form->getData() serializzato in sessione

        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        if ($currentUser->isAdminLimited()) {
            $this->addFlash(
                'error',
                'User not rights!'
            );
            return $this->redirectToRoute('admin_list_words');
        }

        /**
         * Get all formatted Packages and Optionals
         */
        $listPackages = $packageService->getFormattedPublicActivePackages();
        $listOptionals = $optionalService->getFormattedActiveOptionals();

        $transaction = new Transaction();
        $form = $this->createForm(TransactionType::class, $transaction);

        $form->handleRequest($request);

        $formData = array();

        if ($form->isSubmitted()) {

            /** @var Word $word */
            $word = $wordService->initByTransaction($transaction);
            if (!$wordService->isAvailable($word)) {
                $message = $translator->trans('word.not.available');
                $form->get('word')->get('name')->addError(new FormError($message));
            }

            $transactionRequest = $request->request->get("transaction");

            $formData["couponCode"] = isset($transactionRequest["couponCode"])?$transactionRequest["couponCode"]:null;
            //TODO: forse è meglio dare la possibilità di selezionare solo un pacchetto?? Update dei controlli e del salvataggio
            $formData["packages"]   = isset($transactionRequest["packages"])?$transactionRequest["packages"]:null;
            $formData["optionals"]  = isset($transactionRequest["optionals"])?$transactionRequest["optionals"]:null;

            $coupon = null;
            if (!empty($formData["couponCode"])) {
                /** @var Coupon $coupon */
                $coupon = $couponService->getByCode($formData["couponCode"]);
                if (is_null($coupon)) {
                    $message = $translator->trans('coupon.not.found');
                    $form->get('couponCode')->addError(new FormError($message));
                }
            }

            $formData['errors'] = array();

        }

        return $this->renderByAdmin('PurchasingBundle\Transaction\create.html.twig',
            array(
                'form'      => $form->createView(),
                'coupons'   => $couponService->getFormattedSpecials(),
                'packages'  => $listPackages,
                'optionals' => $listOptionals,
                'formData'  => $formData,
            )
        );
    }


    /**
     * @Route("/edit/{word_id}", name="admin_transaction_edit", methods={"GET","POST"})
     * @ParamConverter("word", class="WordBundle:Word", options={"id" = "word_id"})
     *
     * @param Word|null $word
     * @param Request $request
     * @param WordService $wordService
     * @param CouponService $couponService
     * @param TranslatorInterface $translator
     * @param PackageService $packageService
     * @param OptionalService $optionalService
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(?Word $word,
                               Request $request,
                               WordService $wordService,
                               CouponService $couponService,
                               TranslatorInterface $translator,
                               PackageService $packageService,
                               OptionalService $optionalService
    ) {

        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');

        if (is_null($word)) {
            $this->addFlash(
                'error',
                'Word not found!'
            );
            return $this->redirectToRoute('admin_list_words');
        }

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        if ($currentUser->isAdminLimited()) {
            $this->addFlash(
                'error',
                'User not rights!'
            );
            return $this->redirectToRoute('admin_list_words');
        }

        /**
         * Get all formatted Packages and Optionals
         */
        $listPackages = $packageService->getFormattedPublicActivePackages();
        $listOptionals = $optionalService->getFormattedActiveOptionals();

        $transaction = new Transaction();
//        $transaction->setWord($word);
        $form = $this->createForm(EditTransactionType::class, $transaction);

        $form->handleRequest($request);

        $formData = array();

        if ($form->isSubmitted()) {

            /** @var Word $word */
            $word = $wordService->initByTransaction($transaction);
            if (!$wordService->isAvailable($word)) {
                $message = $translator->trans('word.not.available');
                $form->get('word')->get('name')->addError(new FormError($message));
            }

            $transactionRequest = $request->request->get("transaction");

            $formData["couponCode"] = isset($transactionRequest["couponCode"])?$transactionRequest["couponCode"]:null;
            //TODO: forse è meglio dare la possibilità di selezionare solo un pacchetto?? Update dei controlli e del salvataggio
            $formData["packages"]   = isset($transactionRequest["packages"])?$transactionRequest["packages"]:null;
            $formData["optionals"]  = isset($transactionRequest["optionals"])?$transactionRequest["optionals"]:null;

            $coupon = null;
            if (!empty($formData["couponCode"])) {
                /** @var Coupon $coupon */
                $coupon = $couponService->getByCode($formData["couponCode"]);
                if (is_null($coupon)) {
                    $message = $translator->trans('coupon.not.found');
                    $form->get('couponCode')->addError(new FormError($message));
                }
            }

            $formData['errors'] = array();

        }

        return $this->renderByAdmin('PurchasingBundle\Transaction\edit.html.twig',
            array(
                'form'      => $form->createView(),
                'coupons'   => $couponService->getFormattedSpecials(),
                'packages'  => $listPackages,
                'optionals' => $listOptionals,
                'formData'  => $formData,
                'word'      => $word->listSerializer(),
            )
        );
    }


}
