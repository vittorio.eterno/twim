<?php

namespace PurchasingBundle\Controller\Api;


use Psr\Log\LoggerInterface;
use PurchasingBundle\Entity\Coupon;
use PurchasingBundle\Service\CouponService;
use Schema\AbstractRenderController as Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\TranslatorInterface;
use UserBundle\Entity\User;
use Utils\Service\ResponseUtils;


class CouponController extends Controller {


    /**
     * This action is used in ajax call to check in real-time if optionals selected are corrects
     *
     * @Route("/api/coupon/validation", name="api_coupon_validation", methods="POST")
     *
     *
     * @param Request $request
     * @param TranslatorInterface $translator
     * @param CouponService $couponService
     * @param LoggerInterface $logger
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function validationAction(Request $request,
                                     TranslatorInterface $translator,
                                     CouponService $couponService,
                                     LoggerInterface $logger
    ) {

        $response = new ResponseUtils($translator, $logger);

        /** @var User $currentUser */
        $currentUser    = $this->getUser();

        $errorKey       = $request->request->get('errorKey',0);
        $couponCode     = $request->request->get('coupon');

        if (is_null($currentUser)) {
            return $response->getApiResponse(array(), "user.not.rights", 403, array($errorKey=>$translator->trans("user.not.rights")));
        }

        $coupon = null;
        if (!empty($couponCode)) {
            /** @var Coupon $coupon */
            $coupon = $couponService->getByCode($couponCode);
            if (is_null($coupon)) {
                return $response->getApiResponse(array(), "coupon.not.found", 400, array($errorKey=>$translator->trans("coupon.not.found")));
            }
        }

        $discountPercentage = !is_null($coupon) ? $coupon->getDiscountPercentage() : null;

        return $response->getApiResponse(array(
            "discountPercentage" => $discountPercentage,
            "couponCode" => $couponCode,
        ), "OK");

    }
}