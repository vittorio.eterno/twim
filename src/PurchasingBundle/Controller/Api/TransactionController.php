<?php

namespace PurchasingBundle\Controller\Api;


use Exceptions\ValidationException;
use ImageBundle\Entity\PictureFrame;
use ImageBundle\Service\PictureFrameService;
use Psr\Log\LoggerInterface;
use PurchasingBundle\Entity\Coupon;
use PurchasingBundle\Entity\Optional;
use PurchasingBundle\Entity\Transaction;
use PurchasingBundle\Form\EditTransactionType;
use PurchasingBundle\Form\TransactionType;
use PurchasingBundle\Service\CouponService;
use PurchasingBundle\Service\OptionalService;
use PurchasingBundle\Service\TransactionService;
use ResourceBundle\Entity\Country;
use ResourceBundle\Service\CountryService;
use Schema\AbstractRenderController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use UserBundle\Entity\ShippingAddress;
use UserBundle\Entity\User;
use UserBundle\Service\ShippingAddressService;
use Utils\Service\ResponseUtils;
use Utils\Service\RestPayPal;
use Utils\StaticUtil\EmailUtils;
use Utils\StaticUtil\IntegerUtils;
use Utils\StaticUtil\StringUtils;
use Utils\Validation\InvoiceValidation;
use Utils\Validation\PackageOptionalValidation;
use WordBundle\Entity\Word;
use WordBundle\Service\WordService;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


/**
 * Class TransactionController
 * @package PurchasingBundle\Controller
 */
class TransactionController extends Controller {


    /**
     * @Route("/api/admin/transaction/create-payment/", name="paypal_create_payment", methods={"POST"})
     *
     *
     * @param Request $request
     * @param WordService $wordService
     * @param CouponService $couponService
     * @param TranslatorInterface $translator
     * @param TransactionService $transactionService
     * @param RestPayPal $restPayPal
     * @param PackageOptionalValidation $packageOptionalValidation
     * @param LoggerInterface $logger
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function adminPaypalCreateAction(Request $request,
                                            WordService $wordService,
                                            CouponService $couponService,
                                            TranslatorInterface $translator,
                                            TransactionService $transactionService,
                                            RestPayPal $restPayPal,
                                            PackageOptionalValidation $packageOptionalValidation,
                                            LoggerInterface $logger
    ) {

        $status  = 400;
        $message = "";
        $result  = array();
        $errors  = array();

        $transaction = new Transaction();
        $form = $this->createForm(TransactionType::class, $transaction);

        $form->handleRequest($request);

        $formData = array();

        if ($form->isSubmitted()) {

            /** @var Word $word */
            $word = $wordService->initByTransaction($transaction);
            if (!$wordService->isAvailable($word)) {
                $message = $translator->trans('word.not.available');
                $form->get('word')->get('name')->addError(new FormError($message));
            }

            $transactionRequest = $request->request->get("transaction");

            $formData["couponCode"] = isset($transactionRequest["couponCode"])?$transactionRequest["couponCode"]:null;
            $formData["packages"]   = isset($transactionRequest["packages"])?$transactionRequest["packages"]:null;
            $formData["optionals"]  = isset($transactionRequest["optionals"])?$transactionRequest["optionals"]:null;

            $coupon = null;
            if (!empty($formData["couponCode"])) {
                /** @var Coupon $coupon */
                $coupon = $couponService->getByCode($formData["couponCode"]);
                if (is_null($coupon)) {
                    $message = $translator->trans('coupon.not.found');
                    $form->get('couponCode')->addError(new FormError($message));
                }
            }


            $formData['errors'] = array();
            $packagesOptionals  = array("optionals"=>array(),"packages"=>array());

            if ($form->isValid()) {

                if (!$packageOptionalValidation->validatePackagesOptionals($formData['errors'], $packagesOptionals, $formData["packages"], $formData["optionals"], "transaction")) {
                    if (count($formData['errors']) > 0) {
                        $errors = $formData['errors'];
                    }
                } else {
                    $result = $restPayPal->createPayment();

                    if (!is_null($result)) {
                        $result = json_decode($result,true);

                        if (isset($result["id"])) {

                            $status = 200;

                        } else {
                            $result = array();
                            $errors = array("general_error_id" => $translator->trans('system.error.try.again'));
                        }

                    } else {
                        $result = array();
                        $errors = array("general_error_id" => $translator->trans('system.error.try.again'));
                    }
                }

            } else {
                $errors = $this->getFormattedErrors("transaction", $form);
            }


        }

        return $this->json(array("message"=>$message, "errors"=>$errors, "result"=>$result), $status);

    }

    /**
     * @Route("/api/admin/transaction/execute-payment/", name="paypal_execute_payment", methods={"POST"})
     *
     *
     * @param Request $request
     * @param WordService $wordService
     * @param CouponService $couponService
     * @param TranslatorInterface $translator
     * @param TransactionService $transactionService
     * @param RestPayPal $restPayPal
     * @param PackageOptionalValidation $packageOptionalValidation
     * @param LoggerInterface $logger
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function adminPaypalExecuteAction(Request $request,
                                             WordService $wordService,
                                             CouponService $couponService,
                                             TranslatorInterface $translator,
                                             TransactionService $transactionService,
                                             RestPayPal $restPayPal,
                                             PackageOptionalValidation $packageOptionalValidation,
                                             LoggerInterface $logger
    ) {

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        $status  = 400;
        $message = "";
        $result  = array();
        $errors  = array();

        $paymentId  = $request->request->get("paymentID");
        $payerId    = $request->request->get("payerID");


        $transaction = new Transaction();
        $form = $this->createForm(TransactionType::class, $transaction);

        $form->handleRequest($request);

        $formData = array();

        if (is_null($paymentId) || is_null($payerId) || !$form->isSubmitted()) {
            $result = array();
            $errors = array("general_error_id" => $translator->trans('system.error.try.again'));
        } else {


            /** @var Word $word */
            $word = $wordService->initByTransaction($transaction);
            if (!$wordService->isAvailable($word)) {
                $message = $translator->trans('word.not.available');
                $form->get('word')->get('name')->addError(new FormError($message));
            }

            $transactionRequest = $request->request->get("transaction");

            $formData["couponCode"] = isset($transactionRequest["couponCode"])?$transactionRequest["couponCode"]:null;
            $formData["packages"]   = isset($transactionRequest["packages"])?$transactionRequest["packages"]:null;
            $formData["optionals"]  = isset($transactionRequest["optionals"])?$transactionRequest["optionals"]:null;

            $coupon = null;
            if (!empty($formData["couponCode"])) {
                /** @var Coupon $coupon */
                $coupon = $couponService->getByCode($formData["couponCode"]);
                if (is_null($coupon)) {
                    $message = $translator->trans('coupon.not.found');
                    $form->get('couponCode')->addError(new FormError($message));
                }
            }

            $formData['errors'] = array();
            $packagesOptionals  = array("optionals"=>array(),"packages"=>array());

//            /** @var Transaction $transaction */
//            $transaction = $transactionService->getByPaymentId($paymentId);
//            if (is_null($transaction)) {
//                $result = array();
//                $errors = array("general_error_id" => $translator->trans('transaction.not.found'));
//            } else {

//                /**
//                 * Check if user is different when before execute the transaction
//                 */
//                if ($transaction->getUser()->getId() != $currentUser->getId()) {
//                    $status = 403;
//                    $result = array();
//                    $errors = array("general_error_id" => $translator->trans('user.not.rights'));
//                } elseif ($transaction->getPpStatus() == Transaction::PP_STATUS_APPROVED) {
//                    $status = 400;
//                    $result = array();
//                    $errors = array("general_error_id" => $translator->trans('transaction.already.paid'));
//                } else {



            if ($form->isValid()) {

                if (!$packageOptionalValidation->validatePackagesOptionals($formData['errors'], $packagesOptionals, $formData["packages"], $formData["optionals"], "transaction")) {
                    if (count($formData['errors']) > 0) {
                        $errors = $formData['errors'];
                    }
                } else {

                    $result = $restPayPal->execPayment($paymentId,$payerId);
                    if (!is_null($result)) {

                        $result = json_decode($result,true);

                        if (isset($result["id"])) {

                            $status = 200;

                            $logger->info("PayPal payment created by user: ".$currentUser->getId(), $result);


                            $transaction = $transactionService->create($paymentId, $payerId, $currentUser, $transaction, $result, $coupon, $packagesOptionals);

//                            $transaction = $transactionService->updateExecPayment($payerId, $transaction, $result);
                            if (is_null($transaction)) {
//                                $logger->error("Failed transaction update id: ".$transactionId);
//                                $status = 500;
//                                $result = array();
//                                $errors = array("general_error_id" => $translator->trans('system.error.try.again'));

                                $logger->error("Failed transaction created by user: ".$currentUser->getId());
                                $status = 500;
                                $result = array();
                                $errors = array("general_error_id" => $translator->trans('system.error.try.again'));

                            } else {
                                /** @var Word $word */
                                $word = $transaction->getWord();
                                $this->addFlash(
                                    'success',
                                    $translator->trans('congrats.word.bought',array("%word%"=>$word->getName()))
                                );

                                $url = $this->generateUrl('admin_show_user', array('id' => $currentUser->getId()), UrlGeneratorInterface::ABSOLUTE_URL);
                                $result["url"] = $url;
                            }

                        } else {
                            $result = array();
                            $errors = array("general_error_id" => $translator->trans('system.error.try.again'));
                        }

                    } else {
                        $result = array();
                        $errors = array("general_error_id" => $translator->trans('system.error.try.again'));
                    }

                }

            } else {
                $errors = $this->getFormattedErrors("transaction", $form);
            }


//                }

//            }

        }


        return $this->json(array("message"=>$message, "errors"=>$errors, "result"=>$result), $status);
    }




    /**
     * @Route("/api/admin/transaction/upgrade-payment/{word_id}", name="paypal_upgrade_payment", methods={"POST"})
     * @ParamConverter("word", class="WordBundle:Word", options={"id" = "word_id"})
     *
     *
     * @param Word|null $word
     * @param Request $request
     * @param WordService $wordService
     * @param CouponService $couponService
     * @param TranslatorInterface $translator
     * @param TransactionService $transactionService
     * @param RestPayPal $restPayPal
     * @param PackageOptionalValidation $packageOptionalValidation
     * @param LoggerInterface $logger
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function adminPaypalUpgradeAction(?Word $word,
                                             Request $request,
                                             WordService $wordService,
                                             CouponService $couponService,
                                             TranslatorInterface $translator,
                                             TransactionService $transactionService,
                                             RestPayPal $restPayPal,
                                             PackageOptionalValidation $packageOptionalValidation,
                                             LoggerInterface $logger
    ) {

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        $status  = 400;
        $message = "";
        $result  = array();
        $errors  = array();

        if (is_null($word)) {
            $this->addFlash(
                'error',
                $translator->trans('word.not.found')
            );

            $url = $this->generateUrl('admin_show_user', array('id' => $currentUser->getId()), UrlGeneratorInterface::ABSOLUTE_URL);
            $errors["url"] = $url;

        } else {

            $transaction = new Transaction();
            $form = $this->createForm(EditTransactionType::class, $transaction);

            $form->handleRequest($request);

            $formData = array();

            if ($form->isSubmitted()) {

                $transactionRequest = $request->request->get("transaction");

                $formData["couponCode"] = isset($transactionRequest["couponCode"])?$transactionRequest["couponCode"]:null;
                $formData["packages"]   = isset($transactionRequest["packages"])?$transactionRequest["packages"]:null;
                $formData["optionals"]  = isset($transactionRequest["optionals"])?$transactionRequest["optionals"]:null;

                $coupon = null;
                if (!empty($formData["couponCode"])) {
                    /** @var Coupon $coupon */
                    $coupon = $couponService->getByCode($formData["couponCode"]);
                    if (is_null($coupon)) {
                        $message = $translator->trans('coupon.not.found');
                        $form->get('couponCode')->addError(new FormError($message));
                    }
                }


                $formData['errors'] = array();
                $packagesOptionals  = array("optionals"=>array(),"packages"=>array());

                if ($form->isValid()) {

                    if (!$packageOptionalValidation->validatePackagesOptionals($formData['errors'], $packagesOptionals, $formData["packages"], $formData["optionals"], "transaction")) {
                        if (count($formData['errors']) > 0) {
                            $errors = $formData['errors'];
                        }
                    } else {
                        $result = $restPayPal->createPayment();

                        if (!is_null($result)) {
                            $result = json_decode($result,true);

                            if (isset($result["id"])) {

                                $status = 200;

                            } else {
                                $result = array();
                                $errors = array("general_error_id" => $translator->trans('system.error.try.again'));
                            }

                        } else {
                            $result = array();
                            $errors = array("general_error_id" => $translator->trans('system.error.try.again'));
                        }
                    }

                } else {
                    $errors = $this->getFormattedErrors("edit_transaction", $form);
                }


            }
        }

        return $this->json(array("message"=>$message, "errors"=>$errors, "result"=>$result), $status);

    }

    /**
     * @Route("/api/admin/transaction/execute-upgrade-payment/{word_id}", name="paypal_execute_upgrade_payment", methods={"POST"})
     * @ParamConverter("word", class="WordBundle:Word", options={"id" = "word_id"})
     *
     *
     * @param Word|null $word
     * @param Request $request
     * @param WordService $wordService
     * @param CouponService $couponService
     * @param TranslatorInterface $translator
     * @param TransactionService $transactionService
     * @param RestPayPal $restPayPal
     * @param PackageOptionalValidation $packageOptionalValidation
     * @param LoggerInterface $logger
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function adminPaypalUpgradeExecuteAction(?Word $word,
                                                    Request $request,
                                                    WordService $wordService,
                                                    CouponService $couponService,
                                                    TranslatorInterface $translator,
                                                    TransactionService $transactionService,
                                                    RestPayPal $restPayPal,
                                                    PackageOptionalValidation $packageOptionalValidation,
                                                    LoggerInterface $logger
    ) {

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        $status  = 400;
        $message = "";
        $result  = array();
        $errors  = array();

        if (is_null($word)) {
            $this->addFlash(
                'error',
                $translator->trans('word.not.found')
            );

            $url = $this->generateUrl('admin_show_user', array('id' => $currentUser->getId()), UrlGeneratorInterface::ABSOLUTE_URL);
            $errors["url"] = $url;

        } else {

            $paymentId  = $request->request->get("paymentID");
            $payerId    = $request->request->get("payerID");


            $transaction = new Transaction();
            $form = $this->createForm(EditTransactionType::class, $transaction);

            $form->handleRequest($request);

            $formData = array();

            if (is_null($paymentId) || is_null($payerId) || !$form->isSubmitted()) {
                $result = array();
                $errors = array("general_error_id" => $translator->trans('system.error.try.again'));
            } else {

                $transactionRequest = $request->request->get("transaction");

                $formData["couponCode"] = isset($transactionRequest["couponCode"])?$transactionRequest["couponCode"]:null;
                $formData["packages"]   = isset($transactionRequest["packages"])?$transactionRequest["packages"]:null;
                $formData["optionals"]  = isset($transactionRequest["optionals"])?$transactionRequest["optionals"]:null;

                $coupon = null;
                if (!empty($formData["couponCode"])) {
                    /** @var Coupon $coupon */
                    $coupon = $couponService->getByCode($formData["couponCode"]);
                    if (is_null($coupon)) {
                        $message = $translator->trans('coupon.not.found');
                        $form->get('couponCode')->addError(new FormError($message));
                    }
                }

                $formData['errors'] = array();
                $packagesOptionals  = array("optionals"=>array(),"packages"=>array());

                if ($form->isValid()) {

                    //TODO: aggiungere controllo passando ?$word per verificare che gli optional non siano già stati comprati

                    if (!$packageOptionalValidation->validatePackagesOptionals($formData['errors'], $packagesOptionals, $formData["packages"], $formData["optionals"], "transaction")) {
                        if (count($formData['errors']) > 0) {
                            $errors = $formData['errors'];
                        }
                    } else {

                        $result = $restPayPal->execPayment($paymentId,$payerId);
                        if (!is_null($result)) {

                            $result = json_decode($result,true);

                            if (isset($result["id"])) {

                                $status = 200;

                                $logger->info("PayPal payment upgrade word value '".$word->getId()."' by user: ".$currentUser->getId(), $result);

                                $transaction = $transactionService->upgrade($paymentId, $payerId, $word, $currentUser, $transaction, $result, $coupon, $packagesOptionals);

                                if (is_null($transaction)) {

                                    $logger->error("Failed transaction created by user: ".$currentUser->getId());
                                    $status = 500;
                                    $result = array();
                                    $errors = array("general_error_id" => $translator->trans('system.error.try.again'));

                                } else {
                                    /** @var Word $word */
                                    $word = $transaction->getWord();
                                    $this->addFlash(
                                        'success',
                                        $translator->trans('congrats.word.bought',array("%word%"=>$word->getName()))
                                    );

                                    $url = $this->generateUrl('admin_show_user', array('id' => $currentUser->getId()), UrlGeneratorInterface::ABSOLUTE_URL);
                                    $result["url"] = $url;
                                }

                            } else {
                                $result = array();
                                $errors = array("general_error_id" => $translator->trans('system.error.try.again'));
                            }

                        } else {
                            $result = array();
                            $errors = array("general_error_id" => $translator->trans('system.error.try.again'));
                        }

                    }

                } else {
                    $errors = $this->getFormattedErrors("edit_transaction", $form);
                }

            }

        }


        return $this->json(array("message"=>$message, "errors"=>$errors, "result"=>$result), $status);
    }




    /**
     * @Route("/api/transaction/create-payment/", name="paypal_create_payment", methods={"POST"})
     *
     *
     * @param Request $request
     * @param WordService $wordService
     * @param CouponService $couponService
     * @param TranslatorInterface $translator
     * @param TransactionService $transactionService
     * @param RestPayPal $restPayPal
     * @param OptionalService $optionalService
     * @param LoggerInterface $logger
     * @param ValidationException $validationException
     * @param PictureFrameService $pictureFrameService
     * @param ShippingAddressService $shippingAddressService
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function paypalCreateAction(Request $request,
                                       WordService $wordService,
                                       CouponService $couponService,
                                       TranslatorInterface $translator,
                                       TransactionService $transactionService,
                                       RestPayPal $restPayPal,
                                       OptionalService $optionalService,
                                       LoggerInterface $logger,
                                       ValidationException $validationException,
                                       PictureFrameService $pictureFrameService,
                                       ShippingAddressService $shippingAddressService
    ) {

        $response = new ResponseUtils($translator, $logger);

        /** @var User $currentUser */
        $currentUser        = $this->getUser();

        $optCategories      = $request->request->get('opt_categories', array());
        $optionalIds        = $request->request->get('optionals', array());
        $submittedToken     = $request->request->get('token');
        $wordName           = $request->request->get('word_name');
        $wordValue          = $request->request->get('word_value',1);
        $couponCode         = $request->request->get('coupon');
        $invoiceIsRequired  = $request->request->get('chb_require_invoice');
        $shippingAddressId  = $request->request->get('choose_address');
        $invoiceAddressId   = $request->request->get('hdn_invoice_address');
        $certificates       = $request->request->get('cer');

        if (is_null($currentUser)) {
            return $response->getApiResponse(array(), "user.not.rights", 403, array("general"=>$translator->trans("user.not.rights")));
        }

        if (!$this->isCsrfTokenValid('hm_word_inp_to_buy', $submittedToken)) {
            return $response->getApiResponse(array(), "parameter.token.invalid", 400, array("general"=>$translator->trans("parameter.token.invalid")));
        }

        if (is_null($wordName)) {
            return $response->getApiResponse(array(), "parameters.invalid", 400, array("bw_word_name"=>$translator->trans("parameters.invalid")));
        }

        if (!IntegerUtils::checkInt($wordValue)) {
            return $response->getApiResponse(array(), "parameters.invalid", 400, array("bw_word_value"=>$translator->trans("parameters.invalid")));
        }

        $coupon = null;
        if (!empty($couponCode)) {
            /** @var Coupon $coupon */
            $coupon = $couponService->getByCode($couponCode);
            if (is_null($coupon)) {
                return $response->getApiResponse(array(), "coupon.not.found", 400, array("bw_coupon"=>$translator->trans("coupon.not.found")));
            }
        }


        /**
         * WORD VALIDATION
         */
        $validator = $this->get('validator');

        $word = new Word();

        $nameUtf8 = StringUtils::encodeToUtf8(strtolower($wordName));
        $word->setName($nameUtf8);
        $hash     = $wordService->getGeneratedHash($word);
        $word->setHash($hash);
        $word->setValue($wordValue);
        $errors = $validator->validate($word, null, array("word_type"));
        if(count($errors) > 0) {
            $responseError   = array();
            $formattedErrors = $validationException->getFormattedExceptions($errors);
            foreach ($formattedErrors as $formattedError) {
                $responseError["bw_word_name"] = $formattedError;
                break;
            }
            return $response->getApiResponse(array(), "word.invalid", 400, $responseError);
        } else {
            if (!$wordService->isAvailable($word)) {
                return $response->getApiResponse(array(), "word.not.available", 400, array("bw_word_name"=>$translator->trans("word.not.available")));
            }
        }

        $optionals = array();
        if (!empty($optionalIds) && count($optionalIds)>0) {
            $optionals = $optionalService->getByIds($optionalIds, true);
            if (!$optionals && count($optCategories) > 0) {
                return $response->getApiResponse(array(), "optionals.not.found", 400, array("general"=>$translator->trans("optionals.not.found")));
            }
        }


        /**
         * CERTIFICATES VALIDATION
         */

        $addressIds = array();
        $formattedCertificates = array();

        if (!empty($certificates) && count($certificates)>0) {

            if (is_null($shippingAddressId) || !IntegerUtils::checkId($shippingAddressId)) {
                return $response->getApiResponse(array(), "shipping.address.mandatory", 400, array("general"=>$translator->trans("shipping.address.mandatory")));
            }

            if (!is_null($shippingAddressId)) {
                $addressIds[$shippingAddressId] = $shippingAddressId;
            }

            $i = 0;
            foreach ($certificates as $certificate) {

                $printFormat            = isset($certificate['format']) ? $certificate['format'] : null;
                $frame                  = isset($certificate['frame']) ? $certificate['frame'] : null;
                $graphics               = isset($certificate['graphics']) ? $certificate['graphics'] : null;
                $isDedicationRequired   = isset($certificate['is_dedication_required']) ? $certificate['is_dedication_required'] : null;
                $dedicatedTo            = isset($certificate['dedicated_to']) ? $certificate['dedicated_to'] : null;
                $dedicationLine1        = isset($certificate['dedication_line_1']) ? $certificate['dedication_line_1'] : null;
                $dedicationLine2        = isset($certificate['dedication_line_2']) ? $certificate['dedication_line_2'] : null;

                $formattedCertificates[$i] = array(
                    "printFormat"           => null,
                    "frame"                 => null,
                    "pictureFrame"          => null,
                    "isDedicationRequired"  => false,
                    "dedicatedTo"           => null,
                    "dedicationLine1"       => null,
                    "dedicationLine2"       => null
                );


                if (is_null($printFormat) || empty($printFormat) || is_null($frame) || empty($frame) || is_null($graphics) || empty($graphics)) {
                    return $response->getApiResponse(array(), "parameter.mandatory", 400, array("general"=>$translator->trans("parameter.mandatory")));
                }

                if (!IntegerUtils::checkId($printFormat) || !IntegerUtils::checkId($frame) || !IntegerUtils::checkId($graphics)) {
                    return $response->getApiResponse(array(), "parameter.id.invalid", 400, array("general"=>$translator->trans("parameter.id.invalid")));
                }


                $cerOptionalIds = array($printFormat,$frame);
                $cerOptionals = $optionalService->getByIds($cerOptionalIds, true);
                if (!$cerOptionals || count($cerOptionals) < 2) {
                    return $response->getApiResponse(array(), "optionals.not.found", 400, array("general"=>$translator->trans("optionals.not.found")));
                }

                /** @var Optional $cerOptional */
                foreach ($cerOptionals as $cerOptional) {
                    if ($cerOptional->getId() == $printFormat) {
                        $formattedCertificates[$i]["printFormat"] = $cerOptional;
                    }
                    if ($cerOptional->getId() == $frame) {
                        $formattedCertificates[$i]["frame"] = $cerOptional;
                    }
                }


                /** @var PictureFrame $pictureFrame */
                $pictureFrame = $pictureFrameService->getById($graphics);
                if (is_null($pictureFrame)) {
                    return $response->getApiResponse(array(), "picture.frame.not.found", 400, array("general"=>$translator->trans("picture.frame.not.found")));
                }

                $formattedCertificates[$i]["pictureFrame"] = $pictureFrame;

                if ($isDedicationRequired==1) {

                    $formattedCertificates[$i]["isDedicationRequired"] = true;

                    if (!is_null($dedicatedTo) && !empty($dedicatedTo)) {
                        if (!StringUtils::checkNameString($dedicatedTo)) {
                            return $response->getApiResponse(array(), "only.letters.allowed", 400, array("general"=>$translator->trans("only.letters.allowed")));
                        }
                        if (!StringUtils::checkCustomLength($dedicatedTo,0,40)) {
                            return $response->getApiResponse(array(), "length.invalid", 400, array("general"=>$translator->trans("length.invalid")));
                        }
                        $formattedCertificates[$i]["dedicatedTo"] = $dedicatedTo;
                    }
                    if (!is_null($dedicationLine1) && !empty($dedicationLine1)) {
                        if (!StringUtils::checkNameString($dedicationLine1)) {
                            return $response->getApiResponse(array(), "only.letters.allowed", 400, array("general"=>$translator->trans("only.letters.allowed")));
                        }
                        if (!StringUtils::checkCustomLength($dedicationLine1,0,40)) {
                            return $response->getApiResponse(array(), "length.invalid", 400, array("general"=>$translator->trans("length.invalid")));
                        }
                        $formattedCertificates[$i]["dedicationLine1"] = $dedicationLine1;
                    }
                    if (!is_null($dedicationLine2) && !empty($dedicationLine2)) {
                        if (!StringUtils::checkNameString($dedicationLine2)) {
                            return $response->getApiResponse(array(), "only.letters.allowed", 400, array("general"=>$translator->trans("only.letters.allowed")));
                        }
                        if (!StringUtils::checkCustomLength($dedicationLine2,0,40)) {
                            return $response->getApiResponse(array(), "length.invalid", 400, array("general"=>$translator->trans("length.invalid")));
                        }
                        $formattedCertificates[$i]["dedicationLine2"] = $dedicationLine2;
                    }
                }

                $i++;
            }
        }


        /**
         * ADDRESSES VALIDATION
         */

        if (!is_null($invoiceIsRequired)) {
            if (is_null($invoiceAddressId) || !IntegerUtils::checkId($invoiceAddressId)) {
                return $response->getApiResponse(array(), "invoice.address.mandatory", 400, array("general"=>$translator->trans("invoice.address.mandatory")));
            }

            $addressIds[$invoiceAddressId] = $invoiceAddressId;
        }

        $shippingAddress = null;

        if (!empty($addressIds) && count($addressIds) > 0) {
            $addresses = $shippingAddressService->getByIds($addressIds);
            if (!$addresses || count($addresses) < 1) {
                return $response->getApiResponse(array(), "address.not.found", 400, array("general"=>$translator->trans("address.not.found")));
            }

            /** @var ShippingAddress $address */
            foreach ($addresses as $address) {
                if ($address->getId() == $shippingAddressId) {
                    $shippingAddress = $address;
                }
            }

        }


        $wordValue = $transactionService->getFinalWordValue($word->getValue(), $optionals);
        $word->setValue($wordValue);

        $certificatesAmount = $transactionService->getCertificatesAmount($formattedCertificates, $shippingAddress);

        $totalInfo = $transactionService->getPaymentInfo($wordValue, $certificatesAmount, $coupon, $invoiceIsRequired);

        $result = $restPayPal->createPayment($totalInfo);
        if (!is_null($result)) {
            $result = json_decode($result,true);

            if (!isset($result["id"])) {
                return $response->getApiResponse(array(), "system.error.try.again", 400, array("general"=>$translator->trans("system.error.try.again")));
            }

        } else {
            return $response->getApiResponse(array(), "system.error.try.again", 400, array("general"=>$translator->trans("system.error.try.again")));
        }

        return $response->getApiResponse($result, "OK");

    }



    /**
     * @Route("/api/transaction/execute-payment/", name="paypal_execute_payment", methods={"POST"})
     *
     *
     * @param Request $request
     * @param WordService $wordService
     * @param CouponService $couponService
     * @param TranslatorInterface $translator
     * @param TransactionService $transactionService
     * @param RestPayPal $restPayPal
     * @param OptionalService $optionalService
     * @param LoggerInterface $logger
     * @param ValidationException $validationException
     * @param PictureFrameService $pictureFrameService
     * @param ShippingAddressService $shippingAddressService
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function paypalExecuteAction(Request $request,
                                        WordService $wordService,
                                        CouponService $couponService,
                                        TranslatorInterface $translator,
                                        TransactionService $transactionService,
                                        RestPayPal $restPayPal,
                                        OptionalService $optionalService,
                                        LoggerInterface $logger,
                                        ValidationException $validationException,
                                        PictureFrameService $pictureFrameService,
                                        ShippingAddressService $shippingAddressService
    ) {

        $response = new ResponseUtils($translator, $logger);

        /** @var User $currentUser */
        $currentUser        = $this->getUser();

        $optCategories      = $request->request->get('opt_categories', array());
        $optionalIds        = $request->request->get('optionals', array());
        $submittedToken     = $request->request->get('token');
        $wordName           = $request->request->get('word_name');
        $wordValue          = $request->request->get('word_value',1);
        $couponCode         = $request->request->get('coupon');
        $invoiceIsRequired  = $request->request->get('chb_require_invoice');
        $shippingAddressId  = $request->request->get('choose_address');
        $invoiceAddressId   = $request->request->get('hdn_invoice_address');
        $certificates       = $request->request->get('cer');


        $paymentId          = $request->request->get("paymentID");
        $payerId            = $request->request->get("payerID");


        if (is_null($paymentId) || is_null($payerId)) {
            return $response->getApiResponse(array(), "system.error.try.again", 400, array("general"=>$translator->trans("system.error.try.again")));
        }

        if (is_null($currentUser)) {
            return $response->getApiResponse(array(), "user.not.rights", 403, array("general"=>$translator->trans("user.not.rights")));
        }

        if (!$this->isCsrfTokenValid('hm_word_inp_to_buy', $submittedToken)) {
            return $response->getApiResponse(array(), "parameter.token.invalid", 400, array("general"=>$translator->trans("parameter.token.invalid")));
        }

        if (is_null($wordName)) {
            return $response->getApiResponse(array(), "parameters.invalid", 400, array("bw_word_name"=>$translator->trans("parameters.invalid")));
        }

        if (!IntegerUtils::checkInt($wordValue)) {
            return $response->getApiResponse(array(), "parameters.invalid", 400, array("bw_word_value"=>$translator->trans("parameters.invalid")));
        }

        $coupon = null;
        if (!empty($couponCode)) {
            /** @var Coupon $coupon */
            $coupon = $couponService->getByCode($couponCode);
            if (is_null($coupon)) {
                return $response->getApiResponse(array(), "coupon.not.found", 400, array("bw_coupon"=>$translator->trans("coupon.not.found")));
            }
        }

        /**
         * WORD VALIDATION
         */
        $validator = $this->get('validator');

        $word = new Word();

        $nameUtf8 = StringUtils::encodeToUtf8(strtolower($wordName));
        $word->setName($nameUtf8);
        $hash     = $wordService->getGeneratedHash($word);
        $word->setHash($hash);
        $word->setValue($wordValue);
        $errors = $validator->validate($word, null, array("word_type"));
        if(count($errors) > 0) {
            $responseError   = array();
            $formattedErrors = $validationException->getFormattedExceptions($errors);
            foreach ($formattedErrors as $formattedError) {
                $responseError["bw_word_name"] = $formattedError;
                break;
            }
            return $response->getApiResponse(array(), "word.invalid", 400, $responseError);
        } else {
            if (!$wordService->isAvailable($word)) {
                return $response->getApiResponse(array(), "word.not.available", 400, array("bw_word_name"=>$translator->trans("word.not.available")));
            }
        }

        $optionals = array();
        if (!empty($optionalIds) && count($optionalIds)>0) {
            $optionals = $optionalService->getByIds($optionalIds, true);
            if (!$optionals && count($optCategories) > 0) {
                return $response->getApiResponse(array(), "optionals.not.found", 400, array("general"=>$translator->trans("optionals.not.found")));
            }
        }


        /**
         * CERTIFICATES VALIDATION
         */

        $addressIds = array();
        $formattedCertificates = array();

        if (!empty($certificates) && count($certificates)>0) {

            if (is_null($shippingAddressId) || !IntegerUtils::checkId($shippingAddressId)) {
                return $response->getApiResponse(array(), "shipping.address.mandatory", 400, array("general"=>$translator->trans("shipping.address.mandatory")));
            }

            if (!is_null($shippingAddressId)) {
                $addressIds[$shippingAddressId] = $shippingAddressId;
            }

            $i = 0;
            foreach ($certificates as $certificate) {

                $printFormat            = isset($certificate['format']) ? $certificate['format'] : null;
                $frame                  = isset($certificate['frame']) ? $certificate['frame'] : null;
                $graphics               = isset($certificate['graphics']) ? $certificate['graphics'] : null;
                $isDedicationRequired   = isset($certificate['is_dedication_required']) ? $certificate['is_dedication_required'] : null;
                $dedicatedTo            = isset($certificate['dedicated_to']) ? $certificate['dedicated_to'] : null;
                $dedicationLine1        = isset($certificate['dedication_line_1']) ? $certificate['dedication_line_1'] : null;
                $dedicationLine2        = isset($certificate['dedication_line_2']) ? $certificate['dedication_line_2'] : null;

                $formattedCertificates[$i] = array(
                    "printFormat"           => null,
                    "frame"                 => null,
                    "pictureFrame"          => null,
                    "isDedicationRequired"  => false,
                    "dedicatedTo"           => null,
                    "dedicationLine1"       => null,
                    "dedicationLine2"       => null
                );


                if (is_null($printFormat) || empty($printFormat) || is_null($frame) || empty($frame) || is_null($graphics) || empty($graphics)) {
                    return $response->getApiResponse(array(), "parameter.mandatory", 400, array("general"=>$translator->trans("parameter.mandatory")));
                }

                if (!IntegerUtils::checkId($printFormat) || !IntegerUtils::checkId($frame) || !IntegerUtils::checkId($graphics)) {
                    return $response->getApiResponse(array(), "parameter.id.invalid", 400, array("general"=>$translator->trans("parameter.id.invalid")));
                }


                $cerOptionalIds = array($printFormat,$frame);
                $cerOptionals = $optionalService->getByIds($cerOptionalIds, true);
                if (!$cerOptionals || count($cerOptionals) < 2) {
                    return $response->getApiResponse(array(), "optionals.not.found", 400, array("general"=>$translator->trans("optionals.not.found")));
                }

                /** @var Optional $cerOptional */
                foreach ($cerOptionals as $cerOptional) {
                    if ($cerOptional->getId() == $printFormat) {
                        $formattedCertificates[$i]["printFormat"] = $cerOptional;
                    }
                    if ($cerOptional->getId() == $frame) {
                        $formattedCertificates[$i]["frame"] = $cerOptional;
                    }
                }


                /** @var PictureFrame $pictureFrame */
                $pictureFrame = $pictureFrameService->getById($graphics);
                if (is_null($pictureFrame)) {
                    return $response->getApiResponse(array(), "picture.frame.not.found", 400, array("general"=>$translator->trans("picture.frame.not.found")));
                }

                $formattedCertificates[$i]["pictureFrame"] = $pictureFrame;

                if ($isDedicationRequired==1) {

                    $formattedCertificates[$i]["isDedicationRequired"] = true;

                    if (!is_null($dedicatedTo) && !empty($dedicatedTo)) {
                        if (!StringUtils::checkNameString($dedicatedTo)) {
                            return $response->getApiResponse(array(), "only.letters.allowed", 400, array("general"=>$translator->trans("only.letters.allowed")));
                        }
                        if (!StringUtils::checkCustomLength($dedicatedTo,0,40)) {
                            return $response->getApiResponse(array(), "length.invalid", 400, array("general"=>$translator->trans("length.invalid")));
                        }
                        $formattedCertificates[$i]["dedicatedTo"] = $dedicatedTo;
                    }
                    if (!is_null($dedicationLine1) && !empty($dedicationLine1)) {
                        if (!StringUtils::checkNameString($dedicationLine1)) {
                            return $response->getApiResponse(array(), "only.letters.allowed", 400, array("general"=>$translator->trans("only.letters.allowed")));
                        }
                        if (!StringUtils::checkCustomLength($dedicationLine1,0,40)) {
                            return $response->getApiResponse(array(), "length.invalid", 400, array("general"=>$translator->trans("length.invalid")));
                        }
                        $formattedCertificates[$i]["dedicationLine1"] = $dedicationLine1;
                    }
                    if (!is_null($dedicationLine2) && !empty($dedicationLine2)) {
                        if (!StringUtils::checkNameString($dedicationLine2)) {
                            return $response->getApiResponse(array(), "only.letters.allowed", 400, array("general"=>$translator->trans("only.letters.allowed")));
                        }
                        if (!StringUtils::checkCustomLength($dedicationLine2,0,40)) {
                            return $response->getApiResponse(array(), "length.invalid", 400, array("general"=>$translator->trans("length.invalid")));
                        }
                        $formattedCertificates[$i]["dedicationLine2"] = $dedicationLine2;
                    }
                }

                $i++;
            }
        }


        /**
         * ADDRESSES VALIDATION
         */

        if (!is_null($invoiceIsRequired)) {
            if (is_null($invoiceAddressId) || !IntegerUtils::checkId($invoiceAddressId)) {
                return $response->getApiResponse(array(), "invoice.address.mandatory", 400, array("general"=>$translator->trans("invoice.address.mandatory")));
            }

            $addressIds[$invoiceAddressId] = $invoiceAddressId;
        }

        $shippingAddress    = null;
        $invoiceAddress     = null;

        if (!empty($addressIds) && count($addressIds) > 0) {
            $addresses = $shippingAddressService->getByIds($addressIds);
            if (!$addresses || count($addresses) < 1) {
                return $response->getApiResponse(array(), "address.not.found", 400, array("general"=>$translator->trans("address.not.found")));
            }

            /** @var ShippingAddress $address */
            foreach ($addresses as $address) {
                if ($address->getId() == $shippingAddressId) {
                    $shippingAddress = $address;
                }
                if ($address->getId() == $invoiceAddressId) {
                    $invoiceAddress = $address;
                }
            }

        }



        $wordValue = $transactionService->getFinalWordValue($word->getValue(), $optionals);
        $word->setValue($wordValue);

        $transaction = $transactionService->createByUser($paymentId, $payerId,
            $currentUser,
            $word,
            $coupon,
            $optionals,
            null,
            $formattedCertificates,
            $invoiceIsRequired,
            $shippingAddress,
            $invoiceAddress
        );

        if (is_null($transaction)) {
            return $response->getApiResponse(array(), "system.error.try.again", 400, array("general"=>$translator->trans("system.error.try.again")));
        }

        $result = $restPayPal->execPayment($paymentId, $payerId);
        if (!is_null($result)) {

            $result = json_decode($result,true);

            if (isset($result["id"])) {

                $logger->info("PayPal payment created by user: ".$currentUser->getId(), $result);

                $expectedRank = $wordService->getExpectedRank($wordValue);
                $transaction = $transactionService->updateStatusPayment($transaction, $result, $expectedRank);

                if (is_null($transaction)) {
                    return $response->getApiResponse(array(), "system.error.try.again", 400, array("general"=>$translator->trans("system.error.try.again")));
                }

                $this->addFlash(
                    'success',
                    $translator->trans('congrats.word.bought',array("%word%" => $word->getName()))
                );

                $url = $this->generateUrl('private_word_show', array("word_name"=>$word->getName()), UrlGeneratorInterface::ABSOLUTE_URL);
                $result["url"] = $url;

            } else {
                return $response->getApiResponse(array(), "system.error.try.again", 400, array("general"=>$translator->trans("system.error.try.again")));
            }

        } else {
            // delete all prev saved
            $transactionService->deleteTransaction($transaction);

            return $response->getApiResponse(array(), "system.error.try.again", 400, array("general"=>$translator->trans("system.error.try.again")));
        }


//        $result = $restPayPal->execPayment($paymentId, $payerId);
//        if (!is_null($result)) {
//
//            $result = json_decode($result,true);
//
//            if (isset($result["id"])) {
//
//                $logger->info("PayPal payment created by user: ".$currentUser->getId(), $result);
//
//                $wordValue = $transactionService->getFinalWordValue($word->getValue(), $optionals);
//                $word->setValue($wordValue);
//                $expectedRank = $wordService->getExpectedRank($wordValue);
//
//                $transaction = $transactionService->createByUser($paymentId, $payerId,
//                    $currentUser,
//                    $word,
//                    $result,
//                    $coupon,
//                    $optionals,
//                    null,
//                    $formattedCertificates,
//                    $expectedRank,
//                    $invoiceIsRequired,
//                    $shippingAddress,
//                    $invoiceAddress
//                );
//
//                if (is_null($transaction)) {
//                    return $response->getApiResponse(array(), "system.error.try.again", 400, array("general"=>$translator->trans("system.error.try.again")));
//                }
//
//                $this->addFlash(
//                    'success',
//                    $translator->trans('congrats.word.bought',array("%word%" => $word->getName()))
//                );
//
//                $url = $this->generateUrl('show_private_profile', array(), UrlGeneratorInterface::ABSOLUTE_URL);
//                $result["url"] = $url;
//
//            } else {
//                return $response->getApiResponse(array(), "system.error.try.again", 400, array("general"=>$translator->trans("system.error.try.again")));
//            }
//
//        } else {
//            return $response->getApiResponse(array(), "system.error.try.again", 400, array("general"=>$translator->trans("system.error.try.again")));
//        }


        return $response->getApiResponse($result, "OK");
    }

}
