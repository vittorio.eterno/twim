<?php

namespace PurchasingBundle\Controller\Api;


use ImageBundle\Entity\PictureFrame;
use ImageBundle\Service\PictureFrameService;
use Psr\Log\LoggerInterface;
use PurchasingBundle\Entity\Optional;
use PurchasingBundle\Entity\Transaction;
use PurchasingBundle\Service\OptionalService;
use Schema\AbstractRenderController as Controller;
use Symfony\Component\Asset\Packages;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\TranslatorInterface;
use UserBundle\Entity\User;
use Utils\Service\ResponseUtils;
use Utils\StaticUtil\IntegerUtils;
use Utils\StaticUtil\StringUtils;
use WordBundle\Service\WordService;


class OptionalController extends Controller {


    /**
     * This action is used in ajax call to check in real-time if optionals selected are corrects
     *
     * @Route("/api/optionals/validation", name="api_optionals_validation", methods="POST")
     *
     *
     * @param Request $request
     * @param TranslatorInterface $translator
     * @param OptionalService $optionalService
     * @param WordService $wordService
     * @param LoggerInterface $logger
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function valueValidationAction(Request $request,
                                          TranslatorInterface $translator,
                                          OptionalService $optionalService,
                                          WordService $wordService,
                                          LoggerInterface $logger
    ) {

        $response = new ResponseUtils($translator, $logger);

        /** @var User $currentUser */
        $currentUser    = $this->getUser();

        $errorKey       = $request->request->get('errorKey',0);
        $optCategories  = $request->request->get('opt_categories', array());
        $optionalIds    = $request->request->get('optionals', array());
        $wordValue      = $request->request->get('wordValue',Transaction::WORD_DEFAULT_VALUE);

        if (is_null($currentUser)) {
            return $response->getApiResponse(array(), "user.not.rights", 403, array($errorKey=>$translator->trans("user.not.rights")));
        }

        $optionals = array();
        if (!empty($optionalIds) && count($optionalIds)>0) {
            $optionals = $optionalService->getByIds($optionalIds, true);
            if (!$optionals && count($optCategories) > 0) {
                return $response->getApiResponse(array(), "optionals.not.found", 400, array($errorKey=>$translator->trans("optionals.not.found")));
            }
        }

        $optionalsPreview = array();
        if (!empty($optionals) && count($optionals)>0) {
            /** @var Optional $optional */
            foreach ($optionals as $optional) {
                $wordValue += $optional->getPrice();

                $optionalsPreview[] = $translator->trans($optional->getSource(),array(),'frontends') . " ($".$optional->getPrice().")";

            }
        }

        $expectedRank = $wordService->getExpectedRank($wordValue);

        return $response->getApiResponse(array(
            "expectedRank"      => $expectedRank,
            "newWordValue"      => $wordValue,
            "optionalsPreview"  => $optionalsPreview
        ), "OK");

    }




    /**
     * @Route("/api/certificate/validation", name="api_validate_certificate_fields", methods={"POST"})
     *
     * @param Request $request
     * @param TranslatorInterface $translator
     * @param LoggerInterface $logger
     * @param OptionalService $optionalService
     * @param PictureFrameService $pictureFrameService
     * @param Packages $packages
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function validateCertificateAction(Request $request,
                                              TranslatorInterface $translator,
                                              LoggerInterface $logger,
                                              OptionalService $optionalService,
                                              PictureFrameService $pictureFrameService,
                                              Packages $packages
    ) {

        $response = new ResponseUtils($translator, $logger);

        /** @var User $currentUser */
        $currentUser            = $this->getUser();

        $printFormat            = $request->request->get('slc_paper_opt_printformatlabel');
        $frame                  = $request->request->get('slc_paper_opt_framelabel');
        $isDedicationRequired   = $request->request->get('cer_require_dedication',0);
        $dedicatedTo            = $request->request->get('cer_dedicated_to');
        $dedicationLine1        = $request->request->get('cer_dedication_line_1');
        $dedicationLine2        = $request->request->get('cer_dedication_line_2');
        $graphics               = $request->request->get('slc_graphics_certificate');


        if (is_null($currentUser)) {
            return $response->getApiResponse(array(), "user.not.rights", 403, array("general"=>$translator->trans("user.not.rights")));
        }

        $errorsResponse = array();

        if (is_null($printFormat) || empty($printFormat)) {
            $errorsResponse["slc_paper_opt_printformatlabel"] = $translator->trans("parameter.mandatory");
        }

        if (is_null($frame) || empty($frame)) {
            $errorsResponse["slc_paper_opt_framelabel"] = $translator->trans("parameter.mandatory");
        }

        if (is_null($graphics) || empty($graphics)) {
            $errorsResponse["slc_graphics_certificate"] = $translator->trans("parameter.mandatory");
        }

        if (!IntegerUtils::checkId($printFormat)) {
            $errorsResponse["slc_paper_opt_printformatlabel"] = $translator->trans("parameter.id.invalid");
        }

        if (!IntegerUtils::checkId($frame)) {
            $errorsResponse["slc_paper_opt_framelabel"] = $translator->trans("parameter.id.invalid");
        }

        if (!IntegerUtils::checkId($graphics)) {
            $errorsResponse["slc_graphics_certificate"] = $translator->trans("parameter.id.invalid");
        }

        $optionals = array();
        $pictureFrame = null;
        if (count($errorsResponse) == 0) {

            $optionalIds = array($printFormat,$frame);
            $optionals = $optionalService->getByIds($optionalIds, true);
            if (!$optionals || count($optionals) < 2) {
                $errorsResponse["slc_paper_opt_printformatlabel"] = $translator->trans("optionals.not.found");
                $errorsResponse["slc_paper_opt_framelabel"] = $translator->trans("optionals.not.found");
            }

            /** @var PictureFrame $pictureFrame */
            $pictureFrame = $pictureFrameService->getById($graphics);
            if (is_null($pictureFrame)) {
                $errorsResponse["slc_graphics_certificate"] = $translator->trans("picture.frame.not.found");
            }

            if ($isDedicationRequired==1) {
                if (!is_null($dedicatedTo) && !empty($dedicatedTo)) {
                    if (!StringUtils::checkNameString($dedicatedTo)) {
                        $errorsResponse["cer_dedicated_to"] = $translator->trans("only.letters.allowed");
                    }
                    if (!StringUtils::checkCustomLength($dedicatedTo,0,40)) {
                        $errorsResponse["cer_dedicated_to"] = $translator->trans("length.invalid");
                    }
                }
                if (!is_null($dedicationLine1) && !empty($dedicationLine1)) {
                    if (!StringUtils::checkNameString($dedicationLine1)) {
                        $errorsResponse["cer_dedication_line_1"] = $translator->trans("only.letters.allowed");
                    }
                    if (!StringUtils::checkCustomLength($dedicationLine1,0,40)) {
                        $errorsResponse["cer_dedication_line_1"] = $translator->trans("length.invalid");
                    }
                }
                if (!is_null($dedicationLine2) && !empty($dedicationLine2)) {
                    if (!StringUtils::checkNameString($dedicationLine2)) {
                        $errorsResponse["cer_dedication_line_2"] = $translator->trans("only.letters.allowed");
                    }
                    if (!StringUtils::checkCustomLength($dedicationLine2,0,40)) {
                        $errorsResponse["cer_dedication_line_2"] = $translator->trans("length.invalid");
                    }
                }
            }

        }

        $message = "KO";
        $status  = 400;
        $result  = array();

        if (count($errorsResponse)==0) {
            $message = "OK";
            $status  = 200;

            $certificateValue = 0;

            $recap_certificate = array();
            if (!empty($optionals) && count($optionals)>0) {
                $i = 0;

                /** @var Optional $optional */
                foreach ($optionals as $optional) {

                    $structureSource = $optional->serializedOptionaleStructureSource() == "printformatlabel" ? "print.format.recap" : $optional->serializedOptionaleStructureSource();

                    $optStructureTrans      = $translator->trans($structureSource,array(),'frontends');
                    $optTrans               = $translator->trans($optional->getSource(),array(),'frontends');
                    $plus                   = $i > 0 ? "+" : "";
                    $recap_certificate[]    = $optStructureTrans.": ".$optTrans." (".$plus."$".$optional->getPrice().")";

                    $certificateValue       += $optional->getPrice();
                    $i++;
                }
            }

            $assetPath = $packages->getUrl("build/images/".$pictureFrame->getHash().".".$pictureFrame->getExtension());
            $result["graphics_source"]  = $assetPath;
            $result["graphics_alt"]     = $translator->trans($pictureFrame->getSource(), array(),'frontends');


            if ($isDedicationRequired==1) {

                $certificateValue       += Transaction::DEDICATION_PRICE;
                $dedicatedToSource      = $translator->trans("dedicated.to.label",array(),'frontends');
                $dedicationLine1Source  = $translator->trans("dedication.line.1.recap",array(),'frontends');
                $dedicationLine2Source  = $translator->trans("dedication.line.2.recap",array(),'frontends');

                if (!empty($dedicatedTo)) {
                    $recap_certificate[] = $dedicatedToSource.": ".$dedicatedTo;
                }
                if (!empty($dedicationLine1)) {
                    $recap_certificate[] = $dedicationLine1Source.": ".$dedicationLine1;
                }
                if (!empty($dedicationLine2)) {
                    $recap_certificate[] = $dedicationLine2Source.": ".$dedicationLine2;
                }
            }

            $result["recap_certificate"] = $recap_certificate;
            $result["certificate_value"] = $certificateValue;
            $result["certificate_title"] = $translator->trans("certificate.title",array(),'frontends');

        }

        return $response->getApiResponse($result, $message, $status, $errorsResponse);
    }


    /**
     * @Route("/api/certificates/validation", name="api_validate_certificates_fields", methods={"POST"})
     *
     * @param Request $request
     * @param TranslatorInterface $translator
     * @param LoggerInterface $logger
     * @param OptionalService $optionalService
     * @param PictureFrameService $pictureFrameService
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function validateCertificatesAction(Request $request,
                                               TranslatorInterface $translator,
                                               LoggerInterface $logger,
                                               OptionalService $optionalService,
                                               PictureFrameService $pictureFrameService
    ) {

        $response = new ResponseUtils($translator, $logger);

//        Array
//        (
//            [cer] => Array
//            (
//                [0] => Array
//                (
//                    [format] => 6
//                    [frame] => 7
//                    [graphics] => 1
//                    [is_dedication_required] => 1
//                    [dedicated_to] => asd
//                    [dedication_line_1] => s
//                    [dedication_line_2] => s
//                )
//
//                [1] => Array
//                (
//                    [format] => 6
//                    [frame] => 7
//                    [graphics] => 1
//                    [is_dedication_required] => 0
//                    [dedicated_to] => null
//                    [dedication_line_1] => null
//                    [dedication_line_2] => null
//                )
//
//            )
//
//        )

        /** @var User $currentUser */
        $currentUser  = $this->getUser();
        $certificates = $request->request->get('cer');

        if (is_null($currentUser)) {
            return $response->getApiResponse(array(), "user.not.rights", 403, array("general"=>$translator->trans("user.not.rights")));
        }

        $errorsResponse = array();

        if (!empty($certificates) && count($certificates)>0) {
            foreach ($certificates as $certificate) {

                $printFormat            = isset($certificate['format']) ? $certificate['format'] : null;
                $frame                  = isset($certificate['frame']) ? $certificate['frame'] : null;
                $graphics               = isset($certificate['graphics']) ? $certificate['graphics'] : null;
                $isDedicationRequired   = isset($certificate['is_dedication_required']) ? $certificate['is_dedication_required'] : null;
                $dedicatedTo            = isset($certificate['dedicated_to']) ? $certificate['dedicated_to'] : null;
                $dedicationLine1        = isset($certificate['dedication_line_1']) ? $certificate['dedication_line_1'] : null;
                $dedicationLine2        = isset($certificate['dedication_line_2']) ? $certificate['dedication_line_2'] : null;


                if (is_null($printFormat) || empty($printFormat)) {
                    $errorsResponse["mandatory"] = $translator->trans("parameter.mandatory");
                }

                if (is_null($frame) || empty($frame)) {
                    $errorsResponse["mandatory"] = $translator->trans("parameter.mandatory");
                }

                if (is_null($graphics) || empty($graphics)) {
                    $errorsResponse["mandatory"] = $translator->trans("parameter.mandatory");
                }

                if (!IntegerUtils::checkId($printFormat)) {
                    $errorsResponse["invalid"] = $translator->trans("parameter.id.invalid");
                }

                if (!IntegerUtils::checkId($frame)) {
                    $errorsResponse["invalid"] = $translator->trans("parameter.id.invalid");
                }

                if (!IntegerUtils::checkId($graphics)) {
                    $errorsResponse["invalid"] = $translator->trans("parameter.id.invalid");
                }

                $pictureFrame = null;
                if (count($errorsResponse) == 0) {

                    $optionalIds = array($printFormat,$frame);
                    $optionals = $optionalService->getByIds($optionalIds, true);
                    if (!$optionals || count($optionals) < 2) {
                        $errorsResponse["opt_not_found"] = $translator->trans("optionals.not.found");
                    }

                    /** @var PictureFrame $pictureFrame */
                    $pictureFrame = $pictureFrameService->getById($graphics);
                    if (is_null($pictureFrame)) {
                        $errorsResponse["pic_frame"] = $translator->trans("picture.frame.not.found");
                    }

                    if ($isDedicationRequired==1) {
                        if (!is_null($dedicatedTo) && !empty($dedicatedTo)) {
                            if (!StringUtils::checkNameString($dedicatedTo)) {
                                $errorsResponse["dedication"] = $translator->trans("only.letters.allowed");
                            }
                            if (!StringUtils::checkCustomLength($dedicatedTo,0,40)) {
                                $errorsResponse["dedication"] = $translator->trans("length.invalid");
                            }
                        }
                        if (!is_null($dedicationLine1) && !empty($dedicationLine1)) {
                            if (!StringUtils::checkNameString($dedicationLine1)) {
                                $errorsResponse["dedication"] = $translator->trans("only.letters.allowed");
                            }
                            if (!StringUtils::checkCustomLength($dedicationLine1,0,40)) {
                                $errorsResponse["dedication"] = $translator->trans("length.invalid");
                            }
                        }
                        if (!is_null($dedicationLine2) && !empty($dedicationLine2)) {
                            if (!StringUtils::checkNameString($dedicationLine2)) {
                                $errorsResponse["dedication"] = $translator->trans("only.letters.allowed");
                            }
                            if (!StringUtils::checkCustomLength($dedicationLine2,0,40)) {
                                $errorsResponse["dedication"] = $translator->trans("length.invalid");
                            }
                        }
                    }

                }


            }
        }

        $message = "KO";
        $status  = 400;
        $result  = array();

        if (count($errorsResponse)==0) {
            $message = "OK";
            $status  = 200;

        }

        return $response->getApiResponse($result, $message, $status, $errorsResponse);
    }


}