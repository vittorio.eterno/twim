<?php

namespace PurchasingBundle\Controller;


use ImageBundle\Entity\PictureFrame;
use ImageBundle\Service\PictureFrameService;
use Psr\Log\LoggerInterface;
use PurchasingBundle\Entity\Coupon;
use PurchasingBundle\Entity\Optional;
use PurchasingBundle\Entity\Transaction;
use PurchasingBundle\Form\TransactionType;
use PurchasingBundle\Service\CouponService;
use PurchasingBundle\Service\OptionalService;
use PurchasingBundle\Service\PackageService;
use PurchasingBundle\Service\TransactionService;
use ResourceBundle\Entity\Country;
use ResourceBundle\Service\CountryService;
use ResourceBundle\Service\ProvinceService;
use Schema\AbstractRenderController as Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\TranslatorInterface;
use UserBundle\Entity\User;
use UserBundle\Service\ShippingAddressService;
use UserBundle\Service\UserService;
use Utils\Service\CacheService;
use Utils\Service\RequestInfo;
use Utils\Service\RestPayPal;
use Utils\Validation\PackageOptionalValidation;
use WordBundle\Entity\Word;
use WordBundle\Service\WordService;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


/**
 * Class TransactionController
 * @package PurchasingBundle\Controller
 */
class TransactionController extends Controller {


    /**
     * @Route("/word/buy", name="create_buy_word", methods={"GET","POST"})
     *
     * @param Request $request
     * @param UserService $userService
     * @param RequestInfo $requestInfo
     * @param TranslatorInterface $translator
     * @param WordService $wordService
     * @param PackageService $packageService
     * @param OptionalService $optionalService
     * @param PictureFrameService $pictureFrameService
     * @param CountryService $countryService
     * @param ShippingAddressService $shippingAddressService
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request,
                                 UserService $userService,
                                 RequestInfo $requestInfo,
                                 TranslatorInterface $translator,
                                 WordService $wordService,
                                 PackageService $packageService,
                                 OptionalService $optionalService,
                                 PictureFrameService $pictureFrameService,
                                 CountryService $countryService,
                                 ShippingAddressService $shippingAddressService
    ) {

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY', null, 'Unable to access this page!');

        /** @var User $currentUser */
        $currentUser    = $this->getUser();

        $wordName       = $request->query->get('wn');

        $kingWordValue  = $wordService->getKingWordValue();

        $expectedRank   = $wordService->getExpectedRank();

        $deviceName     = $requestInfo->get('deviceName','Desktop');

        /**
         * Get all formatted Packages and Optionals
         */
        $listPackages           = $packageService->getFormattedPublicActivePackages();
        $listDigitalOptionals   = $optionalService->getFormattedDigitalOptionals();
        $listPaperOptionals     = $optionalService->getFormattedPaperOptionals();
        $defaultPaperPicFrame   = $pictureFrameService->getFormattedDefaultPictureFrameByType(Optional::TYPE_PAPER_FORMAT);
        $countries              = $countryService->getCachedCountries();
        $lastAddressesUsed      = $shippingAddressService->getFormattedByLastUsed($currentUser);

//        $listDigitalPicFrames   = $pictureFrameService->getFormattedPictureFramesByType();
//        $listPaperPicFrames     = $pictureFrameService->getFormattedPictureFramesByType(Optional::TYPE_PAPER_FORMAT);


        return $this->renderByDevice($deviceName, 'PurchasingBundle\\Transaction\\save.html.twig', array(
            'wordName'              => $wordName,
            'user'                  => $currentUser->viewProfileSerializer(),
            'kingWordValue'         => $kingWordValue,
            'expectedRank'          => $expectedRank,
            'packages'              => $listPackages,
            'digitalOptionals'      => $listDigitalOptionals,
            'paperOptionals'        => $listPaperOptionals,
//            'listDigitalPicFrames'  => $listDigitalPicFrames,
//            'listPaperPicFrames'    => $listPaperPicFrames,
            'defaultPaperPicFrame'  => $defaultPaperPicFrame,
            'lastAddressesUsed'     => $lastAddressesUsed,
            'countries'             => $countries,
            'wordDefaultValue'      => Transaction::WORD_DEFAULT_VALUE,
            'invoicePrice'          => Transaction::INVOICE_PRICE,
            'dedicationPrice'       => Transaction::DEDICATION_PRICE
        ));

    }

}
