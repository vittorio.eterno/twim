<?php

namespace PurchasingBundle\Form;


use PurchasingBundle\Entity\Transaction;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use WordBundle\Form\WordType;


class TransactionType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('word', WordType::class)
            ->add('couponCode', TextType::class, array(
                'required'  => false,
                'mapped'    => false
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => Transaction::class,
            'allow_extra_fields' => true,
            'validation_groups' => array('word_type'),
        ));
    }

}