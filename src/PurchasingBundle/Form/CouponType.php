<?php

namespace PurchasingBundle\Form;


use PurchasingBundle\Entity\Coupon;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;


class CouponType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('type', ChoiceType::class, array(
                'choices'  => Coupon::getTypes(),
                'choice_translation_domain' => true
            ))
            ->add('discountPercentage', IntegerType::class, array(
                'required'   => true,
                'empty_data' => '0',
            ))
            ->add('startedAt', DateTimeType::class, array(
                'placeholder' => array(
                    'year' => 'Year', 'month' => 'Month', 'day' => 'Day',
                    'hour' => 'Hour', 'minute' => 'Minute', 'second' => 'Second',
                ),
                'required' => false
            ))
            ->add('expiredAt', DateTimeType::class, array(
                'placeholder' => array(
                    'year' => 'Year', 'month' => 'Month', 'day' => 'Day',
                    'hour' => 'Hour', 'minute' => 'Minute', 'second' => 'Second',
                ),
                'required' => false
            ))
            ->add('isDisabled', ChoiceType::class, array(
                'choices'  => array(
                    'Active'    => false,
                    'Disabled'  => true,
                ),
                'label'    => 'Status'
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => Coupon::class,
        ));
    }

}