<?php

namespace PurchasingBundle\Service;


use Schema\EntityService;
use UserBundle\Entity\User;
use WordBundle\Entity\Word;
use Psr\Log\LoggerInterface;
use Utils\StaticUtil\LogUtils;
use Utils\Service\CacheService;
use Utils\StaticUtil\DateUtils;
use ResourceBundle\Entity\Country;
use PurchasingBundle\Entity\Coupon;
use PurchasingBundle\Entity\Package;
use PurchasingBundle\Entity\Optional;
use Utils\StaticUtil\PaginationUtils;
use UserBundle\Entity\ShippingAddress;
use PurchasingBundle\Entity\Transaction;
use PurchasingBundle\Entity\PurchaseOrder;
use ResourceBundle\Service\CountryService;
use PurchasingBundle\Entity\OptionalStructure;
use PurchasingBundle\Entity\TransactionDetail;
use PurchasingBundle\Repository\TransactionRepository;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class TransactionService extends EntityService {

    /** @var TransactionDetailService $transactionDetailService */
    private $transactionDetailService;

    /** @var CacheService $cacheService */
    private $cacheService;

    /** @var CountryService $countryService */
    private $countryService;

    /** @var PurchaseOrderService $purchaseOrderService */
    private $purchaseOrderService;

    private $taxIt;

    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * TransactionService constructor.
     * @param TransactionRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param LoggerInterface $logger
     * @param TransactionDetailService $transactionDetailService
     * @param CacheService $cacheService
     * @param CountryService $countryService
     * @param PurchaseOrderService $purchaseOrderService
     * @param $taxIt
     */
    public function __construct(TransactionRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                LoggerInterface $logger,
                                TransactionDetailService $transactionDetailService,
                                CacheService $cacheService,
                                CountryService $countryService,
                                PurchaseOrderService $purchaseOrderService,
                                $taxIt
    ) {
        $this->repository               = $repository;
        $this->transactionDetailService = $transactionDetailService;
        $this->taxIt                    = $taxIt;
        $this->cacheService             = $cacheService;
        $this->countryService           = $countryService;
        $this->purchaseOrderService     = $purchaseOrderService;

        parent::__construct($repository, $authorizationChecker, $logger);
    }


    /**
     * @param string $paymentId
     * @param string $payerId
     * @param User $user
     * @param Transaction $transaction
     * @param array $payPalResult
     * @param null|Coupon $coupon
     * @param array|null $packagesOptionals
     * @return null|Transaction
     */
    public function create (string $paymentId, string $payerId, User &$user, Transaction &$transaction, array $payPalResult, ?Coupon &$coupon, ?array &$packagesOptionals) {

        $currentUtcTime = DateUtils::getCurrentTimeUTC();

        $transaction->setUser($user);
        $transaction->setCreatedAt($currentUtcTime);
        $transaction->setPpPaymentId($paymentId);
        $transaction->setPpPayerId($payerId);

        $ppStatus = 0;
        if (isset($payPalResult["state"])) {
            $ppStatus = $transaction->getPpStatusBySource($payPalResult["state"]);
            $transaction->setPpStatus($ppStatus);
        }

//        if (isset($payPalResult["id"])) {
//            $transaction->setPpPaymentId($payPalResult["id"]);
//            if (isset($payPalResult["state"])) {
//                $ppStatus = $transaction->getPpStatusBySource($payPalResult["state"]);
//                $transaction->setPpStatus($ppStatus);
//            }
//        }

        /** @var Word $word */
        $word = $transaction->getWord();
        $word->setCreatedAt($currentUtcTime);
        $word->setEditedAt($currentUtcTime);
        $word->setUser($user);
        $word->setStatusPayment($ppStatus);

        $wordValue = $word->getValue();

        $price = 0;
        $wordOptionals = array();
        if (!is_null($packagesOptionals)) {
            if (isset($packagesOptionals["packages"]) && count($packagesOptionals["packages"])>0) {
                /** @var Package $package */
                foreach ($packagesOptionals["packages"] as $package) {

                    /** @var TransactionDetail $transactionDetail */
                    $transactionDetail = $this->transactionDetailService->createPackageByTransaction($transaction, $package, $currentUtcTime);
                    $transaction->addTransactionDetail($transactionDetail);
                    $this->doPersist($transactionDetail);

                    $price += $package->getPrice();

                    if (!$package->getOptionals()->isEmpty()) {
                        /** @var Optional $optPack */
                        foreach ($package->getOptionals() as $optPack) {
                            if ($optPack->getType() == Optional::TYPE_DIGITAL_FORMAT) {
                                $wordOptionals["ids"][]    = $optPack->getId();
                                $wordOptionals["values"][] = $optPack->getValue();
                            }
                        }
                    }

                }
            }

            if (isset($packagesOptionals["optionals"]) && count($packagesOptionals["optionals"])>0) {

                /** @var Optional $optional */
                foreach ($packagesOptionals["optionals"] as $optional) {

                    /** @var TransactionDetail $transactionDetail */
                    $transactionDetail = $this->transactionDetailService->createOptionalByTransaction($transaction, $optional, $currentUtcTime);
                    $transaction->addTransactionDetail($transactionDetail);
                    $this->doPersist($transactionDetail);

                    if ($optional->getType() == Optional::TYPE_DIGITAL_FORMAT) {
                        $wordOptionals["ids"][]    = $optional->getId();
                        $wordOptionals["values"][] = $optional->getValue();
                    }

                    $price += $optional->getPrice();
                }
            }
        }


        $total = $wordValue + $price;

        if (!is_null($coupon)) {
            $transaction->setCoupon($coupon);
            $transaction->setDiscount($coupon->getDiscountPercentage());

            $totalDiscount = $total / (100*$coupon->getDiscountPercentage());
            $total = $total - $totalDiscount;
        } else {
            $transaction->setDiscount(0);
        }

        $subtotal = $total / (($this->taxIt/100)+1);


        $transaction->setSubtotal($subtotal);
        $transaction->setTotal($total);
        $transaction->setTax($this->taxIt);



        // todo: per il momento il valore della parola comprende anche l'acquisto della stampa e della spedizione di questa
        $word->setValue(($wordValue + $price));
        if (!empty($wordOptionals)) {
            $word->setOptionals(json_encode($wordOptionals));
        }

        $this->doPersist($word);

        if (!$this->save($transaction)) {
            return null;
        }

        return $transaction;
    }


    public function upgrade (string $paymentId, string $payerId, Word &$word, User &$user, Transaction &$transaction, array $payPalResult, ?Coupon &$coupon, ?array &$packagesOptionals) {

        $currentUtcTime = DateUtils::getCurrentTimeUTC();

        $transaction->setUser($user);
        $transaction->setCreatedAt($currentUtcTime);
        $transaction->setPpPaymentId($paymentId);
        $transaction->setPpPayerId($payerId);

        $ppStatus = 0;
        if (isset($payPalResult["state"])) {
            $ppStatus = $transaction->getPpStatusBySource($payPalResult["state"]);
            $transaction->setPpStatus($ppStatus);
        }

        /** @var Word $formWord */
        $formWord  = $transaction->getWord();
        $wordValue = $formWord->getValue();

        $price = 0;
        $wordOptionals = array();
        if (!is_null($packagesOptionals)) {
            if (isset($packagesOptionals["packages"]) && count($packagesOptionals["packages"])>0) {
                /** @var Package $package */
                foreach ($packagesOptionals["packages"] as $package) {

                    /** @var TransactionDetail $transactionDetail */
                    $transactionDetail = $this->transactionDetailService->createPackageByTransaction($transaction, $package, $currentUtcTime);
                    $transaction->addTransactionDetail($transactionDetail);
                    $this->doPersist($transactionDetail);

                    $price += $package->getPrice();

                    if (!$package->getOptionals()->isEmpty()) {
                        /** @var Optional $optPack */
                        foreach ($package->getOptionals() as $optPack) {
                            if ($optPack->getType() == Optional::TYPE_DIGITAL_FORMAT) {
                                $wordOptionals["ids"][]    = $optPack->getId();
                                $wordOptionals["values"][] = $optPack->getValue();
                            }
                        }
                    }

                }
            }

            if (isset($packagesOptionals["optionals"]) && count($packagesOptionals["optionals"])>0) {

                /** @var Optional $optional */
                foreach ($packagesOptionals["optionals"] as $optional) {

                    /** @var TransactionDetail $transactionDetail */
                    $transactionDetail = $this->transactionDetailService->createOptionalByTransaction($transaction, $optional, $currentUtcTime);
                    $transaction->addTransactionDetail($transactionDetail);
                    $this->doPersist($transactionDetail);

                    if ($optional->getType() == Optional::TYPE_DIGITAL_FORMAT) {
                        $wordOptionals["ids"][]    = $optional->getId();
                        $wordOptionals["values"][] = $optional->getValue();
                    }

                    $price += $optional->getPrice();
                }
            }
        }


        $total = $wordValue + $price;
        $tax = 22;

        if (!is_null($coupon)) {
            $transaction->setCoupon($coupon);
            $transaction->setDiscount($coupon->getDiscountPercentage());

            $totalDiscount = $total / (100*$coupon->getDiscountPercentage());
            $total = $total - $totalDiscount;
        } else {
            $transaction->setDiscount(0);
        }

        $subtotal = $total / (($tax/100)+1);


        $transaction->setSubtotal($subtotal);
        $transaction->setTotal($total);
        $transaction->setTax($tax);



        // todo: per il momento il valore della parola comprende anche l'acquisto della stampa e della spedizione di questa

        $word->setStatusPayment($ppStatus);
        $word->setValue(($wordValue + $price  + $word->getValue()));
        if (!empty($wordOptionals)) {
            $word->setOptionals(json_encode($wordOptionals));
        }

        $transaction->setWord($word);

        if (!$this->save($transaction)) {
            return null;
        }

        return $transaction;
    }

    /**
     * @param $paymentId
     * @return mixed|null
     */
    public function getByPaymentId($paymentId) {
        return $this->repository->findByPaymentId($paymentId);
    }

    /**
     * @param int $page
     * @param null|string $suggest
     * @param int $maxResults
     * @return array|\Doctrine\ORM\Tools\Pagination\Paginator
     */
    public function getAllPaginatedTransactions ($page=1, ?string $suggest, $maxResults=TransactionRepository::DEFAULT_MAX_RESULTS) {
        return $this->repository->findAllPaginatedTransactions($page, $suggest, $maxResults);
    }

    /**
     * @param int $page
     * @param null|string $suggest
     * @param int $maxResults
     * @return array
     */
    public function getPaginatedList ($page=1, ?string $suggest, $maxResults=TransactionRepository::DEFAULT_MAX_RESULTS) {
        $transactions = $this->getAllPaginatedTransactions($page, $suggest, $maxResults);
        return $this->getFormattedList($transactions, $page, $maxResults);
    }

    /**
     * @param $transactions
     * @param $page
     * @param $maxResults
     * @return array
     */
    private function getFormattedList (&$transactions,$page,$maxResults) {

        $formatted = array(
            "paginator"    => array(),
            "transactions" => array());

        if (!empty($transactions) && count($transactions)>0) {

            $formatted["paginator"] = PaginationUtils::getPaginatorResponse($transactions,$page,$maxResults);

            $i = 0;

            /** @var Transaction $transaction */
            foreach ($transactions as $transaction) {
                $formatted["transactions"][$i] = $transaction->adminSerializer();
                $i++;
            }
        }

        return $formatted;

    }



    /**
     * @param Word $word
     * @param int $page
     * @param null|string $suggest
     * @param int $maxResults
     * @return array|\Doctrine\ORM\Tools\Pagination\Paginator
     */
    public function getAllPaginatedTransactionsByWord (Word &$word, $page=1, ?string $suggest, $maxResults=TransactionRepository::DEFAULT_MAX_RESULTS) {
        return $this->repository->findAllPaginatedTransactionsByWord($word, $page, $suggest, $maxResults);
    }

    /**
     * @param Word $word
     * @param int $page
     * @param null|string $suggest
     * @param int $maxResults
     * @return array
     */
    public function getPaginatedListByWord (Word &$word, $page=1, ?string $suggest, $maxResults=TransactionRepository::DEFAULT_MAX_RESULTS) {
        $transactions = $this->getAllPaginatedTransactionsByWord($word, $page, $suggest, $maxResults);
        return $this->getFormattedList($transactions, $page, $maxResults);
    }


    /**
     * @param string $paymentId
     * @param string $payerId
     * @param User $user
     * @param Word $word
     * @param Coupon|null $coupon
     * @param array|null $optionals
     * @param Package|null $package
     * @param array|null $certificates
     * @param string|null $invoiceIsRequired
     * @param ShippingAddress|null $shippingAddress
     * @param ShippingAddress|null $invoiceAddress
     * @return Transaction|null
     */
    public function createByUser (string $paymentId, string $payerId,
                                  User &$user, Word &$word,
                                  ?Coupon $coupon=null,
                                  ?array $optionals=null,
                                  ?Package $package=null,
                                  ?array $certificates=null,
                                  ?string $invoiceIsRequired=null,
                                  ?ShippingAddress $shippingAddress=null,
                                  ?ShippingAddress $invoiceAddress=null
    ) {

        $currentUtcTime = DateUtils::getCurrentTimeUTC();

        $transaction = new Transaction();
        $transaction->setWord($word);
        $transaction->setWordHash($word->getHash());
        $transaction->setWordName($word->getName());
        $transaction->setUser($user);
        $transaction->setEmail($user->getEmail());
        $transaction->setUsername($user->getUsername());
        $transaction->setCreatedAt($currentUtcTime);
        $transaction->setPpPaymentId($paymentId);
        $transaction->setPpPayerId($payerId);


        $word->setCreatedAt($currentUtcTime);
        $word->setEditedAt($currentUtcTime);
        $word->setUser($user);

        $wordValue = $word->getValue();

        $wordOptionals = array();

        if (!is_null($optionals) && !empty($optionals) && count($optionals) > 0) {
            /** @var Optional $optional */
            foreach ($optionals as $optional) {

                /** @var OptionalStructure $optionalStructure */
                $optionalStructure = $optional->getOptionalStructure();

                /** @var TransactionDetail $transactionDetail */
                $transactionDetail = $this->transactionDetailService->createOptionalByTransaction($transaction, $optional, $currentUtcTime);
                $transaction->addTransactionDetail($transactionDetail);
                $this->doPersist($transactionDetail);

                if ($optional->getType() == Optional::TYPE_DIGITAL_FORMAT) {
                    $wordOptionals["ids"][]    = $optional->getId();
                    $wordOptionals["values"][] = $optional->getValue();

                    $wordOptionals["actives"][$optionalStructure->getId()] = array("id"=>$optional->getId(), "value"=>$optional->getValue());
                }

            }
        }

        //TODO: quando validerò il pacchetto prendo aggiungo il valore alla parola solo per gli optional di type==digital
//        if (!is_null($packagesOptionals)) {
//            if (isset($packagesOptionals["packages"]) && count($packagesOptionals["packages"])>0) {
//                /** @var Package $package */
//                foreach ($packagesOptionals["packages"] as $package) {
//
//                    /** @var TransactionDetail $transactionDetail */
//                    $transactionDetail = $this->transactionDetailService->createPackageByTransaction($transaction, $package, $currentUtcTime);
//                    $transaction->addTransactionDetail($transactionDetail);
//                    $this->doPersist($transactionDetail);
//
//                    $price += $package->getPrice();
//
//                    if (!$package->getOptionals()->isEmpty()) {
//                        /** @var Optional $optPack */
//                        foreach ($package->getOptionals() as $optPack) {
//                            if ($optPack->getType() == Optional::TYPE_DIGITAL_FORMAT) {
//                                $wordOptionals["ids"][]    = $optPack->getId();
//                                $wordOptionals["values"][] = $optPack->getValue();
//                            }
//                        }
//                    }
//
//                }
//            }
//
//            if (isset($packagesOptionals["optionals"]) && count($packagesOptionals["optionals"])>0) {
//
//                /** @var Optional $optional */
//                foreach ($packagesOptionals["optionals"] as $optional) {
//
//                    /** @var TransactionDetail $transactionDetail */
//                    $transactionDetail = $this->transactionDetailService->createOptionalByTransaction($transaction, $optional, $currentUtcTime);
//                    $transaction->addTransactionDetail($transactionDetail);
//                    $this->doPersist($transactionDetail);
//
//                    if ($optional->getType() == Optional::TYPE_DIGITAL_FORMAT) {
//                        $wordOptionals["ids"][]    = $optional->getId();
//                        $wordOptionals["values"][] = $optional->getValue();
//                    }
//
//                    $price += $optional->getPrice();
//                }
//            }
//        }


        // Create order
        if (!empty($certificates) && count($certificates)>0 && !is_null($shippingAddress)) {

            /** @var PurchaseOrder $purchaseOrder */
            $purchaseOrder = $this->purchaseOrderService->createByTransaction($user, $transaction, $certificates, $shippingAddress, $currentUtcTime);
            $this->doPersist($purchaseOrder);

            foreach ($certificates as $certificate) {
                if (isset($certificate['printFormat']) && !is_null($certificate['printFormat'])) {

                    /** @var TransactionDetail $transactionDetail */
                    $transactionDetail = $this->transactionDetailService->createOptionalByTransaction($transaction, $certificate['printFormat'], $currentUtcTime);
                    $transaction->addTransactionDetail($transactionDetail);
                    $this->doPersist($transactionDetail);

                }
                if (isset($certificate['frame']) && !is_null($certificate['frame'])) {

                    /** @var TransactionDetail $transactionDetail */
                    $transactionDetail = $this->transactionDetailService->createOptionalByTransaction($transaction, $certificate['frame'], $currentUtcTime);
                    $transaction->addTransactionDetail($transactionDetail);
                    $this->doPersist($transactionDetail);

                }
            }
        }

        $certificatesAmount = $this->getCertificatesAmount($certificates,$shippingAddress);

        $totalInfo = $this->getPaymentInfo($wordValue, $certificatesAmount, $coupon, $invoiceIsRequired);

        if (!is_null($invoiceIsRequired) && !is_null($invoiceAddress)) {
            $transaction->setIsInvoiceRequested(true);
            $transaction->setInvoicePrice(Transaction::INVOICE_PRICE);
            $transaction->setInvoiceFirstName($invoiceAddress->getFirstName());
            $transaction->setInvoiceLastName($invoiceAddress->getLastName());
            $transaction->setInvoiceCompanyName($invoiceAddress->getCompanyName());
            $transaction->setInvoiceCompanyAddressInfo($invoiceAddress->getCompanyAddressInfo());
            $transaction->setInvoiceStreetAddress($invoiceAddress->getStreetAddress());
            $transaction->setInvoiceAddressDetail($invoiceAddress->getAddressDetail());
            $transaction->setInvoiceCity($invoiceAddress->getCity());
            $transaction->setInvoiceStateProvince($invoiceAddress->getStateProvince());
            $transaction->setInvoicePostalCode($invoiceAddress->getPostalCode());
            $transaction->setInvoiceCountry($invoiceAddress->getCountry());
            $transaction->setInvoiceCountryName($invoiceAddress->getCountry()->getSource());
            $transaction->setInvoiceEmail($invoiceAddress->getEmail());
            $transaction->setInvoicePhone($invoiceAddress->getPhone());
        }


        if (!is_null($coupon)) {
            $transaction->setCoupon($coupon);
            $transaction->setDiscount($coupon->getDiscountPercentage());
            $transaction->setCouponCode($coupon->getCode());
            $transaction->setCouponDiscountPercentage($coupon->getDiscountPercentage());
            $transaction->setCouponType($coupon->getType());
        } else {
            $transaction->setDiscount(0);
        }

        $transaction->setSubtotal($totalInfo["subtotal"]);
        $transaction->setTotal($totalInfo["total"]);
        $transaction->setTax($totalInfo["tax"]);
        $transaction->setTaxPercentage($this->taxIt);


        if (!empty($wordOptionals)) {
            $word->setOptionals(json_encode($wordOptionals));
        }

        $this->doPersist($word);

        if (!$this->save($transaction)) {
            return null;
        }

        return $transaction;
    }

    /**
     * @param Transaction $transaction
     * @param array $payPalResult
     * @param string|null $expectedRank
     * @return Transaction|null
     */
    public function updateStatusPayment (Transaction &$transaction, array $payPalResult, ?string $expectedRank=null) {

        $ppStatus = 0;
        if (isset($payPalResult["state"])) {
            $ppStatus = $transaction->getPpStatusBySource($payPalResult["state"]);
            $transaction->setPpStatus($ppStatus);
        }

        /** @var Word $word */
        $word = $transaction->getWord();
        $word->setStatusPayment($ppStatus);


        $this->doPersist($word);

        if (!$this->save($transaction)) {
            return null;
        }


        //If he is King
        if ($expectedRank == 1) {
            $this->cacheService->set("king_word_only_value", $word->getValue());
        }

        return $transaction;
    }


    /**
     * @param Transaction $transaction
     * @return Transaction|null
     */
    public function deleteTransaction (Transaction &$transaction) {

        if (!$transaction->getPurchaseOrders()->isEmpty()) {
            /** @var PurchaseOrder $purchaseOrder */
            foreach ($transaction->getPurchaseOrders() as $purchaseOrder) {
                $purchaseOrder->setIsDisabled(true);
                $this->doPersist($purchaseOrder);
            }
        }

        $transaction->setIsDisabled(true);

        if (!$this->save($transaction)) {
            return null;
        }

        return $transaction;
    }


    /**
     * @param int $wordValue
     * @param array|null $optionals
     * @return float|int|null
     */
    public function getFinalWordValue ($wordValue, ?array &$optionals) {

        if (!is_null($optionals) && !empty($optionals) && count($optionals) > 0) {
            /** @var Optional $optional */
            foreach ($optionals as $optional) {
                $addOptional = !is_null($optional->getPrice()) ? $optional->getPrice() : 0;
                $wordValue += $addOptional;
            }
        }

        return $wordValue;
    }


    /**
     * @param array $certificates
     * @param ShippingAddress|null $shippingAddress
     * @return float|int|null
     */
    public function getCertificatesAmount (&$certificates=array(), ShippingAddress $shippingAddress=null) {

        $amount = 0;

        if (!empty($certificates) && count($certificates)>0 && !is_null($shippingAddress)) {
            foreach ($certificates as $certificate) {

                /** @var Optional $printFormat */
                $printFormat            = isset($certificate["printFormat"]) && $certificate["printFormat"] instanceof Optional ? $certificate["printFormat"] : null;
                /** @var Optional $frame */
                $frame                  = isset($certificate["frame"]) && $certificate["frame"] instanceof Optional ? $certificate["frame"] : null;
                $isDedicationRequired   = isset($certificate["isDedicationRequired"]) ? $certificate["isDedicationRequired"] : false;

                if (!is_null($printFormat)) {
                    $addFormat = !is_null($printFormat->getPrice()) ? $printFormat->getPrice() : 0;
                    $amount += $addFormat;
                }

                if (!is_null($frame)) {
                    $addFrame = !is_null($frame->getPrice()) ? $frame->getPrice() : 0;
                    $amount += $addFrame;
                }

                if ($isDedicationRequired) {
                    $amount += Transaction::DEDICATION_PRICE;
                }

            }

            $country = $shippingAddress->getCountry();
            if (!is_null($country)) {
                $deliveryCharges = !is_null($country->getDeliveryCharges()) ? $country->getDeliveryCharges() : 0;
                $amount += $deliveryCharges;
            }

        }

        return $amount;
    }


    /**
     * @param $wordValue
     * @param int $certificateAmount
     * @param Coupon|null $coupon
     * @param string|null $invoiceIsRequired
     * @return array
     */
    public function getPaymentInfo ($wordValue, ?int $certificateAmount=0, ?Coupon $coupon=null, ?string $invoiceIsRequired=null) {

        $info = array(
            "total"         => 0,
            "subtotal"      => 0,
            "tax"           => 0
        );

        $total              = $wordValue;

        if (!is_null($certificateAmount)) {
            $total          += $certificateAmount;
        }

        if (!is_null($invoiceIsRequired)) {
            $total          += Transaction::INVOICE_PRICE;
        }

        if (!is_null($coupon)) {
            $totalDiscount  = $total * ($coupon->getDiscountPercentage() / 100);
            $total          = $total - $totalDiscount;
        }

        $subtotal           = $total / (($this->taxIt / 100) + 1);
        $tax                = $total - $subtotal;

        $info["total"]      = $total;
        $info["subtotal"]   = $subtotal;
        $info["tax"]        = $tax;

        return $info;
    }


}