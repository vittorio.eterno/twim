<?php

namespace PurchasingBundle\Service;


use Psr\Log\LoggerInterface;
use PurchasingBundle\Entity\PurchaseOrder;
use PurchasingBundle\Entity\Transaction;
use PurchasingBundle\Repository\PurchaseOrderRepository;
use Schema\EntityService;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use UserBundle\Entity\ShippingAddress;
use UserBundle\Entity\User;
use Utils\StaticUtil\LogUtils;

class PurchaseOrderService extends EntityService {


    /** @var PurchaseOrderDetailService $purchaseOrderDetailService */
    private $purchaseOrderDetailService;


    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * PurchaseOrderService constructor.
     * @param PurchaseOrderRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param LoggerInterface $logger
     * @param PurchaseOrderDetailService $purchaseOrderDetailService
     */
    public function __construct(PurchaseOrderRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                LoggerInterface $logger,
                                PurchaseOrderDetailService $purchaseOrderDetailService
    ) {
        $this->repository = $repository;
        parent::__construct($repository, $authorizationChecker, $logger);

        $this->purchaseOrderDetailService = $purchaseOrderDetailService;
    }


    /**
     * @param User $user
     * @param Transaction $transaction
     * @param array $certificates
     * @param ShippingAddress $shippingAddress
     * @param $currentUtcTime
     * @return PurchaseOrder
     */
    public function createByTransaction (User &$user, Transaction &$transaction, array &$certificates, ShippingAddress &$shippingAddress, $currentUtcTime) {

        $purchaseOrder = new PurchaseOrder();
        $purchaseOrder->setUser($user);
        $purchaseOrder->setUserEmail($user->getEmail());
        $purchaseOrder->setTransaction($transaction);
        $purchaseOrder->setCreatedAt($currentUtcTime);

        $purchaseOrder->setShippingAddress($shippingAddress);
        $purchaseOrder->setFirstName($shippingAddress->getFirstName());
        $purchaseOrder->setLastName($shippingAddress->getLastName());
        $purchaseOrder->setCompanyName($shippingAddress->getCompanyName());
        $purchaseOrder->setCompanyAddressInfo($shippingAddress->getCompanyAddressInfo());
        $purchaseOrder->setCountry($shippingAddress->getCountry());
        $purchaseOrder->setAddressDetail($shippingAddress->getAddressDetail());
        $purchaseOrder->setStreetAddress($shippingAddress->getStreetAddress());
        $purchaseOrder->setCity($shippingAddress->getCity());
        $purchaseOrder->setStateProvince($shippingAddress->getStateProvince());
        $purchaseOrder->setPostalCode($shippingAddress->getPostalCode());
        $purchaseOrder->setDeliveryCharges($shippingAddress->getCountry()->getDeliveryCharges());
        $purchaseOrder->setEmail($shippingAddress->getEmail());
        $purchaseOrder->setPhone($shippingAddress->getPhone());

        $code = $this->createCode();
        $purchaseOrder->setCode($code);

        foreach ($certificates as $certificate) {

            $purchaseOrderDetail = $this->purchaseOrderDetailService->create($purchaseOrder, $currentUtcTime, $certificate);
            $purchaseOrder->addPurchaseOrderDetail($purchaseOrderDetail);
            $this->doPersist($purchaseOrderDetail);
        }


        return $purchaseOrder;

    }

    /**
     * @return string
     */
    private function createCode () {

        $count = $this->repository->findCountOrders();

        $incrementalCode    = 1;
        if (!is_null($count) && isset($count["counted"])) {
            if ($count["counted"] > 0) {
                $incrementalCode = $count["counted"]+1;
            }
        }

        return 'TWIM-ORDER-'.$incrementalCode;
    }


}