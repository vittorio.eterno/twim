<?php

namespace PurchasingBundle\Service;


use Psr\Log\LoggerInterface;
use PurchasingBundle\Entity\Optional;
use PurchasingBundle\Entity\PurchaseOrder;
use PurchasingBundle\Entity\PurchaseOrderDetail;
use PurchasingBundle\Repository\PurchaseOrderDetailRepository;
use Schema\EntityService;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Utils\StaticUtil\LogUtils;

class PurchaseOrderDetailService extends EntityService {


    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * PurchaseOrderDetailService constructor.
     * @param PurchaseOrderDetailRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param LoggerInterface $logger
     */
    public function __construct(PurchaseOrderDetailRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                LoggerInterface $logger
    ) {
        $this->repository = $repository;
        parent::__construct($repository, $authorizationChecker, $logger);
    }

    /**
     * @param PurchaseOrder $purchaseOrder
     * @param $currentTimeUTC
     * @param array $certificateArray
     * @return PurchaseOrderDetail
     */
    public function create (PurchaseOrder &$purchaseOrder, $currentTimeUTC, $certificateArray=array()) {

        $printFormat            = isset($certificateArray["printFormat"]) ? $certificateArray["printFormat"] : null;
        $frame                  = isset($certificateArray["frame"]) ? $certificateArray["frame"] : null;
        $isDedicationRequired   = isset($certificateArray["isDedicationRequired"]) ? $certificateArray["isDedicationRequired"] : false;
        $dedicateTo             = isset($certificateArray["dedicatedTo"]) ? $certificateArray["dedicatedTo"] : null;
        $dedicationLine1        = isset($certificateArray["dedicationLine1"]) ? $certificateArray["dedicationLine1"] : null;
        $dedicationLine2        = isset($certificateArray["dedicationLine2"]) ? $certificateArray["dedicationLine2"] : null;

        $printFormatSource      = !is_null($printFormat) && $printFormat instanceof Optional ? $printFormat->getSource() : null;
        $frameSource            = !is_null($frame) && $frame instanceof Optional ? $frame->getSource() : null;

        $purchaseOrderDetail = new PurchaseOrderDetail();
        $purchaseOrderDetail->setCreatedAt($currentTimeUTC);
        $purchaseOrderDetail->setQuantity(1);

//        $subCode = $this->createSubCode();
//        $purchaseOrderDetail->setSubCode($subCode);
        $purchaseOrderDetail->setOrderCode($purchaseOrder->getCode());

        $purchaseOrderDetail->setPurchaseOrder($purchaseOrder);
        $purchaseOrderDetail->setSourceFrame($frameSource);
        $purchaseOrderDetail->setSourcePrintFormat($printFormatSource);
        $purchaseOrderDetail->setIsDedicationRequired($isDedicationRequired);
        $purchaseOrderDetail->setDedicatedTo($dedicateTo);
        $purchaseOrderDetail->setDedicationFirstLine($dedicationLine1);
        $purchaseOrderDetail->setDedicationSecondLine($dedicationLine2);

        return $purchaseOrderDetail;
    }


    /**
     * @return string
     */
    private function createSubCode () {

        $count = $this->repository->findCountOrderDetails();

        $incrementalCode    = 1;
        if (!is_null($count) && isset($count["counted"])) {
            if ($count["counted"] > 0) {
                $incrementalCode = $count["counted"]+1;
            }
        }

        return 'TWIM-SUBORDER-'.$incrementalCode;
    }

}