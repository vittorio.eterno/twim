<?php

namespace PurchasingBundle\Service;


use Psr\Log\LoggerInterface;
use PurchasingBundle\Entity\Optional;
use PurchasingBundle\Repository\OptionalRepository;
use Schema\EntityService;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Utils\Service\CacheService;
use Utils\StaticUtil\LogUtils;

class OptionalService extends EntityService {

    /** @var CacheService $cacheService */
    private $cacheService;

    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * OptionalService constructor.
     * @param OptionalRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param LoggerInterface $logger
     * @param CacheService $cacheService
     */
    public function __construct(OptionalRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                LoggerInterface $logger,
                                CacheService $cacheService
    ) {
        $this->repository = $repository;
        parent::__construct($repository, $authorizationChecker, $logger);

        $this->cacheService = $cacheService;
    }


    /**
     * @return array|mixed
     */
    public function getAllActiveOptionals () {
        return $this->repository->findAllActiveOptionals();
    }

    /**
     * @return array|mixed|null
     */
    public function getFormattedActiveOptionals () {

        $formatted = $this->cacheService->get("list_active_optionals");
        if (is_null($formatted)) {
            $optionals = $this->getAllActiveOptionals();
            $formatted = $this->getFormattedOptionalsByType($optionals);

            // Expire after 30 days
            $expire = 60*60*24*30;
            $this->cacheService->set("list_active_optionals", $formatted, $expire);
        }

        return $formatted;

    }

    /**
     * @param $optionals
     * @param string $method
     * @return array
     */
    public function getFormattedOptionalsByType (&$optionals, $method="list") {

        $formatted = array();

        if (!empty($optionals) && count($optionals) > 0) {

            /** @var Optional $optional */
            foreach ($optionals as $optional) {
                $serializerMethod = $method."Serializer";
                $formatted[$optional->getTypeSource()][] = $optional->{$serializerMethod}();
            }
        }

        return $formatted;
    }

    /**
     * @param int $type
     * @return array|mixed
     */
    public function getActiveOptionalsByType ($type = Optional::TYPE_DIGITAL_FORMAT) {
        return $this->repository->findActiveOptionalsByType($type);
    }


    /**
     * @return array|mixed|null
     */
    public function getFormattedDigitalOptionals () {

        $formatted = $this->cacheService->get("list_formatted_digital_optionals");
        if (is_null($formatted)) {
            $optionals = $this->getActiveOptionalsByType();
            $formatted = $this->getFormattedFilteredOptionals($optionals);

            // Expire after 30 days
            $expire = 60*60*24*30;
            $this->cacheService->set("list_formatted_digital_optionals", $formatted, $expire);
        }

        return $formatted;

    }

    /**
     * @return array|mixed|null
     */
    public function getFormattedPaperOptionals () {

        $formatted = $this->cacheService->get("list_formatted_paper_optionals");
        if (is_null($formatted)) {
            $optionals = $this->getActiveOptionalsByType(Optional::TYPE_PAPER_FORMAT);
            $formatted = $this->getFormattedFilteredOptionals($optionals);

            // Expire after 30 days
            $expire = 60*60*24*30;
            $this->cacheService->set("list_formatted_paper_optionals", $formatted, $expire);
        }

        return $formatted;

    }

    /**
     * @param $optionals
     * @param string $method
     * @return array
     */
    public function getFormattedFilteredOptionals (&$optionals, $method="list") {

        $formatted = array();

        if (!empty($optionals) && count($optionals) > 0) {

            /** @var Optional $optional */
            foreach ($optionals as $optional) {
                $serializerMethod = $method."Serializer";

                $optionalStructureSource = $optional->serializedOptionaleStructureSource();
                if (is_null($optionalStructureSource)) {
                    $optionalStructureSource = 'nothing';
                }

                $formatted[$optionalStructureSource][] = $optional->{$serializerMethod}();
            }
        }

        return $formatted;
    }


}