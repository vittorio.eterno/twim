<?php

namespace PurchasingBundle\Service;


use Psr\Log\LoggerInterface;
use PurchasingBundle\Entity\Optional;
use PurchasingBundle\Entity\Package;
use PurchasingBundle\Entity\Transaction;
use PurchasingBundle\Entity\TransactionDetail;
use PurchasingBundle\Repository\TransactionDetailRepository;
use Schema\EntityService;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Utils\StaticUtil\LogUtils;

class TransactionDetailService extends EntityService {


    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * TransactionDetailService constructor.
     * @param TransactionDetailRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param LoggerInterface $logger
     */
    public function __construct(TransactionDetailRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                LoggerInterface $logger
    ) {
        $this->repository = $repository;
        parent::__construct($repository, $authorizationChecker, $logger);
    }

    /**
     * @param Transaction $transaction
     * @param Optional $optional
     * @param $currentUtcTime
     * @return TransactionDetail
     */
    public function createOptionalByTransaction (Transaction &$transaction, Optional &$optional, $currentUtcTime) {
        $transactionDetail = new TransactionDetail();
        $transactionDetail->setOptional($optional);
        $transactionDetail->setCreatedAt($currentUtcTime);
        $transactionDetail->setTransaction($transaction);
        $transactionDetail->setOptionalDescription($optional->getDescription());
        $transactionDetail->setPrice($optional->getPrice());
        $transactionDetail->setSource($optional->getSource());
        return $transactionDetail;
    }

    /**
     * @param Transaction $transaction
     * @param Package $package
     * @param $currentUtcTime
     * @return TransactionDetail
     */
    public function createPackageByTransaction (Transaction &$transaction, Package &$package, $currentUtcTime) {
        $transactionDetail = new TransactionDetail();
        $transactionDetail->setPackage($package);
        $transactionDetail->setCreatedAt($currentUtcTime);
        $transactionDetail->setTransaction($transaction);
        return $transactionDetail;
    }

}