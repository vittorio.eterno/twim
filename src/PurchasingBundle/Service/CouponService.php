<?php

namespace PurchasingBundle\Service;


use Psr\Log\LoggerInterface;
use PurchasingBundle\Entity\Coupon;
use PurchasingBundle\Repository\CouponRepository;
use Schema\EntityService;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use UserBundle\Entity\User;
use Utils\StaticUtil\DateUtils;
use Utils\StaticUtil\LogUtils;
use Utils\StaticUtil\PaginationUtils;

class CouponService extends EntityService {


    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * CouponService constructor.
     * @param CouponRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param LoggerInterface $logger
     */
    public function __construct(CouponRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                LoggerInterface $logger
    ) {
        $this->repository = $repository;
        parent::__construct($repository, $authorizationChecker, $logger);
    }

    /**
     * @param int $page
     * @param null|string $suggest
     * @param int $maxResults
     * @return array|\Doctrine\ORM\Tools\Pagination\Paginator
     */
    public function getAllPaginatedCoupons ($page=1, ?string $suggest, $maxResults=CouponRepository::DEFAULT_MAX_RESULTS) {
        return $this->repository->findAllPaginatedCoupons($page, $suggest, $maxResults);
    }

    /**
     * @param int $page
     * @param null|string $suggest
     * @param int $maxResults
     * @return array
     */
    public function getPaginatedList ($page=1, ?string $suggest, $maxResults=CouponRepository::DEFAULT_MAX_RESULTS) {
        $coupons = $this->getAllPaginatedCoupons($page, $suggest, $maxResults);
        return $this->getFormattedList($coupons, $page, $maxResults);
    }

    /**
     * @param $coupons
     * @param $page
     * @param $maxResults
     * @return array
     */
    private function getFormattedList (&$coupons,$page,$maxResults) {

        $formatted = array(
            "paginator" => array(),
            "coupons"     => array()
        );

        if (!empty($coupons) && count($coupons)>0) {

            $formatted["paginator"] = PaginationUtils::getPaginatorResponse($coupons,$page,$maxResults);

            $i = 0;

            /** @var Coupon $coupon */
            foreach ($coupons as $coupon) {
                $formatted["coupons"][$i] = $coupon->adminSerializer();
                $i++;
            }
        }

        return $formatted;

    }


    /**
     * @return array|mixed
     */
    public function getAllSpecials () {
        return $this->repository->findAllSpecials();
    }

    /**
     * @return array
     */
    public function getFormattedSpecials () {
        $coupons = $this->getAllSpecials();
        return $this->getFormattedListSpecials($coupons);
    }

    /**
     * @param array $coupons
     * @return array
     */
    private function getFormattedListSpecials (array &$coupons) {

        $formatted = array();

        if (!empty($coupons) && count($coupons) > 0) {
            /** @var Coupon $coupon */
            foreach ($coupons as $coupon) {
                $formatted[] = $coupon->adminSerializer();
            }
        }

        return $formatted;
    }

    /**
     * @param Coupon $coupon
     * @param $count
     * @param bool $update
     * @return string
     */
    private function createCode (Coupon &$coupon,$count,$update=false) {

        $typeCode           = $coupon->getTypeCode();
        $discountPercentage = $coupon->getDiscountPercentage();

        $incrementalCode    = '0000001';
        if (!is_null($count) && isset($count["counted"])) {
            if ($count["counted"] > 0) {

                if (!$update) {
                    $newCount = (int) $count["counted"] + 1;

                    if (strlen($newCount) == 6) {
                        $incrementalCode = '0'.$newCount;
                    } elseif (strlen($newCount) == 5) {
                        $incrementalCode = '00'.$newCount;
                    } elseif (strlen($newCount) == 4) {
                        $incrementalCode = '000'.$newCount;
                    } elseif (strlen($newCount) == 3) {
                        $incrementalCode = '0000'.$newCount;
                    } elseif (strlen($newCount) == 2) {
                        $incrementalCode = '00000'.$newCount;
                    } elseif (strlen($newCount) == 1) {
                        $incrementalCode = '000000'.$newCount;
                    } else {
                        $incrementalCode = $newCount;
                    }
                } else {
                    $incrementalCode = substr($coupon->getCode(), -7);
                }

            }
        }

        return 'TWIM-'.$typeCode.$discountPercentage.'-'.$incrementalCode;
    }

    /**
     * @param User $currentUser
     * @param Coupon $coupon
     * @return null|Coupon
     */
    public function saveCoupon (User &$currentUser, Coupon &$coupon) {

        $coupon->setCreatedAt(DateUtils::getCurrentTimeUTC());

        $update = is_null($coupon->getId()) ? false : true;

        $count = $this->repository->findCountCoupons();
        $coupon->setCode($this->createCode($coupon,$count,$update));

        if (!$this->save($coupon,$update)) {
            return null;
        }

        return $coupon;
    }

    /**
     * @param null|string $code
     * @return null|Coupon
     */
    public function getByCode (?string $code) {
        $coupon = null;
        if (!is_null($code)) {
            /** @var Coupon $coupon */
            $coupon = $this->repository->findByCode($code);
        }
        return $coupon;
    }

}