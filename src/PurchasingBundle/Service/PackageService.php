<?php

namespace PurchasingBundle\Service;


use Psr\Log\LoggerInterface;
use PurchasingBundle\Entity\Package;
use PurchasingBundle\Repository\PackageRepository;
use Schema\EntityService;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Utils\Service\CacheService;
use Utils\StaticUtil\LogUtils;

class PackageService extends EntityService {

    /** @var CacheService $cacheService */
    private $cacheService;

    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * PackageService constructor.
     * @param PackageRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param LoggerInterface $logger
     * @param CacheService $cacheService
     */
    public function __construct(PackageRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                LoggerInterface $logger,
                                CacheService $cacheService
    ) {
        $this->repository = $repository;
        parent::__construct($repository, $authorizationChecker, $logger);

        $this->cacheService = $cacheService;
    }

    /**
     * @return array|mixed
     */
    public function getAllPublicActivePackages () {
        return $this->repository->findAllActivePackagesByType();
    }

    /**
     * @return array|mixed|null
     */
    public function getFormattedPublicActivePackages () {

        $formatted = $this->cacheService->get("list_public_active_packages");
        if (is_null($formatted)) {
            $packages  = $this->getAllPublicActivePackages();
            $formatted = $this->getFormattedPackages($packages);

            // Expire after 30 days
            $expire = 60*60*24*30;
            $this->cacheService->set("list_public_active_packages", $formatted, $expire);
        }

        return $formatted;
    }

    /**
     * @param $packages
     * @param string $method
     * @return array
     */
    public function getFormattedPackages (&$packages, $method="list") {

        $formatted = array();
        if (!empty($packages) && count($packages) > 0) {
            /** @var Package $package */
            foreach ($packages as $package) {
                $serializerMethod = $method."Serializer";
                $formatted[]      = $package->{$serializerMethod}();
            }
        }

        return $formatted;
    }

}