<?php

namespace PurchasingBundle\Entity;

use Schema\Entity;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * PurchaseOrderDetail
 *
 * @ORM\Table(name="purchase_order_details")
 * @ORM\Entity(repositoryClass="PurchasingBundle\Repository\PurchaseOrderDetailRepository")
 */
class PurchaseOrderDetail extends Entity {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Many PurchaseOrderDetails have One PurchaseOrder.
     * @ORM\ManyToOne(targetEntity="PurchaseOrder", inversedBy="purchaseOrderDetails")
     * @ORM\JoinColumn(name="purchase_order_id", referencedColumnName="id")
     */
    private $purchaseOrder;

//    /**
//     * @var string
//     *
//     * @ORM\Column(name="sub_code", type="string", length=255, unique=true)
//     */
//    private $subCode;

    /**
     * @var string
     *
     * @ORM\Column(name="order_code", type="string", length=255)
     */
    private $orderCode;

    /**
     * @var int|null
     *
     * @ORM\Column(name="quantity", type="integer", nullable=true, options={"default":0})
     */
    private $quantity;

    /**
     * @var string
     *
     * @ORM\Column(name="source_print_format", type="string", length=255)
     */
    private $sourcePrintFormat;

    /**
     * @var string
     *
     * @ORM\Column(name="source_frame", type="string", length=255)
     */
    private $sourceFrame;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_dedication_required", type="boolean", options={"default":0})
     */
    private $isDedicationRequired=false;

    /**
     * @var string|null
     *
     * @ORM\Column(name="dedicated_to", type="string", length=255, nullable=true)
     */
    private $dedicatedTo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="dedication_first_line", type="string", length=255, nullable=true)
     */
    private $dedicationFirstLine;

    /**
     * @var string|null
     *
     * @ORM\Column(name="dedication_second_line", type="string", length=255, nullable=true)
     */
    private $dedicationSecondLine;

    /**
     * @var string|null
     *
     * @ORM\Column(name="note", type="text", nullable=true)
     */
    private $note;

    /**
     * @var int|null
     *
     * @ORM\Column(name="status", type="smallint", nullable=true, options={"default":0})
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;


    ################################################# SERIALIZER FUNCTIONS

    /**
     * Returns the array of fields to serialize in entity administration view.
     * @return array
     */
    public function adminSerializer() {
        $view_vars = $this->viewSerializer();

        $admin_vars = array();

        return array_merge($view_vars, $admin_vars);
    }

    /**
     * Returns the array of fields to serialize in entity view.
     * @return array
     */
    public function viewSerializer() {
        $list_vars = $this->listSerializer();

        $view_vars = array(
        );

        return array_merge($list_vars, $view_vars);
    }

    /**
     * Returns the array of fields to serialize in a list of this entity.
     * @return array
     */
    public function listSerializer() {
        $list_vars = array(
            'id'           => $this->serializedId()
        );
        return $list_vars;
    }

    /**
     * Returns the hash code unique identifier of the entity.
     * @return string
     */
    public function hashCode() {
        // TODO: Implement hashCode() method.
    }

    ################################################# SERIALIZED FUNCTIONS

    /**
     * Setting id
     * @JMS\VirtualProperty
     * @JMS\SerializedName("id")
     * @JMS\Type("integer")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedId() {
        return (is_null($this->id)?null:$this->id);
    }


    ################################################# GETTERS AND SETTERS FUNCTIONS


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return PurchaseOrder
     */
    public function getPurchaseOrder()
    {
        return $this->purchaseOrder;
    }

    /**
     * @param PurchaseOrder $purchaseOrder
     */
    public function setPurchaseOrder(PurchaseOrder $purchaseOrder): void
    {
        $this->purchaseOrder = $purchaseOrder;
    }

//    /**
//     * Set subCode.
//     *
//     * @param string $subCode
//     *
//     * @return PurchaseOrderDetail
//     */
//    public function setSubCode($subCode)
//    {
//        $this->subCode = $subCode;
//
//        return $this;
//    }
//
//    /**
//     * Get subCode.
//     *
//     * @return string
//     */
//    public function getSubCode()
//    {
//        return $this->subCode;
//    }

    /**
     * Set quantity.
     *
     * @param int|null $quantity
     *
     * @return PurchaseOrderDetail
     */
    public function setQuantity($quantity = null)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity.
     *
     * @return int|null
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set dedicatedTo.
     *
     * @param string|null $dedicatedTo
     *
     * @return PurchaseOrderDetail
     */
    public function setDedicatedTo($dedicatedTo = null)
    {
        $this->dedicatedTo = $dedicatedTo;

        return $this;
    }

    /**
     * Get dedicatedTo.
     *
     * @return string|null
     */
    public function getDedicatedTo()
    {
        return $this->dedicatedTo;
    }

    /**
     * Set note.
     *
     * @param string|null $note
     *
     * @return PurchaseOrderDetail
     */
    public function setNote($note = null)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note.
     *
     * @return string|null
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set status.
     *
     * @param int|null $status
     *
     * @return PurchaseOrderDetail
     */
    public function setStatus($status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return int|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getSourcePrintFormat(): string
    {
        return $this->sourcePrintFormat;
    }

    /**
     * @param string $sourcePrintFormat
     */
    public function setSourcePrintFormat(string $sourcePrintFormat): void
    {
        $this->sourcePrintFormat = $sourcePrintFormat;
    }

    /**
     * @return string
     */
    public function getSourceFrame(): string
    {
        return $this->sourceFrame;
    }

    /**
     * @param string $sourceFrame
     */
    public function setSourceFrame(string $sourceFrame): void
    {
        $this->sourceFrame = $sourceFrame;
    }

    /**
     * @return bool
     */
    public function getIsDedicationRequired(): bool
    {
        return $this->isDedicationRequired;
    }

    /**
     * @param bool $isDedicationRequired
     */
    public function setIsDedicationRequired(bool $isDedicationRequired): void
    {
        $this->isDedicationRequired = $isDedicationRequired;
    }


    /**
     * @return null|string
     */
    public function getDedicationFirstLine(): ?string
    {
        return $this->dedicationFirstLine;
    }

    /**
     * @param null|string $dedicationFirstLine
     */
    public function setDedicationFirstLine(?string $dedicationFirstLine): void
    {
        $this->dedicationFirstLine = $dedicationFirstLine;
    }

    /**
     * @return null|string
     */
    public function getDedicationSecondLine(): ?string
    {
        return $this->dedicationSecondLine;
    }

    /**
     * @param null|string $dedicationSecondLine
     */
    public function setDedicationSecondLine(?string $dedicationSecondLine): void
    {
        $this->dedicationSecondLine = $dedicationSecondLine;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function getOrderCode(): string
    {
        return $this->orderCode;
    }

    /**
     * @param string $orderCode
     */
    public function setOrderCode(string $orderCode): void
    {
        $this->orderCode = $orderCode;
    }



}
