<?php

namespace PurchasingBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use ResourceBundle\Entity\Country;
use Schema\Entity;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use UserBundle\Entity\User;
use WordBundle\Entity\Word;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Transaction
 *
 * @ORM\Table(name="transactions")
 * @ORM\Entity(repositoryClass="PurchasingBundle\Repository\TransactionRepository")
 */
class Transaction extends Entity {

    const PP_STATUS_CREATED         = 0;
    const PP_STATUS_APPROVED        = 1;
    const PP_STATUS_FAILED          = 2;

    const PP_STATUS_CREATED_SOURCE  = "created";
    const PP_STATUS_APPROVED_SOURCE = "approved";
    const PP_STATUS_FAILED_SOURCE   = "failed";

    const INVOICE_PRICE             = 5;
    const DEDICATION_PRICE          = 9;

    const WORD_DEFAULT_VALUE        = 10;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Many Transactions have One User.
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * One transaction has Many PurchaseOrders.
     * @ORM\OneToMany(targetEntity="PurchaseOrder", mappedBy="transaction")
     */
    private $purchaseOrders;

    /**
     * @ORM\Column(name="`username`", type="string", length=255)
     */
    private $username;

    /**
     * @ORM\Column(name="`email`", type="string", length=255)
     */
    private $email;

    /**
     * Many Transactions have One Coupon.
     * @ORM\ManyToOne(targetEntity="Coupon")
     * @ORM\JoinColumn(name="coupon_id", referencedColumnName="id")
     */
    private $coupon;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="coupon_discount_percentage", type="integer", nullable=true, options={"default":0})
     */
    private $couponDiscountPercentage;

    /**
     * @var string|null
     *
     * @ORM\Column(name="coupon_code", type="string", length=50, nullable=true)
     */
    private $couponCode;

    /**
     * @var int|null
     *
     * @ORM\Column(name="coupon_type", type="smallint", nullable=true, options={"default":0})
     */
    private $couponType;

    /**
     * Many TransactionsWord have One Word.
     * @ORM\ManyToOne(targetEntity="WordBundle\Entity\Word")
     * @ORM\JoinColumn(name="word_id", referencedColumnName="id")
     * @Assert\Type(type="WordBundle\Entity\Word")
     * @Assert\Valid()
     */
    private $word;

    /**
     * @var string
     *
     * @ORM\Column(name="word_name", type="string", length=80)
     */
    private $wordName;

    /**
     * @var string
     *
     * @ORM\Column(name="word_hash", type="string", length=200)
     */
    private $wordHash;

    /**
     * One Transaction has Many TransactionDetails.
     * @ORM\OneToMany(targetEntity="TransactionDetail", mappedBy="transaction")
     */
    private $transactionDetails;

    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float", options={"unsigned"=true,"default":0})
     */
    private $total;

    /**
     * @var float
     *
     * @ORM\Column(name="subtotal", type="float", options={"unsigned"=true,"default":0})
     */
    private $subtotal;

    /**
     * @var float
     *
     * @ORM\Column(name="discount", type="float", options={"unsigned"=true,"default":0})
     */
    private $discount=0;

    /**
     * @var float
     *
     * @ORM\Column(name="tax", type="float", options={"default":0})
     */
    private $tax;

    /**
     * @var float
     *
     * @ORM\Column(name="tax_percentage", type="float", options={"default":0})
     */
    private $taxPercentage;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="edited_at", type="datetime", nullable=true)
     */
    private $editedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="pp_payment_id", type="string", length=255, nullable=true)
     */
    private $ppPaymentId;

    /**
     * @var string
     *
     * @ORM\Column(name="pp_payer_id", type="string", length=255, nullable=true)
     */
    private $ppPayerId;

    /**
     * @var int
     *
     * @ORM\Column(name="pp_status", type="smallint", nullable=true, options={"default":0})
     */
    private $ppStatus;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_invoice_requested", type="boolean", nullable=true, options={"default":0})
     */
    private $isInvoiceRequested;

    /**
     * @var float
     *
     * @ORM\Column(name="invoice_price", type="float", options={"default":0})
     */
    private $invoicePrice=0;

    /**
     * @var string|null
     *
     * @ORM\Column(name="note", type="string", length=255, nullable=true)
     */
    private $note;

    /**
     * @var string|null
     *
     * @ORM\Column(name="invoice_first_name", type="string", length=255, nullable=true)
     */
    private $invoiceFirstName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="invoice_last_name", type="string", length=255, nullable=true)
     */
    private $invoiceLastName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="invoice_company_name", type="string", length=255, nullable=true)
     */
    private $invoiceCompanyName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="invoice_company_address_info", type="string", length=255, nullable=true)
     */
    private $invoiceCompanyAddressInfo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="invoice_street_address", type="string", length=255, nullable=true)
     */
    private $invoiceStreetAddress;

    /**
     * @var string|null
     *
     * @ORM\Column(name="invoice_address_detail", type="string", length=255, nullable=true)
     */
    private $invoiceAddressDetail;

    /**
     * @var string|null
     *
     * @ORM\Column(name="invoice_postal_code", type="string", length=255, nullable=true)
     */
    private $invoicePostalCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="invoice_city", type="string", length=255, nullable=true)
     */
    private $invoiceCity;

    /**
     * @var string|null
     *
     * @ORM\Column(name="invoice_state_province", type="string", length=255, nullable=true)
     */
    private $invoiceStateProvince;


    /**
     * Many Transactions have One Country.
     * @ORM\ManyToOne(targetEntity="ResourceBundle\Entity\Country")
     * @ORM\JoinColumn(name="invoice_country_id", referencedColumnName="id")
     */
    private $invoiceCountry;

    /**
     * @var string|null
     *
     * @ORM\Column(name="invoice_country_name", type="string", length=255, nullable=true)
     */
    private $invoiceCountryName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="invoice_email", type="string", length=255, nullable=true)
     */
    private $invoiceEmail;

    /**
     * @var string|null
     *
     * @ORM\Column(name="invoice_phone", type="string", length=255, nullable=true)
     */
    private $invoicePhone;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_disabled", type="boolean", nullable=true, options={"default":0})
     */
    private $isDisabled=false; // when it is set to true, then a Paypal Execute Call error has occured


    ################################################# SERIALIZER FUNCTIONS

    /**
     * Returns the array of fields to serialize in entity administration view.
     * @return array
     */
    public function adminSerializer() {
        $view_vars = $this->viewSerializer();

        $admin_vars = array();

        return array_merge($view_vars, $admin_vars);
    }

    /**
     * Returns the array of fields to serialize in entity view.
     * @return array
     */
    public function viewSerializer() {
        $list_vars = $this->listSerializer();

        $view_vars = array(
        );

        return array_merge($list_vars, $view_vars);
    }

    /**
     * Returns the array of fields to serialize in a list of this entity.
     * @return array
     */
    public function listSerializer() {
        $list_vars = array(
            'id'           => $this->serializedId(),
            'total'        => $this->serializedTotal(),
            'subtotal'     => $this->serializedSubtotal(),
            'discount'     => $this->serializedDiscount(),
            'tax'          => $this->serializedTax(),
            'word'         => $this->serializedWord(),
        );
        return $list_vars;
    }

    /**
     * Returns the hash code unique identifier of the entity.
     * @return string
     */
    public function hashCode() {
        // TODO: Implement hashCode() method.
    }

    ################################################# SERIALIZED FUNCTIONS

    /**
     * Transaction id
     * @JMS\VirtualProperty
     * @JMS\SerializedName("id")
     * @JMS\Type("integer")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedId() {
        return (is_null($this->id)?null:$this->id);
    }

    /**
     * Transaction total
     * @JMS\VirtualProperty
     * @JMS\SerializedName("total")
     * @JMS\Type("float")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedTotal() {
        return (is_null($this->total)?null:$this->total);
    }

    /**
     * Transaction subtotal
     * @JMS\VirtualProperty
     * @JMS\SerializedName("subtotal")
     * @JMS\Type("float")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedSubtotal() {
        return (is_null($this->subtotal)?null:$this->subtotal);
    }

    /**
     * Transaction discount
     * @JMS\VirtualProperty
     * @JMS\SerializedName("discount")
     * @JMS\Type("float")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedDiscount() {
        return (is_null($this->discount)?null:$this->discount);
    }

    /**
     * Transaction tax
     * @JMS\VirtualProperty
     * @JMS\SerializedName("tax")
     * @JMS\Type("float")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedTax() {
        return (is_null($this->tax)?null:$this->tax);
    }

    /**
     * Transaction word
     * @JMS\VirtualProperty
     * @JMS\SerializedName("word")
     * @JMS\Type("array")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedWord() {
        return (!is_null($this->getWord()) && $this->getWord() instanceof Word ? $this->getWord()->listSerializer():null);
    }

    ################################################# GETTERS AND SETTERS FUNCTIONS

    /**
     * Transaction constructor.
     */
    public function __construct() {
        $this->transactionDetails   = new ArrayCollection();
        $this->purchaseOrders       = new ArrayCollection();
    }


    /**
     * @param $source
     * @return int
     */
    public function getPpStatusBySource ($source) {
        switch ($source) {
            case self::PP_STATUS_APPROVED_SOURCE:
                $statusPayment = self::PP_STATUS_APPROVED;
                break;
            case self::PP_STATUS_FAILED_SOURCE:
                $statusPayment = self::PP_STATUS_FAILED;
                break;
            default:
                $statusPayment = self::PP_STATUS_CREATED;
        }

        return $statusPayment;
    }



    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Add transactionDetail.
     *
     * @param TransactionDetail $transactionDetail
     *
     * @return $this
     */
    public function addTransactionDetail(TransactionDetail $transactionDetail)
    {
        $this->transactionDetails[] = $transactionDetail;

        return $this;
    }

    /**
     * Remove transactionDetail.
     *
     * @param TransactionDetail $transactionDetail
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeTransactionDetail(TransactionDetail $transactionDetail)
    {
        return $this->transactionDetails->removeElement($transactionDetail);
    }

    /**
     * Get transactionDetails.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTransactionDetails()
    {
        return $this->transactionDetails;
    }



    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return Coupon
     */
    public function getCoupon()
    {
        return $this->coupon;
    }

    /**
     * @param Coupon $coupon
     */
    public function setCoupon(Coupon $coupon): void
    {
        $this->coupon = $coupon;
    }

    /**
     * @return Word
     */
    public function getWord()
    {
        return $this->word;
    }

    /**
     * @param Word $word
     */
    public function setWord(Word $word): void
    {
        $this->word = $word;
    }


    /**
     * Set total.
     *
     * @param float $total
     *
     * @return Transaction
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total.
     *
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set subtotal.
     *
     * @param float $subtotal
     *
     * @return Transaction
     */
    public function setSubtotal($subtotal)
    {
        $this->subtotal = $subtotal;

        return $this;
    }

    /**
     * Get subtotal.
     *
     * @return float
     */
    public function getSubtotal()
    {
        return $this->subtotal;
    }

    /**
     * Set discount.
     *
     * @param float $discount
     *
     * @return Transaction
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount.
     *
     * @return float
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set tax.
     *
     * @param float $tax
     *
     * @return Transaction
     */
    public function setTax($tax)
    {
        $this->tax = $tax;

        return $this;
    }

    /**
     * Get tax.
     *
     * @return float
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Transaction
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getEditedAt(): \DateTime
    {
        return $this->editedAt;
    }

    /**
     * @param \DateTime $editedAt
     */
    public function setEditedAt(\DateTime $editedAt): void
    {
        $this->editedAt = $editedAt;
    }

    /**
     * Set ppPaymentId.
     *
     * @param string $ppPaymentId
     *
     * @return Transaction
     */
    public function setPpPaymentId($ppPaymentId)
    {
        $this->ppPaymentId = $ppPaymentId;

        return $this;
    }

    /**
     * Get ppPaymentId.
     *
     * @return string
     */
    public function getPpPaymentId()
    {
        return $this->ppPaymentId;
    }

    /**
     * @return string
     */
    public function getPpPayerId(): string
    {
        return $this->ppPayerId;
    }

    /**
     * @param string $ppPayerId
     */
    public function setPpPayerId(string $ppPayerId): void
    {
        $this->ppPayerId = $ppPayerId;
    }

    /**
     * Set ppStatus.
     *
     * @param int $ppStatus
     *
     * @return Transaction
     */
    public function setPpStatus($ppStatus)
    {
        $this->ppStatus = $ppStatus;

        return $this;
    }

    /**
     * Get ppStatus.
     *
     * @return int
     */
    public function getPpStatus()
    {
        return $this->ppStatus;
    }

    /**
     * @return null|string
     */
    public function getNote(): ?string
    {
        return $this->note;
    }

    /**
     * @param null|string $note
     */
    public function setNote(?string $note): void
    {
        $this->note = $note;
    }

    /**
     * @return bool|null
     */
    public function getIsInvoiceRequested(): ?bool
    {
        return $this->isInvoiceRequested;
    }

    /**
     * @param bool|null $isInvoiceRequested
     */
    public function setIsInvoiceRequested(?bool $isInvoiceRequested): void
    {
        $this->isInvoiceRequested = $isInvoiceRequested;
    }

    /**
     * @return float
     */
    public function getInvoicePrice(): float
    {
        return $this->invoicePrice;
    }

    /**
     * @param float $invoicePrice
     */
    public function setInvoicePrice(float $invoicePrice): void
    {
        $this->invoicePrice = $invoicePrice;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username): void
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return int|null
     */
    public function getCouponDiscountPercentage(): ?int
    {
        return $this->couponDiscountPercentage;
    }

    /**
     * @param int|null $couponDiscountPercentage
     */
    public function setCouponDiscountPercentage(?int $couponDiscountPercentage): void
    {
        $this->couponDiscountPercentage = $couponDiscountPercentage;
    }

    /**
     * @return null|string
     */
    public function getCouponCode(): ?string
    {
        return $this->couponCode;
    }

    /**
     * @param null|string $couponCode
     */
    public function setCouponCode(?string $couponCode): void
    {
        $this->couponCode = $couponCode;
    }

    /**
     * @return int|null
     */
    public function getCouponType(): ?int
    {
        return $this->couponType;
    }

    /**
     * @param int|null $couponType
     */
    public function setCouponType(?int $couponType): void
    {
        $this->couponType = $couponType;
    }

    /**
     * @return string
     */
    public function getWordName(): string
    {
        return $this->wordName;
    }

    /**
     * @param string $wordName
     */
    public function setWordName(string $wordName): void
    {
        $this->wordName = $wordName;
    }

    /**
     * @return string
     */
    public function getWordHash(): string
    {
        return $this->wordHash;
    }

    /**
     * @param string $wordHash
     */
    public function setWordHash(string $wordHash): void
    {
        $this->wordHash = $wordHash;
    }

    /**
     * @return null|string
     */
    public function getInvoiceFirstName(): ?string
    {
        return $this->invoiceFirstName;
    }

    /**
     * @param null|string $invoiceFirstName
     */
    public function setInvoiceFirstName(?string $invoiceFirstName): void
    {
        $this->invoiceFirstName = $invoiceFirstName;
    }

    /**
     * @return null|string
     */
    public function getInvoiceLastName(): ?string
    {
        return $this->invoiceLastName;
    }

    /**
     * @param null|string $invoiceLastName
     */
    public function setInvoiceLastName(?string $invoiceLastName): void
    {
        $this->invoiceLastName = $invoiceLastName;
    }

    /**
     * @return null|string
     */
    public function getInvoiceCompanyName(): ?string
    {
        return $this->invoiceCompanyName;
    }

    /**
     * @param null|string $invoiceCompanyName
     */
    public function setInvoiceCompanyName(?string $invoiceCompanyName): void
    {
        $this->invoiceCompanyName = $invoiceCompanyName;
    }

    /**
     * @return null|string
     */
    public function getInvoiceCompanyAddressInfo(): ?string
    {
        return $this->invoiceCompanyAddressInfo;
    }

    /**
     * @param null|string $invoiceCompanyAddressInfo
     */
    public function setInvoiceCompanyAddressInfo(?string $invoiceCompanyAddressInfo): void
    {
        $this->invoiceCompanyAddressInfo = $invoiceCompanyAddressInfo;
    }

    /**
     * @return null|string
     */
    public function getInvoiceStreetAddress(): ?string
    {
        return $this->invoiceStreetAddress;
    }

    /**
     * @param null|string $invoiceStreetAddress
     */
    public function setInvoiceStreetAddress(?string $invoiceStreetAddress): void
    {
        $this->invoiceStreetAddress = $invoiceStreetAddress;
    }

    /**
     * @return null|string
     */
    public function getInvoiceAddressDetail(): ?string
    {
        return $this->invoiceAddressDetail;
    }

    /**
     * @param null|string $invoiceAddressDetail
     */
    public function setInvoiceAddressDetail(?string $invoiceAddressDetail): void
    {
        $this->invoiceAddressDetail = $invoiceAddressDetail;
    }

    /**
     * @return null|string
     */
    public function getInvoicePostalCode(): ?string
    {
        return $this->invoicePostalCode;
    }

    /**
     * @param null|string $invoicePostalCode
     */
    public function setInvoicePostalCode(?string $invoicePostalCode): void
    {
        $this->invoicePostalCode = $invoicePostalCode;
    }

    /**
     * @return null|string
     */
    public function getInvoiceCity(): ?string
    {
        return $this->invoiceCity;
    }

    /**
     * @param null|string $invoiceCity
     */
    public function setInvoiceCity(?string $invoiceCity): void
    {
        $this->invoiceCity = $invoiceCity;
    }

    /**
     * @return null|string
     */
    public function getInvoiceStateProvince(): ?string
    {
        return $this->invoiceStateProvince;
    }

    /**
     * @param null|string $invoiceStateProvince
     */
    public function setInvoiceStateProvince(?string $invoiceStateProvince): void
    {
        $this->invoiceStateProvince = $invoiceStateProvince;
    }

    /**
     * @return Country
     */
    public function getInvoiceCountry()
    {
        return $this->invoiceCountry;
    }

    /**
     * @param Country $invoiceCountry
     */
    public function setInvoiceCountry(Country $invoiceCountry): void
    {
        $this->invoiceCountry = $invoiceCountry;
    }

    /**
     * @return null|string
     */
    public function getInvoiceCountryName(): ?string
    {
        return $this->invoiceCountryName;
    }

    /**
     * @param null|string $invoiceCountryName
     */
    public function setInvoiceCountryName(?string $invoiceCountryName): void
    {
        $this->invoiceCountryName = $invoiceCountryName;
    }

    /**
     * @return null|string
     */
    public function getInvoiceEmail(): ?string
    {
        return $this->invoiceEmail;
    }

    /**
     * @param null|string $invoiceEmail
     */
    public function setInvoiceEmail(?string $invoiceEmail): void
    {
        $this->invoiceEmail = $invoiceEmail;
    }

    /**
     * @return null|string
     */
    public function getInvoicePhone(): ?string
    {
        return $this->invoicePhone;
    }

    /**
     * @param null|string $invoicePhone
     */
    public function setInvoicePhone(?string $invoicePhone): void
    {
        $this->invoicePhone = $invoicePhone;
    }

    /**
     * @return float
     */
    public function getTaxPercentage(): float
    {
        return $this->taxPercentage;
    }

    /**
     * @param float $taxPercentage
     */
    public function setTaxPercentage(float $taxPercentage): void
    {
        $this->taxPercentage = $taxPercentage;
    }

    /**
     * @return bool|null
     */
    public function getIsDisabled(): ?bool
    {
        return $this->isDisabled;
    }

    /**
     * @param bool|null $isDisabled
     */
    public function setIsDisabled(?bool $isDisabled): void
    {
        $this->isDisabled = $isDisabled;
    }

    /**
     * Add $purchaseOrder.
     *
     * @param PurchaseOrder $purchaseOrder
     *
     * @return $this
     */
    public function addPurchaseOrder(PurchaseOrder $purchaseOrder)
    {
        $this->purchaseOrders[] = $purchaseOrder;

        return $this;
    }

    /**
     * Remove purchaseOrder.
     *
     * @param PurchaseOrder $purchaseOrder
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePurchaseOrder(PurchaseOrder $purchaseOrder)
    {
        return $this->purchaseOrders->removeElement($purchaseOrder);
    }

    /**
     * Get purchaseOrders.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPurchaseOrders()
    {
        return $this->purchaseOrders;
    }

}
