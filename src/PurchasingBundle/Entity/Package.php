<?php

namespace PurchasingBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Schema\Entity;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Package
 *
 * @ORM\Table(name="packages")
 * @ORM\Entity(repositoryClass="PurchasingBundle\Repository\PackageRepository")
 */
class Package extends Entity {

    const TYPE_PUBLIC       = 0;
    const TYPE_INVITATION   = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="source", type="string", length=255)
     */
    private $source;

    /**
     * @var float|null
     *
     * @ORM\Column(name="price", type="float", nullable=true, options={"default":0})
     */
    private $price;

    /**
     * @var int|null
     *
     * @ORM\Column(name="type", type="smallint", nullable=true, options={"default":0})
     */
    private $type;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="disabled_at", type="datetime", nullable=true)
     */
    private $disabledAt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="disabled_by", type="string", length=22, nullable=true)
     */
    private $disabledBy;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_disabled", type="boolean", nullable=true, options={"default":0})
     */
    private $isDisabled;

    /**
     * Many Packages have Many Optionals.
     * @ORM\ManyToMany(targetEntity="Optional", inversedBy="packages")
     * @ORM\JoinTable(name="packages_optionals")
     */
    private $optionals;


    ################################################# SERIALIZER FUNCTIONS

    /**
     * Returns the array of fields to serialize in entity administration view.
     * @return array
     */
    public function adminSerializer() {
        $view_vars = $this->viewSerializer();

        $admin_vars = array();

        return array_merge($view_vars, $admin_vars);
    }

    /**
     * Returns the array of fields to serialize in entity view.
     * @return array
     */
    public function viewSerializer() {
        $list_vars = $this->listSerializer();

        $view_vars = array(
        );

        return array_merge($list_vars, $view_vars);
    }

    /**
     * Returns the array of fields to serialize in a list of this entity.
     * @return array
     */
    public function listSerializer() {
        $list_vars = array(
            'id'           => $this->serializedId(),
            'source'       => $this->serializedSource(),
            'price'        => $this->serializedPrice(),
            'optionals'    => $this->serializedOptionals(),
        );
        return $list_vars;
    }

    /**
     * Returns the hash code unique identifier of the entity.
     * @return string
     */
    public function hashCode() {
        // TODO: Implement hashCode() method.
    }

    ################################################# SERIALIZED FUNCTIONS

    /**
     * Package id
     * @JMS\VirtualProperty
     * @JMS\SerializedName("id")
     * @JMS\Type("integer")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedId() {
        return (is_null($this->id)?null:$this->id);
    }

    /**
     * Package source
     * @JMS\VirtualProperty
     * @JMS\SerializedName("source")
     * @JMS\Type("string")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedSource() {
        return (is_null($this->source)?null:$this->source);
    }

    /**
     * Package price
     * @JMS\VirtualProperty
     * @JMS\SerializedName("price")
     * @JMS\Type("float")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedPrice() {
        return (is_null($this->price)?null:$this->price);
    }

    /**
     * Package optionals
     * @JMS\VirtualProperty
     * @JMS\SerializedName("optionals")
     * @JMS\Type("array")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedOptionals() {

        $serialized = array();
        if (!$this->getOptionals()->isEmpty()) {
            /** @var Optional $optional */
            foreach ($this->getOptionals() as $optional) {
                $serialized[] = $optional->listSerializer();
            }
        }
        return $serialized;
    }

    ################################################# GETTERS AND SETTERS FUNCTIONS


    public function __construct() {
        $this->optionals = new ArrayCollection();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Add optional.
     *
     * @param Optional $optional
     *
     * @return $this
     */
    public function addOptional(Optional $optional)
    {
        $this->optionals[] = $optional;

        return $this;
    }

    /**
     * Remove optional.
     *
     * @param Optional $optional
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeOptional(Optional $optional)
    {
        return $this->optionals->removeElement($optional);
    }

    /**
     * Get optionals.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOptionals()
    {
        return $this->optionals;
    }


    /**
     * Set source.
     *
     * @param string $source
     *
     * @return Package
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source.
     *
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float|null $price
     */
    public function setPrice(?float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return int|null
     */
    public function getType(): ?int
    {
        return $this->type;
    }

    /**
     * @param int|null $type
     */
    public function setType(?int $type): void
    {
        $this->type = $type;
    }


    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Package
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set disabledAt.
     *
     * @param \DateTime|null $disabledAt
     *
     * @return Package
     */
    public function setDisabledAt($disabledAt = null)
    {
        $this->disabledAt = $disabledAt;

        return $this;
    }

    /**
     * Get disabledAt.
     *
     * @return \DateTime|null
     */
    public function getDisabledAt()
    {
        return $this->disabledAt;
    }

    /**
     * Set disabledBy.
     *
     * @param string|null $disabledBy
     *
     * @return Package
     */
    public function setDisabledBy($disabledBy = null)
    {
        $this->disabledBy = $disabledBy;

        return $this;
    }

    /**
     * Get disabledBy.
     *
     * @return string|null
     */
    public function getDisabledBy()
    {
        return $this->disabledBy;
    }

    /**
     * Set isDisabled.
     *
     * @param bool|null $isDisabled
     *
     * @return Package
     */
    public function setIsDisabled($isDisabled = null)
    {
        $this->isDisabled = $isDisabled;

        return $this;
    }

    /**
     * Get isDisabled.
     *
     * @return bool|null
     */
    public function getIsDisabled()
    {
        return $this->isDisabled;
    }
}
