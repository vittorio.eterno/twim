<?php

namespace PurchasingBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use ResourceBundle\Entity\Country;
use Schema\Entity;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use UserBundle\Entity\ShippingAddress;
use UserBundle\Entity\User;

/**
 * PurchaseOrder
 *
 * @ORM\Table(name="purchase_orders")
 * @ORM\Entity(repositoryClass="PurchasingBundle\Repository\PurchaseOrderRepository")
 */
class PurchaseOrder extends Entity {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Many PurchaseOrders have One User.
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="user_email", type="string", length=255)
     */
    private $userEmail;

    /**
     * Many PurchaseOrders have One Transaction.
     * @ORM\ManyToOne(targetEntity="Transaction", inversedBy="purchaseOrders")
     * @ORM\JoinColumn(name="transaction_id", referencedColumnName="id")
     */
    private $transaction;

    /**
     * Many PurchaseOrders have One ShippingAddress.
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\ShippingAddress")
     * @ORM\JoinColumn(name="shipping_address_id", referencedColumnName="id")
     */
    private $shippingAddress;

    /**
     * @ORM\ManyToOne(targetEntity="ResourceBundle\Entity\Country")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255)
     */
    private $lastName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="company_name", type="string", length=255, nullable=true)
     */
    private $companyName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="company_address_info", type="string", length=255, nullable=true)
     */
    private $companyAddressInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="street_address", type="string", length=255)
     */
    private $streetAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="address_detail", type="string", length=255)
     */
    private $addressDetail;

    /**
     * @var string
     *
     * @ORM\Column(name="state_province", type="string", length=255)
     */
    private $stateProvince;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="postal_code", type="string", length=255)
     */
    private $postalCode;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * One PurchaseOrder has Many PurchaseOrderDetails.
     * @ORM\OneToMany(targetEntity="PurchaseOrderDetail", mappedBy="purchaseOrder")
     */
    private $purchaseOrderDetails;

    /**
     * @var float|null
     *
     * @ORM\Column(name="delivery_charges", type="float", nullable=true, options={"default":0})
     */
    private $deliveryCharges;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, unique=true)
     */
    private $code;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_priority", type="boolean", nullable=true, options={"default":0})
     */
    private $isPriority;

    /**
     * @var string|null
     *
     * @ORM\Column(name="track_code", type="string", length=255, nullable=true)
     */
    private $trackCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="note", type="text", nullable=true)
     */
    private $note;

    /**
     * @var int|null
     *
     * @ORM\Column(name="status", type="smallint", nullable=true, options={"default":0})
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="edited_at", type="datetime", nullable=true)
     */
    private $editedAt;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_disabled", type="boolean", nullable=true, options={"default":0})
     */
    private $isDisabled=false;

    ################################################# SERIALIZER FUNCTIONS

    /**
     * Returns the array of fields to serialize in entity administration view.
     * @return array
     */
    public function adminSerializer() {
        $view_vars = $this->viewSerializer();

        $admin_vars = array();

        return array_merge($view_vars, $admin_vars);
    }

    /**
     * Returns the array of fields to serialize in entity view.
     * @return array
     */
    public function viewSerializer() {
        $list_vars = $this->listSerializer();

        $view_vars = array(
        );

        return array_merge($list_vars, $view_vars);
    }

    /**
     * Returns the array of fields to serialize in a list of this entity.
     * @return array
     */
    public function listSerializer() {
        $list_vars = array(
            'id'           => $this->serializedId()
        );
        return $list_vars;
    }

    /**
     * Returns the hash code unique identifier of the entity.
     * @return string
     */
    public function hashCode() {
        // TODO: Implement hashCode() method.
    }

    ################################################# SERIALIZED FUNCTIONS

    /**
     * Setting id
     * @JMS\VirtualProperty
     * @JMS\SerializedName("id")
     * @JMS\Type("integer")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedId() {
        return (is_null($this->id)?null:$this->id);
    }


    ################################################# GETTERS AND SETTERS FUNCTIONS

    /**
     * PurchaseOrder constructor.
     */
    public function __construct() {
        $this->purchaseOrderDetails = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add purchaseOrderDetail.
     *
     * @param PurchaseOrderDetail $purchaseOrderDetail
     *
     * @return $this
     */
    public function addPurchaseOrderDetail(PurchaseOrderDetail $purchaseOrderDetail)
    {
        $this->purchaseOrderDetails[] = $purchaseOrderDetail;

        return $this;
    }

    /**
     * Remove purchaseOrderDetail.
     *
     * @param PurchaseOrderDetail $purchaseOrderDetail
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePurchaseOrderDetail(PurchaseOrderDetail $purchaseOrderDetail)
    {
        return $this->purchaseOrderDetails->removeElement($purchaseOrderDetail);
    }

    /**
     * Get purchaseOrderDetails.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPurchaseOrderDetails()
    {
        return $this->purchaseOrderDetails;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return Transaction
     */
    public function getTransaction()
    {
        return $this->transaction;
    }

    /**
     * @param Transaction $transaction
     */
    public function setTransaction(Transaction $transaction): void
    {
        $this->transaction = $transaction;
    }

    /**
     * @return ShippingAddress
     */
    public function getShippingAddress()
    {
        return $this->shippingAddress;
    }

    /**
     * @param ShippingAddress $shippingAddress
     */
    public function setShippingAddress(ShippingAddress $shippingAddress): void
    {
        $this->shippingAddress = $shippingAddress;
    }

    /**
     * Set code.
     *
     * @param string $code
     *
     * @return PurchaseOrder
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set isPriority.
     *
     * @param bool|null $isPriority
     *
     * @return PurchaseOrder
     */
    public function setIsPriority($isPriority = null)
    {
        $this->isPriority = $isPriority;

        return $this;
    }

    /**
     * Get isPriority.
     *
     * @return bool|null
     */
    public function getIsPriority()
    {
        return $this->isPriority;
    }

    /**
     * Set trackCode.
     *
     * @param string|null $trackCode
     *
     * @return PurchaseOrder
     */
    public function setTrackCode($trackCode = null)
    {
        $this->trackCode = $trackCode;

        return $this;
    }

    /**
     * Get trackCode.
     *
     * @return string|null
     */
    public function getTrackCode()
    {
        return $this->trackCode;
    }

    /**
     * Set note.
     *
     * @param string|null $note
     *
     * @return PurchaseOrder
     */
    public function setNote($note = null)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note.
     *
     * @return string|null
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set status.
     *
     * @param int|null $status
     *
     * @return PurchaseOrder
     */
    public function setStatus($status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return int|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return float|null
     */
    public function getDeliveryCharges(): ?float
    {
        return $this->deliveryCharges;
    }

    /**
     * @param float|null $deliveryCharges
     */
    public function setDeliveryCharges(?float $deliveryCharges): void
    {
        $this->deliveryCharges = $deliveryCharges;
    }

    /**
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param Country $country
     */
    public function setCountry(Country $country): void
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return null|string
     */
    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    /**
     * @param null|string $companyName
     */
    public function setCompanyName(?string $companyName): void
    {
        $this->companyName = $companyName;
    }

    /**
     * @return null|string
     */
    public function getCompanyAddressInfo(): ?string
    {
        return $this->companyAddressInfo;
    }

    /**
     * @param null|string $companyAddressInfo
     */
    public function setCompanyAddressInfo(?string $companyAddressInfo): void
    {
        $this->companyAddressInfo = $companyAddressInfo;
    }

    /**
     * @return string
     */
    public function getStreetAddress(): string
    {
        return $this->streetAddress;
    }

    /**
     * @param string $streetAddress
     */
    public function setStreetAddress(string $streetAddress): void
    {
        $this->streetAddress = $streetAddress;
    }

    /**
     * @return string
     */
    public function getAddressDetail(): string
    {
        return $this->addressDetail;
    }

    /**
     * @param string $addressDetail
     */
    public function setAddressDetail(string $addressDetail): void
    {
        $this->addressDetail = $addressDetail;
    }

    /**
     * @return string
     */
    public function getStateProvince(): string
    {
        return $this->stateProvince;
    }

    /**
     * @param string $stateProvince
     */
    public function setStateProvince(string $stateProvince): void
    {
        $this->stateProvince = $stateProvince;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getPostalCode(): string
    {
        return $this->postalCode;
    }

    /**
     * @param string $postalCode
     */
    public function setPostalCode(string $postalCode): void
    {
        $this->postalCode = $postalCode;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return null|string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param null|string $phone
     */
    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getUserEmail(): string
    {
        return $this->userEmail;
    }

    /**
     * @param string $userEmail
     */
    public function setUserEmail(string $userEmail): void
    {
        $this->userEmail = $userEmail;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getEditedAt(): \DateTime
    {
        return $this->editedAt;
    }

    /**
     * @param \DateTime $editedAt
     */
    public function setEditedAt(\DateTime $editedAt): void
    {
        $this->editedAt = $editedAt;
    }

    /**
     * @return bool|null
     */
    public function getIsDisabled(): ?bool
    {
        return $this->isDisabled;
    }

    /**
     * @param bool|null $isDisabled
     */
    public function setIsDisabled(?bool $isDisabled): void
    {
        $this->isDisabled = $isDisabled;
    }


}
