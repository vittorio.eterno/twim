<?php

namespace PurchasingBundle\Entity;


use Schema\Entity;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use UserBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Coupon
 *
 * @ORM\Table(name="coupons")
 * @ORM\Entity(repositoryClass="PurchasingBundle\Repository\CouponRepository")
 */
class Coupon extends Entity {

    const TYPE_EVENT                = 0;
    const TYPE_INFLUENCER           = 1;
    const TYPE_SPECIAL              = 2;

    const TYPE_EVENT_SOURCE         = "coupon.type.event";
    const TYPE_INFLUENCER_SOURCE    = "coupon.type.influencer";
    const TYPE_SPECIAL_SOURCE       = "coupon.type.special";

    const TYPE_EVENT_CODE           = "EVE";
    const TYPE_INFLUENCER_CODE      = "INF";
    const TYPE_SPECIAL_CODE         = "SPC";


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Many Coupons have One User.
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var int|null
     *
     * @ORM\Column(name="type", type="smallint", nullable=true, options={"default":0})
     * @Assert\NotBlank(message = "require.coupon.type")
     */
    private $type;

    /**
     * @var string|null
     *
     * @ORM\Column(name="code", type="string", length=50, nullable=true)
     */
    private $code;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="discount_percentage", type="integer", nullable=true, options={"default":0})
     * @Assert\NotBlank(message = "require.discount.percentage")
     * @Assert\Type(
     *     type="integer",
     *     message="invalid.value.type"
     * )
     * @Assert\GreaterThan(
     *     value = 0,
     *     message="value.greater.than"
     * )
     * @Assert\LessThanOrEqual(
     *     value = 100,
     *     message="value.less.than.or.equal"
     * )
     */
    private $discountPercentage;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="started_at", type="datetime", nullable=true)
     * @Assert\DateTime(message = "invalid.datetime")
     */
    private $startedAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="expired_at", type="datetime", nullable=true)
     * @Assert\DateTime(message = "invalid.datetime")
     */
    private $expiredAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="disabled_at", type="datetime", nullable=true)
     */
    private $disabledAt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="disabled_by", type="string", length=22, nullable=true)
     */
    private $disabledBy;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_disabled", type="boolean", nullable=true, options={"default":0})
     */
    private $isDisabled;


    ################################################# SERIALIZER FUNCTIONS

    /**
     * Returns the array of fields to serialize in entity administration view.
     * @return array
     */
    public function adminSerializer() {
        $view_vars = $this->viewSerializer();

        $admin_vars = array(
            'user'         => $this->serializedUser()
        );

        return array_merge($view_vars, $admin_vars);
    }

    /**
     * Returns the array of fields to serialize in entity view.
     * @return array
     */
    public function viewSerializer() {
        $list_vars = $this->listSerializer();

        $view_vars = array(
        );

        return array_merge($list_vars, $view_vars);
    }

    /**
     * Returns the array of fields to serialize in a list of this entity.
     * @return array
     */
    public function listSerializer() {
        $list_vars = array(
            'id'                    => $this->serializedId(),
            'code'                  => $this->serializedCode(),
            'type'                  => $this->serializedType(),
            'typeSource'            => $this->serializedTypeSource(),
            'discountPercentage'    => $this->serializedDiscountPercentage(),
            'startedAt'             => $this->serializedStartedAt(),
            'expiredAt'             => $this->serializedExpiredAt(),
            'isDisabled'            => $this->serializedIsDisabled(),
        );
        return $list_vars;
    }

    /**
     * Returns the hash code unique identifier of the entity.
     * @return string
     */
    public function hashCode() {
        // TODO: Implement hashCode() method.
    }

    ################################################# SERIALIZED FUNCTIONS

    /**
     * Coupon id
     * @JMS\VirtualProperty
     * @JMS\SerializedName("id")
     * @JMS\Type("integer")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedId() {
        return (is_null($this->id)?null:$this->id);
    }

    /**
     * Coupon code
     * @JMS\VirtualProperty
     * @JMS\SerializedName("code")
     * @JMS\Type("string")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedCode() {
        return (is_null($this->code)?null:$this->code);
    }

    /**
     * Coupon type
     * @JMS\VirtualProperty
     * @JMS\SerializedName("type")
     * @JMS\Type("integer")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedType() {
        return (is_null($this->type)?null:$this->type);
    }

    /**
     * Coupon typeSource
     * @JMS\VirtualProperty
     * @JMS\SerializedName("typeSource")
     * @JMS\Type("string")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedTypeSource() {

        switch ($this->type) {
            case self::TYPE_SPECIAL:
                $source = self::TYPE_SPECIAL_SOURCE;
                break;
            case self::TYPE_INFLUENCER:
                $source = self::TYPE_INFLUENCER_SOURCE;
                break;
            default:
                $source = self::TYPE_EVENT_SOURCE;
        }

        return $source;
    }

    /**
     * Coupon discountPercentage
     * @JMS\VirtualProperty
     * @JMS\SerializedName("discountPercentage")
     * @JMS\Type("integer")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedDiscountPercentage() {
        return (is_null($this->discountPercentage)?0:$this->discountPercentage);
    }

    /**
     * user
     * @JMS\VirtualProperty
     * @JMS\SerializedName("user")
     * @JMS\Type("array")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedUser () {
        return (!is_null($this->getUser()) && $this->getUser() instanceof User ? $this->getUser()->listSerializer() : null);
    }

    /**
     * startedAt
     * @JMS\VirtualProperty
     * @JMS\SerializedName("startedAt")
     * @JMS\Type("string")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedStartedAt() {
        return (!is_null($this->startedAt) && $this->startedAt instanceof \DateTime ? $this->startedAt->format("Y-m-d H:i:s") : null);
    }

    /**
     * expiredAt
     * @JMS\VirtualProperty
     * @JMS\SerializedName("expiredAt")
     * @JMS\Type("string")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedExpiredAt() {
        return (!is_null($this->expiredAt) && $this->expiredAt instanceof \DateTime ? $this->expiredAt->format("Y-m-d H:i:s") : null);
    }

    /**
     * Coupon isDisabled
     * @JMS\VirtualProperty
     * @JMS\SerializedName("isDisabled")
     * @JMS\Type("float")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedIsDisabled() {
        return (is_null($this->isDisabled)?false:$this->isDisabled);
    }

    ################################################# GETTERS AND SETTERS FUNCTIONS


    public static function getTypes () {
        return array (
            self::TYPE_EVENT_SOURCE         => self::TYPE_EVENT,
            self::TYPE_INFLUENCER_SOURCE    => self::TYPE_INFLUENCER,
            self::TYPE_SPECIAL_SOURCE       => self::TYPE_SPECIAL
        );
    }


    public function getTypeCode() {

        switch ($this->type) {
            case self::TYPE_SPECIAL:
                $typeCode = self::TYPE_SPECIAL_CODE;
                break;
            case self::TYPE_INFLUENCER:
                $typeCode = self::TYPE_INFLUENCER_CODE;
                break;
            default:
                $typeCode = self::TYPE_EVENT_CODE;
        }

        return $typeCode;
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code.
     *
     * @param string|null $code
     *
     * @return Coupon
     */
    public function setCode($code = null)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string|null
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set discountPercentage.
     *
     * @param integer|null $discountPercentage
     *
     * @return Coupon
     */
    public function setDiscountPercentage($discountPercentage = null)
    {
        $this->discountPercentage = $discountPercentage;

        return $this;
    }

    /**
     * Get discountPercentage.
     *
     * @return integer|null
     */
    public function getDiscountPercentage()
    {
        return $this->discountPercentage;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Coupon
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set disabledAt.
     *
     * @param \DateTime|null $disabledAt
     *
     * @return Coupon
     */
    public function setDisabledAt($disabledAt = null)
    {
        $this->disabledAt = $disabledAt;

        return $this;
    }

    /**
     * Get disabledAt.
     *
     * @return \DateTime|null
     */
    public function getDisabledAt()
    {
        return $this->disabledAt;
    }

    /**
     * Set disabledBy.
     *
     * @param string|null $disabledBy
     *
     * @return Coupon
     */
    public function setDisabledBy($disabledBy = null)
    {
        $this->disabledBy = $disabledBy;

        return $this;
    }

    /**
     * Get disabledBy.
     *
     * @return string|null
     */
    public function getDisabledBy()
    {
        return $this->disabledBy;
    }

    /**
     * Set isDisabled.
     *
     * @param bool|null $isDisabled
     *
     * @return Coupon
     */
    public function setIsDisabled($isDisabled = null)
    {
        $this->isDisabled = $isDisabled;

        return $this;
    }

    /**
     * Get isDisabled.
     *
     * @return bool|null
     */
    public function getIsDisabled()
    {
        return $this->isDisabled;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return int|null
     */
    public function getType(): ?int
    {
        return $this->type;
    }

    /**
     * @param int|null $type
     */
    public function setType(?int $type): void
    {
        $this->type = $type;
    }

    /**
     * @return \DateTime|null
     */
    public function getStartedAt(): ?\DateTime
    {
        return $this->startedAt;
    }

    /**
     * @param \DateTime|null $startedAt
     */
    public function setStartedAt(?\DateTime $startedAt): void
    {
        $this->startedAt = $startedAt;
    }

    /**
     * @return \DateTime|null
     */
    public function getExpiredAt(): ?\DateTime
    {
        return $this->expiredAt;
    }

    /**
     * @param \DateTime|null $expiredAt
     */
    public function setExpiredAt(?\DateTime $expiredAt): void
    {
        $this->expiredAt = $expiredAt;
    }



}
