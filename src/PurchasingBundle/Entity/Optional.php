<?php

namespace PurchasingBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Schema\Entity;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Optional
 *
 * @ORM\Table(name="optionals")
 * @ORM\Entity(repositoryClass="PurchasingBundle\Repository\OptionalRepository")
 */
class Optional extends Entity {

    const TYPE_DIGITAL_FORMAT           = 0;
    const TYPE_DIGITAL_FORMAT_SOURCE    = "type.digital.format";

    const TYPE_PAPER_FORMAT             = 1;
    const TYPE_PAPER_FORMAT_SOURCE      = "type.paper.format";

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Many Optionals have One OptionalStructure.
     * @ORM\ManyToOne(targetEntity="OptionalStructure", inversedBy="optionals")
     * @ORM\JoinColumn(name="optional_structure_id", referencedColumnName="id")
     */
    private $optionalStructure;

    /**
     * @var string
     *
     * @ORM\Column(name="source", type="string", length=255)
     */
    private $source;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var string|null
     *
     * @ORM\Column(name="value", type="string", length=255)
     */
    private $value;

    /**
     * @var float|null
     *
     * @ORM\Column(name="price", type="float", nullable=true, options={"default":0})
     */
    private $price;

    /**
     * @var int|null
     *
     * @ORM\Column(name="type", type="smallint", nullable=true, options={"default":0})
     * @Assert\NotBlank(message = "require.optional.type")
     */
    private $type;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_disabled", type="boolean", nullable=true, options={"default":0})
     */
    private $isDisabled;

    /**
     * Many Optionals have Many Packages.
     * @ORM\ManyToMany(targetEntity="Package", mappedBy="optionals")
     */
    private $packages;


    ################################################# SERIALIZER FUNCTIONS

    /**
     * Returns the array of fields to serialize in entity administration view.
     * @return array
     */
    public function adminSerializer() {
        $view_vars = $this->viewSerializer();

        $admin_vars = array();

        return array_merge($view_vars, $admin_vars);
    }

    /**
     * Returns the array of fields to serialize in entity view.
     * @return array
     */
    public function viewSerializer() {
        $list_vars = $this->listSerializer();

        $view_vars = array(
            'value'         => $this->serializedValue(),
        );

        return array_merge($list_vars, $view_vars);
    }

    /**
     * Returns the array of fields to serialize in a list of this entity.
     * @return array
     */
    public function listSerializer() {
        $list_vars = array(
            'id'            => $this->serializedId(),
            'source'        => $this->serializedSource(),
            'description'   => $this->serializedDescription(),
            'value'         => $this->serializedValue(),
            'price'         => $this->serializedPrice(),
            'type'          => $this->serializedType(),
        );
        return $list_vars;
    }

    /**
     * Returns the hash code unique identifier of the entity.
     * @return string
     */
    public function hashCode() {
        // TODO: Implement hashCode() method.
    }

    ################################################# SERIALIZED FUNCTIONS

    /**
     * Optional id
     * @JMS\VirtualProperty
     * @JMS\SerializedName("id")
     * @JMS\Type("integer")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedId() {
        return (is_null($this->id)?null:$this->id);
    }

    /**
     * Optional source
     * @JMS\VirtualProperty
     * @JMS\SerializedName("source")
     * @JMS\Type("string")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedSource() {
        return (is_null($this->source)?null:$this->source);
    }

    /**
     * Optional price
     * @JMS\VirtualProperty
     * @JMS\SerializedName("price")
     * @JMS\Type("string")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedPrice() {
        return (is_null($this->price)?null:$this->price);
    }

    /**
     * Optional value
     * @JMS\VirtualProperty
     * @JMS\SerializedName("value")
     * @JMS\Type("string")
     * @JMS\Groups({"list","view"})
     * @JMS\Since("1.0.x")
     */
    public function serializedValue() {
        return (is_null($this->value)?null:$this->value);
    }

    /**
     * Optional description
     * @JMS\VirtualProperty
     * @JMS\SerializedName("description")
     * @JMS\Type("string")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedDescription() {
        return (is_null($this->description)?null:$this->description);
    }

    /**
     * Optional type
     * @JMS\VirtualProperty
     * @JMS\SerializedName("type")
     * @JMS\Type("string")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedType() {
        return (is_null($this->type)?null:$this->type);
    }

    /**
     * Optional optionalStructureSource
     * @JMS\VirtualProperty
     * @JMS\SerializedName("optionalStructureSource")
     * @JMS\Type("string")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedOptionaleStructureSource() {

        /** @var OptionalStructure $optionalStructure */
        $optionalStructure = $this->getOptionalStructure();
        return (is_null($optionalStructure) ? null : $optionalStructure->serializedSource());

    }

    ################################################# GETTERS AND SETTERS FUNCTIONS

    /**
     * Optional constructor.
     */
    public function __construct() {
        $this->packages  = new ArrayCollection();
    }


    /**
     * @return string
     */
    public function getTypeSource() {

        switch ($this->type) {
            case self::TYPE_PAPER_FORMAT:
                $typeSource = self::TYPE_PAPER_FORMAT_SOURCE;
                break;
            default:
                $typeSource = self::TYPE_DIGITAL_FORMAT_SOURCE;
        }

        return $typeSource;
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add package.
     *
     * @param Package $package
     *
     * @return $this
     */
    public function addPackage(Package $package)
    {
        $this->packages[] = $package;

        return $this;
    }

    /**
     * Remove package.
     *
     * @param Package $package
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePackage(Package $package)
    {
        return $this->packages->removeElement($package);
    }

    /**
     * Get packages.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPackages()
    {
        return $this->packages;
    }

    /**
     * @return OptionalStructure
     */
    public function getOptionalStructure()
    {
        return $this->optionalStructure;
    }

    /**
     * @param OptionalStructure $optionalStructure
     */
    public function setOptionalStructure(OptionalStructure $optionalStructure): void
    {
        $this->optionalStructure = $optionalStructure;
    }


    /**
     * Set source.
     *
     * @param string $source
     *
     * @return Optional
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source.
     *
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Optional
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set value.
     *
     * @param string|null $value
     *
     * @return Optional
     */
    public function setValue($value = null)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value.
     *
     * @return string|null
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float|null $price
     */
    public function setPrice(?float $price): void
    {
        $this->price = $price;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Optional
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set isDisabled.
     *
     * @param bool|null $isDisabled
     *
     * @return Optional
     */
    public function setIsDisabled($isDisabled = null)
    {
        $this->isDisabled = $isDisabled;

        return $this;
    }

    /**
     * Get isDisabled.
     *
     * @return bool|null
     */
    public function getIsDisabled()
    {
        return $this->isDisabled;
    }

    /**
     * @return int|null
     */
    public function getType(): ?int
    {
        return $this->type;
    }

    /**
     * @param int|null $type
     */
    public function setType(?int $type): void
    {
        $this->type = $type;
    }


}
