<?php

namespace PurchasingBundle\Entity;


use Schema\Entity;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * TransactionDetail
 *
 * @ORM\Table(name="transaction_details")
 * @ORM\Entity(repositoryClass="PurchasingBundle\Repository\TransactionDetailRepository")
 */
class TransactionDetail extends Entity {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Many TransactionDetails have One Transaction.
     * @ORM\ManyToOne(targetEntity="Transaction", inversedBy="transactionDetails")
     * @ORM\JoinColumn(name="transaction_id", referencedColumnName="id")
     */
    private $transaction;

    /**
     * Many TransactionDetails have One Optional.
     * @ORM\ManyToOne(targetEntity="Optional")
     * @ORM\JoinColumn(name="optional_id", referencedColumnName="id")
     */
    private $optional;

    /**
     * Many TransactionDetails have One Package.
     * @ORM\ManyToOne(targetEntity="Package")
     * @ORM\JoinColumn(name="package_id", referencedColumnName="id")
     */
    private $package;

    /**
     * @var string
     *
     * @ORM\Column(name="source", type="string", length=255, nullable=true)
     */
    private $source;

    /**
     * @var float|null
     *
     * @ORM\Column(name="price", type="float", nullable=true, options={"default":0})
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="optional_description", type="string", length=255, nullable=true)
     */
    private $optionalDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="package_optionals", type="text", nullable=true)
     */
    private $packageOptionals;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;


    ################################################# SERIALIZER FUNCTIONS

    /**
     * Returns the array of fields to serialize in entity administration view.
     * @return array
     */
    public function adminSerializer() {
        $view_vars = $this->viewSerializer();

        $admin_vars = array();

        return array_merge($view_vars, $admin_vars);
    }

    /**
     * Returns the array of fields to serialize in entity view.
     * @return array
     */
    public function viewSerializer() {
        $list_vars = $this->listSerializer();

        $view_vars = array(
        );

        return array_merge($list_vars, $view_vars);
    }

    /**
     * Returns the array of fields to serialize in a list of this entity.
     * @return array
     */
    public function listSerializer() {
        $list_vars = array(
            'id'           => $this->serializedId()
        );
        return $list_vars;
    }

    /**
     * Returns the hash code unique identifier of the entity.
     * @return string
     */
    public function hashCode() {
        // TODO: Implement hashCode() method.
    }

    ################################################# SERIALIZED FUNCTIONS

    /**
     * Setting id
     * @JMS\VirtualProperty
     * @JMS\SerializedName("id")
     * @JMS\Type("integer")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedId() {
        return (is_null($this->id)?null:$this->id);
    }


    ################################################# GETTERS AND SETTERS FUNCTIONS



    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Transaction
     */
    public function getTransaction()
    {
        return $this->transaction;
    }

    /**
     * @param Transaction $transaction
     */
    public function setTransaction(Transaction $transaction): void
    {
        $this->transaction = $transaction;
    }

    /**
     * @return Optional
     */
    public function getOptional()
    {
        return $this->optional;
    }

    /**
     * @param Optional $optional
     */
    public function setOptional(Optional $optional): void
    {
        $this->optional = $optional;
    }

    /**
     * @return Package
     */
    public function getPackage()
    {
        return $this->package;
    }

    /**
     * @param Package $package
     */
    public function setPackage(Package $package): void
    {
        $this->package = $package;
    }


    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }

    /**
     * @param string $source
     */
    public function setSource(string $source): void
    {
        $this->source = $source;
    }

    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float|null $price
     */
    public function setPrice(?float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getOptionalDescription(): string
    {
        return $this->optionalDescription;
    }

    /**
     * @param string $optionalDescription
     */
    public function setOptionalDescription(string $optionalDescription): void
    {
        $this->optionalDescription = $optionalDescription;
    }

    /**
     * @return string
     */
    public function getPackageOptionals(): string
    {
        return $this->packageOptionals;
    }

    /**
     * @param string $packageOptionals
     */
    public function setPackageOptionals(string $packageOptionals): void
    {
        $this->packageOptionals = $packageOptionals;
    }

}
