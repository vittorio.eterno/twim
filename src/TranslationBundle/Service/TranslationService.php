<?php

namespace TranslationBundle\Service;

use Psr\Log\LoggerInterface;
use Schema\Entity;
use Schema\EntityService;
use TranslationBundle\Repository\TranslationRepository;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class TranslationService extends EntityService {

    /** @var RequestStack $requestStack */
    private $requestStack;

    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * TranslationService constructor.
     * @param TranslationRepository|null $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param RequestStack $requestStack
     * @param LoggerInterface $logger
     */
    public function __construct(TranslationRepository $repository = null,
                                AuthorizationCheckerInterface $authorizationChecker,
                                RequestStack $requestStack,
                                LoggerInterface $logger
    ) {
        $this->repository = $repository;
        parent::__construct($repository, $authorizationChecker, $logger);

        $this->requestStack = $requestStack->getCurrentRequest();
    }


    /**
     * @param $sources
     * @param null $defaultLocale
     * @return array|mixed|null
     */
    public function getBySources($sources, $defaultLocale = null) {
        $translations = array();

        if(is_null($defaultLocale)) {
            $defaultLocale = $this->requestStack->getLocale();
        }

        if (!empty($sources)) {
           $translations = $this->repository->findBySources($sources, $defaultLocale);
           $translations = $this->getFormattedTranslations($translations);
        }

        return $translations;
    }


    /**
     * @param $translations
     * @return array
     */
    private function getFormattedTranslations(&$translations) {
        $formatted = array();

        if(!empty($translations)) {
            foreach($translations as $translation) {
                if(isset($translation['source']) && isset($translation['value'])) {
                    $formatted[$translation['source']] = $translation['value'];
                }
            }
        }

        return $formatted;
    }


    /**
     * Check if locale language setted in the header is different from the default one
     *
     * @return bool
     */
    public function isToBeTranslated() {
        return $this->requestStack->getLocale() != $this->requestStack->getDefaultLocale();
    }


    /**
     * @return array
     */
    public function getAll() {
        $translations = $this->repository->findAllResults();
        $translations = $this->getFormattedTranslations($translations);

        return $translations;
    }

    public function getParams($translation) {
        $translation_explode = array();
        if(!is_array($translation)) {
            $translation_explode = explode(" ", $translation);
        }
        else{
            foreach($translation as $value) {
                $translation_explode = explode(" ", $value);
            }
        }

        $params = array();

        if(count($translation_explode) > 0 ) {
            foreach($translation_explode as $translation_word) {
                if (substr($translation_word, 0, 1) == '%' && substr($translation_word, -1) == '%') {
                    $params[$translation_word] = $translation_word;
                }
            }
        }

        return $params;
    }

    /**
     * @param string|array $translation
     * @param array $searches
     * @param array $replacements
     * @return mixed|null
     */
    public function paramsReplace($translation, array $searches = array(), array $replacements = array()) {
        $translation_string = '';
        if(!is_array($translation)) {
            $translation_string = $translation;
        }
        else{
            foreach($translation as $value) {
                $translation_string = $value;
            }
        }

        if(count($searches) > 0 || count($replacements) > 0) {
            return str_replace($searches, $replacements, $translation_string);
        }

        return null;
    }
}