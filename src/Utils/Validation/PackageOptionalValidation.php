<?php

namespace Utils\Validation;


use PurchasingBundle\Entity\Optional;
use PurchasingBundle\Entity\Package;
use PurchasingBundle\Service\OptionalService;
use PurchasingBundle\Service\PackageService;
use Symfony\Component\Translation\TranslatorInterface;


class PackageOptionalValidation {

    /** @var PackageService $packageService */
    private $packageService;

    /** @var OptionalService $optionalService */
    private $optionalService;

    /** @var TranslatorInterface $translator */
    private $translator;

    /**
     * PackageOptionalValidation constructor.
     * @param PackageService $packageService
     * @param OptionalService $optionalService
     * @param TranslatorInterface $translator
     */
    public function __construct(PackageService $packageService, OptionalService $optionalService, TranslatorInterface $translator) {
        $this->packageService  = $packageService;
        $this->optionalService = $optionalService;
        $this->translator      = $translator;
    }

    /**
     * @param array $formErrors
     * @param array $packagesOptionals
     * @param array|null $formPackages
     * @param array|null $formOptionals
     * @param null $prefix
     * @return bool
     */
    public function validatePackagesOptionals (array &$formErrors, array &$packagesOptionals, ?array $formPackages, ?array $formOptionals, $prefix=null) {

        $packageIds = array();
        $packages   = array();

        $errorKeyOptionals = is_null($prefix) ? "optionals" : $prefix."_optionals_errors";
        $errorKeyPackages  = is_null($prefix) ? "packages" : $prefix."_packages_errors";

        if (!is_null($formPackages) && count($formPackages)>0) {
            foreach ($formPackages as $packageId=>$val) {
                $packageIds[] = $packageId;
            }
            if (count($packageIds)>0) {
                $packages = $this->packageService->getByIds($packageIds);
                if (!$packages) {
                    $formErrors[$errorKeyPackages] = $this->translator->trans('packages.not.found');
                    return false;
                }
            }

            if (count($packages) != count($packageIds)) {
                $formErrors[$errorKeyPackages] = $this->translator->trans('packages.not.found');
                return false;
            }

            if(count($packages)>0) {
                /** @var Package $package */
                foreach($packages as $package){
                    if ($package->getIsDisabled()) {
                        $formErrors[$errorKeyPackages] = $this->translator->trans('packages.not.found');
                        return false;
                    }
                }
                $packagesOptionals["packages"] = $packages;
            }
        }

        $optionalIds    = array();
        $optionals      = array();
        $optionalKeyIds = array();

        if (!is_null($formOptionals) && count($formOptionals)>0) {
            foreach ($formOptionals as $optionalId=>$val) {
                $optionalIds[] = $optionalId;
            }
            if (count($optionalIds)>0) {
                $optionals = $this->optionalService->getByIds($optionalIds);
                if (!$optionals) {
                    $formErrors[$errorKeyOptionals] = $this->translator->trans('optionals.not.found');
                    return false;
                }
            }
            if (count($optionals) != count($optionalIds)) {
                $formErrors[$errorKeyOptionals] = $this->translator->trans('optionals.not.found');
                return false;
            }

            if(count($optionals)>0) {
                /** @var Optional $optional */
                foreach($optionals as $optional){
                    if ($optional->getIsDisabled()) {
                        $formErrors[$errorKeyOptionals] = $this->translator->trans('optionals.not.found');
                        return false;
                    }

                    $optionalKeyIds[$optional->getId()] = true;
                }
                $packagesOptionals["optionals"] = $optionals;
            }
        }

        if (count($packages)>0 && count($optionalKeyIds)>0) {
            /** @var Package $package */
            foreach ($packages as $package) {
                if (!$package->getOptionals()->isEmpty()) {
                    /** @var Optional $packOptional */
                    foreach ($package->getOptionals() as $packOptional) {
                        if (array_key_exists($packOptional->getId(),$optionalKeyIds)) {
                            $formErrors[$errorKeyOptionals] = $this->translator->trans('optionals.already.included');
                            return false;
                        }
                    }
                }
            }
        }

        return true;

    }


}