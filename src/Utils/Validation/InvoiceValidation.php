<?php

namespace Utils\Validation;


use Exceptions\ValidationException;
use ResourceBundle\Entity\Country;
use ResourceBundle\Service\CountryService;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Utils\StaticUtil\EmailUtils;
use Utils\StaticUtil\StringUtils;

class InvoiceValidation {

    /** @var TranslatorInterface $translator */
    private $translator;

    /** @var CountryService $countryService */
    private $countryService;

    /** @var ValidationException $validationException */
    private $validationException;

    /**
     * InvoiceValidation constructor.
     * @param TranslatorInterface $translator
     * @param CountryService $countryService
     * @param ValidationException $validationException
     */
    public function __construct(TranslatorInterface $translator, CountryService $countryService, ValidationException $validationException) {
        $this->translator           = $translator;
        $this->countryService       = $countryService;
        $this->validationException  = $validationException;
    }

    /**
     * @param $validator
     * @param array $invoiceErrors
     * @param array $invoiceFields
     * @return array
     */
    public function validateInvoiceFields (&$validator, array &$invoiceErrors, array &$invoiceFields) {

        // FIRST NAME
        if (!isset($invoiceFields["first_name"]) || empty($invoiceFields["first_name"])) {
            $invoiceErrors["fid_first_name"] = $this->translator->trans("parameter.mandatory");
        } else {
            if (!StringUtils::checkOnlyLettersRegex($invoiceFields["first_name"])) {
                $invoiceErrors["fid_first_name"] = $this->translator->trans("only.letters.allowed");
            } else {
                if (!StringUtils::checkLength($invoiceFields["first_name"])) {
                    $invoiceErrors["fid_first_name"] = $this->translator->trans("length.invalid");
                }
            }
        }

        // LAST NAME
        if (!isset($invoiceFields["last_name"]) || empty($invoiceFields["last_name"])) {
            $invoiceErrors["fid_last_name"] = $this->translator->trans("parameter.mandatory");
        } else {
            if (!StringUtils::checkOnlyLettersRegex($invoiceFields["last_name"])) {
                $invoiceErrors["fid_last_name"] = $this->translator->trans("only.letters.allowed");
            } else {
                if (!StringUtils::checkLength($invoiceFields["last_name"])) {
                    $invoiceErrors["fid_last_name"] = $this->translator->trans("length.invalid");
                }
            }
        }

        // COMPANY NAME
        if (isset($invoiceFields["company_name"]) && !empty($invoiceFields["company_name"])) {
            if (!StringUtils::checkOnlyLettersRegex($invoiceFields["company_name"])) {
                $invoiceErrors["fid_company_name"] = $this->translator->trans("only.letters.allowed");
            } else {
                if (!StringUtils::checkLength($invoiceFields["company_name"])) {
                    $invoiceErrors["fid_company_name"] = $this->translator->trans("length.invalid");
                }
            }
        }

        // COMPANY ADDRESS INFO
        if (isset($invoiceFields["company_address_info"]) && !empty($invoiceFields["company_address_info"])) {
            if (!StringUtils::checkNameString($invoiceFields["company_address_info"])) {
                $invoiceErrors["fid_company_address_info"] = $this->translator->trans("string.invalid");
            }
        }

        // STREET ADDRESS
        if (!isset($invoiceFields["street_address"]) || empty($invoiceFields["street_address"])) {
            $invoiceErrors["fid_street_address"] = $this->translator->trans("parameter.mandatory");
        } else {
            if (!StringUtils::checkNameString($invoiceFields["street_address"])) {
                $invoiceErrors["fid_street_address"] = $this->translator->trans("string.invalid");
            }
        }

        // ADDRESS DETAIL
        if (!isset($invoiceFields["address_detail"]) || empty($invoiceFields["address_detail"])) {
            $invoiceErrors["fid_address_detail"] = $this->translator->trans("parameter.mandatory");
        } else {
            if (!StringUtils::checkNameString($invoiceFields["address_detail"])) {
                $invoiceErrors["fid_address_detail"] = $this->translator->trans("string.invalid");
            }
        }

        // CITY
        if (!isset($invoiceFields["city"]) || empty($invoiceFields["city"])) {
            $invoiceErrors["fid_city"] = $this->translator->trans("parameter.mandatory");
        } else {
            if (!StringUtils::checkNameString($invoiceFields["city"])) {
                $invoiceErrors["fid_city"] = $this->translator->trans("string.invalid");
            }
        }

        // STATE PROVINCE
        if (!isset($invoiceFields["state_province"]) || empty($invoiceFields["state_province"])) {
            $invoiceErrors["fid_state_province"] = $this->translator->trans("parameter.mandatory");
        } else {
            if (!StringUtils::checkNameString($invoiceFields["state_province"])) {
                $invoiceErrors["fid_state_province"] = $this->translator->trans("string.invalid");
            }
        }

        // POSTAL CODE
        if (!isset($invoiceFields["postal_code"]) || empty($invoiceFields["postal_code"])) {
            $invoiceErrors["fid_postal_code"] = $this->translator->trans("parameter.mandatory");
        } else {
            if (!StringUtils::checkNameString($invoiceFields["postal_code"])) {
                $invoiceErrors["fid_postal_code"] = $this->translator->trans("string.invalid");
            }
        }

        // COUNTRY
        if (!isset($invoiceFields["country"]) || empty($invoiceFields["country"])) {
            $invoiceErrors["fid_country"] = $this->translator->trans("parameter.mandatory");
        } else {
            /** @var Country $country */
            $country = $this->countryService->getById($invoiceFields["country"]);
            if (is_null($country)) {
                $invoiceErrors["fid_country"] = $this->translator->trans("country.not.found");
            }
        }

        // EMAIL
        if (!isset($invoiceFields["email"]) || empty($invoiceFields["email"])) {
            $invoiceErrors["fid_email"] = $this->translator->trans("parameter.mandatory");
        } else {
            $emailErrors = EmailUtils::validateEmails($validator, $invoiceFields["email"]);
            if (count($emailErrors) > 0 && $emailErrors instanceof ConstraintViolationListInterface) {
                $formattedErrors = $this->validationException->getFormattedExceptions($emailErrors);
                foreach ($formattedErrors as $formattedError) {
                    $invoiceErrors["fid_email"] = $formattedError;
                    break;
                }
            }
        }

        // PHONE
        if (isset($invoiceFields["phone"]) && !empty($invoiceFields["phone"])) {
            if (!StringUtils::checkPhoneNumber($invoiceFields["phone"])) {
                $invoiceErrors["fid_phone"] = $this->translator->trans("phone.number.invalid");
            }
        }

        return $invoiceErrors;
    }

}