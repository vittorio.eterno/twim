<?php

namespace Utils\StaticUtil;


use ResourceBundle\Entity\CalendarTimezone;

class DateUtils {

    /**
     * @return \DateTime
     */
    static public function getCurrentTime () {

        $current_time = new \DateTime();
        $current_time->setTimestamp(time())->getTimestamp();

        return $current_time;
    }


    static public function getCurrentTimeByTimezone (CalendarTimezone &$calendarTimezone) {

        $UTC   = new \DateTimeZone("UTC");
        $newTZ = new \DateTimeZone($calendarTimezone->getTimezone());
        $date  = new \DateTime( "now", $UTC );
        $date->setTimezone( $newTZ );
        return $date;

    }


    static public function getCurrentTimeUTC () {

        $UTC   = new \DateTimeZone("UTC");
        $date  = new \DateTime( "now", $UTC );
        return $date;
    }


    static public function getMostRecent($dates) {
        $mostRecent = 0;
        foreach($dates as $date){
            $curDate = strtotime($date);
            if ($curDate > $mostRecent) {
                $mostRecent = $curDate;
            }
        }

        return $mostRecent;
    }


    static public function getLessRecent($dates) {
        $lessRecent = strtotime($dates[0]);
        foreach($dates as $date){
            $curDate = strtotime($date);
            if ($curDate < $lessRecent) {
                $lessRecent = $curDate;
            }
        }

        return $lessRecent;
    }

}