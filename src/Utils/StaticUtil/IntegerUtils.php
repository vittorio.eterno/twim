<?php

namespace Utils\StaticUtil;


class IntegerUtils{

    public static function checkId($id){

        if(is_null($id))
            return false;

        if(!is_numeric($id))
            return false;

        if($id < 1)
            return false;

        return true;
    }

    public static function checkNum($val) {

        if(is_null($val))
            return false;

        if(!is_numeric($val))
            return false;

        return true;
    }


    public static function checkInt($val) {

        if(is_null($val))
            return false;

        if(!is_numeric($val))
            return false;

        if($val <= 0)
            return false;

        return true;
    }

}