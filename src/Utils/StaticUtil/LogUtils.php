<?php

namespace Utils\StaticUtil;


class LogUtils {

    public static function getFormattedExceptions(\Exception $e) {
        $exception = array();

        if(!is_null($e)) {
            $exception = array(
                'backtrace'        => $e->getTraceAsString(),
                'file'             => $e->getFile(),
                'line'             => $e->getLine(),
                'exceptionMessage' => $e->getMessage()
            );
        }

        return $exception;
    }


    public static function createFormattedExceptions($file,$line,$message) {
        $exception = array(
            'backtrace'        => '',
            'file'             => $file,
            'line'             => $line,
            'exceptionMessage' => $message
        );

        return $exception;
    }

}