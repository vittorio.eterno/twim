<?php

namespace Utils\StaticUtil;

use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Validator\TraceableValidator;

class EmailUtils {

    /**
     * @param TraceableValidator $validator
     * @param $emails
     * @return array|\Symfony\Component\Validator\ConstraintViolationListInterface
     */
    public static function validateEmails(TraceableValidator $validator, $emails){

        $errors = array();
        $emails = is_array($emails) ? $emails : array($emails);

        $emailConstraint = new Email();
        $emailConstraint->checkHost = true;
        $emailConstraint->checkMX   = true;

        $constraints = array(
            $emailConstraint,
            new \Symfony\Component\Validator\Constraints\NotBlank()
        );

        foreach ($emails as $email) {

            $error = $validator->validate($email, $constraints);

            if (count($error) > 0) {
                return $error;
            }
        }

        return $errors;
    }

}