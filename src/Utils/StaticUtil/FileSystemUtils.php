<?php

namespace Utils\StaticUtil;

use SplFileInfo;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;

class FileSystemUtils {

    const PREFIX_SMALL_THUMB  = "thumb_s_";
    const PREFIX_MEDIUM_THUMB = "thumb_m_";
    const PREFIX_LARGE_THUMB  = "thumb_l_";

    public static function checkPath($path) {

        $fs = new Filesystem();
        try {
            if(!$fs->exists($path)) {
                $fs->mkdir($path);
            }
        } catch (IOExceptionInterface $e) {
            return false;
        }
        return true;
    }

    public static function checkFile($filename) {
        $fs = new Filesystem();
        try {
            if(!$fs->exists($filename)) {
                return false;
            }
        } catch (IOExceptionInterface $e) {
            return false;
        }
        return true;
    }

    public static function deleteFile($filename) {
        $fs = new Filesystem();
        try {
            if($fs->exists($filename)) {
                $fs->remove($filename);
            }

            $elements = explode("/", $filename);
            $name = $elements[count($elements)-1];
            $path = str_replace($name, "", $filename);

            $filename = $path . "thumb_s_" . $name;
            if($fs->exists($filename)) {
                $fs->remove($filename);
            }

            $filename = $path . "thumb_m_" . $name;
            if($fs->exists($filename)) {
                $fs->remove($filename);
            }

            $filename = $path . "thumb_l_" . $name;
            if($fs->exists($filename)) {
                $fs->remove($filename);
            }

        } catch (IOExceptionInterface $e) {
            return false;
        }

        return true;
    }

    public static function renameFile($oldFile, $newFile) {
        $fs = new Filesystem();
        try {
            if($fs->exists($oldFile)) {
                $fs->rename($oldFile, $newFile);
            }
        } catch (IOExceptionInterface $e) {
            return false;
        }
        return true;
    }

    public static function saveFile($path, $name, File $file) {
        if(!FileSystemUtils::checkPath($path))
            return false;

        $info = new SplFileInfo($file->getFilename());
        switch (strtolower($info->getExtension())) {
            case 'sh':
            case 'exe':
            case 'zip':
            case 'tar':
            case 'rar':
            case 'php':
            case 'sql':
            case 'gz':
            case 'html':
            case 'js':
            case 'css':
            case 'py':
            case 'c':
            case 'cpp':
            case 'pif':
            case 'msi':
            case 'msp':
            case 'jar':
            case 'java':
            case 'vb':
            case 'vbs':
            case 'vba':
            case 'chm':
            case 'bat':
            case 'app':
            case 'crt':
            case 'gse':
            case 'gadget':
            case 'jsp':
            case 'hta':
                return false;
        }

        if($file->move($path, $name))
            return true;

        return false;
    }

    public static function createSmallThumbnail($path, $name) {
        $filename = $path.$name;
        $prefix = self::PREFIX_SMALL_THUMB;
        $width = 60;
        $thumb_name = $prefix . $name;

        if(!FileSystemUtils::checkFile($filename))
            return false;

        try {
            $size = getimagesize($filename);
            $ratio = $size[0]/$size[1]; // width/height
            $height = $width / $ratio;

            $src = imagecreatefromstring(file_get_contents($filename));
            $dst = imagecreatetruecolor($width,$height);
            imagefill($dst, 0, 0, imagecolorallocate($dst, 255, 255, 255));
            imagecopyresampled($dst,$src,0,0,0,0,$width,$height,$size[0],$size[1]);
            imagedestroy($src);
            imagepng($dst,$path.$thumb_name, 2); // adjust format as needed
            imagedestroy($dst);
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    public static function createMediumThumbnail($path, $name) {
        $filename = $path.$name;
        $prefix = self::PREFIX_MEDIUM_THUMB;
        $width = 100;
        $thumb_name = $prefix . $name;

        if(!FileSystemUtils::checkFile($filename))
            return false;

        try {
            $size = getimagesize($filename);
            $ratio = $size[0]/$size[1]; // width/height
            $height = $width / $ratio;

            $src = imagecreatefromstring(file_get_contents($filename));
            $dst = imagecreatetruecolor($width,$height);
            imagefill($dst, 0, 0, imagecolorallocate($dst, 255, 255, 255));
            imagecopyresampled($dst,$src,0,0,0,0,$width,$height,$size[0],$size[1]);
            imagedestroy($src);
            imagepng($dst,$path.$thumb_name, 2); // adjust format as needed
            imagedestroy($dst);
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    public static function createLargeThumbnail($path, $name) {
        $filename = $path.$name;
        $prefix = self::PREFIX_LARGE_THUMB;
        $width = 150;
        $thumb_name = $prefix . $name;

        if(!FileSystemUtils::checkFile($filename))
            return false;

        try {
            $size = getimagesize($filename);
            $ratio = $size[0]/$size[1]; // width/height
            $height = $width / $ratio;

            $src = imagecreatefromstring(file_get_contents($filename));
            $dst = imagecreatetruecolor($width,$height);
            imagefill($dst, 0, 0, imagecolorallocate($dst, 255, 255, 255));
            imagecopyresampled($dst,$src,0,0,0,0,$width,$height,$size[0],$size[1]);
            imagedestroy($src);
            imagepng($dst,$path.$thumb_name, 2); // adjust format as needed
            imagedestroy($dst);
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    static public function getTmpZipFolder () {
        $tmpPath = "/tmp/";
        if(!file_exists($tmpPath."zip/")){
            mkdir($tmpPath."zip/");
        }
        return $tmpPath."zip/";
    }

}