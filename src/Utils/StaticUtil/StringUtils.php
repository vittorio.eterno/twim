<?php

namespace Utils\StaticUtil;

class StringUtils {


    public static function checkLanguage ($language) {
        $result = "en_GB";
        if ($language == "it_IT" || $language == "en_GB") {
            $result = $language;
        }
        return $result;
    }


    public static function encodeToUtf8($string) {
        return mb_convert_encoding($string, "UTF-8", mb_detect_encoding($string, "UTF-8", true));
    }


    public static function isValidClientId ($clientId) {
        return preg_match("/^\d*_[a-z0-9]*$/",$clientId);
    }

    public static function checkMongoId($id) {
        if (\MongoId::isValid($id)) {
            return true;
        }
        else {
            return false;
        }
    }

    public static function checkUsername ($val) {
        return preg_match("/^[\p{L}0-9]+$/u",$val);
    }

    public static function checkSuggestString($str) {
        if(is_null($str)) {
            return false;
        }

        if(!is_string($str)) {
            return false;
        }

        if(strlen($str) < 2 || strlen($str) > 150) {
            return false;
        }

        if(strtolower($str) == "null"){
            return false;
        }

        $pattern = "/^[^<>]*$/";
        if(!preg_match($pattern, $str)) {
            return false;
        }

        return true;
    }

    public static function checkHashString($str) {
        if(is_null($str)) {
            return false;
        }

        if(!is_string($str)) {
            return false;
        }

        if(strlen($str) < 30 || strlen($str) > 500) {
            return false;
        }

        if(addslashes($str) != $str) {
            return false;
        }

        return true;
    }

    public static function checkPhoneNumber($value) {
        return preg_match('/^[0-9\-\(\)\/\+\s]*$/', $value);
//        if (count($cleared)<10 || count($cleared)>14) {
//            echo 'Please enter a valid phone number';
//        }
//        return true;
    }

    public static function checkNameString($str) {

        if(is_null($str)) {
            return false;
        }

        if(!is_string($str)) {
            return false;
        }

        if(strlen($str) < 1 || strlen($str) > 255) {
            return false;
        }

        $pattern = "/^[^<>]*$/";
        if(!preg_match($pattern, $str)) {
            return false;
        }

        return true;
    }

    public static function checkLength ($str) {
        if(strlen($str) < 1 || strlen($str) > 255) {
            return false;
        }
        return true;
    }

    public static function checkCustomLength ($str, $minLength=1, $maxLength=255) {
        if(strlen($str) < $minLength || strlen($str) > $maxLength) {
            return false;
        }
        return true;
    }

    public static function checkOnlyLettersRegex ($str) {
        return preg_match("/^[\p{L} .]+$/u", $str);
    }


    public static function checkEmailString($str) {
        if(is_null($str)) {
            return false;
        }

        if(!is_string($str)) {
            return false;
        }

        if(strlen($str) < 1 || strlen($str) > 255) {
            return false;
        }

        $pattern = "/^[a-zA-Z0-9.!#$%&’*+\/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/";
        if(!preg_match($pattern, $str)) {
            return false;
        }

        return true;
    }

    public static function checkPasswordString($str) {
        if(is_null($str)) {
            return false;
        }

        if(!is_string($str)) {
            return false;
        }

        if(strlen($str) < 2 || strlen($str) > 255) {
            return false;
        }

        return true;
    }

    public static function truncateLongText($longText) {
        if(strlen($longText) <= 320)
            return $longText;
        $subStr = substr($longText, 0, 300);

        $lastSpacePos = strrpos($subStr, " ");
        $subStr = substr($subStr, 0, $lastSpacePos) . "...";

        return $subStr;
    }

    public static function generatePassword($seed="") {
        $rand_num = rand(0,100000);
        $now = time();
        $hash = md5($seed.$now.$rand_num);
        $sym = ".,;:!?";
        $password = "";
        for($i=0;$i<20;$i++) {
            $type = rand(0,9);
            if($type < 3) {
                $password .= strtolower(substr($hash,rand(0,strlen($hash)-1),1));
            } elseif($type < 6) {
                $password .= strtoupper(substr($hash,rand(0,strlen($hash)-1),1));
            } elseif($type < 9) {
                $password .= abs($rand_num+rand(0, strlen($password))-rand(0, 100000))%10;
            } else {
                $password .= substr($sym,rand(0,strlen($hash)-1),1);
            }
        }
        return $password;
    }

    public static function countKeywords(string $str) {
        $str = trim($str);
        $tmp = explode(' ', $str);
        $counter = 0;
        foreach($tmp as $sub) {
            if(strlen($sub) > 2)
                $counter++;
        }
        return $counter;
    }

    public static function slugGenerator(string $str) {
        if(is_null($str))
            return "";

        $string = strtolower($str);
        $find = array("à", "è", "é", "ì", "ò", "ù");
        $replace = array("a", "e", "e", "i", "o", "u");
        $string = str_replace($find, $replace, $string);

        $find = array(".", ",", "!", "?");
        $string = str_replace($find, '', $string);

        $string = preg_replace('/^e /','',$string);
        $string = preg_replace('/^di /','',$string);
        $string = preg_replace('/^a /','',$string);
        $string = preg_replace('/^da /','',$string);
        $string = preg_replace('/^in /','',$string);
        $string = preg_replace('/^con /','',$string);
        $string = preg_replace('/^su /','',$string);
        $string = preg_replace('/^per /','',$string);
        $string = preg_replace('/^tra /','',$string);
        $string = preg_replace('/^fra /','',$string);
        $string = preg_replace('/^del /','',$string);
        $string = preg_replace('/^delle /','',$string);
        $string = preg_replace('/^della /','',$string);
        $string = preg_replace('/^dei /','',$string);
        $string = preg_replace('/^d\'/','',$string);
        $string = preg_replace('/^dell\'/','',$string);
        $string = preg_replace('/^il /','',$string);
        $string = preg_replace('/^lo /','',$string);
        $string = preg_replace('/^la /','',$string);
        $string = preg_replace('/^i /','',$string);
        $string = preg_replace('/^gli /','',$string);
        $string = preg_replace('/^le /','',$string);
        $string = preg_replace('/^l\'/','',$string);
        $string = preg_replace('/^un /','',$string);
        $string = preg_replace('/^uno /','',$string);
        $string = preg_replace('/^una /','',$string);
        $string = preg_replace('/^un\'/','',$string);

        $string = preg_replace('/ e /',' ',$string);
        $string = preg_replace('/ di /',' ',$string);
        $string = preg_replace('/ a /',' ',$string);
        $string = preg_replace('/ da /',' ',$string);
        $string = preg_replace('/ in /',' ',$string);
        $string = preg_replace('/ con /',' ',$string);
        $string = preg_replace('/ su /',' ',$string);
        $string = preg_replace('/ per /',' ',$string);
        $string = preg_replace('/ tra /',' ',$string);
        $string = preg_replace('/ fra /',' ',$string);
        $string = preg_replace('/ del /',' ',$string);
        $string = preg_replace('/ delle /',' ',$string);
        $string = preg_replace('/ della /',' ',$string);
        $string = preg_replace('/ dei /',' ',$string);
        $string = preg_replace('/ d\'/',' ',$string);
        $string = preg_replace('/ dell\'/',' ',$string);
        $string = preg_replace('/ il /',' ',$string);
        $string = preg_replace('/ lo /',' ',$string);
        $string = preg_replace('/ la /',' ',$string);
        $string = preg_replace('/ i /',' ',$string);
        $string = preg_replace('/ gli /',' ',$string);
        $string = preg_replace('/ le /',' ',$string);
        $string = preg_replace('/ l\'/',' ',$string);
        $string = preg_replace('/ un /',' ',$string);
        $string = preg_replace('/ uno /',' ',$string);
        $string = preg_replace('/ una /',' ',$string);
        $string = preg_replace('/ un\'/',' ',$string);

        $string = trim($string);
        $previous = '';
        $slug = "";

        for($i=0;$i<strlen($string);$i++) {

            if($i == 0) {
                $external = true;
            } elseif ($i == strlen($string)-1) {
                $external = true;
            } else {
                $external = false;
            }

            $char = substr($string, $i, 1);

            if(!preg_match("/^[a-z0-9]$/", $char)) {
                $char = '-';
            }

            if($char == '-') {
                if($previous == $char) {
                    continue;
                }
                if($external) {
                    continue;
                }
            }

            $previous = $char;
            $slug .= $char;
        }

        return $slug;
    }


    static public function unify($text) {
        // replace all non letters or digits by ''
        $text = preg_replace('/[^a-zA-Z0-9 ]/', '', $text);
        $text = str_replace(" ","-", $text);

        return $text;
    }

    static public function slugify($text) {
        $text = str_replace(" ","-", $text);
        $text = iconv('UTF-8', 'US-ASCII//TRANSLIT', $text);
        $text = strtolower(trim($text));

        return $text;
    }

    static public function isInvalidSlug($text) {
        return !preg_match('/^[a-zA-Z0-9-_]+$/', $text);
    }


    static public function sanitize($val){
        $val = iconv('UTF-8', 'US-ASCII//TRANSLIT', str_replace(" ","_",strtolower($val)));
        $val = str_replace('(','',$val);
        $val = str_replace(')','',$val);
        $val = str_replace('-','_',$val);
        $val = str_replace('\'','_',$val);
        return $val;
    }

    static public function randomString($length) {
        $keys = array_merge(range(0,9), range('a', 'z'));

        $key = "";
        for($i=0; $i < $length; $i++) {
            $key .= $keys[mt_rand(0, count($keys) - 1)];
        }
        return $key;
    }


    static public function toBool($var) {
        if (!is_string($var)) return (bool) $var;
        switch (strtolower($var)) {
            case '1':
            case 'true':
            case 'on':
            case 'yes':
            case 'y':
                return true;
            default:
                return false;
        }
    }

    static public function checkIsBool($string){
        $string = strtolower($string);
        return (in_array($string, array("true", "false", "1", "0", "yes", "no"), true));
    }


    static public function checkImageSize ($size) {
        if($size == "small" || $size == "medium" || $size == "large") {
            return true;
        }
        return false;
    }


}
