<?php

namespace Utils\StaticUtil;


class CurlUtils {

    //TODO: handle this constants ad MySQL database

    const URL_DOMAIN = "http://127.0.0.1:8000";
    const URL_MEEDOX_SHARING_REPORT      = "/private/share/cardio/report";
    const URL_MEEDOX_CHECK_REGISTRATION  = "/private/profile/check/exist";
    const URL_MEEDOX_CREATE_REGISTRATION = "/private/profile/external/create";
    const URL_MEEDOX_LOGIN               = "/private/profile/login/external";


    public static function getCompleteUrl($domain, $url){
        return $domain.$url;
    }


    /**
     * @param $url
     * @param null $domain
     * @param array $postFields
     * @return mixed
     */
    public static function curlPost ($url, $domain=null, $postFields=array()) {

        // Get cURL resource
        $curl = curl_init();

        if(!is_null($domain)) {
            $url = self::getCompleteUrl($domain, $url);
        }

        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_USERAGENT => 'Cardio cURL',
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $postFields
        ));

        // Send the request & save response to $resp
        $resp = curl_exec($curl);

        // Close request to clear up some resources
        curl_close($curl);

        return $resp;

    }

    /**
     * @param $url
     * @param null $domain
     * @return mixed
     */
    public static function curlGet ($url,$domain=null) {

        // Get cURL resource
        $curl = curl_init();

        if(!is_null($domain)) {
            $url = self::getCompleteUrl($domain, $url);
        }

        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_USERAGENT => 'Cardio cURL'
        ));

        // Send the request & save response to $resp
        $resp = curl_exec($curl);

        // Close request to clear up some resources
        curl_close($curl);

        return $resp;

    }

}