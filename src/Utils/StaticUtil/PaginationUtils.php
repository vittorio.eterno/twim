<?php

namespace Utils\StaticUtil;


use Doctrine\ORM\Tools\Pagination\Paginator;

class PaginationUtils {


    public static function getOffset($page=0, $maxResults=50){

        $offset = 0;
        if($page > 1){
            $offset = ($page - 1) * $maxResults;
        }

        return $offset;
    }


    public static function getPaginatorResponse ($object, $page, $maxResults) {

        $formatted = array(
            "totalReturned" => 0,
            "total"         => 0,
            "maxPages"      => 0,
            "currentPage"   => 1,
            "limitPages"    => 5
        );

        if ($object instanceof Paginator) {
            $totalReturned = $object->getIterator()->count();
            $total         = $object->count();
            $maxPages      = ceil($total / $maxResults);

            $formatted["totalReturned"] = $totalReturned;
            $formatted["total"]         = $total;
            $formatted["maxPages"]      = $maxPages;
            $formatted["currentPage"]   = $page;
        }

        return $formatted;
    }

}