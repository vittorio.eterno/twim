<?php

namespace Utils\StaticUtil;


class GZipUtils {

    /**
     * @param $source
     * @param int $level
     * @return bool|string
     */
    static public function gzCompressFile($source,$level=9){
        $destination = $source.'.gz';
        $mode        = 'wb'.$level;
        $error       = false;
        if($fp_out=gzopen($destination,$mode)){
            if($fp_in=fopen($source,'rb')){
                while(!feof($fp_in))
                    gzwrite($fp_out,fread($fp_in,1024*512));
                fclose($fp_in);
            }
            else $error=true;
            gzclose($fp_out);
        }
        else $error=true;
        if($error) return false;
        else return $destination;
    }

    /**
     * @param $source
     * @return array|string
     */
    static public function gzReadFile($source) {
        try {
            $gzip = file_get_contents($source);
            $rest = substr($gzip, -4);
            $unpack = unpack("V", $rest);
            $GZFileSize = end($unpack);

            $FileRead = $source;
            $HandleRead = gzopen($FileRead, "rb");
            $ContentRead = gzread($HandleRead, $GZFileSize);
            gzclose($HandleRead);

            return $ContentRead;
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            return $exception;
        }
    }

    static public function extractGZFile($file_name) {
        try {
            // Raising this value may increase performance
            $buffer_size = 4096; // read 4kb at a time
            $out_file_name = str_replace('.gz', '', $file_name);

            // Open our files (in binary mode)
            $file = gzopen($file_name, 'rb');
            $out_file = fopen($out_file_name, 'wb');

            // Keep repeating until the end of the input file
            while (!gzeof($file)) {
                // Read buffer-size bytes
                // Both fwrite and gzread and binary-safe
                fwrite($out_file, gzread($file, $buffer_size));
            }

            // Files are done, close files
            fclose($out_file);
            gzclose($file);

            return true;
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            return $exception;
        }
    }

}