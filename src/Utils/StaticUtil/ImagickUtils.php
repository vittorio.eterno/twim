<?php

namespace Utils\StaticUtil;

use Imagick;

class ImagickUtils {

    const PREFIX_PDF_IMAGE = "pdf_";

    /**
     * @param $source
     * @param $target
     * @return array|string
     */
    static public function createPdfThumbnail ($source, $target) {

        try {
            $prefix = self::PREFIX_PDF_IMAGE;
            $target = dirname($source) . DIRECTORY_SEPARATOR . $prefix . $target;
            $im = new Imagick($source . "[0]"); // 0-first page, 1-second page

            $im->setImageColorspace(255); // prevent image colors from inverting
            $im->setimageformat("jpeg");
            $im->thumbnailimage(160, 120); // width and height
            $im->writeimage($target);
            $im->clear();
            $im->destroy();

            return $target;
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            return $exception;
        }
    }

}