<?php

namespace Utils\StaticUtil;

use Symfony\Component\HttpFoundation\JsonResponse;

class ListUtils {

    public static function getResponse($results)
    {
        $response = new JsonResponse();
        $response->setStatusCode(200);
        $response->headers->set("Access-Control-Expose-Headers", "CURRENT_PAGE, TOTAL_PAGES, TOTAL_ITEMS");

        if(is_null($results) || count($results) == 0) {
            $response->setContent(json_encode(array()));
            $response->headers->set("CURRENT_PAGE", 0);
            $response->headers->set("TOTAL_PAGES", 0);
            $response->headers->set("TOTAL_ITEMS", 0);

            return $response;
        }

        $response->setContent(json_encode($results['list']));
        $response->headers->set("CURRENT_PAGE", $results['current_page']);
        $response->headers->set("TOTAL_PAGES", $results['total_pages']);
        $response->headers->set("TOTAL_ITEMS", $results['total_items']);

        return $response;
    }

}