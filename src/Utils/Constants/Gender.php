<?php

namespace Utils\Constants;


class Gender {
    
    const MALE    = 0;
    const FEMALE  = 1;

    const MALE_SOURCE   = "male";
    const FEMALE_SOURCE = "female";
}