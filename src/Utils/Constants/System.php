<?php

namespace Utils\Constants;


class System {

    const LOCKING_LOCK     = "lock";
    const LOCKING_UNLOCK   = "unlock";

    const ENABLING_ENABLE  = "enable";
    const ENABLING_DISABLE = "disable";

    const MANAGE_ADD       = "add";
    const MANAGE_REMOVE    = "remove";
}