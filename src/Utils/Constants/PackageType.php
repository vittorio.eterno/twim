<?php

namespace Utils\Constants;


class PackageType {

    const PUBLIC            = 0;
    const INVITATION        = 1;

}