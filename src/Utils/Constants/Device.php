<?php


namespace Utils\Constants;


class Device {
    
    const UNKNOWN = 0;
    const DESKTOP = 1;
    const TABLET  = 2;
    const MOBILE  = 3;
    
    const DESKTOP_NAME  = "DESKTOP";
    const TABLET_NAME   = "TABLET";
    const MOBILE_NAME   = "MOBILE";
    const UNKNOWN_NAME  = "UNKNOWN";
}
