<?php

namespace Utils\Constants;


class AdminLimitation {

    const MODERATOR                   = 0;
    const SUPPLIER                    = 1;

    const MODERATOR_SOURCE            = "moderator";
    const SUPPLIER_SOURCE             = "supplier";

    const AREA_USERS                  = 0;
    const AREA_WORDS                  = 1;
    const AREA_TRANSACTIONS           = 2;
    const AREA_REPORT_USERS           = 3;
    const AREA_REPORT_WORDS           = 4;
    const AREA_VERIFICATION           = 5;
    const AREA_CONTESTS               = 6;
    const AREA_COUPONS                = 7;
    const AREA_PURCHASE_ORDERS        = 8;
    const AREA_PICTURE_FRAMES         = 9;
    const AREA_OPTIONALS              = 10;

    const AREA_USERS_SOURCE           = "area.users";
    const AREA_WORDS_SOURCE           = "area.words";
    const AREA_TRANSACTIONS_SOURCE    = "area.transactions";
    const AREA_REPORT_USERS_SOURCE    = "area.report.users";
    const AREA_REPORT_WORDS_SOURCE    = "area.report.words";
    const AREA_VERIFICATION_SOURCE    = "area.verification.users";
    const AREA_CONTESTS_SOURCE        = "area.contests";
    const AREA_COUPONS_SOURCE         = "area.coupons";
    const AREA_PURCHASE_ORDERS_SOURCE = "area.purchase.orders";
    const AREA_PICTURE_FRAMES_SOURCE  = "area.picture.frames";
    const AREA_OPTIONALS_SOURCE       = "area.optionals";

    const PRIVILEGE_CREATE            = 0;
    const PRIVILEGE_READ              = 1;
    const PRIVILEGE_UPDATE            = 2;
    const PRIVILEGE_DELETE            = 3;

    const PRIVILEGE_CREATE_SOURCE     = "create";
    const PRIVILEGE_READ_SOURCE       = "read";
    const PRIVILEGE_UPDATE_SOURCE     = "update";
    const PRIVILEGE_DELETE_SOURCE     = "delete";


    public static function getAllRoles () {
        return array(
            self::MODERATOR_SOURCE            => self::MODERATOR,
            self::SUPPLIER_SOURCE             => self::SUPPLIER,
        );
    }


    public static function getAllAreas () {
        return array(
            self::AREA_USERS_SOURCE           => self::AREA_USERS,
            self::AREA_WORDS_SOURCE           => self::AREA_WORDS,
            self::AREA_TRANSACTIONS_SOURCE    => self::AREA_TRANSACTIONS,
            self::AREA_REPORT_USERS_SOURCE    => self::AREA_REPORT_USERS,
            self::AREA_REPORT_WORDS_SOURCE    => self::AREA_REPORT_WORDS,
            self::AREA_VERIFICATION_SOURCE    => self::AREA_VERIFICATION,
            self::AREA_CONTESTS_SOURCE        => self::AREA_CONTESTS,
            self::AREA_COUPONS_SOURCE         => self::AREA_COUPONS,
            self::AREA_PURCHASE_ORDERS_SOURCE => self::AREA_PURCHASE_ORDERS,
            self::AREA_PICTURE_FRAMES_SOURCE  => self::AREA_PICTURE_FRAMES,
            self::AREA_OPTIONALS_SOURCE       => self::AREA_OPTIONALS,
        );
    }


    public static function getAllPrivileges () {
        return array(
            self::PRIVILEGE_CREATE_SOURCE     => self::PRIVILEGE_CREATE,
            self::PRIVILEGE_READ_SOURCE       => self::PRIVILEGE_READ,
            self::PRIVILEGE_UPDATE_SOURCE     => self::PRIVILEGE_UPDATE,
            self::PRIVILEGE_DELETE_SOURCE     => self::PRIVILEGE_DELETE,
        );
    }



}