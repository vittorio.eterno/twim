<?php

namespace Utils\Constants;


class Figure {

    const NOTHING           = 0;
    const KING              = 1;
    const PRINCE            = 2;
    const JACK              = 3;
    const KNIGHT            = 4;
    const GOD               = 100;

    const NOTHING_SOURCE    = "nothing";
    const KING_SOURCE       = "king";
    const QUEEN_SOURCE      = "queen";
    const PRINCE_SOURCE     = "prince";
    const PRINCESS_SOURCE   = "princess";
    const JACK_SOURCE       = "jack";
    const KNIGHT_SOURCE     = "knight";
    const GOD_SOURCE        = "god";

}