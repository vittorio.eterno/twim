<?php

namespace Utils\Service;

use Psr\Log\LoggerInterface;

class RestPayPal {

//    const PAYPAL_RETURN_URL = "/paypal/confirmation";
    const PAYPAL_RETURN_URL = "/";

    /** @var string $clientId */
    private $clientId;

    /** @var string $secret */
    private $secret;

    /** @var string $baseUrl */
    private $baseUrl;

    /** @var LoggerInterface $logger */
    private $logger;


    /**
     * RestPayPal constructor.
     * @param $clientId
     * @param $secret
     * @param $baseUrl
     * @param LoggerInterface $logger
     */
    public function __construct($clientId,
                                $secret,
                                $baseUrl,
                                LoggerInterface $logger
    ) {
        $this->clientId          = $clientId;
        $this->secret            = $secret;
        $this->baseUrl           = $baseUrl;
        $this->logger            = $logger;
    }


    public function getAccessToken() {

        $url = $this->baseUrl . '/v1/oauth2/token';

        $data = array(
            'grant_type' => 'client_credentials'
        );

        $header = array(
            'Accept: application/json',
            'Accept-Language: en_US'
        );

        $addition_data = array(
            CURLOPT_USERPWD => $this->clientId . ":" . $this->secret,
            CURLOPT_VERBOSE => 1,
        );

        $result = $this->execCurl($url, $data, $header, $addition_data);
        if(is_null($result))
            return null;

        $data = json_decode($result, true);

        if($data) {
            $access_token = $data['access_token'];
        } else {
            // something wrong ...
            return null;
        }

        return $access_token;
    }


//    private function getReturnUrl (Profile &$profile) {
//        $domain = $this->getMeedoxDomain($profile);
//        return $domain . self::PAYPAL_RETURN_URL;
//    }
//
//    private function getCancelUrl (Profile &$profile) {
//        $domain = $this->getMeedoxDomain($profile);
//        return $domain;
//    }


    public function createPayment ($totalInfo) {

        $params = array (
            "intent" => "sale",
            "payer" => array(
                "payment_method" => "paypal"
            ),
            "transactions" => array(
                array(
                    "amount" => array(
                        "total" => $totalInfo["total"],
                        "currency" => "USD",
                        "details" => array(
                            "subtotal" => round($totalInfo["subtotal"], 2),
                            "tax" => round($totalInfo["tax"], 2)
                        )
                    )
                )
            ),
            "redirect_urls" => array(
                "return_url" => "http://127.0.0.1:8000",
                "cancel_url" => "http://127.0.0.1:8000"
            )
        );

        $data = json_encode($params);

        $header = array(
            'Accept: application/json',
            'Accept-Language: en_US',
            'Content-Type: application/json',
            'Content-Length: '.strlen($data),
            'Authorization: Bearer '.$this->getAccessToken()
        );

        $addition_data = array(
            CURLOPT_VERBOSE => 1,
        );

        $result = $this->execCurl($this->baseUrl."/v1/payments/payment", $data, $header, $addition_data);
        if(is_null($result))
            return null;

        return $result;
    }


    public function execPayment(string $paymentId, string $payerId) {

        $url = $this->baseUrl . '/v1/payments/payment/'. $paymentId .'/execute/';

        $params = array(
            'payer_id' => $payerId
        );

        $data = json_encode($params);
        $header = array(
            'Accept: application/json',
            'Accept-Language: en_US',
            'Content-Type: application/json',
            'Content-Length: '.strlen($data),
            'Authorization: Bearer '.$this->getAccessToken()
        );

        $addition_data = array(
            CURLOPT_VERBOSE => 1,
        );

        $result = $this->execCurl($url, $data, $header, $addition_data);
        if(is_null($result))
            return null;

        return $result;
    }


    private function execCurl($url, $data, $header=array(), $addition_data=array(),$method="POST") {

        if(is_array($data)) {
            $data = http_build_query($data, '', "&");
        }

        $defaults = array(
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_POST => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_URL => $url,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_CUSTOMREQUEST => $method
        );

        if($addition_data) {
            $defaults = $addition_data + $defaults;
        }

        $ch = curl_init($url);
        curl_setopt_array($ch, $defaults);
        $result = curl_exec($ch);

        if(!$result) {
            //return "CURL FAILED: ".curl_error($ch);
            return null;
        }

        curl_close($ch);

        return $result;
    }



    private function execGetCurl($url, $header=array(), $addition_data=array()) {

        $defaults = array(
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_URL => $url,
        );

        if($addition_data) {
            $defaults = $addition_data + $defaults;
        }

        $ch = curl_init($url);
        curl_setopt_array($ch, $defaults);
        $result = curl_exec($ch);

        if(!$result) {
            //return "CURL FAILED: ".curl_error($ch);
            return null;
        }

        curl_close($ch);

        return $result;
    }

}