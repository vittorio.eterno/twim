<?php

namespace Utils\Service;


class Encryption {

    const ENCRYPT_METHOD = "AES-256-CBC";

    private $secretKey;
    private $secretIv;

    /**
     * Encryption constructor.
     * @param $secretKey
     * @param $secretIv
     */
    public function __construct($secretKey, $secretIv) {
        $this->secretKey  = $secretKey;
        $this->secretIv   = $secretIv;
    }

    
    /**
     * simple methods to encrypt or decrypt a plain text string
     * initialization vector(IV) has to be the same when encrypting and decrypting
     *
     */


    /**
     * @param $value
     * @return null|string
     */
    public function encrypting ($value) {

        // hash
        $key = hash('sha256', $this->secretKey);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $this->secretIv), 0, 16);

        $output = openssl_encrypt($value, self::ENCRYPT_METHOD, $key, 0, $iv);
        $output = base64_encode($output);

        return $output;

    }

    /**
     * @param $encrypted
     * @return null|string
     */
    public function decrypting ($encrypted) {

        // hash
        $key = hash('sha256', $this->secretKey);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $this->secretIv), 0, 16);

        $output = openssl_decrypt(base64_decode($encrypted), self::ENCRYPT_METHOD, $key, 0, $iv);

        return $output;
    }

}