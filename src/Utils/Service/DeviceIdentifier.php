<?php

namespace Utils\Service;

use Utils\Constants\Device;
use Utils\Mobile_Detect;

/**
 * Description of DeviceIdentifier
 *
 * @author vittorio
 */
class DeviceIdentifier {
    
    private $device;

    /** @var RequestInfo $requestInfo */
    private $requestInfo;

    /**
     * DeviceIdentifier constructor.
     * @param RequestInfo $requestInfo
     */
    public function __construct( RequestInfo $requestInfo ) {
        $this->requestInfo = $requestInfo;
        $this->device      = Device::DESKTOP;
    }


    public function detect(){
        $detect = new Mobile_Detect;
        
        if( $detect->isTablet() ){
            $this->device = Device::TABLET;
        }
        if( $detect->isMobile() && !$detect->isTablet() ){
            $this->device = Device::MOBILE;
        }

        $this->requestInfo->set("device",$this->device);
        $this->requestInfo->set("deviceName",ucfirst(strtolower($this->getTwigDir())));

    }

    /**
     * @return int
     */
    public function getDevice(){
        return $this->device;
    }

    /**
     * @return string
     */
    public function getName(){
        $name = Device::DESKTOP_NAME;
        if($this->device == Device::UNKNOWN){ $name = Device::UNKNOWN_NAME; }
        else if($this->device == Device::TABLET){ $name = Device::TABLET_NAME; }
        else if($this->device == Device::MOBILE){ $name = Device::MOBILE_NAME; }
        return $name;
    }

    /**
     * @return string
     */
    public function getTwigDir(){
        $name = Device::DESKTOP_NAME;
        if($this->device == Device::TABLET || $this->device == Device::MOBILE){ $name = Device::MOBILE_NAME; }
        return $name;
    }
    
}
