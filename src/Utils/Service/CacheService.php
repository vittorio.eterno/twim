<?php

namespace Utils\Service;


use Predis\Client;
use Psr\Cache\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Cache\Adapter\RedisAdapter;
use Utils\StaticUtil\LogUtils;

class CacheService {

    /** @var Client $cache */
    private $cache;

    /** @var LoggerInterface $logger */
    private $logger;

    /**
     * CacheService constructor.
     * @param Client $client
     * @param LoggerInterface $logger
     */
    public function __construct(Client $client, LoggerInterface $logger){
        $this->cache  = new RedisAdapter($client);
        $this->logger = $logger;
    }

    /**
     * @param string $key
     * @param int $expire
     * @return mixed|null
     */
    public function get(string $key, $expire=600){

        $obj = null;

        $cacheKey = md5($key);

        try {

            $cachedItem = $this->cache->getItem($cacheKey);
            $cachedItem->expiresAfter($expire);

            if ($cachedItem->isHit()) {
                $obj = $cachedItem->get();
            }

        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            $this->logger->error("Failed get cache", $exception);
        } catch (InvalidArgumentException $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            $this->logger->error("Invalid argument cache", $exception);
        }

        return $obj;
    }

    /**
     * @param string $key
     * @param $value
     * @param int $expire
     * @return mixed|null
     */
    public function set(string $key, $value, $expire=600){
        $cacheKey = md5($key);

        $obj = null;

        try {

            $cachedItem = $this->cache->getItem($cacheKey);

            // imagine we do some expensive task here
            // such as calling a remote API
            // $expensiveValue = $this->injectedApiService->get("some-url/etc");

            $cachedItem->set($value);
            $cachedItem->expiresAfter($expire);
            $this->cache->save($cachedItem);

            $obj = $cachedItem->get();

        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            $this->logger->error("Failed get cache", $exception);
        } catch (InvalidArgumentException $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            $this->logger->error("Invalid argument cache", $exception);
        }

        return $obj;
    }
}