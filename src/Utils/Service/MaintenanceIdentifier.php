<?php

namespace Utils\Service;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

/**
 * Description of MaintenanceIdentifier
 *
 * @author vittorio
 */
class MaintenanceIdentifier {
    
    private $maintenance;

    /** @var RequestInfo $requestInfo */
    private $requestInfo;

    private $templating;

    /**
     * MaintenanceIdentifier constructor.
     * @param RequestInfo $requestInfo
     * @param $maintenance
     * @param $templating
     */
    public function __construct( RequestInfo $requestInfo, $maintenance, $templating ) {
        $this->requestInfo = $requestInfo;
        $this->maintenance = $maintenance;
        $this->templating  = $templating;
    }


    public function detect(GetResponseEvent $event){
        if ($this->maintenance && $this->requestInfo->isProdEnvironment()) {
            $engine     = $this->templating;
            $deviceName = $this->requestInfo->get("deviceName", "Desktop");
            $content    = $engine->render($deviceName.'/'.'service/maintenance.html.twig');

            $event->setResponse(new Response($content, 503));
            $event->stopPropagation();
        }
    }
    
}
