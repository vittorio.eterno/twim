<?php

namespace Utils\Service;


use Symfony\Component\Translation\TranslatorInterface;
use UserBundle\Entity\User;
use Utils\Constants\Figure;
use Utils\Constants\Gender;

class FigureIdentifier {


    /** @var TranslatorInterface $translator */
    private $translator;

    /**
     * FigureIdentifier constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator) {
        $this->translator  = $translator;
    }


    /**
     * @param User $user
     * @return null|string
     */
    public function getFigureName (User &$user) {
        $gender = $user->getGender();

        $source=null;

        if ($gender == Gender::FEMALE) {
            if ($this->isKing($user->getFigureType())) {
                $source = Figure::QUEEN_SOURCE;
            } elseif ($this->isPrince($user->getFigureType())) {
                $source = Figure::PRINCESS_SOURCE;
            }
        } else {
            if ($this->isKing($user->getFigureType())) {
                $source = Figure::KING_SOURCE;
            } elseif ($this->isPrince($user->getFigureType())) {
                $source = Figure::PRINCE_SOURCE;
            }
        }

        if ($this->isJack($user->getFigureType())) {
            $source = Figure::JACK_SOURCE;
        } elseif ($this->isKnight($user->getFigureType())) {
            $source = Figure::KNIGHT_SOURCE;
        } elseif ($this->isGod($user->getFigureType())) {
            $source = Figure::GOD_SOURCE;
        }

        if (!is_null($source)) {
            return $this->translator->trans($source);
        }

        return null;
    }

    public function isKing ($userType) {
        return $userType == Figure::KING;
    }

    public function isPrince ($userType) {
        return $userType == Figure::PRINCE;
    }

    public function isJack ($userType) {
        return $userType == Figure::JACK;
    }

    public function isKnight ($userType) {
        return $userType == Figure::KNIGHT;
    }

    public function isGod ($userType) {
        return $userType == Figure::GOD;
    }

}