<?php

namespace Utils\Service;

/**
 * Description of RequestInfo
 *
 * @author vittorio
 */
class RequestInfo {

    private $className;
    private $device;
    private $deviceName;
    private $prodEnvironment = true;

    /**
     * RequestInfo constructor.
     */
    public function __construct() {
        $this->className = get_class($this);
    }

    /**
     * @param $property
     * @return bool
     */
    public function has ($property) {
        return property_exists($this->className,$property);
    }

    /**
     * @param $property
     * @param null $defaultValue
     * @return mixed|null
     */
    public function get ($property, $defaultValue=null) {
        return property_exists($this->className,$property) ? $this->{$property} : $defaultValue ;
    }

    /**
     * @param $property
     * @param $value
     */
    public function set ($property, $value) {
        if (property_exists($this->className,$property)) {
            $this->{$property} = $value;
        }
    }

    /**
     * @return bool
     */
    public function isProdEnvironment(): bool {
        return $this->prodEnvironment;
    }

}
