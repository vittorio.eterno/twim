<?php

namespace Utils\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Utils\StaticUtil\GZipUtils;


class FileUploader {

    private $targetDirectory;

    private $targetTmpDirectory;

    /**
     * FileUploader constructor.
     * @param $targetDirectory
     * @param $targetTmpDirectory
     */
    public function __construct($targetDirectory, $targetTmpDirectory) {
        $this->targetDirectory    = $targetDirectory;
        $this->targetTmpDirectory = $targetTmpDirectory;
    }

    /**
     * @param UploadedFile $file
     * @param null $fileNameHashed
     * @param bool $isByProfessional
     * @return bool
     */
    public function upload(UploadedFile $file, $fileNameHashed=null, $isByProfessional=false) {

        if(!is_null($fileNameHashed)) {
            $targetDirectory = !$isByProfessional?$this->getTargetDirectory():$this->getTargetTmpDirectory();
            $file->move($targetDirectory, $fileNameHashed);
            if(!GZipUtils::gzCompressFile($targetDirectory.$fileNameHashed)){
                return false;
            }
            if (file_exists($targetDirectory.$fileNameHashed)) {
                unlink($targetDirectory.$fileNameHashed);
            }
            return true;
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function getTargetDirectory() {
        return $this->targetDirectory;
    }

    /**
     * @return mixed
     */
    public function getTargetTmpDirectory() {
        return $this->targetTmpDirectory;
    }

    /**
     * @param $fileNameHashed
     * @return array|null|string
     */
    public function getGZFile ($fileNameHashed) {

        $targetDirectory = $this->getTargetDirectory();
        $fileContent = GZipUtils::gzReadFile($targetDirectory.$fileNameHashed.'.gz');

        if(is_array($fileContent)) {
            //TODO: add file.error log
            return null;
        }

        return $fileContent;
    }

    /**
     * @param $fileNameHashed
     * @return string
     */
    public function getFilePath ($fileNameHashed) {
        $targetDirectory = $this->getTargetDirectory();
        return $targetDirectory.$fileNameHashed.'.gz';
    }

    /**
     * @param $filePath
     * @return bool
     */
    public function extractGZFile ($filePath) {
        $extractedFile = GZipUtils::extractGZFile($filePath);
        if(is_array($extractedFile)) {
            //TODO: add file.error log
            return false;
        }
        return true;
    }

    /**
     * @param $fileNameHashed
     * @return bool
     */
    public function moveFromTmp ($fileNameHashed) {
        $targetDirectory    = $this->getTargetDirectory();
        $targetTmpDirectory = $this->getTargetTmpDirectory();

        $currentFilePath = $targetTmpDirectory.$fileNameHashed.'.gz';
        $newFilePath     = $targetDirectory.$fileNameHashed.'.gz';

        $fileMoved = rename($currentFilePath, $newFilePath);

        return $fileMoved;

    }

    /**
     * @param $fileNameHashed
     * @return bool
     */
    public function removeTmp ($fileNameHashed) {

        $targetTmpDirectory = $this->getTargetTmpDirectory();
        if (file_exists($targetTmpDirectory.$fileNameHashed)) {
            unlink($targetTmpDirectory.$fileNameHashed);
            return true;
        }

        return false;
    }


    public function createThumbnail($filename) {

        $targetDirThumbs = $this->getTargetDirectory(). '/thumbs';
        $finalWidthImg = 200;

        if(preg_match('/[.](jpg)|(jpeg)$/', $filename)) {
            $im = imagecreatefromjpeg($this->getTargetDirectory() . '/' . $filename);
        } else if (preg_match('/[.](gif)$/', $filename)) {
            $im = imagecreatefromgif($this->getTargetDirectory() . '/' . $filename);
        } else if (preg_match('/[.](png)$/', $filename)) {
            $im = imagecreatefrompng($this->getTargetDirectory() . '/' . $filename);
        }

        $ox = imagesx($im);
        $oy = imagesy($im);

        $nx = $finalWidthImg;
        $ny = floor($oy * ($finalWidthImg / $ox));

        $nm = imagecreatetruecolor($nx, $ny);

        imagecopyresized($nm, $im, 0,0,0,0,$nx,$ny,$ox,$oy);

        if(!file_exists($targetDirThumbs)) {
            if(!mkdir($targetDirThumbs)) {
                die("There was a problem. Please try again!");
            }
        }

        imagejpeg($nm, $targetDirThumbs . '/' . $filename);
    }


}