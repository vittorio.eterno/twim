<?php

namespace Utils\Service;

use ImageBundle\Entity\Image;
use ImageBundle\Service\ImageService;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Translation\TranslatorInterface;
use Utils\StaticUtil\FileSystemUtils;

class ResponseUtils {

    /** @var TranslatorInterface $translator */
    private $translator;

    /** @var JsonResponse $response */
    private $response;

    /** @var LoggerInterface $logger */
    private $logger;

    /**
     * ResponseUtils constructor.
     * @param TranslatorInterface $translator
     * @param LoggerInterface $logger
     */
    public function __construct(TranslatorInterface $translator, LoggerInterface $logger) {
        $this->translator  = $translator;
        $this->logger      = $logger;
        $this->response    = new JsonResponse();
     }

    /**
     * $message param is the source.id of the message to translate
     * $params is an array of optional parameters required for translation:
     * - array('%name%' => $name)
     *
     * @param array $results
     * @param string $message
     * @param int $status
     * @param array $params
     * @return JsonResponse
     */
    public function getResponse($results=null, $message='', $status=200, $params=array()) {
        $trans_message = $this->translator->trans($message, $params);

        //TODO: add trace log

        if(count($results) == 0) {
            $results=null;
        }

        $this->response->setStatusCode($status);
        $this->response->setData(array(
            "status"  => $status,
            "message" => $trans_message,
            "results" => $results
        ));

        return $this->response;
    }

    /**
     * @param null $results
     * @param string $message
     * @param int $status
     * @param array $errors
     * @param array $params
     * @return JsonResponse
     */
    public function getApiResponse($results=null, $message='', $status=200, $errors=array(), $params=array()) {
        $trans_message = $this->translator->trans($message, $params);

        //TODO: add trace log

        if(count($results) == 0) {
            $results=null;
        }

        $this->response->setStatusCode($status);
        $this->response->setData(array(
            "message" => $trans_message,
            "result"  => $results,
            "errors"  => $errors
        ));

        return $this->response;
    }

    /**
     * $message param is the source.id of the message to translate
     * $params is an array of optional parameters required for translation:
     * - array('%name%' => $name)
     *
     * @param ImageService $imageService
     * @param $image
     * @return JsonResponse
     */
    public function getResponseImage(ImageService &$imageService, $image) {
        $this->response->headers->set("Access-Control-Expose-Headers", "Content-type, Content-Disposition, Content-length");

        $this->response->headers->set('Cache-Control', 'max-age=2629746, public');
        $this->response->headers->set('Content-type', mime_content_type($imageService->getFullFilePath($image)));
        $this->response->headers->set('Content-Disposition', 'inline');
        $this->response->headers->set('Content-length', filesize($imageService->getFullFilePath($image)));

        $this->response->setStatusCode(200);
        $this->response->sendHeaders();
        $this->response->setContent(file_get_contents($imageService->getFullFilePath($image)));

        return $this->response;
    }

    /**
     * @param $imagePath
     * @return JsonResponse
     */
    public function getResponseImageByPath($imagePath) {
        $this->response->headers->set("Access-Control-Expose-Headers", "Content-type, Content-Disposition, Content-length");

        $this->response->headers->set('Cache-Control', 'max-age=2629746, public');
        $this->response->headers->set('Content-type', mime_content_type($imagePath));
        $this->response->headers->set('Content-Disposition', 'inline');
        $this->response->headers->set('Content-length', filesize($imagePath));

        $this->response->setStatusCode(200);
        //$this->response->sendHeaders();
        $this->response->setContent(file_get_contents($imagePath));

        return $this->response;
    }

    /**
     * @param $file
     * @param $mimeType
     * @param $size
     * @param $filename
     * @return JsonResponse
     */
    public function getResponseDownload(&$file, $mimeType, $size, $filename) {

        $this->response->headers->set("Access-Control-Expose-Headers", "Content-type, Content-Disposition, Content-length");

        $this->response->headers->set('Cache-Control', 'max-age=2629746, public');
        $this->response->headers->set('Content-type', $mimeType);
        $this->response->headers->set('Content-Disposition', 'attachment; filename="' . $filename . '";');
        $this->response->headers->set('Content-length', $size);

        $this->response->setStatusCode(200);
        $this->response->sendHeaders();
        $this->response->setContent($file);

        return $this->response;
    }

    /**
     * @param $zipName
     * @return JsonResponse
     */
    public function getResponseDownloadZip($zipName) {

        $this->response->headers->set("Access-Control-Expose-Headers", "Content-type, Content-Disposition, Content-length");

        $name    = $zipName;
        $tmpPath = FileSystemUtils::getTmpZipFolder();
        if (strpos($name, $tmpPath) !== false) {
            $name = str_replace($tmpPath,'',$name);
        }

        $this->response->headers->set('Cache-Control', 'max-age=2629746, public');
        $this->response->headers->set('Content-type', 'application/zip');
        $this->response->headers->set('Content-Disposition', 'attachment; filename="' . $name . '";');
        $this->response->headers->set('Content-length', filesize($zipName));

        $this->response->setStatusCode(200);
        $this->response->sendHeaders();
        $this->response->setContent(file_get_contents($zipName));

        return $this->response;
    }

    /**
     * This method check if there are results and return correct json response
     *
     * @param array $results
     * @param string $message
     * @param int $status
     * @param array $params
     * @return JsonResponse
     */
    public function getListResponse($results=array(), $message='', $status=200, $params=array()){

        $trans_message = $this->translator->trans($message, $params);

        //TODO: add trace log
        //$this->log_monitor->trace($channel, $status, $trans_message);

        $this->response->setStatusCode($status);
        $this->response->headers->set("Access-Control-Expose-Headers", "CURRENT_PAGE, TOTAL_PAGES, TOTAL_ITEMS");

        if(is_null($results) || count($results) == 0 || !isset($results['list'])) {
            $results['list']         = array();
            $results['current_page'] = 0;
            $results['total_pages']  = 0;
            $results['total_items']  = 0;
        }

        $this->response->headers->set("CURRENT_PAGE", $results['current_page']);
        $this->response->headers->set("TOTAL_PAGES", $results['total_pages']);
        $this->response->headers->set("TOTAL_ITEMS", $results['total_items']);


        $this->response->setData(array(
            "status"  => $status,
            "message" => $trans_message,
            "results" => $results['list']
        ));

        return $this->response;
    }

    /**
     * @param $totalItems
     * @param string $message
     * @param int $status
     * @param array $params
     * @return JsonResponse
     */
    public function getHeadResponseTotalItems($totalItems, $message='', $status=200, $params=array()){

        $trans_message = $this->translator->trans($message, $params);

        //TODO: add trace log
        //$this->log_monitor->trace($channel, $status, $trans_message);


        $this->response->setStatusCode($status);
        $this->response->headers->set("Access-Control-Expose-Headers", "TOTAL_ITEMS");

        $this->response->headers->set("TOTAL_ITEMS", $totalItems);

        return $this->response;
    }

}