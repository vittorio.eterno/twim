<?php

namespace Utils\Service;



/**
 * Description of EnvironmentIdentifier
 *
 * @author vittorio
 */
class EnvironmentIdentifier {
    
    private $environment;

    /** @var RequestInfo $requestInfo */
    private $requestInfo;

    /**
     * EnvironmentIdentifier constructor.
     * @param RequestInfo $requestInfo
     * @param $env
     */
    public function __construct( RequestInfo $requestInfo , $env ) {
        $this->requestInfo = $requestInfo;
        $this->environment = $env;
    }


    public function detect() {
        $isProdEnvironment = !in_array($this->environment, array('test', 'dev'));
        $this->requestInfo->set("prodEnvironment",$isProdEnvironment);
    }
    
}
