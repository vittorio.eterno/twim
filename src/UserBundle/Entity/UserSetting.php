<?php

namespace UserBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Schema\Entity;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table("`user_settings`")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\UserSettingRepository")
 */
class UserSetting extends Entity {

    /**
     * @ORM\Column(name="`id`", type="bigint", options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer $id
     */
    private $id;

    /**
     * Many UserSettings have One User.
     * @ORM\ManyToOne(targetEntity="User", inversedBy="settings")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * Many UserSettings have One Setting.
     * @ORM\ManyToOne(targetEntity="Setting")
     * @ORM\JoinColumn(name="setting_id", referencedColumnName="id")
     */
    private $setting;

    /**
     * @ORM\Column(name="`value`", type="boolean", nullable=false, options={"default":0})
     */
    private $value = 0;

    /**
     * @ORM\Column(name="`created_at`", type="datetime", nullable=false)
     * @var DateTime $createdAt
     */
    private $createdAt;

    /**
     * @ORM\Column(name="`disabled_by`", type="string", length=22, nullable=true)
     */
    private $disabledBy;

    /**
     * @ORM\Column(name="`disabled_at`", type="datetime", nullable=true)
     * @var DateTime $disabledAt
     */
    private $disabledAt;

    /**
     * @ORM\Column(name="`is_disabled`", type="boolean", nullable=false, options={"default":0})
     */
    private $isDisabled = false;

    ################################################# SERIALIZER FUNCTIONS

    /**
     * Returns the array of fields to serialize in entity administration view.
     * @return array
     */
    public function adminSerializer()
    {
        $view_vars = $this->viewSerializer();

        $admin_vars = array();

        return array_merge($view_vars, $admin_vars);
    }

    /**
     * Returns the array of fields to serialize in entity view.
     * @return array
     */
    public function viewSerializer()
    {
        $list_vars = $this->listSerializer();

        $view_vars = array(
            'id'           => $this->serializedId(),
        );

        return array_merge($list_vars, $view_vars);
    }

    /**
     * Returns the array of fields to serialize in a list of this entity.
     * @return array
     */
    public function listSerializer()
    {
        $list_vars = array(
            'value' => $this->serializedValue(),
        );
        return $list_vars;
    }

    public function settingSerializer() {

        /** @var Setting $setting */
        $setting = $this->getSetting();

        $vars = array(
            'id'            => $this->serializedId(),
            "category"      => $setting->serializedCategoryType(),
            "value"         => $this->serializedValue(),
            "source"        => $setting->serializedSource(),
            "defaultValue"  => $setting->serializedDefaultValue(),
            "settingId"     => $setting->serializedId(),
        );

        return $vars;
    }

    /**
     * Returns the hash code unique identifier of the entity.
     * @return string
     */
    public function hashCode()
    {
        // TODO: Implement hashCode() method.
    }

    ################################################# SERIALIZED FUNCTIONS

    /**
     * Setting id
     * @JMS\VirtualProperty
     * @JMS\SerializedName("id")
     * @JMS\Type("integer")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedId() {
        return (is_null($this->id)?null:$this->id);
    }

    /**
     * Setting value
     * @JMS\VirtualProperty
     * @JMS\SerializedName("value")
     * @JMS\Type("boolean")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedValue() {
        return $this->value;
    }


    ################################################# GETTERS AND SETTERS FUNCTIONS

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return Setting
     */
    public function getSetting()
    {
        return $this->setting;
    }

    /**
     * @param Setting $setting
     */
    public function setSetting(Setting $setting): void
    {
        $this->setting = $setting;
    }



    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @param $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param string|null $disabledBy
     * @return $this
     */
    public function setDisabledBy($disabledBy = null)
    {
        $this->disabledBy = $disabledBy;

        return $this;
    }

    /**
     * Get disabledBy.
     *
     * @return string|null
     */
    public function getDisabledBy()
    {
        return $this->disabledBy;
    }

    /**
     * @param null $disabledAt
     * @return $this
     */
    public function setDisabledAt($disabledAt = null)
    {
        $this->disabledAt = $disabledAt;

        return $this;
    }

    /**
     * Get disabledAt.
     *
     * @return \DateTime|null
     */
    public function getDisabledAt()
    {
        return $this->disabledAt;
    }

    /**
     * @return mixed
     */
    public function getIsDisabled()
    {
        return $this->isDisabled;
    }

    /**
     * @param $isDisabled
     * @return $this
     */
    public function setIsDisabled($isDisabled)
    {
        $this->isDisabled = $isDisabled;

        return $this;
    }

}
