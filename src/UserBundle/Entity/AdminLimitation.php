<?php

namespace UserBundle\Entity;

use Schema\Entity;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Utils\Constants\AdminLimitation as ConstantsAdminLimitation;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AdminLimitation
 *
 * @ORM\Table(name="admin_limitations")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\AdminLimitationRepository")
 */
class AdminLimitation extends Entity {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Many AdminLimitations have One User.
     * @ORM\ManyToOne(targetEntity="User", inversedBy="adminLimitations")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var int|null
     *
     * @ORM\Column(name="role_type", type="smallint", nullable=true, options={"default":0})
     * @Assert\NotBlank(message = "require.limitation.role.type")
     */
    private $roleType;

    /**
     * @var int|null
     *
     * @ORM\Column(name="area", type="smallint", nullable=true)
     * @Assert\NotBlank(message = "require.limitation.area")
     */
    private $area;

    /**
     * @var array|null
     *
     * @ORM\Column(name="privileges", type="json_array", nullable=true)
     * @Assert\NotBlank(message = "require.limitation.privileges")
     */
    private $privileges;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="edited_at", type="datetime", nullable=true)
     */
    private $editedAt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="edited_by", type="string", length=22, nullable=true)
     */
    private $editedBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="disabled_at", type="datetime", nullable=true)
     */
    private $disabledAt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="disabled_by", type="string", length=22, nullable=true)
     */
    private $disabledBy;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_disabled", type="boolean", nullable=true, options={"default":0})
     */
    private $isDisabled;


    ################################################# SERIALIZER FUNCTIONS

    /**
     * Returns the array of fields to serialize in entity administration view.
     * @return array
     */
    public function adminSerializer() {
        $view_vars = $this->viewSerializer();

        $admin_vars = array();

        return array_merge($view_vars, $admin_vars);
    }

    /**
     * Returns the array of fields to serialize in entity view.
     * @return array
     */
    public function viewSerializer() {
        $list_vars = $this->listSerializer();

        $view_vars = array(
        );

        return array_merge($list_vars, $view_vars);
    }

    /**
     * Returns the array of fields to serialize in a list of this entity.
     * @return array
     */
    public function listSerializer() {
        $list_vars = array(
            'id'           => $this->serializedId(),
            'roleType'     => $this->serializedRoleTypeSource(),
            'area'         => $this->serializedAreaSource(),
            'privileges'   => $this->serializedPrivilegesSource(),
            'isDisabled'   => $this->serializedIsDisabled(),
        );
        return $list_vars;
    }

    /**
     * Returns the hash code unique identifier of the entity.
     * @return string
     */
    public function hashCode() {
        // TODO: Implement hashCode() method.
    }

    ################################################# SERIALIZED FUNCTIONS

    /**
     * id
     * @JMS\VirtualProperty
     * @JMS\SerializedName("id")
     * @JMS\Type("integer")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedId() {
        return (is_null($this->id)?null:$this->id);
    }

    /**
     * roleType
     * @JMS\VirtualProperty
     * @JMS\SerializedName("roleType")
     * @JMS\Type("string")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedRoleTypeSource() {

        switch ($this->roleType) {
            case ConstantsAdminLimitation::SUPPLIER:
                $source = ConstantsAdminLimitation::SUPPLIER_SOURCE;
                break;
            default:
                $source = ConstantsAdminLimitation::MODERATOR_SOURCE;
        }

        return $source;
    }

    /**
     * area
     * @JMS\VirtualProperty
     * @JMS\SerializedName("area")
     * @JMS\Type("string")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedAreaSource() {

        switch ($this->area) {
            case ConstantsAdminLimitation::AREA_WORDS:
                $source = ConstantsAdminLimitation::AREA_WORDS_SOURCE;
                break;
            case ConstantsAdminLimitation::AREA_TRANSACTIONS:
                $source = ConstantsAdminLimitation::AREA_TRANSACTIONS_SOURCE;
                break;
            case ConstantsAdminLimitation::AREA_REPORT_USERS:
                $source = ConstantsAdminLimitation::AREA_REPORT_USERS_SOURCE;
                break;
            case ConstantsAdminLimitation::AREA_REPORT_WORDS:
                $source = ConstantsAdminLimitation::AREA_REPORT_WORDS_SOURCE;
                break;
            case ConstantsAdminLimitation::AREA_VERIFICATION:
                $source = ConstantsAdminLimitation::AREA_VERIFICATION_SOURCE;
                break;
            case ConstantsAdminLimitation::AREA_CONTESTS:
                $source = ConstantsAdminLimitation::AREA_CONTESTS_SOURCE;
                break;
            case ConstantsAdminLimitation::AREA_COUPONS:
                $source = ConstantsAdminLimitation::AREA_COUPONS_SOURCE;
                break;
            case ConstantsAdminLimitation::AREA_PURCHASE_ORDERS:
                $source = ConstantsAdminLimitation::AREA_PURCHASE_ORDERS_SOURCE;
                break;
            case ConstantsAdminLimitation::AREA_PICTURE_FRAMES:
                $source = ConstantsAdminLimitation::AREA_PICTURE_FRAMES_SOURCE;
                break;
            case ConstantsAdminLimitation::AREA_OPTIONALS:
                $source = ConstantsAdminLimitation::AREA_OPTIONALS_SOURCE;
                break;
            default:
                $source = ConstantsAdminLimitation::AREA_USERS_SOURCE;
        }

        return $source;
    }


    /**
     * privileges
     * @JMS\VirtualProperty
     * @JMS\SerializedName("privileges")
     * @JMS\Type("string")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedPrivilegesSource() {

        $results = array ();

        if (!is_null($this->privileges)) {
            foreach ($this->privileges as $privilege) {
                switch ($privilege) {
                    case ConstantsAdminLimitation::PRIVILEGE_CREATE:
                        array_push($results, ConstantsAdminLimitation::PRIVILEGE_CREATE_SOURCE);
                        break;
                    case ConstantsAdminLimitation::PRIVILEGE_UPDATE:
                        array_push($results, ConstantsAdminLimitation::PRIVILEGE_UPDATE_SOURCE);
                        break;
                    case ConstantsAdminLimitation::PRIVILEGE_DELETE:
                        array_push($results, ConstantsAdminLimitation::PRIVILEGE_DELETE_SOURCE);
                        break;
                    default:
                        array_push($results, ConstantsAdminLimitation::PRIVILEGE_READ_SOURCE);
                }
            }
        }

        return $results;
    }


    /**
     * isDisabled
     * @JMS\VirtualProperty
     * @JMS\SerializedName("isDisabled")
     * @JMS\Type("boolean")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedIsDisabled() {
        return (is_null($this->isDisabled)?false:$this->isDisabled);
    }


    ################################################# GETTERS AND SETTERS FUNCTIONS


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * Set roleType.
     *
     * @param int|null $roleType
     *
     * @return AdminLimitation
     */
    public function setRoleType($roleType = null)
    {
        $this->roleType = $roleType;

        return $this;
    }

    /**
     * Get roleType.
     *
     * @return int|null
     */
    public function getRoleType()
    {
        return $this->roleType;
    }

    /**
     * @return int|null
     */
    public function getArea(): ?int
    {
        return $this->area;
    }

    /**
     * @param int|null $area
     */
    public function setArea(?int $area): void
    {
        $this->area = $area;
    }

    /**
     * @return array|null
     */
    public function getPrivileges(): ?array
    {
        return $this->privileges;
    }

    /**
     * @param array|null $privileges
     */
    public function setPrivileges(?array $privileges): void
    {
        $this->privileges = $privileges;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return AdminLimitation
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set editedAt.
     *
     * @param \DateTime|null $editedAt
     *
     * @return AdminLimitation
     */
    public function setEditedAt($editedAt = null)
    {
        $this->editedAt = $editedAt;

        return $this;
    }

    /**
     * Get editedAt.
     *
     * @return \DateTime|null
     */
    public function getEditedAt()
    {
        return $this->editedAt;
    }

    /**
     * Set editedBy.
     *
     * @param string|null $editedBy
     *
     * @return AdminLimitation
     */
    public function setEditedBy($editedBy = null)
    {
        $this->editedBy = $editedBy;

        return $this;
    }

    /**
     * Get editedBy.
     *
     * @return string|null
     */
    public function getEditedBy()
    {
        return $this->editedBy;
    }

    /**
     * Set disabledAt.
     *
     * @param \DateTime|null $disabledAt
     *
     * @return AdminLimitation
     */
    public function setDisabledAt($disabledAt = null)
    {
        $this->disabledAt = $disabledAt;

        return $this;
    }

    /**
     * Get disabledAt.
     *
     * @return \DateTime|null
     */
    public function getDisabledAt()
    {
        return $this->disabledAt;
    }

    /**
     * Set disabledBy.
     *
     * @param string|null $disabledBy
     *
     * @return AdminLimitation
     */
    public function setDisabledBy($disabledBy = null)
    {
        $this->disabledBy = $disabledBy;

        return $this;
    }

    /**
     * Get disabledBy.
     *
     * @return string|null
     */
    public function getDisabledBy()
    {
        return $this->disabledBy;
    }

    /**
     * Set isDisabled.
     *
     * @param bool|null $isDisabled
     *
     * @return AdminLimitation
     */
    public function setIsDisabled($isDisabled = null)
    {
        $this->isDisabled = $isDisabled;

        return $this;
    }

    /**
     * Get isDisabled.
     *
     * @return bool|null
     */
    public function getIsDisabled()
    {
        return $this->isDisabled;
    }
}
