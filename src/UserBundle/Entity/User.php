<?php

namespace UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use EmailBundle\Entity\Email;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use ResourceBundle\Entity\CalendarTimezone;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use DateTime;
use Utils\Constants\Figure;
use Utils\Constants\Gender;
use WordBundle\Entity\Word;

/**
 * @ORM\Table(name="`users`")
 * @ORM\Entity
 * @UniqueEntity(
 *     fields="email",
 *     message="exist.email",
 *     groups={"registration"}
 * )
 */
class User extends BaseUser {

    /**
     * @ORM\Column(name="`id`", type="bigint", options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer $id
     */
    protected $id;

    /** @ORM\Column(name="facebook_id", type="string", length=255, nullable=true) */
    protected $facebook_id;

    /** @ORM\Column(name="facebook_access_token", type="string", length=255, nullable=true) */
    protected $facebook_access_token;

    /** @ORM\Column(name="googleplus_id", type="string", length=255, nullable=true) */
    protected $googleplus_id;

    /** @ORM\Column(name="googleplus_access_token", type="string", length=255, nullable=true) */
    protected $googleplus_access_token;

    /**
     * @Assert\NotBlank(message = "require.email", groups={"registration"})
     * @Assert\Email(
     *     message = "invalid.email",
     *     checkMX = true,
     *     checkHost = true,
     *     groups={"registration"}
     * )
     */
    protected $email;

    /**
     * @Assert\NotBlank(message = "require.password", groups={"registration","password"})
     * @Assert\Length(
     *      min = 8,
     *      max = 20,
     *      minMessage = "min.length.password",
     *      maxMessage = "max.length.password",
     *      groups={"registration","password"}
     * )
     * @Assert\Regex(
     *     pattern="/^(?=^.{8,}$)(?=.*\d)(?=.*[!@#$%^&*]+)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/",
     *     message="special.chars.password",
     *     groups={"registration","password"}
     * )
     */
    protected $plainPassword;
//     *     pattern="/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d!#$%&?]*$/",

    /**
     * @ORM\OneToMany(targetEntity="EmailBundle\Entity\Email", mappedBy="user")
     */
    private $emails;

    /**
     * One User has Many ExternalLinks.
     * @ORM\OneToMany(targetEntity="ExternalLink", mappedBy="user")
     */
    private $externalLinks;

    /**
     * One User has Many Settings.
     * @ORM\OneToMany(targetEntity="UserBundle\Entity\UserSetting", mappedBy="user")
     */
    private $settings;

    /**
     * One User has Many AdminLimitations.
     * @ORM\OneToMany(targetEntity="AdminLimitation", mappedBy="user")
     */
    private $adminLimitations;

    /**
     * Many Users have One CalendarTimezone
     * @ORM\ManyToOne(targetEntity="ResourceBundle\Entity\CalendarTimezone")
     * @ORM\JoinColumn(name="calendar_timezone_id", referencedColumnName="id")
     */
    private $calendarTimezone;

    /**
     * @ORM\Column(name="`image_id`", type="string", length=22, nullable=true)
     */
    private $imageId;

    /**
     * @ORM\Column(name="`image_hash`", type="string", length=45, nullable=true)
     */
    private $imageHash;

    /**
     * @ORM\Column(name="`image_social_url`", type="string", length=255, nullable=true)
     */
    private $imageSocialUrl;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="gender", type="boolean", nullable=true, options={"default":0})
     */
    private $gender;

    /**
     * @var string|null
     *
     * @ORM\Column(name="`most_expensive_word`", type="string", length=255, nullable=true)
     */
    private $mostExpensiveWord;

    /**
     * @var float
     *
     * @ORM\Column(name="most_expensive_word_amount", type="float", nullable=true, options={"unsigned"=true,"default":0})
     */
    private $mostExpensiveWordAmount;

    /**
     * @var float
     *
     * @ORM\Column(name="total_words_amount", type="float", nullable=true, options={"unsigned"=true,"default":0})
     */
    private $totalWordsAmount;

    /**
     * @var int|null
     *
     * @ORM\Column(name="figure_type", type="smallint", nullable=true, options={"default":0})
     */
    private $figureType;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_verified", type="boolean", nullable=true, options={"default":0})
     */
    private $isVerified;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="last_verification_request", type="datetime", nullable=true)
     */
    private $lastVerificationRequest;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="status_verification_bonus", type="boolean", nullable=true)
     */
    private $statusVerificationBonus;

    /**
     * @var \DateTime
     */
    private $credentialsExpireAt;

    /**
     * @ORM\Column(name="`created_at`", type="datetime", nullable=true)
     * @var DateTime $createdAt
     */
    private $createdAt;

    /**
     * @ORM\Column(name="`edited_by`", type="string", length=22, nullable=true)
     */
    private $editedBy;

    /**
     * @ORM\Column(name="`edited_at`", type="datetime", nullable=true)
     * @var DateTime $editedAt
     */
    private $editedAt;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_disabled", type="boolean", nullable=true, options={"default":0})
     */
    private $isDisabled;

    /**
     * @ORM\Column(name="`disabled_by`", type="string", length=22, nullable=true)
     */
    private $disabledBy;

    /**
     * @ORM\Column(name="`disabled_at`", type="datetime", nullable=true)
     * @var DateTime $disabledAt
     */
    private $disabledAt;

    /**
     * @ORM\Column(name="`changed_role_by`", type="string", length=22, nullable=true)
     */
    private $changedRoleBy;

    /**
     * @ORM\Column(name="`changed_role_at`", type="datetime", nullable=true)
     * @var DateTime $changedRoleAt
     */
    private $changedRoleAt;

    /**
     * @ORM\Column(name="`locked_by`", type="string", length=22, nullable=true)
     */
    private $lockedBy;

    /**
     * @ORM\Column(name="`locked_at`", type="datetime", nullable=true)
     * @var DateTime $disabledAt
     */
    private $lockedAt;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_locked", type="boolean", nullable=true, options={"default":0})
     */
    private $isLocked;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_reported", type="boolean", nullable=true, options={"default":0})
     */
    private $isReported;

    /**
     * @var boolean
     */
    private $expired;

    /**
     * @var \DateTime
     */
    private $expiresAt;

    /**
     * @var boolean
     */
    private $credentialsExpired;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_terms_privacy_accepted", type="boolean", nullable=true, options={"default":0})
     */
    private $isTermsPrivacyAccepted;

    /**
     * @ORM\Column(name="`locale`", type="string", length=10, nullable=true, options={"default":"en_GB"})
     */
    private $locale;


    ################################################# SERIALIZER FUNCTIONS

    public function jsonSerialize()
    {
        return $this->listSerializer();
    }

    public function adminSerializer()
    {
        $view_vars = $this->viewSerializer();

        $admin_vars = array(
            'email'                   => $this->serializedEmail(),
            'locked'                  => $this->serializedLocked(),
            'isReported'              => $this->serializedIsReported(),
            'role'                    => $this->serializedRole(),
            'lastLogin'               => $this->serializedLastLogin(),
            'mostExpensiveWord'       => $this->serializedMostExpensiveWord(),
            'mostExpensiveWordAmount' => $this->serializedMostExpensiveWordAmount(),
            'lastVerificationRequest' => $this->serializedLastVerificationRequest(),
            'statusVerificationBonus' => $this->serializedStatusVerificationBonus(),
            'totalWordsAmount'        => $this->serializedTotalWordsAmount(),
        );

        return array_merge($view_vars, $admin_vars);
    }

    public function viewSerializer()
    {
        $list_vars = $this->viewLimitationsSerializer();

        $view_vars = array(
            'gender'                => $this->serializedGender(),
            'imageId'               => $this->serializedImageId(),
            'imageHash'             => $this->serializedImageHash(),
            'credential_expiration' => $this->serializedCredentialExpiration(),
            'imageSocialUrl'        => $this->serializedImageSocialUrl(),
            'externalLinks'         => $this->serializedExternalLinks(),
            'settings'              => $this->serializedSettings(),
        );

        return array_merge($list_vars, $view_vars);
    }


    public function viewLimitationsSerializer()
    {
        $list_vars = $this->listSerializer();

        $view_vars = array(
            'adminLimitations'      => $this->serializedAdminLimitations(),
            'isAdmin'               => $this->serializedIsAdmin(),
            'isAdminLimited'        => $this->serializedIsAdminLimited(),
        );

        return array_merge($list_vars, $view_vars);
    }


    public function viewProfileSerializer () {

        $list_vars = $this->listSerializer();

        $view_vars = array(
            'gender'                => $this->serializedGender(),
            'imageId'               => $this->serializedImageId(),
            'imageHash'             => $this->serializedImageHash(),
            'imageSocialUrl'        => $this->serializedImageSocialUrl(),
            'externalLinks'         => $this->serializedExternalLinks(),
            'settingIds'            => $this->serializedSettingIds(),
            'timezoneId'            => $this->serializedTimezoneId(),
            'isFromSocial'          => $this->serializedIsFromSocial(),
            'language'              => $this->serializedLanguage(),
            'countryCode'           => $this->serializedCountryCode(),
        );

        return array_merge($list_vars, $view_vars);
    }


    public function listSerializer()
    {
        $list_vars = array(
            'id'                    => $this->serializedId(),
            'username'              => $this->serializedUsername(),
            'enabled'               => $this->serializedEnabled(),
            'isVerified'            => $this->serializedIsVerified(),
            'figureSource'          => $this->serializedFigureSource(),
        );
        return $list_vars;
    }

    ################################################# SERIALIZED FUNCTIONS

    /**
     * if user is locked or not
     * @JMS\VirtualProperty
     * @JMS\SerializedName("locked")
     * @JMS\Type("boolean")
     * @JMS\Groups({"admin"})
     * @JMS\Since("1.0.x")
     */
    public function serializedLocked()
    {
        return $this->isLocked();
    }

    /**
     * user credential expiration
     * @JMS\VirtualProperty
     * @JMS\SerializedName("credential_expiration")
     * @JMS\Type("string")
     * @JMS\Groups({"admin","view"})
     * @JMS\Since("1.0.x")
     */
    public function serializedCredentialExpiration()
    {
        return (is_null($this->credentialsExpireAt)?null:strftime('%Y-%m-%d %H:%M',$this->credentialsExpireAt->getTimestamp()));
    }

    /**
     * user id
     * @JMS\VirtualProperty
     * @JMS\SerializedName("id")
     * @JMS\Type("integer")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedId()
    {
        return (is_null($this->id)?null:$this->id);
    }

    /**
     * user email
     * @JMS\VirtualProperty
     * @JMS\SerializedName("email")
     * @JMS\Type("string")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedEmail()
    {
        return (is_null($this->email)?null:$this->email);
    }

    /**
     * user username
     * @JMS\VirtualProperty
     * @JMS\SerializedName("username")
     * @JMS\Type("string")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedUsername()
    {
        return (is_null($this->username)?null:$this->username);
    }


    /**
     * if user is an admin
     * @JMS\VirtualProperty
     * @JMS\SerializedName("admin")
     * @JMS\Type("boolean")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedIsAdmin()
    {
        return $this->isAdmin();
    }


    /**
     * if user is an admin limited
     * @JMS\VirtualProperty
     * @JMS\SerializedName("isAdminLimited")
     * @JMS\Type("boolean")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedIsAdminLimited()
    {
        return $this->isAdminLimited();
    }

    /**
     * if user credentials are expired or not
     * @JMS\VirtualProperty
     * @JMS\SerializedName("credential_expired")
     * @JMS\Type("boolean")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedCredentialExpired()
    {
        return $this->isCredentialsExpired();
    }

    /**
     * if user is enabled or not
     * @JMS\VirtualProperty
     * @JMS\SerializedName("credential_expired")
     * @JMS\Type("boolean")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedEnabled()
    {
        return $this->isEnabled();
    }

    /**
     * user gender
     * @JMS\VirtualProperty
     * @JMS\SerializedName("gender")
     * @JMS\Type("string")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedGender()
    {
        return (is_null($this->gender) || !$this->gender ? Gender::MALE_SOURCE : Gender::FEMALE_SOURCE);
    }


    /**
     * user role
     * @JMS\VirtualProperty
     * @JMS\SerializedName("role")
     * @JMS\Type("string")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedRole() {

        $roleTxt = "";

        if(!is_null($this->roles)) {
            foreach ($this->roles as $role) {
                $roleTxt .= $role." ";
            }
        }

        if(empty($roleTxt)) {
            $roleTxt = "ROLE_USER";
        }

        return $roleTxt;
    }

    /**
     * user lastLogin
     * @JMS\VirtualProperty
     * @JMS\SerializedName("lastLogin")
     * @JMS\Type("string")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedLastLogin() {
        return (!is_null($this->lastLogin) && $this->lastLogin instanceof DateTime? $this->lastLogin->format("Y-m-d H:i:s") : null);
    }

    /**
     * isReported
     * @JMS\VirtualProperty
     * @JMS\SerializedName("isReported")
     * @JMS\Type("boolean")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedIsReported()
    {
        return (is_null($this->isReported) ? false : $this->isReported);
    }

    /**
     * user imageId
     * @JMS\VirtualProperty
     * @JMS\SerializedName("imageId")
     * @JMS\Type("string")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedImageId()
    {
        return (is_null($this->imageId)?null:$this->imageId);
    }

    /**
     * user imageHash
     * @JMS\VirtualProperty
     * @JMS\SerializedName("imageHash")
     * @JMS\Type("string")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedImageHash()
    {
        return (is_null($this->imageHash)?null:$this->imageHash);
    }


    /**
     * user imageSocialUrl
     * @JMS\VirtualProperty
     * @JMS\SerializedName("imageSocialUrl")
     * @JMS\Type("string")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedImageSocialUrl()
    {
        return (is_null($this->imageSocialUrl)?null:$this->imageSocialUrl);
    }

    /**
     * user mostExpensiveWord
     * @JMS\VirtualProperty
     * @JMS\SerializedName("mostExpensiveWord")
     * @JMS\Type("string")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedMostExpensiveWord()
    {
        return (is_null($this->mostExpensiveWord)?null:$this->mostExpensiveWord);
    }

    /**
     * user mostExpensiveWordAmount
     * @JMS\VirtualProperty
     * @JMS\SerializedName("mostExpensiveWordAmount")
     * @JMS\Type("float")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedMostExpensiveWordAmount()
    {
        return (is_null($this->mostExpensiveWordAmount)?null:$this->mostExpensiveWordAmount);
    }

    /**
     * user totalWordsAmount
     * @JMS\VirtualProperty
     * @JMS\SerializedName("totalWordsAmount")
     * @JMS\Type("float")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedTotalWordsAmount()
    {
        return (is_null($this->totalWordsAmount)?null:$this->totalWordsAmount);
    }

    /**
     * user figureType
     * @JMS\VirtualProperty
     * @JMS\SerializedName("figureType")
     * @JMS\Type("integer")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedFigureType()
    {
        return (is_null($this->figureType)?null:$this->figureType);
    }

    /**
     * user figureSource
     * @JMS\VirtualProperty
     * @JMS\SerializedName("figureSource")
     * @JMS\Type("integer")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedFigureSource() {
        $gender = $this->getGender();

        $source = Figure::NOTHING_SOURCE;

        if ($gender == Gender::FEMALE) {
            if ($this->getFigureType() == Figure::KING) {
                $source = Figure::QUEEN_SOURCE;
            } elseif ($this->getFigureType() == Figure::PRINCE) {
                $source = Figure::PRINCESS_SOURCE;
            }
        } else {
            if ($this->getFigureType() == Figure::KING) {
                $source = Figure::KING_SOURCE;
            } elseif ($this->getFigureType() == Figure::PRINCE) {
                $source = Figure::PRINCE_SOURCE;
            }
        }

        if ($this->getFigureType() == Figure::JACK) {
            $source = Figure::JACK_SOURCE;
        } elseif ($this->getFigureType() == Figure::KNIGHT) {
            $source = Figure::KNIGHT_SOURCE;
        } elseif ($this->getFigureType() == Figure::GOD) {
            $source = Figure::GOD_SOURCE;
        }


        return $source;
    }

    /**
     * user isVerified
     * @JMS\VirtualProperty
     * @JMS\SerializedName("isVerified")
     * @JMS\Type("boolean")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedIsVerified()
    {
        return (is_null($this->isVerified)?false:$this->isVerified);
    }

    /**
     * user language
     * @JMS\VirtualProperty
     * @JMS\SerializedName("language")
     * @JMS\Type("string")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedLanguage()
    {
        return (is_null($this->locale)?null:$this->locale);
    }

    /**
     * user lastVerificationRequest
     * @JMS\VirtualProperty
     * @JMS\SerializedName("lastVerificationRequest")
     * @JMS\Type("string")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedLastVerificationRequest()
    {
        return (is_null($this->lastVerificationRequest) || !$this->lastVerificationRequest instanceof DateTime ? null : $this->lastVerificationRequest->format("Y-m-d H:i:s"));
    }

    /**
     * user statusVerificationBonus
     * @JMS\VirtualProperty
     * @JMS\SerializedName("statusVerificationBonus")
     * @JMS\Type("string")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedStatusVerificationBonus()
    {
        return (is_null($this->statusVerificationBonus)?null:$this->statusVerificationBonus);
    }

    /**
     * user timezoneId
     * @JMS\VirtualProperty
     * @JMS\SerializedName("timezoneId")
     * @JMS\Type("string")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedTimezoneId()
    {
        return (!is_null($this->getCalendarTimezone()) && $this->getCalendarTimezone() instanceof CalendarTimezone ? $this->getCalendarTimezone()->getId() : null);
    }

    /**
     * user countryCode
     * @JMS\VirtualProperty
     * @JMS\SerializedName("countryCode")
     * @JMS\Type("string")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedCountryCode()
    {
        return (!is_null($this->getCalendarTimezone()) && $this->getCalendarTimezone() instanceof CalendarTimezone ? $this->getCalendarTimezone()->getCountryCode() : null);
    }

    /**
     * user adminLimitations
     * @JMS\VirtualProperty
     * @JMS\SerializedName("adminLimitations")
     * @JMS\Type("array")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedAdminLimitations() {

        $results = array();

        if (!$this->getAdminLimitations()->isEmpty()) {
            /** @var AdminLimitation $adminLimitation */
            foreach ($this->getAdminLimitations() as $adminLimitation) {
                $results[] = $adminLimitation->listSerializer();
            }
        }

        return $results;

    }

    /**
     * user externalLinks
     * @JMS\VirtualProperty
     * @JMS\SerializedName("externalLinks")
     * @JMS\Type("array")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedExternalLinks() {

        $results  = array();

        if (!$this->getExternalLinks()->isEmpty()) {
            /** @var ExternalLink $externalLink */
            foreach ($this->getExternalLinks() as $externalLink) {
                $extLinks = $externalLink->listSerializer();
                if (isset($extLinks["source"]) && !empty($extLinks["source"])) {
                    $results[$extLinks["source"]] = $extLinks["value"];
                }
            }
        }

        return $results;

    }

    /**
     * user settings
     * @JMS\VirtualProperty
     * @JMS\SerializedName("settings")
     * @JMS\Type("array")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedSettings() {

        $results  = array();

        if (!$this->getUserSettings()->isEmpty()) {
            /** @var UserSetting $userSetting */
            foreach ($this->getUserSettings() as $userSetting) {
                $setting = $userSetting->settingSerializer();
                if (isset($setting["category"]) && !empty($setting["category"])) {
                    $results[$setting["category"]] = $setting;
                }
            }
        }

        return $results;

    }

    /**
     * user settings
     * @JMS\VirtualProperty
     * @JMS\SerializedName("settings")
     * @JMS\Type("array")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedSettingIds() {

        $results  = array();

        if (!$this->getUserSettings()->isEmpty()) {
            /** @var UserSetting $userSetting */
            foreach ($this->getUserSettings() as $userSetting) {
                if (is_null($userSetting->getIsDisabled()) || !$userSetting->getIsDisabled()) {
                    $setting = $userSetting->settingSerializer();
                    if (isset($setting["settingId"]) && !empty($setting["settingId"])) {
                        $results[] = $setting["settingId"];
                    }
                }
            }
        }

        return $results;

    }

    /**
     * user isFromSocial
     * @JMS\VirtualProperty
     * @JMS\SerializedName("isFromSocial")
     * @JMS\Type("array")
     * @JMS\Groups({"admin","view","list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedIsFromSocial() {

        if (!is_null($this->facebook_access_token) || !is_null($this->googleplus_access_token)) {
            return true;
        }

        return false;
    }



    ################################################# UTILS FUNCTIONS

    /**
     * @return bool
     */
    public function isAdmin() {
        return ($this->hasRole("ROLE_ADMIN")?true:false);
    }

    /**
     * @return bool
     */
    public function isAdminLimited() {
        if ($this->isAdmin())  {
            if (!$this->getAdminLimitations()->isEmpty()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param \DateInterval $interval
     *
     * @return int
     */
    public static function getSeconds(\DateInterval $interval)
    {
        $datetime = new \DateTime('@0');
        return $datetime->add($interval)->getTimestamp();
    }

    public function isAccountNonExpired()
    {
        if (true === $this->expired) {
            return false;
        }
        if (null !== $this->expiresAt && $this->getSeconds($this->expiresAt->diff(new \DateTime())) <= 0) {
            return false;
        }
        return true;
    }

    public function isAccountNonLocked()
    {
        return !$this->isLocked;
    }

    public function isCredentialsNonExpired()
    {
        if (true === $this->credentialsExpired) {
            return false;
        }
        if (null !== $this->credentialsExpireAt && $this->credentialsExpireAt->getTimestamp() < time()) {
            return false;
        }
        return true;
    }

    public function isCredentialsExpired()
    {
        return !$this->isCredentialsNonExpired();
    }

    public function isEnabled()
    {
        return $this->enabled;
    }

    public function isExpired()
    {
        return !$this->isAccountNonExpired();
    }

    public function isLocked()
    {
        return !$this->isAccountNonLocked();
    }

    /**
     * @param int $area
     * @param array $privileges
     * @return bool
     */
    public function isGrantedLimitation (int $area, array $privileges) {

        if ($this->isSuperAdmin()) {
            return true;
        }

        if (!$this->adminLimitations->isEmpty()) {

//            var_dump($this->adminLimitations->count());die("ciao");

            /** @var AdminLimitation $adminLimitation */
            foreach ($this->adminLimitations as $adminLimitation) {
                if ($adminLimitation->getArea() == $area && true !== $adminLimitation->getIsDisabled()) {
                    $result = array_intersect($adminLimitation->getPrivileges(), $privileges);
                    return count($result) > 0;
                }
            }

        }

        return false;
    }

    /**
     * @param $genderName
     * @return int
     */
    public function getGenderBySocial($genderName) {
        if ($genderName == Gender::FEMALE_SOURCE) {
            return Gender::FEMALE;
        }

        return Gender::MALE;
    }


    ################################################# GETTERS AND SETTERS FUNCTIONS

    public function __construct() {
        parent::__construct();
        $this->enabled            = false;
        $this->isLocked           = false;
        $this->expired            = false;
        $this->credentialsExpired = false;
        $this->emails             = new ArrayCollection();
        $this->externalLinks      = new ArrayCollection();
        $this->adminLimitations   = new ArrayCollection();
        $this->settings           = new ArrayCollection();
    }

    /**
     * Add email.
     *
     * @param Email $email
     *
     * @return User
     */
    public function addEmail(Email $email)
    {
        $this->emails[] = $email;

        return $this;
    }

    /**
     * Remove email.
     *
     * @param Email $email
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeEmail(Email $email)
    {
        return $this->emails->removeElement($email);
    }

    /**
     * Get emails.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmails()
    {
        return $this->emails;
    }


    /**
     * Add externalLink.
     *
     * @param ExternalLink $externalLink
     *
     * @return $this
     */
    public function addExternalLink(ExternalLink $externalLink)
    {
        $this->externalLinks[] = $externalLink;

        return $this;
    }

    /**
     * Remove externalLink.
     *
     * @param ExternalLink $externalLink
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeExternalLink(ExternalLink $externalLink)
    {
        return $this->externalLinks->removeElement($externalLink);
    }

    /**
     * Get externalLink.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExternalLinks()
    {
        return $this->externalLinks;
    }

    /**
     * Add adminLimitations.
     *
     * @param AdminLimitation $adminLimitations
     *
     * @return $this
     */
    public function addAdminLimitation(AdminLimitation $adminLimitations)
    {
        $this->adminLimitations[] = $adminLimitations;

        return $this;
    }

    /**
     * Remove adminLimitations.
     *
     * @param AdminLimitation $adminLimitations
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeAdminLimitation(AdminLimitation $adminLimitations)
    {
        return $this->adminLimitations->removeElement($adminLimitations);
    }

    /**
     * Get adminLimitations.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdminLimitations()
    {
        return $this->adminLimitations;
    }


    /**
     * @param $boolean
     * @return $this
     */
    public function setLocked($boolean)
    {
        $this->isLocked = $boolean;
        return $this;
    }

    /**
     * @param \DateTime $date
     *
     * @return User
     */
    public function setCredentialsExpireAt(\DateTime $date)
    {
        $this->credentialsExpireAt = $date;
        return $this;
    }
    /**
     * @param boolean $boolean
     *
     * @return User
     */
    public function setCredentialsExpired($boolean)
    {
        $this->credentialsExpired = $boolean;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getEditedBy()
    {
        return $this->editedBy;
    }

    /**
     * @param mixed $editedBy
     */
    public function setEditedBy($editedBy)
    {
        $this->editedBy = $editedBy;
    }

    /**
     * @return DateTime
     */
    public function getEditedAt()
    {
        return $this->editedAt;
    }

    /**
     * @param DateTime $editedAt
     */
    public function setEditedAt($editedAt)
    {
        $this->editedAt = $editedAt;
    }

    /**
     * @return mixed
     */
    public function getDisabledBy()
    {
        return $this->disabledBy;
    }

    /**
     * @param mixed $disabledBy
     */
    public function setDisabledBy($disabledBy)
    {
        $this->disabledBy = $disabledBy;
    }

    /**
     * @return DateTime
     */
    public function getDisabledAt()
    {
        return $this->disabledAt;
    }

    /**
     * @param DateTime $disabledAt
     */
    public function setDisabledAt($disabledAt)
    {
        $this->disabledAt = $disabledAt;
    }

    /**
     * @return CalendarTimezone
     */
    public function getCalendarTimezone()
    {
        return $this->calendarTimezone;
    }

    /**
     * @param CalendarTimezone $calendarTimezone
     */
    public function setCalendarTimezone(CalendarTimezone $calendarTimezone): void
    {
        $this->calendarTimezone = $calendarTimezone;
    }

    /**
     * @return mixed
     */
    public function getImageId()
    {
        return $this->imageId;
    }

    /**
     * @param mixed $imageId
     */
    public function setImageId($imageId): void
    {
        $this->imageId = $imageId;
    }

    /**
     * @return mixed
     */
    public function getImageHash()
    {
        return $this->imageHash;
    }

    /**
     * @param mixed $imageHash
     */
    public function setImageHash($imageHash): void
    {
        $this->imageHash = $imageHash;
    }

    /**
     * @return bool|null
     */
    public function getGender(): ?bool
    {
        return $this->gender;
    }

    /**
     * @param bool|null $gender
     */
    public function setGender(?bool $gender): void
    {
        $this->gender = $gender;
    }

    /**
     * @return null|string
     */
    public function getMostExpensiveWord(): ?string
    {
        return $this->mostExpensiveWord;
    }

    /**
     * @param null|string $mostExpensiveWord
     */
    public function setMostExpensiveWord(?string $mostExpensiveWord): void
    {
        $this->mostExpensiveWord = $mostExpensiveWord;
    }


    /**
     * @return float
     */
    public function getMostExpensiveWordAmount(): float
    {
        return $this->mostExpensiveWordAmount;
    }

    /**
     * @param float $mostExpensiveWordAmount
     */
    public function setMostExpensiveWordAmount(float $mostExpensiveWordAmount): void
    {
        $this->mostExpensiveWordAmount = $mostExpensiveWordAmount;
    }

    /**
     * @return float
     */
    public function getTotalWordsAmount(): float
    {
        return $this->totalWordsAmount;
    }

    /**
     * @param float $totalWordsAmount
     */
    public function setTotalWordsAmount(float $totalWordsAmount): void
    {
        $this->totalWordsAmount = $totalWordsAmount;
    }

    /**
     * @return int|null
     */
    public function getFigureType(): ?int
    {
        return $this->figureType;
    }

    /**
     * @param int|null $figureType
     */
    public function setFigureType(?int $figureType): void
    {
        $this->figureType = $figureType;
    }

    /**
     * @return bool|null
     */
    public function getIsVerified(): ?bool
    {
        return $this->isVerified;
    }

    /**
     * @param bool|null $isVerified
     */
    public function setIsVerified(?bool $isVerified): void
    {
        $this->isVerified = $isVerified;
    }

    /**
     * @return bool|null
     */
    public function getStatusVerificationBonus(): ?bool
    {
        return $this->statusVerificationBonus;
    }

    /**
     * @param bool|null $statusVerificationBonus
     */
    public function setStatusVerificationBonus(?bool $statusVerificationBonus): void
    {
        $this->statusVerificationBonus = $statusVerificationBonus;
    }

    /**
     * @return DateTime|null
     */
    public function getLastVerificationRequest(): ?DateTime
    {
        return $this->lastVerificationRequest;
    }

    /**
     * @param DateTime|null $lastVerificationRequest
     */
    public function setLastVerificationRequest(?DateTime $lastVerificationRequest): void
    {
        $this->lastVerificationRequest = $lastVerificationRequest;
    }

    /**
     * @return bool|null
     */
    public function getIsReported(): ?bool
    {
        return $this->isReported;
    }

    /**
     * @param bool|null $isReported
     */
    public function setIsReported(?bool $isReported): void
    {
        $this->isReported = $isReported;
    }

    /**
     * @return mixed
     */
    public function getLockedBy()
    {
        return $this->lockedBy;
    }

    /**
     * @param mixed $lockedBy
     */
    public function setLockedBy($lockedBy): void
    {
        $this->lockedBy = $lockedBy;
    }

    /**
     * @return DateTime
     */
    public function getLockedAt(): DateTime
    {
        return $this->lockedAt;
    }

    /**
     * @param DateTime $lockedAt
     */
    public function setLockedAt(DateTime $lockedAt): void
    {
        $this->lockedAt = $lockedAt;
    }

    /**
     * @return mixed
     */
    public function getChangedRoleBy()
    {
        return $this->changedRoleBy;
    }

    /**
     * @param mixed $changedRoleBy
     */
    public function setChangedRoleBy($changedRoleBy): void
    {
        $this->changedRoleBy = $changedRoleBy;
    }

    /**
     * @return DateTime
     */
    public function getChangedRoleAt(): DateTime
    {
        return $this->changedRoleAt;
    }

    /**
     * @param DateTime $changedRoleAt
     */
    public function setChangedRoleAt(DateTime $changedRoleAt): void
    {
        $this->changedRoleAt = $changedRoleAt;
    }

    /**
     * @return mixed
     */
    public function getFacebookId()
    {
        return $this->facebook_id;
    }

    /**
     * @param mixed $facebook_id
     */
    public function setFacebookId($facebook_id): void
    {
        $this->facebook_id = $facebook_id;
    }

    /**
     * @return mixed
     */
    public function getFacebookAccessToken()
    {
        return $this->facebook_access_token;
    }

    /**
     * @param mixed $facebook_access_token
     */
    public function setFacebookAccessToken($facebook_access_token): void
    {
        $this->facebook_access_token = $facebook_access_token;
    }

    /**
     * @return mixed
     */
    public function getGoogleplusId()
    {
        return $this->googleplus_id;
    }

    /**
     * @param mixed $googleplus_id
     */
    public function setGoogleplusId($googleplus_id): void
    {
        $this->googleplus_id = $googleplus_id;
    }

    /**
     * @return mixed
     */
    public function getGoogleplusAccessToken()
    {
        return $this->googleplus_access_token;
    }

    /**
     * @param mixed $googleplus_access_token
     */
    public function setGoogleplusAccessToken($googleplus_access_token): void
    {
        $this->googleplus_access_token = $googleplus_access_token;
    }

    /**
     * @return mixed
     */
    public function getImageSocialUrl()
    {
        return $this->imageSocialUrl;
    }

    /**
     * @param mixed $imageSocialUrl
     */
    public function setImageSocialUrl($imageSocialUrl): void
    {
        $this->imageSocialUrl = $imageSocialUrl;
    }

    /**
     * @return bool|null
     */
    public function getIsTermsPrivacyAccepted(): ?bool
    {
        return (is_null($this->isTermsPrivacyAccepted) ? false : $this->isTermsPrivacyAccepted);
    }

    /**
     * @param bool|null $isTermsPrivacyAccepted
     */
    public function setIsTermsPrivacyAccepted(?bool $isTermsPrivacyAccepted): void
    {
        $this->isTermsPrivacyAccepted = $isTermsPrivacyAccepted;
    }

    /**
     * @return mixed
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param mixed $locale
     */
    public function setLocale($locale): void
    {
        $this->locale = $locale;
    }

    /**
     * @return bool|null
     */
    public function getIsDisabled(): ?bool
    {
        return (is_null($this->isDisabled) ? false : $this->isDisabled);
    }

    /**
     * @param bool|null $isDisabled
     */
    public function setIsDisabled(?bool $isDisabled): void
    {
        $this->isDisabled = $isDisabled;
    }



    /**
     * Add userSetting.
     *
     * @param UserSetting $userSetting
     *
     * @return $this
     */
    public function addSetting(UserSetting $userSetting)
    {
        $this->settings[] = $userSetting;

        return $this;
    }

    /**
     * Remove userSetting.
     *
     * @param UserSetting $userSetting
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeSetting(UserSetting $userSetting)
    {
        return $this->settings->removeElement($userSetting);
    }

    /**
     * Get userSetting.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserSettings()
    {
        return $this->settings;
    }


}
