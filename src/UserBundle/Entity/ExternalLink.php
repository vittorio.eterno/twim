<?php

namespace UserBundle\Entity;

use Schema\Entity;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * ExternalLink
 *
 * @ORM\Table(name="external_links")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\ExternalLinkRepository")
 */
class ExternalLink extends Entity {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Many ExternalLinks have One User.
     * @ORM\ManyToOne(targetEntity="User", inversedBy="externalLinks")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * Many ExternalLinks have One ExternalLinkStructure.
     * @ORM\ManyToOne(targetEntity="ExternalLinkStructure", inversedBy="externalLinks")
     * @ORM\JoinColumn(name="external_link_structure_id", referencedColumnName="id")
     */
    private $externalLinkStructure;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255)
     */
    private $value;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="edited_at", type="datetime", nullable=true)
     */
    private $editedAt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="edited_by", type="string", length=22, nullable=true)
     */
    private $editedBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="disabled_at", type="datetime", nullable=true)
     */
    private $disabledAt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="disabled_by", type="string", length=22, nullable=true)
     */
    private $disabledBy;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_disabled", type="boolean", nullable=true, options={"default":0})
     */
    private $isDisabled;


    ################################################# SERIALIZER FUNCTIONS

    /**
     * Returns the array of fields to serialize in entity administration view.
     * @return array
     */
    public function adminSerializer() {
        $view_vars = $this->viewSerializer();

        $admin_vars = array(
            'createdAt'     => $this->serializedCreatedAt()
        );

        return array_merge($view_vars, $admin_vars);
    }

    /**
     * Returns the array of fields to serialize in entity view.
     * @return array
     */
    public function viewSerializer() {
        $list_vars = $this->listSerializer();

        $view_vars = array(
            'id'            => $this->serializedId()
        );

        return array_merge($list_vars, $view_vars);
    }

    /**
     * Returns the array of fields to serialize in a list of this entity.
     * @return array
     */
    public function listSerializer() {
        $list_vars = array(
            'value'         => $this->serializedValue(),
            'source'        => $this->serializedStructureSource()
        );
        return $list_vars;
    }

//    /**
//     * @return null
//     */
//    public function getProfileSerializer () {
//
//        $externalLink = null;
//        $source = $this->serializedStructureSource();
//
//        if (!is_null($source)) {
//            $externalLink[$source] = array(
//                'value' => $this->serializedValue(),
//            );
//        }
//
//        return $externalLink;
//    }


    /**
     * Returns the hash code unique identifier of the entity.
     * @return string
     */
    public function hashCode() {
        // TODO: Implement hashCode() method.
    }

    ################################################# SERIALIZED FUNCTIONS

    /**
     * id
     * @JMS\VirtualProperty
     * @JMS\SerializedName("id")
     * @JMS\Type("integer")
     * @JMS\Groups({"view"})
     * @JMS\Since("1.0.x")
     */
    public function serializedId() {
        return (is_null($this->id)?null:$this->id);
    }

    /**
     * value
     * @JMS\VirtualProperty
     * @JMS\SerializedName("value")
     * @JMS\Type("string")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedValue() {
        return (is_null($this->value)?null:$this->value);
    }

    /**
     * createdAt
     * @JMS\VirtualProperty
     * @JMS\SerializedName("createdAt")
     * @JMS\Type("string")
     * @JMS\Groups({"admin"})
     * @JMS\Since("1.0.x")
     */
    public function serializedCreatedAt() {
        return (!is_null($this->createdAt) && $this->createdAt instanceof \DateTime ? $this->createdAt->format("Y-m-d H:i:s") : null);
    }


    /**
     * value
     * @JMS\VirtualProperty
     * @JMS\SerializedName("value")
     * @JMS\Type("string")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedStructureSource() {

        /** @var ExternalLinkStructure $externalLinkStructure */
        $externalLinkStructure = $this->getExternalLinkStructure();
        return (is_null($externalLinkStructure)?null:$externalLinkStructure->getSource());
    }

    ################################################# GETTERS AND SETTERS FUNCTIONS



    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return ExternalLinkStructure
     */
    public function getExternalLinkStructure()
    {
        return $this->externalLinkStructure;
    }

    /**
     * @param ExternalLinkStructure $externalLinkStructure
     */
    public function setExternalLinkStructure(ExternalLinkStructure $externalLinkStructure): void
    {
        $this->externalLinkStructure = $externalLinkStructure;
    }

    /**
     * Set value.
     *
     * @param string $value
     *
     * @return ExternalLink
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value.
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return ExternalLink
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set editedAt.
     *
     * @param \DateTime|null $editedAt
     *
     * @return ExternalLink
     */
    public function setEditedAt($editedAt = null)
    {
        $this->editedAt = $editedAt;

        return $this;
    }

    /**
     * Get editedAt.
     *
     * @return \DateTime|null
     */
    public function getEditedAt()
    {
        return $this->editedAt;
    }

    /**
     * Set editedBy.
     *
     * @param string|null $editedBy
     *
     * @return ExternalLink
     */
    public function setEditedBy($editedBy = null)
    {
        $this->editedBy = $editedBy;

        return $this;
    }

    /**
     * Get editedBy.
     *
     * @return string|null
     */
    public function getEditedBy()
    {
        return $this->editedBy;
    }

    /**
     * Set disabledAt.
     *
     * @param \DateTime|null $disabledAt
     *
     * @return ExternalLink
     */
    public function setDisabledAt($disabledAt = null)
    {
        $this->disabledAt = $disabledAt;

        return $this;
    }

    /**
     * Get disabledAt.
     *
     * @return \DateTime|null
     */
    public function getDisabledAt()
    {
        return $this->disabledAt;
    }

    /**
     * Set disabledBy.
     *
     * @param string|null $disabledBy
     *
     * @return ExternalLink
     */
    public function setDisabledBy($disabledBy = null)
    {
        $this->disabledBy = $disabledBy;

        return $this;
    }

    /**
     * Get disabledBy.
     *
     * @return string|null
     */
    public function getDisabledBy()
    {
        return $this->disabledBy;
    }

    /**
     * Set isDisabled.
     *
     * @param bool $isDisabled
     *
     * @return ExternalLink
     */
    public function setIsDisabled($isDisabled)
    {
        $this->isDisabled = $isDisabled;

        return $this;
    }

    /**
     * Get isDisabled.
     *
     * @return bool
     */
    public function getIsDisabled()
    {
        return $this->isDisabled;
    }
}
