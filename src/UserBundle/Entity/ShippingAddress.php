<?php

namespace UserBundle\Entity;

use ResourceBundle\Entity\Country;
use Schema\Entity;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Utils\Service\Encryption;

/**
 * ShippingAddress
 *
 * @ORM\Table(name="shipping_addresses")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\ShippingAddressRepository")
 */
class ShippingAddress extends Entity {

    const LIMIT_USER_ADDRESSES = 10;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Many Shipping addresses have one User. This is the owning side.
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="ResourceBundle\Entity\Country")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=255)
     */
    private $hash;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255)
     */
    private $lastName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="company_name", type="string", length=255, nullable=true)
     */
    private $companyName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="company_address_info", type="string", length=255, nullable=true)
     */
    private $companyAddressInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="street_address", type="string", length=255)
     */
    private $streetAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="address_detail", type="string", length=255)
     */
    private $addressDetail;

    /**
     * @var string
     *
     * @ORM\Column(name="state_province", type="string", length=255)
     */
    private $stateProvince;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="postal_code", type="string", length=255)
     */
    private $postalCode;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_used_at", type="datetime")
     */
    private $lastUsedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="disabled_at", type="datetime", nullable=true)
     */
    private $disabledAt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="disabled_by", type="string", length=22, nullable=true)
     */
    private $disabledBy;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_disabled", type="boolean", nullable=true)
     */
    private $isDisabled;


    ################################################# SERIALIZER FUNCTIONS

    /**
     * Returns the array of fields to serialize in entity administration view.
     * @return array
     */
    public function adminSerializer() {
        $view_vars = $this->viewSerializer();

        $admin_vars = array();

        return array_merge($view_vars, $admin_vars);
    }

    /**
     * Returns the array of fields to serialize in entity view.
     * @return array
     */
    public function viewSerializer() {
        $list_vars = $this->listSerializer();

        $view_vars = array(
        );

        return array_merge($list_vars, $view_vars);
    }

    /**
     * Returns the array of fields to serialize in a list of this entity.
     * @return array
     */
    public function listSerializer() {
        $list_vars = array(
            'id'           => $this->serializedId()
        );
        return $list_vars;
    }

    /**
     * @param Encryption $encryption
     * @return array
     */
    public function listDecryptedSerializer(Encryption &$encryption) {

        $firstName                  = !is_null($this->firstName)            ? $encryption->decrypting($this->firstName)             : null;
        $lastName                   = !is_null($this->lastName)             ? $encryption->decrypting($this->lastName)              : null;
        $companyName                = !is_null($this->companyName)          ? $encryption->decrypting($this->companyName)           : null;
        $companyAddressInfo         = !is_null($this->companyAddressInfo)   ? $encryption->decrypting($this->companyAddressInfo)    : null;
        $streetAddress              = !is_null($this->streetAddress)        ? $encryption->decrypting($this->streetAddress)         : null;
        $addressDetail              = !is_null($this->addressDetail)        ? $encryption->decrypting($this->addressDetail)         : null;
        $city                       = !is_null($this->city)                 ? $encryption->decrypting($this->city)                  : null;
        $stateProvince              = !is_null($this->stateProvince)        ? $encryption->decrypting($this->stateProvince)         : null;
        $postalCode                 = !is_null($this->postalCode)           ? $encryption->decrypting($this->postalCode)            : null;
        $email                      = !is_null($this->email)                ? $encryption->decrypting($this->email)                 : null;
        $phone                      = !is_null($this->phone)                ? $encryption->decrypting($this->phone)                 : null;


        $list_vars = array(
            'id'                    => $this->serializedId(),
            'firstName'             => $firstName,
            'lastName'              => $lastName,
            'companyName'           => $companyName,
            'companyAddressInfo'    => $companyAddressInfo,
            'streetAddress'         => $streetAddress,
            'addressDetail'         => $addressDetail,
            'city'                  => $city,
            'stateProvince'         => $stateProvince,
            'postalCode'            => $postalCode,
            'email'                 => $email,
            'phone'                 => $phone,
            'country'               => $this->serializedCountry(),
            'lastUsedAt'            => $this->serializedLastUsedAt(),
        );
        return $list_vars;
    }

    /**
     * Returns the hash code unique identifier of the entity.
     * @return string
     */
    public function hashCode() {
        // TODO: Implement hashCode() method.
    }

    ################################################# SERIALIZED FUNCTIONS

    /**
     * id
     * @JMS\VirtualProperty
     * @JMS\SerializedName("id")
     * @JMS\Type("integer")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedId() {
        return (is_null($this->id)?null:$this->id);
    }

    /**
     * country
     * @JMS\VirtualProperty
     * @JMS\SerializedName("country")
     * @JMS\Type("array")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedCountry() {
        return (!is_null($this->getCountry()) && $this->getCountry() instanceof Country ? $this->getCountry()->listSerializer() : null);
    }

    /**
     * lastUsedAt
     * @JMS\VirtualProperty
     * @JMS\SerializedName("lastUsedAt")
     * @JMS\Type("array")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedLastUsedAt() {
        return (!is_null($this->getLastUsedAt()) && $this->getLastUsedAt() instanceof \DateTime ? $this->getLastUsedAt()->format('Y-m-d H:i:s') : null);
    }


    /**
     * firstName
     * @JMS\VirtualProperty
     * @JMS\SerializedName("firstName")
     * @JMS\Type("string")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedFirstName() {
        return (is_null($this->firstName)?null:$this->firstName);
    }


    /**
     * lastName
     * @JMS\VirtualProperty
     * @JMS\SerializedName("lastName")
     * @JMS\Type("string")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedLastName() {
        return (is_null($this->lastName)?null:$this->lastName);
    }


    ################################################# GETTERS AND SETTERS FUNCTIONS


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return null|string
     */
    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    /**
     * @param null|string $companyName
     */
    public function setCompanyName(?string $companyName): void
    {
        $this->companyName = $companyName;
    }

    /**
     * @return null|string
     */
    public function getCompanyAddressInfo(): ?string
    {
        return $this->companyAddressInfo;
    }

    /**
     * @param null|string $companyAddressInfo
     */
    public function setCompanyAddressInfo(?string $companyAddressInfo): void
    {
        $this->companyAddressInfo = $companyAddressInfo;
    }

    /**
     * @return string
     */
    public function getStreetAddress(): string
    {
        return $this->streetAddress;
    }

    /**
     * @param string $streetAddress
     */
    public function setStreetAddress(string $streetAddress): void
    {
        $this->streetAddress = $streetAddress;
    }

    /**
     * @return string
     */
    public function getAddressDetail(): string
    {
        return $this->addressDetail;
    }

    /**
     * @param string $addressDetail
     */
    public function setAddressDetail(string $addressDetail): void
    {
        $this->addressDetail = $addressDetail;
    }

    /**
     * @return string
     */
    public function getStateProvince(): string
    {
        return $this->stateProvince;
    }

    /**
     * @param string $stateProvince
     */
    public function setStateProvince(string $stateProvince): void
    {
        $this->stateProvince = $stateProvince;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getPostalCode(): string
    {
        return $this->postalCode;
    }

    /**
     * @param string $postalCode
     */
    public function setPostalCode(string $postalCode): void
    {
        $this->postalCode = $postalCode;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return null|string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param null|string $phone
     */
    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return \DateTime
     */
    public function getLastUsedAt(): \DateTime
    {
        return $this->lastUsedAt;
    }

    /**
     * @param \DateTime $lastUsedAt
     */
    public function setLastUsedAt(\DateTime $lastUsedAt): void
    {
        $this->lastUsedAt = $lastUsedAt;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime|null
     */
    public function getDisabledAt(): ?\DateTime
    {
        return $this->disabledAt;
    }

    /**
     * @param \DateTime|null $disabledAt
     */
    public function setDisabledAt(?\DateTime $disabledAt): void
    {
        $this->disabledAt = $disabledAt;
    }

    /**
     * @return null|string
     */
    public function getDisabledBy(): ?string
    {
        return $this->disabledBy;
    }

    /**
     * @param null|string $disabledBy
     */
    public function setDisabledBy(?string $disabledBy): void
    {
        $this->disabledBy = $disabledBy;
    }

    /**
     * @return bool|null
     */
    public function getIsDisabled(): ?bool
    {
        return $this->isDisabled;
    }

    /**
     * @param bool|null $isDisabled
     */
    public function setIsDisabled(?bool $isDisabled): void
    {
        $this->isDisabled = $isDisabled;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param Country $country
     */
    public function setCountry(Country $country): void
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getHash(): string
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash(string $hash): void
    {
        $this->hash = $hash;
    }

}
