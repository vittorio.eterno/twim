<?php

namespace UserBundle\Entity;

use PurchasingBundle\Entity\Package;
use Schema\Entity;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use WordBundle\Entity\Word;

/**
 * Invitation
 *
 * @ORM\Table(name="invitations")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\InvitationRepository")
 */
class Invitation extends Entity {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Many Invitations have One Word
     * @ORM\ManyToOne(targetEntity="WordBundle\Entity\Word")
     * @ORM\JoinColumn(name="word_id", referencedColumnName="id")
     */
    private $word;

    /**
     * Many Invitations have One Package
     * @ORM\ManyToOne(targetEntity="PurchasingBundle\Entity\Package")
     * @ORM\JoinColumn(name="package_id", referencedColumnName="id")
     */
    private $package;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=50)
     */
    private $token;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="disabled_at", type="datetime", nullable=true)
     */
    private $disabledAt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="disabled_by", type="string", length=22, nullable=true)
     */
    private $disabledBy;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_disabled", type="boolean", nullable=true, options={"default":0})
     */
    private $isDisabled;



    ################################################# SERIALIZER FUNCTIONS

    /**
     * Returns the array of fields to serialize in entity administration view.
     * @return array
     */
    public function adminSerializer() {
        $view_vars = $this->viewSerializer();

        $admin_vars = array();

        return array_merge($view_vars, $admin_vars);
    }

    /**
     * Returns the array of fields to serialize in entity view.
     * @return array
     */
    public function viewSerializer() {
        $list_vars = $this->listSerializer();

        $view_vars = array(
        );

        return array_merge($list_vars, $view_vars);
    }

    /**
     * Returns the array of fields to serialize in a list of this entity.
     * @return array
     */
    public function listSerializer() {
        $list_vars = array(
            'id'           => $this->serializedId()
        );
        return $list_vars;
    }

    /**
     * Returns the hash code unique identifier of the entity.
     * @return string
     */
    public function hashCode() {
        // TODO: Implement hashCode() method.
    }

    ################################################# SERIALIZED FUNCTIONS

    /**
     * Setting id
     * @JMS\VirtualProperty
     * @JMS\SerializedName("id")
     * @JMS\Type("integer")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedId() {
        return (is_null($this->id)?null:$this->id);
    }


    ################################################# GETTERS AND SETTERS FUNCTIONS




    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Word
     */
    public function getWord()
    {
        return $this->word;
    }

    /**
     * @param Word $word
     */
    public function setWord(Word $word): void
    {
        $this->word = $word;
    }

    /**
     * @return Package
     */
    public function getPackage()
    {
        return $this->package;
    }

    /**
     * @param Package $package
     */
    public function setPackage(Package $package): void
    {
        $this->package = $package;
    }

    /**
     * Set token.
     *
     * @param string $token
     *
     * @return Invitation
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token.
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Invitation
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set disabledAt.
     *
     * @param \DateTime|null $disabledAt
     *
     * @return Invitation
     */
    public function setDisabledAt($disabledAt = null)
    {
        $this->disabledAt = $disabledAt;

        return $this;
    }

    /**
     * Get disabledAt.
     *
     * @return \DateTime|null
     */
    public function getDisabledAt()
    {
        return $this->disabledAt;
    }

    /**
     * Set disabledBy.
     *
     * @param string|null $disabledBy
     *
     * @return Invitation
     */
    public function setDisabledBy($disabledBy = null)
    {
        $this->disabledBy = $disabledBy;

        return $this;
    }

    /**
     * Get disabledBy.
     *
     * @return string|null
     */
    public function getDisabledBy()
    {
        return $this->disabledBy;
    }

    /**
     * Set isDisabled.
     *
     * @param bool|null $isDisabled
     *
     * @return Invitation
     */
    public function setIsDisabled($isDisabled = null)
    {
        $this->isDisabled = $isDisabled;

        return $this;
    }

    /**
     * Get isDisabled.
     *
     * @return bool|null
     */
    public function getIsDisabled()
    {
        return $this->isDisabled;
    }
}
