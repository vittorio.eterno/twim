<?php

namespace UserBundle\Controller;

use Psr\Log\LoggerInterface;
use ResourceBundle\Entity\CalendarTimezone;
use ResourceBundle\Service\CalendarTimezoneService;
use Schema\AbstractRenderController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\TranslatorInterface;
use UserBundle\Entity\User;
use UserBundle\Service\UserService;
use Utils\Constants\Gender;
use Utils\Service\Encryption;
use Utils\Service\RequestInfo;
use Utils\StaticUtil\IntegerUtils;
use Utils\StaticUtil\StringUtils;

/**
 * Class UserController
 * @package UserBundle\Controller
 *
 */
class UserController extends Controller {

    /**
     *
     * @Route("/profile/", name="show_private_profile", methods="GET")
     * @Route("/profile/{username}", name="show_public_profile", methods="GET")
     *
     * @param null|string $username
     * @param Request $request
     * @param UserService $userService
     * @param RequestInfo $requestInfo
     * @param TranslatorInterface $translator
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showProfileAction(?string $username, Request $request, UserService $userService, RequestInfo $requestInfo, TranslatorInterface $translator) {

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY', null, 'Unable to access this page!');

        $deviceName = $requestInfo->get('deviceName','Desktop');

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        $page = $request->query->get('p'  , 1);

        $responseUser = null;
        if (!empty($username) && StringUtils::checkUsername($username)) {
            /** @var User $user */
            $user = $userService->getByUsername($username);

            if (is_null($user)) {
                return $this->renderError(array("message" => $translator->trans("user.empty.or.disabled")));
            }

            /**
             * If is another user to see profile, then he will see the public side
             */
            if ($user !== $currentUser) {
//                if ($user->isEnabled()) {
//                    $responseUser = $userService->getFormattedProfile($user, $page, true);
//                } else {
                    return $this->renderError(array("message" => $translator->trans("user.empty.or.disabled")));
//                }
            }
        }

        if (is_null($responseUser)) {
            $responseUser = $userService->getFormattedProfile($currentUser, $page);
        }


        return $this->renderByDevice($deviceName, 'UserBundle\\show.profile.html.twig', array(
            'user' => $responseUser
        ));
    }

    /**
     * @Route("/private/confirmation/social/{user_id}/{token}", name="private_registration_social_user", methods={"GET","POST"})
     * @ParamConverter("user", class="UserBundle:User", options={"id" = "user_id"})
     *
     * @param null|User $user
     * @param $user_id
     * @param null|string $token
     * @param Request $request
     * @param RequestInfo $requestInfo
     * @param UserService $userService
     * @param LoggerInterface $logger
     * @param CalendarTimezoneService $calendarTimezoneService
     * @param TranslatorInterface $translator
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function registrationSocialAction (?User $user, $user_id, ?string $token, Request $request,
                                              RequestInfo $requestInfo, UserService $userService,
                                              LoggerInterface $logger,
                                              CalendarTimezoneService $calendarTimezoneService,
                                              TranslatorInterface $translator
    ) {

        if(!IntegerUtils::checkId($user_id)) {
            return $this->renderError(array("message" => $translator->trans("parameter.id.invalid")));
        }

        if(!StringUtils::checkHashString($token)) {
            return $this->renderError(array("message" => $translator->trans("parameter.token.invalid")));
        }

        if(is_null($user)){
            return $this->renderError(array("message" => $translator->trans("data.not.found.404")));
        }

        /** @var User $currentUser */
        $currentUser    = $this->getUser();
        $tokenCompare   = $userService->getSocialToken($user);

        if ($currentUser !== $user || $tokenCompare != $token) {
            return $this->renderError(array("message" => $translator->trans("user.not.rights")));
        }

        $errors = array();
        if ($request->isMethod('POST')) {

            $submittedToken     = $request->request->get('token');
            $gRecaptcha         = $request->request->get('g-recaptcha-response');
            $username           = $request->request->get('username');
            $genre              = $request->request->get('genre');
            $timezoneId         = $request->request->get('timezone');
            $acceptingTerms     = $request->request->get('accepting_terms');
            $acceptingPrivacy   = $request->request->get('accepting_privacy');

            /**
             * Validation of CSRF Token and ReCaptcha
             */
            if (!$this->isCsrfTokenValid('registration_social', $submittedToken)) {
                $errors["general"] = $translator->trans("parameter.token.invalid");
            }

            if (is_null($gRecaptcha)) {
                $errors["recaptcha"] = $translator->trans("click.recaptcha.box");
            } else {

                $recaptchaSecret = $this->getParameter("recaptcha_secret");

                //get verify response data
                $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$recaptchaSecret.'&response='.$gRecaptcha);
                $responseData = json_decode($verifyResponse);
                if(!$responseData->success){
                    //TODO: add specific log to this user (create field "isBotSuspect" to true)
                }

                /**
                 * Validation Username
                 */
                if (is_null($username) || !StringUtils::checkUsername($username)) {
                    $errors["username"] = $translator->trans("username.invalid");
                }

                if ($username != $user->getUsername() && $userService->isUsernameAlreadyExist($username)) {
                    $errors["username"] = $translator->trans("username.already.exist");
                }

                /**
                 * Validation Genre
                 */
                if (is_null($genre) || ($genre != Gender::MALE && $genre != Gender::FEMALE)) {
                    $errors["genre"] = $translator->trans("genre.invalid");
                }

                /**
                 * Validation Timezone
                 */
                $listTimezone = $calendarTimezoneService->getFormattedListTimezone();

                $calendarTimezone = null;
                if (!array_key_exists($timezoneId,$listTimezone)) {
                    $errors["timezone"] = $translator->trans("invalid.timezone");
                }else {
                    /** @var CalendarTimezone $calendarTimezone */
                    $calendarTimezone = $calendarTimezoneService->getById($timezoneId);
                }

                /**
                 * Validation Terms & Privacy
                 */
                if (is_null($acceptingTerms)) {
                    $errors["terms"] = $translator->trans("accepting.terms.required");
                }

                if (is_null($acceptingPrivacy)) {
                    $errors["privacy"] = $translator->trans("accepting.privacy.required");
                }


                if (count($errors) == 0) {

                    /**
                     * Save User
                     */
                    $user->setLocale($request->getLocale());
                    $user->setUsername($username);
                    $user->setGender($genre);
                    if (!is_null($calendarTimezone)) {
                        $user->setCalendarTimezone($calendarTimezone);
                    }

                    $userResponse = $userService->signupFromSocial($user);
                    if (is_null($userResponse)) {
                        $errors["general"] = $translator->trans("error.save");
                    } else {
                        $this->addFlash(
                            'success',
                            $translator->trans('successful.user.activation', array("%username%" => $user->getUsername()))
                        );
                        return $this->redirectToRoute('show_private_profile');
                    }
                }

            }
        }


        $deviceName = $requestInfo->get('deviceName','Desktop');
        return $this->renderByDevice($deviceName, 'UserBundle\\signup.html.twig', array(
            "user"  => $user->adminSerializer(),
            "token" => $token,
            "errors" => $errors
        ));
    }


}
