<?php

namespace UserBundle\Controller\Admin;

use Schema\AbstractRenderController as Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use UserBundle\Entity\User;
use UserBundle\Repository\UserRepository;
use UserBundle\Service\UserService;
use Utils\Constants\AdminLimitation;
use Utils\Constants\System;

/**
 * Class UserController
 * @package UserBundle\Controller\Admin
 *
 * @Route("/admin")
 */
class UserController extends Controller {


    /**
     * Return a paginated list of all users
     *
     * @Route("/user/list", name="admin_list_users", methods="GET")
     *
     * @param Request $request
     * @param UserService $userService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(Request $request,
                               UserService $userService
    ) {

        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        /**
         * Is admin authorized to this Action?
         */
        if ($currentUser->isAdminLimited() &&
            !$currentUser->isGrantedLimitation(AdminLimitation::AREA_USERS, array(AdminLimitation::PRIVILEGE_READ))
        ) {
            throw new AccessDeniedException();
        }

        $page       = $request->query->get('p'  , 1);
        $maxResults = $request->query->get('mxr', UserRepository::DEFAULT_MAX_RESULTS_USERS);
        $suggest    = $request->query->get('s');

        $formatted  = $userService->getPaginatedList($page, $suggest, $maxResults);

        return $this->renderByAdmin('UserBundle\list.html.twig',
            array(
                "users"     => $formatted["users"],
                "paginator" => $formatted["paginator"]
            )
        );

    }

    /**
     * Return a user by id
     *
     * @Route("/user/{id}", name="admin_show_user", methods="GET")
     *
     * @param User|null $user
     * @param Request $request
     * @param UserService $userService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(?User $user, Request $request, UserService $userService) {

        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        $page = $request->query->get('p'  , 1);

        /**
         * Is admin authorized to this Action?
         */
        if ($currentUser->isAdminLimited() &&
            !$currentUser->isGrantedLimitation(AdminLimitation::AREA_USERS, array(AdminLimitation::PRIVILEGE_READ))
        ) {
            throw new AccessDeniedException();
        }

        if (!is_null($user)) {
            $user = $userService->getAdminFormattedUser($user, $page);
        }

        return $this->renderByAdmin('UserBundle\show.html.twig', array(
            "user" => $user
        ));
    }


    /**
     * Lock/unlock a user by id
     *
     * @Route("/user/locking/{locking}/{id}", name="super_locking_user", methods="GET")
     *
     * @param User|null $user
     * @param $locking
     * @param UserService $userService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function lockingAction(?User $user, $locking, UserService $userService) {

        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        /**
         * Is admin authorized to this Action?
         */
        if ($currentUser->isAdminLimited() &&
            !$currentUser->isGrantedLimitation(AdminLimitation::AREA_USERS, array(AdminLimitation::PRIVILEGE_UPDATE))
        ) {
            throw new AccessDeniedException();
        }

        if (!is_null($user)) {

            if ($locking != System::LOCKING_LOCK && $locking != System::LOCKING_UNLOCK) {
                $this->addFlash(
                    'error',
                    'Uncorrect {locking} parameter!'
                );
            } else {
                $user = $userService->adminLocking($currentUser, $user, $locking);
            }
        }

        return $this->redirectToRoute('admin_show_user', array('id' => $user->getId()));
    }


    /**
     * Enable/disable a user by id
     *
     * @Route("/user/enabling/{enabling}/{id}", name="super_enabling_user", methods="GET")
     *
     * @param User|null $user
     * @param $enabling
     * @param UserService $userService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function enablingAction(?User $user, $enabling, UserService $userService) {

        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this page!');

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        /**
         * Is admin authorized to this Action?
         */
        if ($currentUser->isAdminLimited() &&
            !$currentUser->isGrantedLimitation(AdminLimitation::AREA_USERS, array(AdminLimitation::PRIVILEGE_UPDATE))
        ) {
            throw new AccessDeniedException();
        }

        if (!is_null($user)) {

            if ($enabling != System::ENABLING_ENABLE && $enabling != System::ENABLING_DISABLE) {
                $this->addFlash(
                    'error',
                    'Uncorrect {enabling} parameter!'
                );
            } else {
                $user = $userService->adminEnabling($currentUser, $user, $enabling);
            }
        }

        return $this->redirectToRoute('admin_show_user', array('id' => $user->getId()));
    }

}
