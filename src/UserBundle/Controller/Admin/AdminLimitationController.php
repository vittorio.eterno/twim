<?php

namespace UserBundle\Controller\Admin;


use Schema\AbstractRenderController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use UserBundle\Entity\AdminLimitation;
use UserBundle\Entity\User;
use UserBundle\Form\AdminLimitationType;
use UserBundle\Service\AdminLimitationService;
use UserBundle\Service\UserService;
use Utils\Constants\System;


/**
 * Class AdminLimitationController
 * @package UserBundle\Controller\Admin
 *
 * @Route("/super")
 */
class AdminLimitationController extends Controller {


    /**
     * Display panel to limit access to admin
     *
     * @Route("/admin/{user_id}/save/limitation/", name="super_admin_create_limitation", methods={"GET","POST"})
     * @Route("/admin/{user_id}/save/limitation/{id}", name="super_admin_edit_limitation", methods={"GET","POST"})
     * @ParamConverter("user", class="UserBundle:User", options={"id" = "user_id"})
     *
     * @param null|User $user
     * @param null|AdminLimitation $adminLimitation
     * @param Request $request
     * @param AdminLimitationService $adminLimitationService
     * @param UserService $userService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function saveAction(?User $user,
                               ?AdminLimitation $adminLimitation,
                               Request $request,
                               AdminLimitationService $adminLimitationService,
                               UserService $userService
    ) {

        $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN', null, 'Unable to access this page!');

        if (is_null($user)) {
            $this->addFlash(
                'error',
                'User not found!'
            );
            return $this->redirectToRoute('admin_list_users');
        }

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        if (is_null($adminLimitation)) {
            $adminLimitation = new AdminLimitation();
        }

        $form = $this->createForm(AdminLimitationType::class, $adminLimitation);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $result = $adminLimitationService->saveLimitation($userService, $currentUser, $user, $adminLimitation);
            if (is_null($result)) {
                $this->addFlash(
                    'error',
                    'Error on save!'
                );
            }

            return $this->redirectToRoute('admin_show_user', array('id' => $user->getId()));
        }

        return $this->renderByAdmin('UserBundle\AdminLimitation\save.html.twig',
                array(
                    "user"              => $user->viewLimitationsSerializer(),
                    'form'              => $form->createView()
                )
            );
    }

    /**
     * Enable/disable an Admin limitation
     *
     * @Route("/admin/limitation/enabling/{enabling}/{id}", name="super_admin_limitation_enabling", methods="GET")
     *
     * @param AdminLimitation|null $adminLimitation
     * @param $enabling
     * @param AdminLimitationService $adminLimitationService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function enablingAction(?AdminLimitation $adminLimitation,
                                   $enabling,
                                   AdminLimitationService $adminLimitationService
    ) {

        $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN', null, 'Unable to access this page!');

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        if (!is_null($adminLimitation)) {

            /** @var User $user */
            $user = $adminLimitation->getUser();

            if ($enabling != System::ENABLING_ENABLE && $enabling != System::ENABLING_DISABLE) {
                $this->addFlash(
                    'error',
                    'Uncorrect {enabling} parameter!'
                );
            } else {
                $adminLimitation = $adminLimitationService->adminEnabling($currentUser, $adminLimitation, $enabling);
                if (is_null($adminLimitation)) {
                    $this->addFlash(
                        'error',
                        'Error on save!'
                    );
                }
            }

            return $this->redirectToRoute('admin_show_user', array('id' => $user->getId()));
        }

        return $this->redirectToRoute('admin_list_users');
    }

}
