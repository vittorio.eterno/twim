<?php

namespace UserBundle\Controller;

use EmailBundle\Service\EmailService;
use Exceptions\ValidationException;
use Psr\Log\LoggerInterface;
use Schema\AbstractRenderController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use UserBundle\Entity\User;
use UserBundle\Service\UserService;
use Utils\Service\Encryption;
use Utils\Service\RequestInfo;
use Utils\StaticUtil\EmailUtils;
use Utils\StaticUtil\IntegerUtils;
use Utils\StaticUtil\StringUtils;

/**
 * Class ResettingController
 * @package UserBundle\Controller
 *
 * @Route("/resetting")
 */
class ResettingController extends Controller {


    /**
     * Reset password user.
     * @Route("/user/reset", name="public_user_reset_password", methods="POST")
     *
     * @param Request $request
     * @param UserService $userService
     * @param EmailService $emailService
     * @param ValidationException $validationException
     * @param LoggerInterface $logger
     * @param TranslatorInterface $translator
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function resetAction(Request $request, UserService $userService, EmailService $emailService, ValidationException $validationException, LoggerInterface $logger, TranslatorInterface $translator) {

        $email = $request->request->get("username");

        $isEmail = false;

        if(StringUtils::checkEmailString($email)) {
            // It is an email

            $isEmail = true;

            $validator = $this->get('validator');

            $errors = EmailUtils::validateEmails($validator, $email);
            if(count($errors) > 0 && $errors instanceof ConstraintViolationListInterface) {
                $formattedErrors = $validationException->getFormattedExceptions($errors);
                return $this->renderCustom("@FOSUser/Resetting/request.html.twig",array(
                    "email_error" => $formattedErrors,
                    "email"       => $email
                ));
            }

        }

        /** @var User $user */
        $user = $userService->getUserByUsernameOrEmail($email);

        if (!is_null($user)) {
            $just_sent_reset = $emailService->getJustSentReset($user);
            if($just_sent_reset) {
                $logger->info("User ID: ".$user->getId(). " has just sent a reset password");
            }

            $user = $userService->reset($user);
            if(is_null($user)) {
                $this->addFlash(
                    'warning',
                    $translator->trans('system.error.try.again')
                );
                return $this->renderCustom("@FOSUser/Resetting/request.html.twig");
            }
        }

        return $this->render('@User/Resetting/check_email.html.twig', array("isEmail" => $isEmail, "username" => $email));
    }

    /**
     * Reset password confirm.
     *
     * @Route("/user/{user_id}/reset/confirmation/{token}", name="public_user_reset_password_confirmation", methods={"GET","POST"})
     * @ParamConverter("user", class="UserBundle:User", options={"id" = "user_id"})
     *
     *
     * @param null|User $user
     * @param null $user_id
     * @param null $token
     * @param UserService $userService
     * @param Request $request
     * @param ValidationException $validationException
     * @param LoggerInterface $logger
     * @param TranslatorInterface $translator
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function resetConfirmAction(?User $user, $user_id=null, $token=null,
                                       UserService $userService,
                                       Request $request,
                                       ValidationException $validationException,
                                       LoggerInterface $logger,
                                       TranslatorInterface $translator
    ) {

        if(!IntegerUtils::checkId($user_id)) {
            $this->addFlash(
                'warning',
                $translator->trans('parameter.id.invalid')
            );
            return $this->renderError();
        }

        if(!StringUtils::checkHashString($token)) {
            $this->addFlash(
                'warning',
                $translator->trans('parameter.token.invalid')
            );
            return $this->renderError();
        }

        if(is_null($user)){
            $this->addFlash(
                'warning',
                $translator->trans('parameters.invalid')
            );
            return $this->renderError();
        }

        $username           = $user->getUsername();
        $newPlainPassword   = $request->request->get("new_password");

        $responseParameters = array(
            "user_id"           => $user_id,
            "token"             => $token,
            "newPlainPassword"  => $newPlainPassword,
            "username"          => $username
        );

        if (!is_null($newPlainPassword)) {

            if(!StringUtils::checkPasswordString($newPlainPassword)) {
                $responseParameters["error_password"] = array($translator->trans("password.invalid"));
                return $this->renderCustom("@User/Resetting/reset.html.twig", $responseParameters);
            }

            $user->setPlainPassword($newPlainPassword);
            $validator = $this->get('validator');
            $errors = $validator->validate($user, null, array("password"));
            if(count($errors) > 0) {
                $formattedErrors = $validationException->getFormattedExceptions($errors);
                $responseParameters["error_password"] = $formattedErrors;
                return $this->renderCustom("@User/Resetting/reset.html.twig", $responseParameters);
            }

            if(!$userService->confirmReset($user, $token)) {
                $this->addFlash(
                    'warning',
                    $translator->trans('parameter.token.invalid')
                );
                return $this->renderError();
            } else {
                $this->addFlash(
                    'success',
                    $translator->trans('success.reset.password')
                );

                $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
                $this->get('security.token_storage')->setToken($token);
                $this->get('session')->set('_security_main', serialize($token));

                return $this->redirectToRoute('homepage');
            }

        }

        return $this->renderCustom("@User/Resetting/reset.html.twig", $responseParameters);
    }



}
