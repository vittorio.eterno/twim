<?php

namespace UserBundle\Controller;


use Exceptions\ValidationException;
use Psr\Log\LoggerInterface;
use ResourceBundle\Entity\CalendarTimezone;
use ResourceBundle\Service\CalendarTimezoneService;
use Schema\AbstractRenderController as Controller;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use UserBundle\Entity\ExternalLink;
use UserBundle\Entity\User;
use UserBundle\Service\ExternalLinkStructureService;
use UserBundle\Service\SettingService;
use UserBundle\Service\UserService;
use UserBundle\Service\UserSettingService;
use Utils\Constants\Gender;
use Utils\Service\Encryption;
use Utils\Service\RequestInfo;
use Symfony\Component\Translation\TranslatorInterface;
use Utils\StaticUtil\EmailUtils;
use Utils\StaticUtil\StringUtils;
use UserBundle\Service\ShippingAddressService;
use ResourceBundle\Service\CountryService;


/**
 * Class UserSettingController
 * @package UserBundle\Controller
 */
class UserSettingController extends Controller {

    /**
     *
     * @Route("/profile/{username}/settings", name="edit_profile_settings", methods={"GET","POST"})
     *
     * @param null|string $username
     * @param UserService $userService
     * @param Request $request
     * @param UserSettingService $userSettingService
     * @param RequestInfo $requestInfo
     * @param TranslatorInterface $translator
     * @param Encryption $encryption
     * @param LoggerInterface $logger
     * @param CalendarTimezoneService $calendarTimezoneService
     * @param SettingService $settingService
     * @param CountryService $countryService
     * @param ShippingAddressService $shippingAddressService
     * @param ValidationException $validationException
     *
     * @param ExternalLinkStructureService $externalLinkStructureService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(?string $username, UserService $userService, Request $request,
                               UserSettingService $userSettingService,
                               RequestInfo $requestInfo,
                               TranslatorInterface $translator,
                               Encryption $encryption,
                               LoggerInterface $logger,
                               CalendarTimezoneService $calendarTimezoneService,
                               SettingService $settingService,
                               CountryService $countryService,
                               ShippingAddressService $shippingAddressService,
                               ValidationException $validationException,
                               ExternalLinkStructureService $externalLinkStructureService,
                               SessionInterface $session
    ) {

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY', null, 'Unable to access this page!');

        if (is_null($username) || !StringUtils::checkUsername($username)) {
            return $this->renderError(array("message" => $translator->trans("username.invalid")));
        }

        $deviceName = $requestInfo->get('deviceName','Desktop');

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        /** @var User $user */
        $user = $userService->getByUsername($username);

        if (is_null($user)) {
            return $this->renderError(array("message" => $translator->trans("user.empty.or.disabled")));
        }

        /**
         * If is another user to see profile, then he will see the public side
         */
        if ($user !== $currentUser) {
            return $this->renderError(array("message" => $translator->trans("user.not.rights")));
        }

        $settings = $settingService->getFormattedSettings();

        $errors       = array();
        $postedParams = array();
        if ($request->isMethod('POST')) {

            $submittedToken     = $request->request->get('token');
            $usernamePosted     = $request->request->get('username');
            $genre              = $request->request->get('genre');
            $timezoneId         = $request->request->get('timezone');
            $email              = $request->request->get('email');
            $externalLinks      = $request->request->get('external_links');
            $language           = $request->request->get('language');
            $settingChecks      = $request->request->get('settings');


            $postedParams["username"]       = $usernamePosted;
            $postedParams["genre"]          = $genre;
            $postedParams["timezoneId"]     = $timezoneId;
            $postedParams["email"]          = $email;
            $postedParams["externalLinks"]  = $externalLinks;
            $postedParams["language"]       = $language;

            $postedSettingCheckIds = array();
            if (!empty($settingChecks) && count($settingChecks)>0) {
                foreach ($settingChecks as $ksc=>$vsc) {
                    $postedSettingCheckIds[] = $ksc;
                }
            }
            $postedParams["settingChecks"]  = $postedSettingCheckIds;


            //return values to the view

            /**
             * Validation of CSRF Token
             */
            if (!$this->isCsrfTokenValid('edit_user_settings_token', $submittedToken)) {
                $errors["general"] = $translator->trans("parameter.token.invalid");
            }

            /**
             * Validation Username
             */
            if (is_null($usernamePosted) || !StringUtils::checkUsername($usernamePosted)) {
                $errors["username"] = $translator->trans("username.invalid");
            }

            if ($usernamePosted != $user->getUsername() && $userService->isUsernameAlreadyExist($usernamePosted)) {
                $errors["username"] = $translator->trans("username.already.exist");
            }

            /**
             * Validation Email
             */
            $emailCrypted = null;
            if (!is_null($email)) {
                $emailCrypted = $encryption->encrypting($email);
                if ($user->getEmail() != $emailCrypted) {
                    $validator   = $this->get('validator');
                    $emailErrors = EmailUtils::validateEmails($validator, $email);
                    if (count($emailErrors) > 0 && $emailErrors instanceof ConstraintViolationListInterface) {
                        $formattedErrors = $validationException->getFormattedExceptions($emailErrors);
                        foreach ($formattedErrors as $formattedError) {
                            $errors["email"] = $formattedError;
                            break;
                        }
                    }
                }
            }

            /**
             * Validation External Links
             */
            if (isset($externalLinks["blog"])) {

            }

            if (isset($externalLinks["facebook"])) {

            }

            if (isset($externalLinks["instagram"])) {

            }

            if (isset($externalLinks["youtube"])) {

            }

            if (isset($externalLinks["twitter"])) {

            }

            /**
             * Validation Genre
             */
            if (is_null($genre) || ($genre != Gender::MALE && $genre != Gender::FEMALE)) {
                $errors["genre"] = $translator->trans("genre.invalid");
            }

            /**
             * Validation Timezone
             */
            $listTimezone = $calendarTimezoneService->getFormattedListTimezone();

            $calendarTimezone = null;
            if (!array_key_exists($timezoneId,$listTimezone)) {
                $errors["timezone"] = $translator->trans("invalid.timezone");
            }else {
                /** @var CalendarTimezone $calendarTimezone */
                $calendarTimezone = $calendarTimezoneService->getById($timezoneId);
            }


            /**
             * Validation setting checks
             */
            $settingsToAdd      = array();
            $settingIdsToRemove = array();
            if (!empty($settingChecks) && count($settingChecks)>0) {

                $userSettingIds     = $user->serializedSettingIds();
                $settingIdsToAdd    = array();

                if (!empty($userSettingIds) && count($userSettingIds)>0) {
                    foreach ($userSettingIds as $settingId) {
                        if (array_key_exists($settingId, $settingChecks)) {
                            unset($settingChecks[$settingId]);
                        } else {
                            $settingIdsToRemove[] = $settingId;
                        }
                    }
                }

                if (!empty($settingChecks) && count($settingChecks)>0) {
                    foreach ($settingChecks as $keyId => $value) {
                        $settingIdsToAdd[] = $keyId;
                    }
                }

                if (!empty($settingIdsToAdd) && count($settingIdsToAdd)>0) {
                    $settingsToAdd = $settingService->getAllByIds($settingIdsToAdd);
                }

            }


            if (count($errors) == 0) {

                $externalLinkStructures = $externalLinkStructureService->getAllExternalLinkStructures();

                /**
                 * Save User
                 */
                $userResponse = $userSettingService->saveSettings($user, $emailCrypted, $usernamePosted, $genre, $language, $calendarTimezone, $externalLinks, $externalLinkStructures, $settingsToAdd, $settingIdsToRemove);

                if (is_null($userResponse)) {
                    $errors["general"] = $translator->trans("error.save");
                } else {

                    $session->set('_locale', $userResponse->getLocale());

                    //se è cambiata la mail, il messaggio sarà quello di controllare la mail per cliccare il link di conferma
                    //altrimenti il messaggio è quello classico "impostazioni modificate con successo"
                    $this->addFlash(
                        'success',
                        $translator->trans('success.edit.settings', array("%username%" => $user->getUsername()), null, $userResponse->getLocale())
                    );
                    return $this->redirectToRoute('show_private_profile');
                }
            }
        }

        $responseUser = $userSettingService->getFormattedView($user);

        $countries         = $countryService->getCachedCountries();
        $lastAddressesUsed = $shippingAddressService->getFormattedByLastUsed($currentUser);

        return $this->renderByDevice($deviceName, 'UserBundle\\UserSetting\\edit.html.twig', array(
            'user'              => $responseUser,
            'email'             => $encryption->decrypting($user->getEmail()),
            'settings'          => $settings,
            'errors'            => $errors,
            'posted'            => $postedParams,
            'countries'         => $countries,
            'lastAddressesUsed' => $lastAddressesUsed
        ));
    }



}
