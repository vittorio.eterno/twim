<?php

namespace UserBundle\Controller\Api;


use Exceptions\ValidationException;
use Psr\Log\LoggerInterface;
use ResourceBundle\Entity\Country;
use ResourceBundle\Service\CountryService;
use Schema\AbstractRenderController as Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use UserBundle\Entity\ShippingAddress;
use UserBundle\Entity\User;
use UserBundle\Service\ShippingAddressService;
use Utils\Service\ResponseUtils;
use Utils\StaticUtil\EmailUtils;
use Utils\StaticUtil\IntegerUtils;
use Utils\StaticUtil\StringUtils;
use Utils\Validation\InvoiceValidation;


/**
 * Class ShippingAddressController
 * @package PurchasingBundle\Controller\Api
 */
class ShippingAddressController extends Controller {

    /**
     * @Route("/api/shipping/address/validate", name="api_validate_shipping_address_fields", methods={"POST"})
     *
     * @param Request $request
     * @param TranslatorInterface $translator
     * @param LoggerInterface $logger
     * @param CountryService $countryService
     * @param ValidationException $validationException
     * @param ShippingAddressService $shippingAddressService
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function validateInvoiceAction(Request $request,
                                          TranslatorInterface $translator,
                                          LoggerInterface $logger,
                                          CountryService $countryService,
                                          ValidationException $validationException,
                                          ShippingAddressService $shippingAddressService
    ) {

        $response = new ResponseUtils($translator, $logger);

        /** @var User $currentUser */
        $currentUser        = $this->getUser();

        $submittedToken     = $request->request->get('token');
        $firstName          = $request->request->get('first_name');
        $lastName           = $request->request->get('last_name');
        $companyName        = $request->request->get('company_name');
        $companyAddressInfo = $request->request->get('company_address_info');
        $streetAddress      = $request->request->get('street_address');
        $addressDetail      = $request->request->get('address_detail');
        $city               = $request->request->get('city');
        $stateProvince      = $request->request->get('state_province');
        $postalCode         = $request->request->get('postal_code');
        $countryId          = $request->request->get('country');
        $email              = $request->request->get('email');
        $phone              = $request->request->get('phone');


        if (is_null($currentUser)) {
            return $response->getApiResponse(array(), "user.not.rights", 403, array("general"=>$translator->trans("user.not.rights")));
        }

        if ($shippingAddressService->isReachedLimit($currentUser)) {
            return $response->getApiResponse(array(), "shipping.address.limit.reached", 400, array("general"=>$translator->trans("shipping.address.limit.reached")));
        }

        if (!$this->isCsrfTokenValid('validate_csfr_invoice_form', $submittedToken)) {
            return $response->getApiResponse(array(), "parameter.token.invalid", 400, array("general"=>$translator->trans("parameter.token.invalid")));
        }


        if (!is_null($firstName)) {
            if (empty($firstName)) {
                return $response->getApiResponse(array(), "parameter.mandatory", 400, array("fid_first_name"=>$translator->trans("parameter.mandatory")));
            }
            if (!StringUtils::checkOnlyLettersRegex($firstName)) {
                return $response->getApiResponse(array(), "only.letters.allowed", 400, array("fid_first_name"=>$translator->trans("only.letters.allowed")));
            }
            if (!StringUtils::checkLength($firstName)) {
                return $response->getApiResponse(array(), "length.invalid", 400, array("fid_first_name"=>$translator->trans("length.invalid")));
            }
        }

        if (!is_null($lastName)) {
            if (empty($lastName)) {
                return $response->getApiResponse(array(), "parameter.mandatory", 400, array("fid_last_name"=>$translator->trans("parameter.mandatory")));
            }
            if (!StringUtils::checkOnlyLettersRegex($lastName)) {
                return $response->getApiResponse(array(), "only.letters.allowed", 400, array("fid_last_name"=>$translator->trans("only.letters.allowed")));
            }
            if (!StringUtils::checkLength($lastName)) {
                return $response->getApiResponse(array(), "length.invalid", 400, array("fid_last_name"=>$translator->trans("length.invalid")));
            }
        }

        if (!is_null($companyName) && !empty($companyName)) {
            if (!StringUtils::checkOnlyLettersRegex($companyName)) {
                return $response->getApiResponse(array(), "only.letters.allowed", 400, array("fid_company_name"=>$translator->trans("only.letters.allowed")));
            }
            if (!StringUtils::checkLength($companyName)) {
                return $response->getApiResponse(array(), "length.invalid", 400, array("fid_company_name"=>$translator->trans("length.invalid")));
            }
        }

        if (!is_null($companyAddressInfo) && !empty($companyName)) {
            if (!StringUtils::checkNameString($companyAddressInfo)) {
                return $response->getApiResponse(array(), "string.invalid", 400, array("fid_company_address_info"=>$translator->trans("string.invalid")));
            }
        }

        if (!is_null($streetAddress)) {
            if (empty($streetAddress)) {
                return $response->getApiResponse(array(), "parameter.mandatory", 400, array("fid_street_address"=>$translator->trans("parameter.mandatory")));
            }
            if (!StringUtils::checkNameString($streetAddress)) {
                return $response->getApiResponse(array(), "string.invalid", 400, array("fid_street_address"=>$translator->trans("string.invalid")));
            }
        }

        if (!is_null($addressDetail)) {
            if (empty($addressDetail)) {
                return $response->getApiResponse(array(), "parameter.mandatory", 400, array("fid_address_detail"=>$translator->trans("parameter.mandatory")));
            }
            if (!StringUtils::checkNameString($addressDetail)) {
                return $response->getApiResponse(array(), "string.invalid", 400, array("fid_address_detail"=>$translator->trans("string.invalid")));
            }
        }

        if (!is_null($city)) {
            if (empty($city)) {
                return $response->getApiResponse(array(), "parameter.mandatory", 400, array("fid_city"=>$translator->trans("parameter.mandatory")));
            }
            if (!StringUtils::checkNameString($city)) {
                return $response->getApiResponse(array(), "string.invalid", 400, array("fid_city"=>$translator->trans("string.invalid")));
            }
        }

        if (!is_null($stateProvince)) {
            if (empty($stateProvince)) {
                return $response->getApiResponse(array(), "parameter.mandatory", 400, array("fid_state_province"=>$translator->trans("parameter.mandatory")));
            }
            if (!StringUtils::checkNameString($stateProvince)) {
                return $response->getApiResponse(array(), "string.invalid", 400, array("fid_state_province"=>$translator->trans("string.invalid")));
            }
        }

        if (!is_null($postalCode)) {
            if (empty($postalCode)) {
                return $response->getApiResponse(array(), "parameter.mandatory", 400, array("fid_postal_code"=>$translator->trans("parameter.mandatory")));
            }
            if (!StringUtils::checkNameString($postalCode)) {
                return $response->getApiResponse(array(), "string.invalid", 400, array("fid_postal_code"=>$translator->trans("string.invalid")));
            }
        }

        if (!is_null($countryId)) {
            if (empty($countryId)) {
                return $response->getApiResponse(array(), "parameter.mandatory", 400, array("fid_country"=>$translator->trans("parameter.mandatory")));
            }
            /** @var Country $country */
            $country = $countryService->getById($countryId);
            if (is_null($country)) {
                return $response->getApiResponse(array(), "country.not.found", 400, array("fid_country"=>$translator->trans("country.not.found")));
            }
        }

        if (!is_null($email)) {
            if (empty($email)) {
                return $response->getApiResponse(array(), "parameter.mandatory", 400, array("fid_email"=>$translator->trans("parameter.mandatory")));
            }
            $validator   = $this->get('validator');
            $emailErrors = EmailUtils::validateEmails($validator, $email);
            if (count($emailErrors) > 0 && $emailErrors instanceof ConstraintViolationListInterface) {
                $formattedErrors = $validationException->getFormattedExceptions($emailErrors);
                foreach ($formattedErrors as $formattedError) {
                    return $response->getApiResponse(array(), $formattedError, 400, array("fid_email"=>$formattedError));
                }
            }
        }

        if (!is_null($phone) && !empty($phone)) {
            if (!StringUtils::checkPhoneNumber($phone)) {
                return $response->getApiResponse(array(), "phone.number.invalid", 400, array("fid_phone"=>$translator->trans("phone.number.invalid")));
            }
        }


        return $response->getApiResponse(array(), "OK");
    }

    /**
     * @Route("/api/shipping/address/save", name="api_save_shipping_address", methods={"POST"})
     *
     * @param Request $request
     * @param TranslatorInterface $translator
     * @param LoggerInterface $logger
     * @param ShippingAddressService $shippingAddressService
     * @param InvoiceValidation $invoiceValidation
     * @param CountryService $countryService
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function saveAction(Request $request,
                               TranslatorInterface $translator,
                               LoggerInterface $logger,
                               ShippingAddressService $shippingAddressService,
                               InvoiceValidation $invoiceValidation,
                               CountryService $countryService
    ) {

        $response = new ResponseUtils($translator, $logger);

        /** @var User $currentUser */
        $currentUser        = $this->getUser();
        $invoiceFields      = $request->request->get('fid');


        if (is_null($currentUser)) {
            return $response->getApiResponse(array(), "user.not.rights", 403, array("general-invoice-form-errors"=>$translator->trans("user.not.rights")));
        }

        /**
         * ADDRESS VALIDATION
         */
        $result = array();
        if (!is_null($invoiceFields)) {
            if (!isset($invoiceFields["invoice_token"]) || !$this->isCsrfTokenValid('validate_csfr_invoice_form', $invoiceFields["invoice_token"])) {
                return $response->getApiResponse(array(), "parameter.token.invalid", 400, array("general-invoice-form-errors"=>$translator->trans("parameter.token.invalid")));
            }

            $shippingAddress = new ShippingAddress();
            if (isset($invoiceFields["id"]) && IntegerUtils::checkId($invoiceFields["id"])) {
                /** @var ShippingAddress $shippingAddress */
                $shippingAddress = $shippingAddressService->getById($invoiceFields["id"]);
                if (is_null($shippingAddress)) {
                    return $response->getApiResponse(array(), "data.not.found.404", 404, array("general-invoice-form-errors"=>$translator->trans("data.not.found.404")));
                }
            }

            $validator = $this->get('validator');

            $invoiceErrors = array();
            $invoiceErrors = $invoiceValidation->validateInvoiceFields($validator,$invoiceErrors, $invoiceFields);

            if (!empty($invoiceErrors) && count($invoiceErrors) > 0) {
                return $response->getApiResponse(array(), "parameters.invalid", 400, $invoiceErrors);
            }


            if ($shippingAddressService->isAlreadyExist($currentUser, $invoiceFields)) {
                return $response->getApiResponse(array(), "shipping.address.already.exist", 400, array("general-invoice-form-errors"=>$translator->trans("shipping.address.already.exist")));
            }


            /** @var Country $country */
            $country = $countryService->getById($invoiceFields["country"]);

            $result = $shippingAddressService->saveAddress($shippingAddress, $currentUser, $country, $invoiceFields);
            if (is_null($result)) {
                return $response->getApiResponse(array(), "system.error.try.again", 500, array("general-invoice-form-errors"=>$translator->trans("system.error.try.again")));
            }

        }

        return $response->getApiResponse($result, "OK");
    }


    /**
     * @Route("/api/shipping/address", name="api_delete_shipping_address", methods={"DELETE"})
     *
     * @param Request $request
     * @param TranslatorInterface $translator
     * @param LoggerInterface $logger
     * @param ShippingAddressService $shippingAddressService
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function deleteAction(Request $request,
                                 TranslatorInterface $translator,
                                 LoggerInterface $logger,
                                 ShippingAddressService $shippingAddressService
    ) {

        $response = new ResponseUtils($translator, $logger);

        /** @var User $currentUser */
        $currentUser        = $this->getUser();
        $addressId          = $request->request->get('addressId');

        if (is_null($currentUser)) {
            return $response->getApiResponse(array(), "user.not.rights", 403, array($translator->trans("user.not.rights")));
        }

        if (!IntegerUtils::checkId($addressId)) {
            return $response->getApiResponse(array(), "parameter.id.invalid", 400, array($translator->trans("parameter.id.invalid")));
        }

        /** @var ShippingAddress $shippingAddress */
        $shippingAddress = $shippingAddressService->getById($addressId);
        if (is_null($shippingAddress)) {
            return $response->getApiResponse(array(), "data.not.found.404", 404, array($translator->trans("data.not.found.404")));
        }

        $result = $shippingAddressService->deleteAddress($shippingAddress);
        if (is_null($result)) {
            return $response->getApiResponse(array(), "system.error.try.again", 500, array($translator->trans("system.error.try.again")));
        }

        return $response->getApiResponse(array(), "OK");
    }

}
