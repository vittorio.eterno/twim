<?php

namespace UserBundle\Controller\Api;

use EmailBundle\Service\EmailService;
use Exceptions\ValidationException;
use FOS\UserBundle\Model\UserManagerInterface;
use ImageBundle\Entity\Image;
use ImageBundle\Service\ImageService;
use Psr\Log\LoggerInterface;
use ResourceBundle\Entity\CalendarTimezone;
use ResourceBundle\Service\CalendarTimezoneService;
use Schema\AbstractRenderController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Translation\TranslatorInterface;
use UserBundle\Entity\User;
use UserBundle\Service\UserService;
use Utils\Constants\Gender;
use Utils\Constants\ModalType;
use Utils\Service\Encryption;
use Utils\Service\ResponseUtils;
use Utils\StaticUtil\DateUtils;
use Utils\StaticUtil\FileSystemUtils;
use Utils\StaticUtil\IntegerUtils;
use Utils\StaticUtil\LogUtils;
use Utils\StaticUtil\StringUtils;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class UserController
 * @package UserBundle\Controller
 *
 */
class UserController extends Controller {

    /**
     *
     * @Route("/signup/first-validate", name="first_signup_validation", methods="POST")
     *
     * @param Request $request
     * @param ValidationException $validationException
     * @param TranslatorInterface $translator
     * @param LoggerInterface $logger
     * @param Encryption $encryption
     * @param UserService $userService
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function firstValidationAction(Request $request,
                                          ValidationException $validationException,
                                          TranslatorInterface $translator,
                                          LoggerInterface $logger,
                                          Encryption $encryption,
                                          UserService $userService
    ) {

        $response = new ResponseUtils($translator, $logger);


        $email          = $request->request->get('email');
        $plainPassword  = $request->request->get('password');
        $submittedToken = $request->request->get('token');

        if (!$this->isCsrfTokenValid('registration', $submittedToken)) {
            return $response->getApiResponse(array(), "parameter.token.invalid", 400);
        }

        if (is_null($email) || is_null($plainPassword)) {
            return $response->getApiResponse(array(), "parameters.invalid", 400);
        }

        $validator = $this->get('validator');

        $user = new User();
        $user->setEmail($email);
        $user->setPlainPassword($plainPassword);
        $errors = $validator->validate($user, null, array("registration"));

        if(count($errors) > 0) {
            $formattedErrors = $validationException->getFormattedExceptions($errors);
            return $response->getApiResponse(array(), "parameters.invalid", 400,$formattedErrors);
        }

        $emailCrypted = $encryption->encrypting($email);
        if (is_null($emailCrypted)){
            return $response->getApiResponse(array(), "system.error.try.again", 500);
        }

        if($userService->checkIfExistByEmail($emailCrypted)) {
            return $response->getApiResponse(array(), "email.already.exist", 400);
        }

        return $response->getApiResponse(array(), "success.save");
    }


    /**
     * @Route("/signup/final-validate", name="final_signup_validation", methods="POST")
     *
     * @param Request $request
     * @param ValidationException $validationException
     * @param TranslatorInterface $translator
     * @param Encryption $encryption
     * @param LoggerInterface $logger
     * @param UserService $userService
     * @param CalendarTimezoneService $calendarTimezoneService
     * @param UserManagerInterface $userManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function finalSignupAction(Request $request,
                                      ValidationException $validationException,
                                      TranslatorInterface $translator,
                                      Encryption $encryption,
                                      LoggerInterface $logger,
                                      UserService $userService,
                                      CalendarTimezoneService $calendarTimezoneService,
                                      UserManagerInterface $userManager
    ) {

        $response = new ResponseUtils($translator, $logger);


        $email           = $request->request->get('email');
        $plainPassword   = $request->request->get('password');
        $submittedToken  = $request->request->get('token');
        $gRecaptcha      = $request->request->get('g-recaptcha-response');

        /**
         * Validation of CSRF Token and ReCaptcha
         */
        if (!$this->isCsrfTokenValid('registration', $submittedToken)) {
            return $response->getApiResponse(array(), "parameter.token.invalid", 400, array("general_error"=>$translator->trans("parameter.token.invalid")));
        }

        if (is_null($gRecaptcha)) {
            return $response->getApiResponse(array(), "click.recaptcha.box", 400);
        }

        $recaptchaSecret = $this->getParameter("recaptcha_secret");

        //get verify response data
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$recaptchaSecret.'&response='.$gRecaptcha);
        $responseData = json_decode($verifyResponse);
        if(!$responseData->success){
            //TODO: add specific log to this user (create field "isBotSuspect" to true)
//            return $response->getApiResponse(array(), "recaptcha.invalid", 400, array("dv-g-recaptcha-container"=>$translator->trans("recaptcha.invalid")));
        }

        /**
         * Start Validation of USER's fields
         */

        if (is_null($email) || is_null($plainPassword)) {
            return $response->getApiResponse(array(), "parameters.invalid", 400);
        }

        $validator = $this->get('validator');

        /** @var User $user */
        $user = $userManager->createUser();
        $user->setEmail($email);
        $user->setPlainPassword($plainPassword);
        $errors = $validator->validate($user, null, array("registration"));

        if(count($errors) > 0) {
            $formattedErrors = $validationException->getFormattedExceptions($errors);
            return $response->getApiResponse(array(), "parameters.invalid", 400,$formattedErrors);
        }

        $emailCrypted = $encryption->encrypting($email);
        if (is_null($emailCrypted)){
            return $response->getApiResponse(array(), "system.error.try.again", 500);
        }

        if($userService->checkIfExistByEmail($emailCrypted)) {
            return $response->getApiResponse(array(), "email.already.exist", 400);
        }

        $user->setEmail($emailCrypted);
        $user->setEmailCanonical($emailCrypted);

        /**
         * Validation Username
         */
        $username = $request->request->get('username');

        if (is_null($username) || !StringUtils::checkUsername($username)) {
            return $response->getApiResponse(array(), "username.invalid", 400, array("signup_username" => $translator->trans("username.invalid")));
        }

        if ($userService->isUsernameAlreadyExist($username)) {
            return $response->getApiResponse(array(), "username.already.exist", 400, array("signup_username" => $translator->trans("username.already.exist")));
        }

        $user->setUsername($username);

        /**
         * Validation Genre
         */
        $genre = $request->request->get('genre');

        if (is_null($genre) || ($genre != Gender::MALE && $genre != Gender::FEMALE)) {
            return $response->getApiResponse(array(), "genre.invalid", 400, array("signup_genre" => $translator->trans("genre.invalid")));
        }

        $user->setGender($genre);

        /**
         * Validation Timezone
         */
        $timezoneId = $request->request->get('timezone');

        $listTimezone = $calendarTimezoneService->getFormattedListTimezone();

        if (!array_key_exists($timezoneId,$listTimezone)) {
            return $response->getApiResponse(array(), "invalid.timezone", 400, array("signup_timezone" => $translator->trans("invalid.timezone")));
        }

        /** @var CalendarTimezone $calendarTimezone */
        $calendarTimezone = $calendarTimezoneService->getById($timezoneId);

        $user->setCalendarTimezone($calendarTimezone);

        /**
         * Validation Terms & Privacy
         */
        $acceptingTerms     = $request->request->get('accepting_terms');
        $acceptingPrivacy   = $request->request->get('accepting_privacy');

        if (is_null($acceptingTerms)) {
            return $response->getApiResponse(array(), "accepting.terms.required", 400, array("signup_terms" => $translator->trans("accepting.terms.required")));
        }

        if (is_null($acceptingPrivacy)) {
            return $response->getApiResponse(array(), "accepting.privacy.required", 400, array("signup_privacy" => $translator->trans("accepting.privacy.required")));
        }

        /**
         * Save User
         */
        $user->setLocale($request->getLocale());
        $userResponse = $userService->signup($user);
        if (is_null($userResponse)) {
            return $response->getApiResponse(array(), "error.save", 500, array("general_error" => $translator->trans("error.save")));
        }

        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_main', serialize($token));

        $targetUrl = $this->generateUrl('show_private_profile', array(/*'username' => $user->getUsernameCanonical()*/), UrlGeneratorInterface::ABSOLUTE_URL);

        return $response->getApiResponse(array("targetUrl" => $targetUrl), "success.save");

    }


    /**
     * This action is used in ajax call to check in real-time if username is valid or not
     *
     * @Route("/public/username/validate", name="username_validation", methods="POST")
     *
     *
     * @param Request $request
     * @param TranslatorInterface $translator
     * @param UserService $userService
     * @param LoggerInterface $logger
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function validationUsernameAction(Request $request,
                                             TranslatorInterface $translator,
                                             UserService $userService,
                                             LoggerInterface $logger
    ) {

        $response = new ResponseUtils($translator, $logger);

        $username = $request->request->get('username');

        if (is_null($username) || !StringUtils::checkUsername($username)) {
            return $response->getApiResponse(array(), "username.invalid", 400, array("signup_username"=>$translator->trans("username.invalid")));
        }

        if ($userService->isUsernameAlreadyExist($username)) {
            return $response->getApiResponse(array(), "username.already.exist", 400, array("signup_username"=>$translator->trans("username.already.exist")));
        }


        return $response->getApiResponse(array(), "success.save");

    }


    /**
     * This action is used in ajax call to check in real-time if username of a logged user is valid or not
     *
     * @Route("/profile/username/validate", name="private_username_validation", methods="POST")
     *
     *
     * @param Request $request
     * @param TranslatorInterface $translator
     * @param UserService $userService
     * @param LoggerInterface $logger
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function validationPrivateUsernameAction(Request $request,
                                                    TranslatorInterface $translator,
                                                    UserService $userService,
                                                    LoggerInterface $logger
    ) {

        $response = new ResponseUtils($translator, $logger);

        $username = $request->request->get('username');
        $key      = $request->request->get('key','signup_username');

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        if (is_null($username) || !StringUtils::checkUsername($username)) {
            return $response->getApiResponse(array(), "username.invalid", 400, array($key=>$translator->trans("username.invalid")));
        }

        if (strtolower($currentUser->getUsername()) != strtolower($username) && $userService->isUsernameAlreadyExist($username)) {
            return $response->getApiResponse(array(), "username.already.exist", 400, array($key=>$translator->trans("username.already.exist")));
        }


        return $response->getApiResponse(array(), "success.save");

    }


    /**
     * This action is used in ajax call to verify password validation of the current user
     *
     * @Route("/profile/verify/password", name="private_verify_password", methods="POST")
     *
     *
     * @param Request $request
     * @param TranslatorInterface $translator
     * @param UserService $userService
     * @param LoggerInterface $logger
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function verifyPasswordAction(Request $request,
                                         TranslatorInterface $translator,
                                         UserService $userService,
                                         LoggerInterface $logger
    ) {

        $response = new ResponseUtils($translator, $logger);

        $submittedToken     = $request->request->get('token');
        $plainPassword   = $request->request->get('password');

        /**
         * Validation of CSRF Token and ReCaptcha
         */
        if (!$this->isCsrfTokenValid('hdn_token_verify_password', $submittedToken)) {
            return $response->getApiResponse(array(), "parameter.token.invalid", 400, array("delete_account_error"=>$translator->trans("parameter.token.invalid")));
        }

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        if(!StringUtils::checkPasswordString($plainPassword)) {
            return $response->getApiResponse(array(), "password.incorrect", 400, array("da_password"=>$translator->trans("password.incorrect")));
        }

        if(!$this->get('security.password_encoder')->isPasswordValid($currentUser, $plainPassword)) {
            return $response->getApiResponse(array(), "user.not.valid.password", 403, array("da_password"=>$translator->trans("password.incorrect")));
        }

        return $response->getApiResponse(array(), "success.save");
    }

    /**
     * This action is used in ajax call to delete the current user account
     *
     * @Route("/profile/account/delete", name="private_delete_account", methods="DELETE")
     *
     *
     * @param Request $request
     * @param TranslatorInterface $translator
     * @param UserService $userService
     * @param LoggerInterface $logger
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function deleteAccountAction(Request $request,
                                        TranslatorInterface $translator,
                                        UserService $userService,
                                        LoggerInterface $logger
    ) {

        $response = new ResponseUtils($translator, $logger);

        $submittedToken     = $request->request->get('token');
        $plainPassword   = $request->request->get('password');

        /**
         * Validation of CSRF Token and ReCaptcha
         */
        if (!$this->isCsrfTokenValid('delete_account_confirmation', $submittedToken)) {
            return $response->getApiResponse(array(), "parameter.token.invalid", 400, array("delete_account_error"=>$translator->trans("parameter.token.invalid")));
        }

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        if(!StringUtils::checkPasswordString($plainPassword)) {
            return $response->getApiResponse(array(), "password.incorrect", 400, array("da_password"=>$translator->trans("password.incorrect")));
        }

        if(!$this->get('security.password_encoder')->isPasswordValid($currentUser, $plainPassword)) {
            return $response->getApiResponse(array(), "user.not.valid.password", 403, array("da_password"=>$translator->trans("password.incorrect")));
        }

        $currentTime = DateUtils::getCurrentTimeUTC();
        $currentUser->setIsDisabled(true);
        $currentUser->setDisabledBy($currentUser->getId());
        $currentUser->setDisabledAt($currentTime);

        if (!$userService->save($currentUser)) {
            return $response->getApiResponse(array(), "error.save", 500, array("change_password_error" => $translator->trans("error.save")));
        }

        $targetUrl = $this->generateUrl('twim_logout', array(), UrlGeneratorInterface::ABSOLUTE_URL);

        return $response->getApiResponse(array("targetUrl" => $targetUrl), "success.save");
    }


    /**
     * This action is used in ajax call to password validation and change of it
     *
     * @Route("/profile/change/password", name="private_change_password", methods="POST")
     *
     *
     * @param Request $request
     * @param TranslatorInterface $translator
     * @param UserService $userService
     * @param LoggerInterface $logger
     * @param ValidationException $validationException
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function changePasswordAction(Request $request,
                                         TranslatorInterface $translator,
                                         UserService $userService,
                                         LoggerInterface $logger,
                                         ValidationException $validationException
    ) {

        $response = new ResponseUtils($translator, $logger);

        $submittedToken     = $request->request->get('token');
        $oldPlainPassword   = $request->request->get('old_password');
        $newPlainPassword   = $request->request->get('new_password');

        /**
         * Validation of CSRF Token and ReCaptcha
         */
        if (!$this->isCsrfTokenValid('change_password', $submittedToken)) {
            return $response->getApiResponse(array(), "parameter.token.invalid", 400, array("change_password_error"=>$translator->trans("parameter.token.invalid")));
        }

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        if(!StringUtils::checkPasswordString($oldPlainPassword)) {
            return $response->getApiResponse(array(), "password.incorrect", 400, array("cp_oldPlainPassword"=>$translator->trans("password.incorrect")));
        }

        if(!StringUtils::checkPasswordString($newPlainPassword)) {
            return $response->getApiResponse(array(), "password.incorrect", 400, array("cp_newPlainPassword"=>$translator->trans("password.incorrect")));
        }

        if($newPlainPassword == $oldPlainPassword) {
            return $response->getApiResponse(array(), "password.equals", 400, array("cp_newPlainPassword"=>$translator->trans("password.equals")));
        }

        if(!$this->get('security.password_encoder')->isPasswordValid($currentUser, $oldPlainPassword)) {
            return $response->getApiResponse(array(), "user.not.valid.password", 403, array("cp_oldPlainPassword"=>$translator->trans("user.not.valid.password")));
        }

        $currentUser->setPlainPassword($newPlainPassword);
        $validator = $this->get('validator');
        $errors = $validator->validate($currentUser, null, array("password"));
        if(count($errors) > 0) {
            $responseError   = array();
            $formattedErrors = $validationException->getFormattedExceptions($errors);
            foreach ($formattedErrors as $formattedError) {
                $responseError["cp_newPlainPassword"] = $formattedError;
                break;
            }
            return $response->getApiResponse(array(), "password.incorrect", 400, $responseError);
        }

        if (!$userService->save($currentUser)) {
            return $response->getApiResponse(array(), "error.save", 500, array("change_password_error" => $translator->trans("error.save")));
        }

        $this->addFlash(
            'success',
            $translator->trans("change.password.success")
        );

        $targetUrl = $this->generateUrl('show_private_profile', array(), UrlGeneratorInterface::ABSOLUTE_URL);

        return $response->getApiResponse(array("targetUrl" => $targetUrl), "success.save");
    }


    /**
     * Confirm a new user account.
     *
     * @Route("/public/user/{user_id}/confirmation/{token}", name="public_user_confirmation", methods="GET")
     * @ParamConverter("user", class="UserBundle:User", options={"id" = "user_id"})
     *
     * @param null|User $user
     * @param $user_id
     * @param $token
     * @param UserService $userService
     * @param LoggerInterface $logger
     * @param TranslatorInterface $translator
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createConfirmAction(?User $user, $user_id, $token, UserService $userService, LoggerInterface $logger, TranslatorInterface $translator) {

        if(!IntegerUtils::checkId($user_id)) {
            return $this->renderError(array("message" => $translator->trans("parameter.id.invalid")));
        }

        if(!StringUtils::checkHashString($token)) {
            return $this->renderError(array("message" => $translator->trans("parameter.token.invalid")));
        }

        if(is_null($user)){
            return $this->renderError(array("message" => $translator->trans("data.not.found.404")));
        }

        if($user->isEnabled()) {
            $this->addFlash(
                'danger',
                $translator->trans("user.already.enabled")
            );

            return $this->redirectToRoute('homepage');
        }

        if(!$userService->confirmCreate($user, $token)) {
            return $this->renderError(array("message" => $translator->trans("token.expired")));
        }

        $this->addFlash(
            'success',
            $translator->trans("successful.user.activation", array("%username%" => $user->getUsername()))
        );

        return $this->redirectToRoute('homepage');
    }

    /**
     * Resend confirmation email.
     *
     * @Route("/public/user/resend/confirmation", name="public_user_resend_confirmation_email", methods="POST")
     *
     * @param Request $request
     * @param UserService $userService
     * @param EmailService $emailService
     * @param TranslatorInterface $translator
     * @param LoggerInterface $logger
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function resendMailConfirmationAction(Request $request, UserService $userService, EmailService $emailService, TranslatorInterface $translator, LoggerInterface $logger) {

        $response = new ResponseUtils($translator, $logger);
        $email    = $request->request->get('email');

//        if(!StringUtils::checkEmailString($email)) {
//            return $response->getApiResponse(array(), "error.email.invalid", 400);
//        }

        /** @var User $user */
        $user = $userService->getByEmail($email);
        if(is_null($user)) {
            return $response->getApiResponse(array(), "error.email.invalid", 400);
        }

        $just_sent_confirmation = $emailService->getJustSentConfirmation($user);
        if($just_sent_confirmation) {
            return $response->getApiResponse(array(), "error.email.sending", 403);
        }

        try {
            $token = $userService->confirmationToken($user);
            $user  = $userService->sendMailRegistrationConfirm($user, $token);
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            $logger->error("Failed resend email registration to user ID: ".$user->getId(), $exception);
            $user = null;
        }

        if(is_null($user)) {
            return $response->getApiResponse(array(), "error.email.sending", 403);
        }

        $emailDecrypted = $userService->getEmailDecrypted($email);
        $mailer_user    = $this->getParameter('mailer_user');

        return $response->getApiResponse(array(
            "message_body"   => $translator->trans('success.email.resending', array("%email%" => $emailDecrypted, "%mailer%" => $mailer_user)),
            "message_footer" => $translator->trans('success.email.resending.footer')
        ), "success.email.sending");
    }


    /**
     * Logout the user
     * @Route("/logout", name="twim_logout", methods={"GET","POST"})
     *
     * @param Request $request
     * @param LoggerInterface $logger
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function logoutAction(Request $request, LoggerInterface $logger) {

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY', null, 'Unable to access this page!');

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        if (!is_null($currentUser)) {
            $this->get('security.token_storage')->setToken(null);
            $request->getSession()->invalidate();

            $logger->info("Logout of user ID: ".$currentUser->getId());
        }

        return $this->redirectToRoute('homepage');
    }


    /**
     * Login redirect the user
     * @Route("/login", name="twim_login_redirect", methods={"GET","POST"})
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function loginRedirectAction() {
        return $this->redirectToRoute('homepage');
    }


    /**
     * Create photo profile.
     *
     * @Route("/profile/upload/picture", name="user_upload_picture", methods={"POST"})
     *
     * @param Request $request
     * @param UserService $userService
     * @param ImageService $imageService
     * @param LoggerInterface $logger
     * @param TranslatorInterface $translator
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function uploadPictureAction(Request $request, UserService $userService, ImageService $imageService, LoggerInterface $logger, TranslatorInterface $translator) {

        $response = new ResponseUtils($translator, $logger);

        $submittedToken     = $request->request->get('token');

        /**
         * Validation of CSRF Token and ReCaptcha
         */
        if (!$this->isCsrfTokenValid('upload_picture', $submittedToken)) {
            return $response->getApiResponse(array(), "parameter.token.invalid", 400, array("upload_picture_error"=>$translator->trans("parameter.token.invalid")));
        }

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        $old_image_hash = $currentUser->getImageHash();

        /** @var Image $image */
        $image = $imageService->init($request, $currentUser);
        if(is_null($image)) {
            return $response->getApiResponse(array(), "image.not.valid", 400, array("upload_picture_error"=>$translator->trans("image.not.valid")));
        }

        $image->setPath($imageService->getSpecificPath('profile_photo'));
        $image = $imageService->save($image);
        if(!$image) {
            return $response->getApiResponse(array(), "error.image.saving", 500, array("upload_picture_error"=>$translator->trans("error.image.saving")));
        }

        if(!FileSystemUtils::saveFile($imageService->getFullPath($image), $imageService->getFileName($image), $request->files->get('image'))) {
            $imageService->remove($image);
            return $response->getApiResponse(array(), "error.image.saving", 500, array("upload_picture_error"=>$translator->trans("error.image.saving")));
        }

        $currentUser->setImageId($image->getId());
        $currentUser->setImageHash($image->getHash());

        if(!$userService->save($currentUser)) {
            FileSystemUtils::deleteFile($imageService->getFullFilePath($image));
            $imageService->remove($image);
            return $response->getApiResponse(array(), "error.image.saving", 500, array("upload_picture_error"=>$translator->trans("error.image.saving")));
        }

        if(!is_null($old_image_hash)) {
            /** @var Image $oldImage */
            $oldImage = $imageService->getByHash($old_image_hash);

            if(!is_null($oldImage)) {
                FileSystemUtils::deleteFile($imageService->getFullFilePath($oldImage));
                $imageService->remove($oldImage);
            }
        }


        $targetUrl = $this->generateUrl('edit_profile_settings', array("username"=>$currentUser->getUsername()), UrlGeneratorInterface::ABSOLUTE_URL);
        return $response->getApiResponse(array("targetUrl" => $targetUrl), "success.save", 201);
    }

    /**
     * Delete photo profile.
     *
     * @Route("/profile/picture/delete", name="user_delete_image", methods={"DELETE"})
     *
     * @param UserService $userService
     * @param ImageService $imageService
     * @param LoggerInterface $logger
     * @param TranslatorInterface $translator
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function deletePictureAction(UserService $userService, ImageService $imageService, LoggerInterface $logger, TranslatorInterface $translator) {

        $response = new ResponseUtils($translator, $logger);

        /** @var User $currentUser */
        $currentUser = $this->getUser();
        $old_image_hash = $currentUser->getImageHash();

        if(!is_null($old_image_hash)) {
            /** @var Image $oldImage */
            $oldImage = $imageService->getByHash($old_image_hash);

            if(!is_null($oldImage)) {
                FileSystemUtils::deleteFile($imageService->getFullFilePath($oldImage));
                $imageService->remove($oldImage);
            }
        }

        $currentUser->setImageId(null);
        $currentUser->setImageHash(null);
        $currentUser->setImageSocialUrl(null);

        if(!$userService->save($currentUser)) {
            return $response->getApiResponse(array(), "error.save", 500, array("error_image"=>$translator->trans("error.save")));
        }

        $targetUrl = $this->generateUrl('edit_profile_settings', array("username"=>$currentUser->getUsername()), UrlGeneratorInterface::ABSOLUTE_URL);
        return $response->getApiResponse(array("targetUrl" => $targetUrl), "success.delete");
    }



}
