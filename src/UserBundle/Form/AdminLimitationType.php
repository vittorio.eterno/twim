<?php

namespace UserBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use UserBundle\Entity\AdminLimitation;


class AdminLimitationType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('roleType', ChoiceType::class, array(
                'choices'  => \Utils\Constants\AdminLimitation::getAllRoles(),
                'choice_translation_domain' => true
            ))
            ->add('area', ChoiceType::class, array(
                'choices'  => \Utils\Constants\AdminLimitation::getAllAreas(),
                'choice_translation_domain' => true
            ))
            ->add('privileges', ChoiceType::class, array(
                'multiple' => true,
                'choices'  => \Utils\Constants\AdminLimitation::getAllPrivileges(),
                'choice_translation_domain' => true
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => AdminLimitation::class,
        ));
    }

}