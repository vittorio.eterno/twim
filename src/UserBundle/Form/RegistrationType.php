<?php

namespace UserBundle\Form;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('gender', ChoiceType::class, array(
                'choices'  => array(
                    'male'   => false,
                    'female' => true,
                ),
                'choice_translation_domain' => true
            ))
            ->add('calendarTimezone', EntityType::class, array(
                'class' => 'ResourceBundle:CalendarTimezone',

                // uses the CalendarTimezone.timezone property as the visible option string
                'choice_label' => 'timezone',
            ))
        ;
    }

    public function getParent() {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix() {
        return 'app_user_registration';
    }

}