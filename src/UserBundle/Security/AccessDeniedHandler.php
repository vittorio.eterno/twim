<?php

namespace UserBundle\Security;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Http\Authorization\AccessDeniedHandlerInterface;
use Symfony\Component\Translation\TranslatorInterface;

class AccessDeniedHandler implements AccessDeniedHandlerInterface {

    /** @var TranslatorInterface $translator */
    private $translator;

    /** @var LoggerInterface $logger */
    private $logger;


    /**
     * AccessDeniedHandler constructor.
     * @param TranslatorInterface $translator
     * @param LoggerInterface $logger
     */
    public function __construct(TranslatorInterface $translator, LoggerInterface $logger){
        $this->translator   = $translator;
        $this->logger       = $logger;
    }


    /**
     * @param Request $request
     * @param AccessDeniedException $accessDeniedException
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function handle(Request $request, AccessDeniedException $accessDeniedException) {

        //TODO ADD LOG

//        $response = new JsonResponse();
//        $response->setStatusCode(403);
//        $response->setData(array(
//            "status"  => 403,
//            "message" => $this->translator->trans('user.not.rights'))
//        );
//        return $response;
    }
}