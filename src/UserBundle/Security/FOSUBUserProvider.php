<?php

namespace UserBundle\Security;


use FOS\UserBundle\Model\UserManagerInterface;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseClass;
use Symfony\Component\Security\Core\User\UserInterface;
use UserBundle\Entity\User;
use UserBundle\Service\UserService;
use Utils\Constants\Gender;


class FOSUBUserProvider extends BaseClass {

    /** @var UserService $userService */
    private $userService;

    /**
     * FOSUBUserProvider constructor.
     * @param UserManagerInterface $userManager
     * @param array $properties
     * @param UserService $userService
     */
    public function __construct(UserManagerInterface $userManager, array $properties, UserService $userService){
        parent::__construct($userManager, $properties);

        $this->userService  = $userService;
    }

    /**
     * {@inheritDoc}
     */
    public function connect(UserInterface $user, UserResponseInterface $response) {

        // get property from provider configuration by provider name
        $property = $this->getProperty($response);
        $username = $response->getUsername();

        // On connect, retrieve the access token and the user id
        $service = $response->getResourceOwner()->getName();

        $setter = 'set' . ucfirst($service);
        $setter_id = $setter . 'Id';
        $setter_token = $setter . 'AccessToken';

        // Disconnect previously connected users
        if (null !== $previousUser = $this->userManager->findUserBy(array($property => $username))) {
            $previousUser->$setter_id(null);
            $previousUser->$setter_token(null);
            $this->userManager->updateUser($previousUser);
        }

        // Connect using the current user
        $user->$setter_id($username);
        $user->$setter_token($response->getAccessToken());
        $this->userManager->updateUser($user);
    }

    /**
     * {@inheritDoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response) {

        $data       = $response->getData(); // get data setted in infos_url option in config.yml
        $username   = $response->getUsername();
        $email      = $response->getEmail() ? $response->getEmail() : $username;
        $firstName  = $response->getFirstName() ? $response->getFirstName() : null;
        //$user       = $this->userManager->findUserBy(array($this->getProperty($response) => $username));
        $user       = $this->userService->getUserByUsernameOrEmail($email);

        $urlProfilePicture = $response->getProfilePicture();


        $service = $response->getResourceOwner()->getName();
        $setter = 'set' . ucfirst($service);
        $setter_id = $setter . 'Id';
        $setter_token = $setter . 'AccessToken';

        // If the user is new
        if (null === $user) {

            // create new user here
            /** @var User $user */
            $user = $this->userManager->createUser();
            $user->$setter_id($username);
            $user->$setter_token($response->getAccessToken());
            $user->setImageSocialUrl($urlProfilePicture);

            $gender = isset($data["gender"]) ? $user->getGenderBySocial($data["gender"]) : Gender::MALE;
            $locale = isset($data["locale"]) ? $data["locale"] : null;

            $user   = $this->userService->setBySocial($user,$username, $email, $response->getResourceOwner()->getName(), $gender, $locale, $firstName);

            $this->userManager->updateUser($user);

            return $user;

        } else {
            $user->$setter_id($username);
            $user->$setter_token($response->getAccessToken());
            $this->userManager->updateUser($user);
        }

        // If the user exists, use the HWIOAuth
        $user = parent::loadUserByOAuthUserResponse($response);

        $serviceName = $response->getResourceOwner()->getName();

        $setter = 'set' . ucfirst($serviceName) . 'AccessToken';

        // Update the access token
        $user->$setter($response->getAccessToken());

        return $user;
    }


}