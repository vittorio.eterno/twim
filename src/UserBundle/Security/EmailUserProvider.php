<?php

namespace UserBundle\Security;

use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Security\UserProvider;
use UserBundle\Entity\User;
use UserBundle\Service\UserService;

class EmailUserProvider extends UserProvider {


    /** @var UserService $userService */
    private $userService;


    /**
     * EmailUserProvider constructor.
     * @param UserManagerInterface $userManager
     * @param UserService $userService
     */
    public function __construct(UserManagerInterface $userManager, UserService $userService) {
        parent::__construct($userManager);
        $this->userService = $userService;
    }


    /**
     * {@inheritdoc}
     */
    protected function findUser($username) {
//        $user = $this->userManager->findUserByUsernameOrEmail($username);

        $user = $this->userService->getUserByUsernameOrEmail($username);

        if (!is_null($user) && $user instanceof User) {
            if($user->isLocked() || $user->getIsDisabled()) {
                return null;
            }
        }

        return $user;

    }

}
