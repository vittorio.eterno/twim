<?php

namespace UserBundle\Service;


use Psr\Log\LoggerInterface;
use ResourceBundle\Entity\Country;
use Schema\EntityService;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use UserBundle\Entity\ShippingAddress;
use UserBundle\Entity\User;
use UserBundle\Repository\ShippingAddressRepository;
use Utils\Service\Encryption;
use Utils\StaticUtil\DateUtils;
use Utils\StaticUtil\LogUtils;

class ShippingAddressService extends EntityService {

    /** @var Encryption $encryption */
    private $encryption;

    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * ShippingAddressService constructor.
     * @param ShippingAddressRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param Encryption $encryption
     * @param LoggerInterface $logger
     */
    public function __construct(ShippingAddressRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                Encryption $encryption,
                                LoggerInterface $logger
    ) {
        $this->repository = $repository;
        $this->logger     = $logger;
        $this->encryption = $encryption;
        parent::__construct($repository, $authorizationChecker, $logger);
    }

    /**
     * @param ShippingAddress $shippingAddress
     * @param User $user
     * @param Country $country
     * @param $fields
     * @return array|null
     */
    public function saveAddress (ShippingAddress &$shippingAddress, User &$user, Country &$country, &$fields) {

        $currentTime            = DateUtils::getCurrentTimeUTC();

        $firstName              = isset($fields["first_name"]) && !empty($fields["first_name"]) ? $this->encryption->encrypting($fields["first_name"]) : null;
        $lastName               = isset($fields["last_name"]) && !empty($fields["last_name"]) ? $this->encryption->encrypting($fields["last_name"]) : null;
        $companyName            = isset($fields["company_name"]) && !empty($fields["company_name"]) ? $this->encryption->encrypting($fields["company_name"]) : null;
        $companyAddressInfo     = isset($fields["company_address_info"]) && !empty($fields["company_address_info"]) ? $this->encryption->encrypting($fields["company_address_info"]) : null;
        $streetAddress          = isset($fields["street_address"]) && !empty($fields["street_address"]) ? $this->encryption->encrypting($fields["street_address"]) : null;
        $addressDetail          = isset($fields["address_detail"]) && !empty($fields["address_detail"]) ? $this->encryption->encrypting($fields["address_detail"]) : null;
        $city                   = isset($fields["city"]) && !empty($fields["city"]) ? $this->encryption->encrypting($fields["city"]) : null;
        $stateProvince          = isset($fields["state_province"]) && !empty($fields["state_province"]) ? $this->encryption->encrypting($fields["state_province"]) : null;
        $postalCode             = isset($fields["postal_code"]) && !empty($fields["postal_code"]) ? $this->encryption->encrypting($fields["postal_code"]) : null;
        $email                  = isset($fields["email"]) && !empty($fields["email"]) ? $this->encryption->encrypting($fields["email"]) : null;
        $phone                  = isset($fields["phone"]) && !empty($fields["phone"]) ? $this->encryption->encrypting($fields["phone"]) : null;
        $hash                   = $this->generateHash($user, $fields);

        $shippingAddress->setFirstName($firstName);
        $shippingAddress->setLastName($lastName);
        $shippingAddress->setCompanyName($companyName);
        $shippingAddress->setCompanyAddressInfo($companyAddressInfo);
        $shippingAddress->setStreetAddress($streetAddress);
        $shippingAddress->setAddressDetail($addressDetail);
        $shippingAddress->setCity($city);
        $shippingAddress->setStateProvince($stateProvince);
        $shippingAddress->setPostalCode($postalCode);
        $shippingAddress->setCountry($country);
        $shippingAddress->setEmail($email);
        $shippingAddress->setPhone($phone);
        $shippingAddress->setCreatedAt($currentTime);
        $shippingAddress->setIsDisabled(false);
        $shippingAddress->setLastUsedAt($currentTime);
        $shippingAddress->setUser($user);
        $shippingAddress->setHash($hash);

//        $user->addShippingAddress($shippingAddress);
//        $this->doPersist($user);

        if (!$this->save($shippingAddress)) {
            return null;
        }

        return $this->getFormattedByLastUsed($user, 'DESC');

    }

    /**
     * @param User $user
     * @param string $order
     * @return array|mixed
     */
    public function getActiveLastUsedByUser (User &$user, $order='DESC') {
        return $this->repository->findActiveLastUsedByUser($user, $order);
    }

    /**
     * @param User $user
     * @param string $order
     * @return array
     */
    public function getFormattedByLastUsed (User &$user, $order='DESC') {
        $latestUsed = $this->getActiveLastUsedByUser($user, $order);

        $formatted = array();

        if (!empty($latestUsed) && count($latestUsed)>0) {
            /** @var ShippingAddress $shippingAddress */
            foreach ($latestUsed as $shippingAddress) {
                $formatted[] = $shippingAddress->listDecryptedSerializer($this->encryption);
            }
        }

        return $formatted;
    }

    /**
     * @param User $user
     * @param $fields
     * @return bool
     */
    public function isAlreadyExist (User &$user, &$fields) {
        $hash   = $this->generateHash($user, $fields);
        $result = $this->getByHash($hash);
        return !is_null($result);
    }

    /**
     * @param User $user
     * @param $fields
     * @return string
     */
    private function generateHash (User &$user, &$fields) {

        $firstName              = isset($fields["first_name"]) && !empty($fields["first_name"]) ? trim(strtolower($fields["first_name"])) : null;
        $lastName               = isset($fields["last_name"]) && !empty($fields["last_name"]) ? trim(strtolower($fields["last_name"])) : null;
        $streetAddress          = isset($fields["street_address"]) && !empty($fields["street_address"]) ? trim(strtolower($fields["street_address"])) : null;
        $addressDetail          = isset($fields["address_detail"]) && !empty($fields["address_detail"]) ? trim(strtolower($fields["address_detail"])) : null;
        $country                = isset($fields["country"]) && !empty($fields["country"]) ? trim(strtolower($fields["country"])) : null;
        $city                   = isset($fields["city"]) && !empty($fields["city"]) ? trim(strtolower($fields["city"])) : null;
        $stateProvince          = isset($fields["state_province"]) && !empty($fields["state_province"]) ? trim(strtolower($fields["state_province"])) : null;
        $postalCode             = isset($fields["postal_code"]) && !empty($fields["postal_code"]) ? trim(strtolower($fields["postal_code"])) : null;
        $email                  = isset($fields["email"]) && !empty($fields["email"]) ? trim(strtolower($fields["email"])) : null;
        $userId                 = $user->getId();

        $stringToHash = "Th1S H@asH TO Sh1p@DDre3SS ".$firstName.$userId.$lastName. " --- ".$streetAddress.$addressDetail." _:; ".$country." Cc ". $city.$stateProvince. "pol".$postalCode."EM@ - .".$email;

        return hash('sha384', $stringToHash);
    }

    /**
     * @param User $user
     * @return int
     */
    public function getCountAddresses (User &$user) {
        $count  = 0;
        $result = $this->repository->findCountAddresses($user);

        if (!is_null($result) && isset($result["counted"])) {
            $count = $result["counted"];
        }

        return $count;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function isReachedLimit (User &$user) {
        $count = $this->getCountAddresses($user);
        return $count >= ShippingAddress::LIMIT_USER_ADDRESSES;
    }

    /**
     * @param ShippingAddress $shippingAddress
     * @return bool
     */
    public function deleteAddress (ShippingAddress &$shippingAddress) {

        $this->doRemove($shippingAddress);
        return $this->doFlush();

    }


}