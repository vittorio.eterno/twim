<?php

namespace UserBundle\Service;


use Psr\Log\LoggerInterface;
use ResourceBundle\Entity\CalendarTimezone;
use UserBundle\Entity\ExternalLink;
use UserBundle\Entity\ExternalLinkStructure;
use UserBundle\Entity\Setting;
use UserBundle\Entity\User;
use UserBundle\Entity\UserSetting;
use UserBundle\Repository\UserSettingRepository;
use Schema\Entity;
use Schema\EntityService;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Utils\StaticUtil\DateUtils;
use Utils\StaticUtil\StringUtils;

class UserSettingService extends EntityService {

    /** @var SettingService $settingService */
    private $settingService;

    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * UserSettingService constructor.
     * @param UserSettingRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param SettingService $settingService
     * @param LoggerInterface $logger
     */
    public function __construct(UserSettingRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                SettingService $settingService,
                                LoggerInterface $logger
    ) {
        $this->repository = $repository;
        parent::__construct($repository, $authorizationChecker, $logger);

        $this->settingService = $settingService;
    }


    /**
     * @param User $user
     * @return array
     */
    public function getFormattedView (User &$user) {

        $formatted = $user->viewProfileSerializer();

        return $formatted;

    }


    /**
     * @param User $user
     * @param null|string $emailCrypted
     * @param string $username
     * @param $genre
     * @param null|string $language
     * @param null|CalendarTimezone $calendarTimezone
     * @param array $externalLinks
     * @param array $externalLinkStructures
     * @param array $settingsToAdd
     * @param array $settingIdsToRemove
     * @return null|User
     */
    public function saveSettings (User &$user, ?string $emailCrypted, string $username, $genre, ?string $language, ?CalendarTimezone $calendarTimezone, array $externalLinks, array $externalLinkStructures, array $settingsToAdd, array $settingIdsToRemove) {

        $locale = StringUtils::checkLanguage($language);

        $user->setLocale($locale);
        $user->setUsername($username);
        $user->setGender($genre);

        if (!is_null($calendarTimezone)) {
            $user->setCalendarTimezone($calendarTimezone);
        }

        if (!empty($externalLinkStructures) && count($externalLinkStructures)>0 && !empty($externalLinks) && count($externalLinks)>0) {
            $this->saveExternalLinks($user, $externalLinks, $externalLinkStructures);
        }

        if (!empty($settingIdsToRemove) && count($settingIdsToRemove)>0) {
            $this->removeUserSettings($user, $settingIdsToRemove);
        }

        if (!empty($settingsToAdd) && count($settingsToAdd)>0) {
            $this->saveNewUserSettings($user, $settingsToAdd);
        }


        if (!$this->save($user)) {
            return null;
        }

        return $user;

//        /**
//         * If email is changed, send an email with confirmation token
//         */
//        if (!is_null($emailCrypted)) {
////            $user->setEmailToChange($emailCrypted);
//        }
        //email + invio email di conferma
    }


    /**
     * @param User $user
     * @param $settingIdsToRemove
     */
    private function removeUserSettings (User &$user, $settingIdsToRemove) {

        $currentTimeUTC     = DateUtils::getCurrentTimeUTC();

        if (!$user->getUserSettings()->isEmpty()) {
            /** @var UserSetting $userSetting */
            foreach ($user->getUserSettings() as $userSetting) {
                $setting = $userSetting->settingSerializer();
                if (isset($setting["settingId"]) && !empty($setting["settingId"])) {

                    foreach ($settingIdsToRemove as $settingIdToRemove) {
                        if ($setting["settingId"] == $settingIdToRemove) {
                            $user->removeSetting($userSetting);
                            $userSetting->setDisabledAt($currentTimeUTC);
                            $userSetting->setDisabledBy($user->getId());
                            $userSetting->setIsDisabled(true);
                            $this->doMerge($userSetting);
                        }
                    }

                }
            }
        }

        $this->doFlush();
    }

    /**
     * @param User $user
     * @param $settingsToAdd
     * @return bool
     */
    private function saveNewUserSettings (User &$user, $settingsToAdd) {

        $currentTimeUTC     = DateUtils::getCurrentTimeUTC();


        if (!$user->getUserSettings()->isEmpty()) {

            /** @var UserSetting $userSetting */
            foreach ($user->getUserSettings() as $userSetting) {

                /** @var Setting $setting */
                $setting = $userSetting->getSetting();

                /** @var Setting $settingToAdd */
                foreach ($settingsToAdd as $k=>$settingToAdd) {

                    if ($settingToAdd === $setting) {
                        $userSetting->setIsDisabled(false);

                        $this->doMerge($user);

                        unset($settingsToAdd[$k]);
                    }

                }

            }
        }

        if (!empty($settingsToAdd) && count($settingsToAdd)>0) {

            /** @var Setting $settingToAdd */
            foreach ($settingsToAdd as $settingToAdd) {

                $userSetting = new UserSetting();
                $userSetting->setCreatedAt($currentTimeUTC);
                $userSetting->setUser($user);
                $userSetting->setSetting($settingToAdd);
                $userSetting->setValue(1);
                $userSetting->setIsDisabled(false);

                $userSettingMerged = $this->doMerge($userSetting);

                if ($userSettingMerged instanceof UserSetting) {
                    $user->addSetting($userSettingMerged);
                    $this->doMerge($user);
                }

            }
        }

        $this->doFlush();

        return true;
    }

    /**
     * @param User $user
     * @param $externalLinks
     * @param $externalLinkStructures
     * @return bool
     */
    private function saveExternalLinks (User &$user, $externalLinks, &$externalLinkStructures) {

        $currentTimeUTC     = DateUtils::getCurrentTimeUTC();

        if (!$user->getExternalLinks()->isEmpty()) {

            /** @var ExternalLink $userExternalLink */
            foreach ($user->getExternalLinks() as $userExternalLink) {

                /** @var ExternalLinkStructure $externalLinkStructure */
                $externalLinkStructure = $userExternalLink->getExternalLinkStructure();
                $sourceStructure = $externalLinkStructure->getSource();

                foreach ($externalLinks as $source=>$value) {

                    if ($sourceStructure == $source) {
                        $userExternalLink->setValue($value);
                        $userExternalLink->setEditedAt($currentTimeUTC);
                        $userExternalLink->setEditedBy($user->getId());

                        $this->doMerge($user);

                        unset($externalLinks[$source]);
                    }

                }

            }

            $this->doFlush();
        }

        if (!empty($externalLinks) && count($externalLinks)>0) {

            /** @var ExternalLinkStructure $externalLinkStructure */
            foreach ($externalLinkStructures as $externalLinkStructure) {
                $sourceStructure = $externalLinkStructure->getSource();

                foreach ($externalLinks as $source=>$value) {

                    if ($sourceStructure == $source && !empty($value)) {
                        $externalLinkObj = new ExternalLink();
                        $externalLinkObj->setCreatedAt($currentTimeUTC);
                        $externalLinkObj->setExternalLinkStructure($externalLinkStructure);
                        $externalLinkObj->setIsDisabled(false);
                        $externalLinkObj->setValue($value);
                        $externalLinkObj->setUser($user);

                        $externalLinkMerged = $this->doMerge($externalLinkObj);
                        if ($externalLinkMerged instanceof ExternalLink) {
                            $user->addExternalLink($externalLinkMerged);
                            $this->doMerge($user);
                        }

                    }

                }

            }


            $this->doFlush();
        }

        return true;
    }

}