<?php

namespace UserBundle\Service;

use Doctrine\ORM\Tools\Pagination\Paginator;
use EmailBundle\Entity\Email;
use EmailBundle\Service\EmailService;
use FOS\UserBundle\Model\UserManagerInterface;
use Psr\Log\LoggerInterface;
use Schema\Entity;
use Schema\EntityService;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Twig_Environment;
use UserBundle\Entity\AdminLimitation;
use UserBundle\Entity\ExternalLink;
use UserBundle\Entity\ExternalLinkStructure;
use UserBundle\Entity\User;
use UserBundle\Repository\UserRepository;
use Utils\Constants\Gender;
use Utils\Constants\Role;
use Utils\Constants\System;
use Utils\Service\Encryption;
use Utils\Service\FigureIdentifier;
use Utils\StaticUtil\DateUtils;
use Utils\StaticUtil\LogUtils;
use Utils\StaticUtil\PaginationUtils;
use WordBundle\Entity\Word;
use WordBundle\Service\WordService;

class UserService extends EntityService {

    ///////////////////////////////////////////
    /// CONSTRUCTOR

    private $um;

    private $encoder;

    private $mailer;

    /** @var Twig_Environment $templating */
    protected $templating;

    /**
     * Number of seconds delay between tow resets
     * @var integer $delay_reset
     */
    public $delay_reset = 3600;

    /**
     * Number of seconds credentials validity
     * @var integer $timeout_credentials
     */
    public $timeout_credentials = 31500000;

    /** @var TranslatorInterface $translator */
    private $translator;

    /** @var FigureIdentifier $figureIdentifier */
    private $figureIdentifier;

    /** @var Encryption $encryption */
    private $encryption;

    /** @var RouterInterface $router */
    private $router;

    /** @var WordService $wordService */
    private $wordService;

    /**
     * UserService constructor.
     * @param UserRepository $repository
     * @param TranslatorInterface $translator
     * @param LoggerInterface $logger
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param UserManagerInterface $userManager
     * @param UserPasswordEncoderInterface $encoder
     * @param EmailService $mailer
     * @param Twig_Environment $templating
     * @param FigureIdentifier $figureIdentifier
     * @param Encryption $encryption
     * @param RouterInterface $router
     * @param WordService $wordService
     */
    public function __construct(
        UserRepository $repository,
        TranslatorInterface $translator,
        LoggerInterface $logger,
        AuthorizationCheckerInterface $authorizationChecker,
        UserManagerInterface $userManager,
        UserPasswordEncoderInterface $encoder,
        EmailService $mailer,
        Twig_Environment $templating,
        FigureIdentifier $figureIdentifier,
        Encryption $encryption,
        RouterInterface $router,
        WordService $wordService
    ) {
        $this->um               = $userManager;
        $this->encoder          = $encoder;
        $this->repository       = $repository;
        $this->mailer           = $mailer;
        $this->templating       = $templating;
        $this->logger           = $logger;
        $this->translator       = $translator;
        $this->figureIdentifier = $figureIdentifier;
        $this->encryption       = $encryption;
        $this->router           = $router;
        $this->wordService      = $wordService;

        parent::__construct($repository, $authorizationChecker, $logger);
    }

    /**
     * Get an user given its email.
     * NULL will be returned if the user does not exist or it is locked or its creator is locked.
     *
     * @param $email
     * @return null|User
     */
    public function getByEmail($email) {
        /** @var User $user */
        $user = $this->repository->findByEmail($email);

        if(is_null($user) || $user->isLocked() || $user->getIsDisabled()) {
            return null;
        }

        return $user;
    }

    /**
     * @param $email
     * @return bool
     */
    public function checkIfExistByEmail($email){
        $userId = $this->repository->findIdByEmail($email);
        return !is_null($userId);
    }

    /**
     * @param $username
     * @return null|User
     */
    public function getByUsername($username) {
        /** @var User $user */
        $user = $this->repository->findByUsername($username);

        if(is_null($user) || $user->isLocked() || $user->getIsDisabled()) {
            return null;
        }

        return $user;
    }

    /**
     * @param $username
     * @return mixed|null
     */
    public function getIdByUsername ($username) {
        return $this->repository->findIdByUsername($username);
    }


    /**
     * @param $username
     * @return bool
     */
    public function isUsernameAlreadyExist($username) {
        $result = $this->getIdByUsername($username);
        return !is_null($result);
    }

    /**
     * @param  User $user
     * @param  null $plainPassword
     * @return bool
     */
    public function isPasswordValid(User $user, $plainPassword=null) {
        if(is_null($plainPassword)) {
            $plainPassword = $user->getPlainPassword();
        }
        return $this->encoder->isPasswordValid($user, $plainPassword);
    }


    /**
     * @param User $user
     * @return null|User
     */
    public function signup (User &$user) {

        $currentUTCTime = DateUtils::getCurrentTimeUTC();

        $user->setIsTermsPrivacyAccepted(true);
        $user->setCreatedAt($currentUTCTime);
        $user->addRole(Role::ROLE_USER);
        $user->setCredentialsExpired(false);

        if (!$this->save($user)) {
            return null;
        }

        try {
            $token = $this->confirmationToken($user);
            $user = $this->sendMailRegistrationConfirm($user, $token);
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            $this->logger->error("Failed send email registration to user ID: ".$user->getId(), $exception);
        }

//        try {
//            $this->um->updateUser($user);
//        } catch (\Exception $e) {
//            $exception = LogUtils::getFormattedExceptions($e);
//            $this->logger->error("Failed user creation: ".$user->getUsername(), $exception);
//            return null;
//        }

        return $user;
    }


    /**
     * @param User $user
     * @return null|User
     */
    public function signupFromSocial (User &$user) {

        try {
            $user->setIsTermsPrivacyAccepted(true);
            $this->save($user);
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            $this->logger->error("Failed save signup from social user ID: ".$user->getId(), $exception);
            return null;
        }

        return $user;
    }


    /**
     * @param User $user
     * @return string
     */
    public function getSocialToken (User $user) {
        if (!is_null($user)) {
            $hash = $user->getId(). " S0c1Al 1ngReSS!@! " . $user->getUsername() . " AND " . $user->getEmail();
            return hash("sha384", $hash);
        }
        return "";
    }


    /**
     * Given the user returns a random token
     * @param User $user
     * @return string
     */
    public function generateToken(User $user) {
        return sha1(md5(rand(0,1000)).$user->getEmail().$user->getPassword().time());
    }

    /**
     * Given the user and token returns the mail confirmation link
     *
     * @param User $user
     * @param $token
     * @return string
     */
    private function urlConfirmRegistration(User &$user, $token) {

        return $this->router->generate(
            'public_user_confirmation',
            array('user_id' => $user->getId(), 'token' => $token),
            UrlGeneratorInterface::ABSOLUTE_URL
        );
    }

    /**
     * @param User $user
     * @return string
     */
    public function confirmationToken(User $user) {
        $token = $user->getConfirmationToken();
        if(is_null($token)) {
            $token = $this->generateToken($user);
        }

        $user->setConfirmationToken($token);

        return $token;
    }

    /**
     * Sends the registration mail asking to confirm the mail address
     *
     * @param User $user
     * @param $token
     * @return null|User
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function sendMailRegistrationConfirm(User &$user, $token) {
        $url = $this->urlConfirmRegistration($user, $token);

        $subject   = $this->translator->trans("subject.registration.confirm.email");

        $bodyMessageSource   = array();
        $bodyMessageSource[] = $this->translator->trans("body.email.registration.confirm", array("%username%"=>$user->getUsername(),"%confirmationUrl%"=>$url));

        try {
            if (!$this->mailer->emailEnqueue(
                $user->getEmail(),
                $subject,
                $this->templating->render(
                    ":Email:registration.email.twig",
                    array(
                        'subject'  => $subject,
                        'messages' => $bodyMessageSource
                    )
                ),
                $user,
                null,
                Email::TYPE_REGISTRATION_CONFIRM
            )) {
                return null;
            }
        } catch (\Twig_Error_Loader $e) {
            throw $e;
        } catch (\Twig_Error_Runtime $e) {
            throw $e;
        } catch (\Twig_Error_Syntax $e) {
            throw $e;
        }

        return $user;
    }

    /**
     * Given the user and relative token confirms email validity
     *
     * @param User $user
     * @param $token
     * @return bool|null
     */
    public function confirmCreate(User &$user, $token) {
        if($user->getConfirmationToken() != $token)
            return false;

        $user->setEnabled(true);
        $user->setConfirmationToken($this->generateToken($user));

        if (!$this->save($user)) {
            return null;
        }

        return true;
    }

    /**
     * Given the user and token returns the reset link
     * @param User $user
     * @param string $token
     * @return string
     */
    private function urlConfirmReset(User $user, $token) {

        return $this->router->generate(
            'public_user_reset_password_confirmation',
            array('user_id' => $user->getId(), 'token' => $token),
            UrlGeneratorInterface::ABSOLUTE_URL
        );

    }

    /**
     * Sends the reset mail asking to confirm the password reset
     *
     * @param User $user
     * @return null|User
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    private function sendMailResetConfirm(User $user) {
        $token = $this->confirmationToken($user);
        $url   = $this->urlConfirmReset($user, $token);

        $subject = $this->translator->trans("subject.reset.email");

        $bodyMessageSource = array();
        $bodyMessageSource[] = $this->translator->trans("body.email.reset", array("%username%"=>$user->getUsername(),"%confirmationUrl%"=>$url));

        try {
            if (!$this->mailer->emailEnqueue(
                $user->getEmail(),
                $subject,
                $this->templating->render(
                    "Email/password_resetting.email.twig",
                    array(
                        'subject'  => $subject,
                        'messages' => $bodyMessageSource
                    )
                ),
                $user,
                null,
                Email::TYPE_PASSWORD_RESETTING
            )) {
                return null;
            }
        } catch (\Twig_Error_Loader $e) {
            throw $e;
        } catch (\Twig_Error_Runtime $e) {
            throw $e;
        } catch (\Twig_Error_Syntax $e) {
            throw $e;
        }

        return $user;
    }

    /**
     * Start password reset procedure.
     * Returns NULL if some error occurs otherwise it returns the persisted object.
     *
     * @param $user
     * @return null|User
     */
    public function reset($user) {
        /** @var User $user */
        $requested = DateUtils::getCurrentTimeUTC();
        $user->setPasswordRequestedAt($requested);

        $userId = $user->getId();

        try {

            $user = $this->sendMailResetConfirm($user);

            if (is_null($user)) {
                return null;
            }

            if (!$this->save($user)) {
                return null;
            }

        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            $this->logger->error("Failed send email resetting password to user ID: ".$userId, $exception);
        }

        return $user;
    }

    /**
     * Update credentials.
     * @param User $user
     */
    public function updateCredentials(User &$user) {
        $expiration = new \DateTime();
        $expiration->setTimestamp(time()+$this->timeout_credentials);
        $password = $this->encoder->encodePassword($user, $user->getPlainPassword());

        $user->setCredentialsExpired(false);
        $user->setCredentialsExpireAt($expiration);
        $user->setPassword($password);

        return;
    }


    /**
     * @param User $user
     * @param null|string $plainPassword
     * @return string
     */
    public function getEncodedPassword (User &$user, string $plainPassword=null) {
        if (is_null($plainPassword)) {
            $plainPassword = $user->getPlainPassword();
        }

        return $this->encoder->encodePassword($user, $plainPassword);
    }


    /**
     * Given the user and relative token confirms password reset.
     *
     * @param User $user
     * @param $token
     * @return bool|User
     */
    public function confirmReset(User &$user, $token) {
        if ($user->getConfirmationToken() != $token) {
            return false;
        }

        $this->updateCredentials($user);
        $user->setConfirmationToken($this->generateToken($user));

        $userId = $user->getId();

        try {
            if(!$this->save($user)) {
                return false;
            }
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            $this->logger->error("Failed save confirm reset password to user ID: ".$userId, $exception);
        }

        return $user;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function isAdminAuthorized (User &$user) {

        if( $user->isEnabled() && (
                in_array("ROLE_ADMIN", $user->getRoles()) ||
                in_array("ROLE_SUPER_ADMIN", $user->getRoles())
            )
        ) {
            return true;
        }

        return false;
    }


    /**
     * @return array|mixed
     */
    public function getAllOldPending () {
        return $this->repository->findAllOldPending();
    }

    /**
     * @param int $page
     * @param null|string $suggest
     * @param int $maxResults
     * @return array|mixed
     */
    public function getAllPaginatedUsers ($page=1, ?string $suggest, $maxResults=UserRepository::DEFAULT_MAX_RESULTS_USERS) {
        return $this->repository->findAllPaginatedUsers($page,$suggest,$maxResults);
    }

    /**
     * @param int $page
     * @param null|string $suggest
     * @param int $maxResults
     * @return array
     */
    public function getPaginatedList ($page=1, ?string $suggest, $maxResults=UserRepository::DEFAULT_MAX_RESULTS_USERS) {
        $users = $this->getAllPaginatedUsers($page, $suggest, $maxResults);
        return $this->getFormattedListUsers($users,$page,$maxResults);
    }

    /**
     * @param $users
     * @param $page
     * @param $maxResults
     * @return array
     */
    private function getFormattedListUsers (&$users,$page,$maxResults) {

        $formatted = array(
            "paginator" => array(),
            "users"     => array());

        if (!empty($users) && count($users)>0) {

            $formatted["paginator"] = PaginationUtils::getPaginatorResponse($users,$page,$maxResults);

            $i = 0;

            /** @var User $user */
            foreach ($users as $user) {
                $formatted["users"][$i] = $user->adminSerializer();
                $i++;
            }
        }

        return $formatted;

    }


    /**
     * @param User $user
     * @param User $userTarget
     * @param $locking
     * @return User
     */
    public function adminLocking (User &$user, User &$userTarget, $locking) {

        $currentTime = DateUtils::getCurrentTimeUTC();

        $userTarget->setLockedAt($currentTime);
        $userTarget->setLockedBy($user->getId());

        if ($locking == System::LOCKING_LOCK) {
            $userTarget->setLocked(true);
        } else {
            $userTarget->setLocked(false);
        }


        try {
            $this->save($userTarget);
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            $this->logger->error("Failed locking user", $exception);
        }

        return $userTarget;

    }


    /**
     * @param User $user
     * @param User $userTarget
     * @param $enabling
     * @return User
     */
    public function adminEnabling (User &$user, User &$userTarget, $enabling) {

        $currentTime = DateUtils::getCurrentTimeUTC();

        $userTarget->setEditedAt($currentTime);
        $userTarget->setEditedBy($user->getId());

        if ($enabling == System::ENABLING_ENABLE) {
            $userTarget->setEnabled(true);
        } else {
            $userTarget->setEnabled(false);
        }


        try {
            $this->save($userTarget);
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            $this->logger->error("Failed enabling user", $exception);
        }

        return $userTarget;

    }


    /**
     * @param User $user
     * @return array
     */
    public function getAdminFormattedUser (User &$user, $page) {

        $i         = 0;
        $formatted = $user->adminSerializer();

        $formatted["figureName"] = $this->figureIdentifier->getFigureName($user);

//        /**
//         * Get External Links
//         */
//        $formatted["externalLinks"] = array();
//        if (!$user->getExternalLinks()->isEmpty()) {
//            /** @var ExternalLink $externalLink */
//            foreach ($user->getExternalLinks() as $externalLink) {
//                $formatted["externalLinks"][$i] = $externalLink->adminSerializer();
//
//                /** @var ExternalLinkStructure $externalLinkStructure */
//                $externalLinkStructure = $externalLink->getExternalLinkStructure();
//                $formatted["externalLinks"][$i]["source"] = $this->translator->trans($externalLinkStructure->getSource());
//
//                $i++;
//            }
//        }

        /**
         * Get Words
         */
        $formatted["wordsList"] = $this->wordService->getPaginatedListByUser($user, $page, null);
//        $formatted["words"] = array();
//        if (!$user->getWords()->isEmpty()) {
//            /** @var Word $word */
//            foreach ($user->getWords() as $word) {
//                $formatted["words"][$i]           = $word->adminSerializer();
//
//                $i++;
//            }
//        }


        /**
         * Get Admin Limitations
         */
        $formatted["adminLimitations"] = array();
        if (!$user->getAdminLimitations()->isEmpty()) {
            /** @var AdminLimitation $adminLimitation */
            foreach ($user->getAdminLimitations() as $adminLimitation) {
                $formatted["adminLimitations"][$i] = $adminLimitation->adminSerializer();

                $i++;
            }
        }


        return $formatted;
    }


    /**
     * @param User $user
     * @param $page
     * @param bool $isPublic
     * @return array
     */
    public function getFormattedProfile (User &$user, $page, $isPublic=false) {

        $formatted = $user->viewProfileSerializer();

        $formatted["isPublic"]  = $isPublic;
        $formatted["wordsList"] = $this->wordService->getPaginatedListByUser($user, $page, null);

        return $formatted;

    }


    /**
     * @param User $currentUser
     * @param User $user
     * @param string $operation
     * @param string $role
     * @return bool|object
     */
    public function manageRole(User &$currentUser, User &$user, $operation=System::MANAGE_ADD, $role=Role::ROLE_ADMIN) {

        $currentTime = DateUtils::getCurrentTimeUTC();
        $user->setChangedRoleAt($currentTime);
        $user->setChangedRoleBy($currentUser->getId());

        if ($operation == System::MANAGE_ADD) {
            $user->addRole($role);
        } else {
            $user->removeRole($role);
        }

        return $this->save($user);

    }


    /**
     * @param $value
     * @return mixed|null
     */
    public function getUserByUsernameOrEmail ($value) {
        $value = strtolower($value);
        $valueCrypted = $this->encryption->encrypting($value);
        return $this->repository->findUserByUsernameOrEmail($value, $valueCrypted);
    }

    /**
     * @param $emailCrypted
     * @return null|string
     */
    public function getEmailDecrypted ($emailCrypted) {
        return $this->encryption->decrypting($emailCrypted);
    }


    /**
     * Generates a random username with the given
     * e.g 12345_github, 12345_facebook
     *
     * @param string $username
     * @param string $serviceName
     * @return string
     */
    private function generateRandomUsername($username, $serviceName){
        if(!$username){
            $username = "user". uniqid((rand()), true) . $serviceName;
        }

        return $username . $serviceName;
    }

    /**
     * @param $user
     * @param $username
     * @param $serviceName
     * @return string
     */
    private function generateRandomPassword($user, $username, $serviceName) {
        $username = !$username ? "user". uniqid((rand()), true) . $serviceName : $username;
        $password = md5($serviceName . $username . " Random!123 ". uniqid());
        return $this->encoder->encodePassword($user, $password);
    }

    /**
     * @param User $user
     * @param $username
     * @param $email
     * @param $serviceName
     * @param int $gender
     * @param null $locale
     * @param null $firstName
     * @return User
     */
    public function setBySocial (User &$user, $username, $email, $serviceName, $gender=Gender::MALE, $locale=null, $firstName=null) {

        $currentUTCTime = DateUtils::getCurrentTimeUTC();

        $randomUsername = $this->generateRandomUsername($username, $serviceName);
        $emailCrypted   = $this->encryption->encrypting($email);
        $password       = $this->generateRandomPassword($user, $username, $serviceName);

        $user->setUsername($randomUsername);
        $user->setEmail($emailCrypted);
        $user->setPassword($password);
        $user->setGender($gender);
//        $user->setCalendarTimezone($calendarTimezone);
        if (!is_null($locale)) {
            $user->setLocale($locale);
        }
        $user->setCreatedAt($currentUTCTime);
        $user->addRole(Role::ROLE_USER);
        $user->setCredentialsExpired(false);
        $user->setEnabled(true);

        return $user;
    }

}