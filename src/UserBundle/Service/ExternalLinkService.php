<?php

namespace UserBundle\Service;


use Psr\Log\LoggerInterface;
use UserBundle\Repository\ExternalLinkRepository;
use Schema\EntityService;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Utils\StaticUtil\LogUtils;

class ExternalLinkService extends EntityService {


    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * ExternalLinkService constructor.
     * @param ExternalLinkRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param LoggerInterface $logger
     */
    public function __construct(ExternalLinkRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                LoggerInterface $logger
    ) {
        $this->repository = $repository;
        parent::__construct($repository, $authorizationChecker, $logger);
    }

}