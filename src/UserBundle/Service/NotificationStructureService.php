<?php

namespace UserBundle\Service;


use Psr\Log\LoggerInterface;
use Schema\EntityService;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use UserBundle\Repository\NotificationStructureRepository;
use Utils\StaticUtil\LogUtils;

class NotificationStructureService extends EntityService {


    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * NotificationStructureService constructor.
     * @param NotificationStructureRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param LoggerInterface $logger
     */
    public function __construct(NotificationStructureRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                LoggerInterface $logger
    ) {
        $this->repository = $repository;
        parent::__construct($repository, $authorizationChecker, $logger);
    }

}