<?php

namespace UserBundle\Service;


use Psr\Log\LoggerInterface;
use UserBundle\Entity\AdminLimitation;
use UserBundle\Entity\User;
use UserBundle\Repository\AdminLimitationRepository;
use Schema\EntityService;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Utils\Constants\System;
use Utils\StaticUtil\DateUtils;
use Utils\StaticUtil\LogUtils;

class AdminLimitationService extends EntityService {


    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * AdminLimitationService constructor.
     * @param AdminLimitationRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param LoggerInterface $logger
     */
    public function __construct(AdminLimitationRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                LoggerInterface $logger
    ) {
        $this->repository = $repository;
        parent::__construct($repository, $authorizationChecker, $logger);
    }

    /**
     * @param UserService $userService
     * @param User $currentUser
     * @param User $user
     * @param AdminLimitation $adminLimitation
     * @return null|AdminLimitation
     */
    public function saveLimitation (UserService $userService, User &$currentUser, User &$user, AdminLimitation &$adminLimitation) {

        $adminLimitation->setCreatedAt(DateUtils::getCurrentTimeUTC());
        $adminLimitation->setUser($user);

        if (!$user->isAdmin()) {
            $userService->manageRole($currentUser,$user);
        }

        $update = false;
        if (!$user->getAdminLimitations()->isEmpty()) {
            /** @var AdminLimitation $al */
            foreach ($user->getAdminLimitations() as $al) {
                if ($al->getArea() == $adminLimitation->getArea() &&
                    $al->getRoleType() == $adminLimitation->getRoleType()
                ) {

                    $al->setPrivileges($adminLimitation->getPrivileges());
                    $adminLimitation = $al;
                    $update          = true;
                    break;
                }
            }
        }

        if (!$this->save($adminLimitation,$update)) {
            return null;
        }

        return $adminLimitation;
    }

    /**
     * @param User $currentUser
     * @param AdminLimitation $adminLimitation
     * @param $enabling
     * @return AdminLimitation
     */
    public function adminEnabling (User &$currentUser, AdminLimitation &$adminLimitation, $enabling) {

        $currentTime = DateUtils::getCurrentTimeUTC();

        if ($enabling == System::ENABLING_ENABLE) {
            $adminLimitation->setIsDisabled(false);
        } else {
            $adminLimitation->setDisabledAt($currentTime);
            $adminLimitation->setDisabledBy($currentUser->getId());
            $adminLimitation->setIsDisabled(true);
        }

        try {
            $this->save($adminLimitation);
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            $this->logger->error("Failed enabling admin limitation", $exception);
            return null;
        }

        return $adminLimitation;

    }

}