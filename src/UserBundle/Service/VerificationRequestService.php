<?php

namespace UserBundle\Service;


use Psr\Log\LoggerInterface;
use Schema\EntityService;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use UserBundle\Repository\VerificationRequestRepository;
use Utils\StaticUtil\LogUtils;

class VerificationRequestService extends EntityService {


    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * VerificationRequestService constructor.
     * @param VerificationRequestRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param LoggerInterface $logger
     */
    public function __construct(VerificationRequestRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                LoggerInterface $logger
    ) {
        $this->repository = $repository;
        $this->logger     = $logger;
        parent::__construct($repository, $authorizationChecker, $logger);
    }

}