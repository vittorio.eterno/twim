<?php

namespace UserBundle\Service;


use Psr\Log\LoggerInterface;
use UserBundle\Repository\HistoryRoleRepository;
use Schema\EntityService;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Utils\StaticUtil\LogUtils;

class HistoryRoleService extends EntityService {


    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * HistoryRoleService constructor.
     * @param HistoryRoleRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param LoggerInterface $logger
     */
    public function __construct(HistoryRoleRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                LoggerInterface $logger
    ) {
        $this->repository = $repository;
        parent::__construct($repository, $authorizationChecker, $logger);
    }

}