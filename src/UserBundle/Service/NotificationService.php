<?php

namespace UserBundle\Service;


use Psr\Log\LoggerInterface;
use Schema\EntityService;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use UserBundle\Repository\NotificationRepository;
use Utils\StaticUtil\LogUtils;

class NotificationService extends EntityService {


    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * NotificationService constructor.
     * @param NotificationRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param LoggerInterface $logger
     */
    public function __construct(NotificationRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                LoggerInterface $logger
    ) {
        $this->repository = $repository;
        parent::__construct($repository, $authorizationChecker, $logger);
    }

}