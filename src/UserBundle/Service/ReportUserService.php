<?php

namespace UserBundle\Service;


use Psr\Log\LoggerInterface;
use Schema\EntityService;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use UserBundle\Repository\ReportUserRepository;
use Utils\StaticUtil\LogUtils;

class ReportUserService extends EntityService {


    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * ReportUserService constructor.
     * @param ReportUserRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param LoggerInterface $logger
     */
    public function __construct(ReportUserRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                LoggerInterface $logger
    ) {
        $this->repository = $repository;
        parent::__construct($repository, $authorizationChecker, $logger);
    }

}