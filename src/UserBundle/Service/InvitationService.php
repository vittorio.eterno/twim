<?php

namespace UserBundle\Service;


use Psr\Log\LoggerInterface;
use Schema\EntityService;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use UserBundle\Repository\InvitationRepository;
use Utils\StaticUtil\LogUtils;

class InvitationService extends EntityService {


    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * InvitationService constructor.
     * @param InvitationRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param LoggerInterface $logger
     */
    public function __construct(InvitationRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                LoggerInterface $logger
    ) {
        $this->repository = $repository;
        parent::__construct($repository, $authorizationChecker, $logger);
    }

}