<?php

namespace UserBundle\Service;


use Psr\Log\LoggerInterface;
use UserBundle\Repository\ExternalLinkStructureRepository;
use Schema\EntityService;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Utils\Service\CacheService;
use Utils\StaticUtil\LogUtils;

class ExternalLinkStructureService extends EntityService {

    /** @var CacheService $cacheService */
    private $cacheService;

    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * ExternalLinkStructureService constructor.
     * @param ExternalLinkStructureRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param LoggerInterface $logger
     * @param CacheService $cacheService
     */
    public function __construct(ExternalLinkStructureRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                LoggerInterface $logger,
                                CacheService $cacheService
    ) {
        $this->repository = $repository;
        parent::__construct($repository, $authorizationChecker, $logger);

        $this->cacheService = $cacheService;
    }

    /**
     * @return array|mixed|null
     */
    public function getAllExternalLinkStructures () {
        $externalLinkStructures = $this->cacheService->get("all_external_link_structures");
        if (is_null($externalLinkStructures)) {
            $externalLinkStructures = $this->repository->findAllExternalLinkStructures();
            // Expire after 6 month
            $expire = 60*60*24*30*6;
            $this->cacheService->set("all_external_link_structures", $externalLinkStructures, $expire);
        }
        return $externalLinkStructures;
    }

}