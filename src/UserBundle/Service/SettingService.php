<?php

namespace UserBundle\Service;


use Psr\Log\LoggerInterface;
use UserBundle\Entity\Setting;
use UserBundle\Repository\SettingRepository;
use Schema\Entity;
use Schema\EntityService;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Utils\Service\CacheService;
use Utils\StaticUtil\LogUtils;

class SettingService extends EntityService {

    /** @var CacheService $cacheService */
    private $cacheService;

    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * SettingService constructor.
     * @param SettingRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param LoggerInterface $logger
     * @param CacheService $cacheService
     */
    public function __construct(SettingRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                LoggerInterface $logger,
                                CacheService $cacheService
    ) {
        $this->repository = $repository;
        parent::__construct($repository, $authorizationChecker, $logger);

        $this->logger       = $logger;
        $this->cacheService = $cacheService;
    }

    /**
     * @return mixed|null
     */
    public function getFormattedSettings() {

        $formatted = $this->cacheService->get("user_settings");
        if (is_null($formatted)) {
            $settings   = $this->getAllSettings();
            if (!empty($settings) && count($settings) > 0) {
                /** @var Setting $setting */
                foreach ($settings as $setting) {
                    $formatted[$setting->serializedCategoryType()][] = $setting->listSerializer();
                }
            }

            // Expire after 6 month
            $expire = 60*60*24*30*6;
            $this->cacheService->set("user_settings", $formatted, $expire);
        }

        return $formatted;
    }

    /**
     * @return array|mixed
     */
    public function getAllSettings () {
        return $this->repository->findAllSettings();
    }

    /**
     * @param $ids
     * @return array|mixed
     */
    public function getAllByIds ($ids) {
        return $this->repository->findAllByIds($ids);
    }

}