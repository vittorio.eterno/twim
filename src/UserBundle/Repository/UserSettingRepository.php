<?php

namespace UserBundle\Repository;


use UserBundle\Entity\User;
use UserBundle\Entity\UserSetting;
use Schema\SchemaEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Schema\Entity;
use Utils\StaticUtil\LogUtils;

class UserSettingRepository extends SchemaEntityRepository {

    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * UserSettingRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, UserSetting::class);
    }

    /**
     * @param User $user
     * @return array
     */
    public function findAllUserSettings(User &$user) {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('us')
            ->from('UserBundle:UserSetting', 'us')
            ->where('us.idUser = :idUser AND us.isDisabled=0')
            ->setParameter('idUser', $user->getId());

        $results = array();

        try {
            $results = $qb->getQuery()->getResult();
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            // TODO: add log
        }

        return $results;
    }


}