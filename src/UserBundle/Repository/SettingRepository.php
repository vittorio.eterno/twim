<?php

namespace UserBundle\Repository;

use UserBundle\Entity\Setting;
use Schema\SchemaEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Schema\Entity;
use Utils\StaticUtil\LogUtils;
use Psr\Log\LoggerInterface;

class SettingRepository extends SchemaEntityRepository {

    /** @var LoggerInterface $logger */
    private $logger;

    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * SettingRepository constructor.
     * @param ManagerRegistry $registry
     * @param LoggerInterface $logger
     */
    public function __construct(ManagerRegistry $registry, LoggerInterface $logger) {
        parent::__construct($registry, Setting::class);
        $this->logger = $logger;
    }


    /**
     * @return array|mixed
     */
    public function findAllSettings() {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('s')
            ->from('UserBundle:Setting', 's')
            ->where('s.isDisabled=0')
        ;

        $results = array();

        try {
            $results = $qb->getQuery()->getResult();
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            $this->logger->error("Failed get all settings", $exception);
        }

        return $results;
    }


    /**
     * @param $ids
     * @return array|mixed
     */
    public function findAllByIds($ids) {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('s')
            ->from('UserBundle:Setting', 's')
            ->where('s.isDisabled=0 AND s.id IN (:ids)')
            ->setParameter('ids',$ids)
        ;

        $results = array();

        try {
            $results = $qb->getQuery()->getResult();
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            $this->logger->error("Failed get all settings by ids", $exception);
        }

        return $results;
    }

}