<?php

namespace UserBundle\Repository;

use Doctrine\Common\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;
use ResourceBundle\Entity\CalendarTimezone;
use Schema\SchemaEntityRepository;
use UserBundle\Entity\User;
use UserBundle\Service\UserService;
use Utils\StaticUtil\LogUtils;
use Utils\StaticUtil\PaginationUtils;

class UserRepository extends SchemaEntityRepository {


    const DEFAULT_MAX_RESULTS_USERS = 50;


    /** @var LoggerInterface $logger */
    private $logger;


    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * UserRepository constructor.
     * @param ManagerRegistry $registry
     * @param LoggerInterface $logger
     */
    public function __construct(ManagerRegistry $registry, LoggerInterface $logger) {
        parent::__construct($registry, User::class);

        $this->logger = $logger;
    }

    /**
     * @param  User $user
     * @param  $token
     * @param  UserService $userService
     * @param  CalendarTimezone $calendarTimezone
     * @param  $keyword
     * @return array|null|User
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function saveUser(User $user, $token, $userService, CalendarTimezone $calendarTimezone, $keyword) {

        $profile = null;
        $this->getEntityManager()->getConnection()->beginTransaction();

        try {
            $this->getEntityManager()->persist($user);

            try {
                $this->getEntityManager()->flush();
                $profile = $this->profileService->create($user, $calendarTimezone, $keyword);
                $user = $userService->sendMailRegistrationConfirm($user, $token);
            } catch (\Exception $e) {
                $exception = LogUtils::getFormattedExceptions($e);
                $this->getEntityManager()->getConnection()->rollBack();
                return $exception;
            }

            $this->getEntityManager()->getConnection()->commit();

        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);

            if(is_null($profile)) {
                $this->profileService->forceDelete($profile);
            }
            $this->getEntityManager()->getConnection()->rollBack();
            return $exception;
        }

        return $user;
    }

    /**
     * @param  User $user
     * @param  $profile
     * @return array|null|User
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function enableUser(User $user, Profile $profile) {
        $this->getEntityManager()->getConnection()->beginTransaction();

        try {
            $this->getEntityManager()->persist($user);

            try {
                $this->getEntityManager()->flush();
                $result = $this->profileService->enabling($profile, $user, 'enable');
                if(!$result || is_null($result)){
                    $exception = array(
                        'backtrace'        => '',
                        'file'             => 'UserRepository',
                        'line'             => '86',
                        'exceptionMessage' => 'Failed save of enabling Profile'
                    );
                    $this->getEntityManager()->getConnection()->rollBack();
                    return $exception;
                }
            } catch (\Exception $e) {
                $exception = LogUtils::getFormattedExceptions($e);

                $this->getEntityManager()->getConnection()->rollBack();
                return $exception;
            }

            $this->getEntityManager()->getConnection()->commit();

        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);

            $this->getEntityManager()->getConnection()->rollBack();
            return $exception;
        }

        return $user;
    }


    /**
     * Find an user given its email.
     * NULL will be returned if the user does not exist or it is locked or its creator is locked.
     * @param string $email
     * @return User|object
     */
    public function findByEmail($email) {

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('u')
            ->from('UserBundle:User', 'u')
            ->where('u.email = :email')
            ->setParameter('email', $email)
        ;

        $result = null;

        try {
            $result = $qb->setMaxResults(1)->getQuery()->getOneOrNullResult();
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            $this->logger->error("Failed get user by crypted email: ".$email, $exception);
        }

        return $result;

    }

    /**
     * @param $email
     * @return mixed|null
     */
    public function findIdByEmail ($email) {

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('u.id')
            ->from('UserBundle:User', 'u')
            ->where('u.email = :email')
            ->setParameter('email', $email)
        ;

        $result = null;

        try {
            $result = $qb->setMaxResults(1)->getQuery()->getOneOrNullResult();
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            $this->logger->error("Failed get id user by crypted email: ".$email, $exception);
        }

        return $result;
    }

    /**
     * @param $username
     * @return null|User
     */
    public function findByUsername($username) {

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('u')
            ->from('UserBundle:User', 'u')
            ->where('u.usernameCanonical = :username')
            ->setParameter('username', strtolower($username))
        ;

        $result = null;

        try {
            $result = $qb->setMaxResults(1)->getQuery()->getOneOrNullResult();
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            $this->logger->error("Failed get id user by username: ".$username, $exception);
        }

        return $result;
    }


    /**
     * @param $username
     * @return mixed|null
     */
    public function findIdByUsername ($username) {

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('u.id')
            ->from('UserBundle:User', 'u')
            ->where('u.usernameCanonical = :username')
            ->setParameter('username', $username)
        ;

        $result = null;

        try {
            $result = $qb->setMaxResults(1)->getQuery()->getOneOrNullResult();
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            $this->logger->error("Failed get id user by username: ".$username, $exception);
        }

        return $result;
    }



    /**
     * @return array|mixed
     */
    public function findAllOldPending () {

        $date    = new \DateTime();
        $oldDate = $date->modify('-1 year')->format('Y-m-d');

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('u')
            ->from('UserBundle:User', 'u')
            ->where('u.enabled=0 AND u.createdAt <= :oldDate')
            ->setParameter('oldDate', $oldDate)
        ;

        $results = array();

        try {
            $results = $qb->getQuery()->getResult();
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            // TODO: add query.error log
        }

        return $results;
    }



//    public function findByHash ($hash) {
//        $qb = $this->getEntityManager()->createQueryBuilder();
//        $qb->select('ao.id')
//            ->from('ArchivingBundle:ArchiveOwner', 'ao')
//            ->where('ao.isDisable=0 AND ao.hash=:hash')
//            ->setParameter('hash',$hash);
//
//        $results = null;
//
//        try {
//            $results = $qb->getQuery()->getResult();
//        } catch (\Exception $e) {
//            $exception = LogUtils::getFormattedExceptions($e);
//            $this->log_monitor->trace(LogMonitor::LOG_CHANNEL_QUERY, 500, "query.error", $exception);
//        }
//
//        return $results;
//    }


    /**
     * @param int $page
     * @param null|string $suggest
     * @param int $maxResults
     * @param string $sort
     * @return array|\Doctrine\ORM\Tools\Pagination\Paginator
     */
    public function findAllPaginatedUsers ($page=1, ?string $suggest, $maxResults=self::DEFAULT_MAX_RESULTS_USERS, $sort='DESC') {


        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('u')
            ->from('UserBundle:User', 'u');

        if (!is_null($suggest)) {
            $qb->where('u.username LIKE :username OR u.email LIKE :username')
                ->setParameter('username', '%'.$suggest.'%');
        }

        $qb->orderBy('u.id',$sort);

        $results = array();

        try {
            $query   = $qb->getQuery();
            $results = $this->paginate($query,$page,$maxResults);
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            $this->logger->error("Failed get all users", $exception);
        }

        return $results;

    }

    /**
     * @param $value
     * @param $valueCrypted
     * @return mixed|null
     */
    public function findUserByUsernameOrEmail ($value, $valueCrypted) {

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('u')
            ->from('UserBundle:User', 'u')
            ->where('u.usernameCanonical = :username OR u.email = :email')
            ->setParameter('username', $value)
            ->setParameter('email', $valueCrypted)
        ;

        $result = null;

        try {
            $result = $qb->setMaxResults(1)->getQuery()->getOneOrNullResult();
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            $this->logger->error("Failed get user by username or email", $exception);
        }

        return $result;
    }


//    public function getBootstrapTableAllUsers() {
//
//        $users = $this->findAll();
//
//        $users_arr = array();
//        $users_arr["total"] = count($users);
//        foreach($users as $user){
//            $users_arr["rows"][] = array(
//                "id"         => $user->getId(),
//                "username"   => $user->getUsername(),
//                "email"      => $user->getEmail(),
//                "enabled"    => $user->isEnabled()?'y':'n',
//                "last_login" => is_object($user->getLastLogin())?$user->getLastLogin()->format('Y-m-d H:i:s'):$user->getLastLogin(),
//                "roles"      => $user->getRoles()
//            );
//        }
//
//        return $users_arr;
//    }


}