<?php

namespace UserBundle\Repository;


use Psr\Log\LoggerInterface;
use Schema\SchemaEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use UserBundle\Entity\VerificationRequest;


/**
 * VerificationRequestRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class VerificationRequestRepository extends SchemaEntityRepository {

    /** @var LoggerInterface $logger */
    private $logger;

    /**
     * VerificationRequestRepository constructor.
     * @param ManagerRegistry $registry
     * @param LoggerInterface $logger
     */
    public function __construct(ManagerRegistry $registry, LoggerInterface $logger) {
        parent::__construct($registry, VerificationRequest::class);

        $this->logger = $logger;
    }

}
