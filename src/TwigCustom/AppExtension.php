<?php

namespace TwigCustom;

use Utils\Service\Markdown;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension {

    private $parser;

    public function __construct(Markdown $parser) {
        $this->parser = $parser;
    }

    public function getFilters() {
        return array(
            new TwigFilter(
                'md2html',
                array($this, 'markdownToHtml'),
                array('is_safe' => array('html'), 'pre_escape' => 'html')
            ),
        );
    }

    public function markdownToHtml($content) {
        return $this->parser->toHtml($content);
    }

    public function getName() {
        return 'app_extension';
    }
}
