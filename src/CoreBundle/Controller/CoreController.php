<?php

namespace CoreBundle\Controller;

use Schema\AbstractRenderController as Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\TranslatorInterface;
use Utils\Service\RequestInfo;

class CoreController extends Controller {

    /**
     *
     * @Route("/", name="homepage", methods="GET")
     *
     * @param RequestInfo $requestInfo
     * @param TranslatorInterface $translator
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showHomeAction(RequestInfo $requestInfo, TranslatorInterface $translator) {

        $deviceName = $requestInfo->get('deviceName','Desktop');

        return $this->renderByDevice($deviceName,'CoreBundle\home.html.twig');
    }
}
