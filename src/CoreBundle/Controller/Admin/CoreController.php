<?php

namespace CoreBundle\Controller\Admin;


use Schema\AbstractRenderController as Controller;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class CoreController
 * @package CoreBundle\Controller\Admin
 *
 * @Route("/admin")
 */
class CoreController extends Controller {


    /**
     *
     * @Route("/", name="admin_homepage", methods="GET")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction() {

        return $this->render('Admin\CoreBundle\home.html.twig');
    }

}