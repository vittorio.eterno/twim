<?php

namespace EventSubscriber;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Utils\StaticUtil\IpUtils;

class LocaleSubscriber implements EventSubscriberInterface {

    private $defaultLocale;

    private $session;

    public function __construct(SessionInterface $session, $defaultLocale = 'en_GB') {
        $this->defaultLocale = $defaultLocale;
        $this->session       = $session;
    }

    public function onKernelRequest(GetResponseEvent $event) {

        $request = $event->getRequest();
        if (!$request->hasPreviousSession()) {
            return;
        }

        // try to see if the locale has been set as a _locale routing parameter
        if ($locale = $request->attributes->get('_locale')) {
            $request->getSession()->set('_locale', $locale);
        } else {

            $ip = $request->getClientIp();
            if($ip == 'unknown'){
                $ip = $_SERVER['REMOTE_ADDR'];
            }

            $ipInfo = IpUtils::ipInfo($ip, "Country Code");

            $useDefault = true;
            if (!is_null($ipInfo)) {
                if (isset($ipInfo["countryCode"]) && $ipInfo["countryCode"]=="IT") {
                    $request->setLocale("it_IT");
                    $useDefault = false;
                }
                if (isset($ipInfo["timezone"])) {
                    $this->session->set("timezone", $ipInfo["timezone"]);
                }
            }

            if ($useDefault) {
                // if no explicit locale has been set on this request, use one from the session
                $request->setLocale($request->getSession()->get('_locale', $this->defaultLocale));
            }

        }
    }

    public static function getSubscribedEvents() {

        return array(
            // must be registered before (i.e. with a higher priority than) the default Locale listener
            KernelEvents::REQUEST => array(array('onKernelRequest', 20)),
        );
    }
}