<?php

namespace ContestBundle\Service;


use ContestBundle\Repository\EggBonusRepository;
use Psr\Log\LoggerInterface;
use Schema\EntityService;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Utils\StaticUtil\LogUtils;

class EggBonusService extends EntityService {


    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * EggBonusService constructor.
     * @param EggBonusRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param LoggerInterface $logger
     */
    public function __construct(EggBonusRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                LoggerInterface $logger
    ) {
        $this->repository = $repository;
        parent::__construct($repository, $authorizationChecker, $logger);
    }

}