<?php

namespace ContestBundle\Service;


use ContestBundle\Repository\UserEggWordRepository;
use Psr\Log\LoggerInterface;
use Schema\EntityService;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Utils\StaticUtil\LogUtils;

class UserEggWordService extends EntityService {


    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * UserEggWordService constructor.
     * @param UserEggWordRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param LoggerInterface $logger
     */
    public function __construct(UserEggWordRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                LoggerInterface $logger
    ) {
        $this->repository = $repository;
        parent::__construct($repository, $authorizationChecker, $logger);
    }

}