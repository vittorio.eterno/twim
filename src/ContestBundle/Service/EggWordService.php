<?php

namespace ContestBundle\Service;


use ContestBundle\Repository\EggWordRepository;
use Psr\Log\LoggerInterface;
use Schema\EntityService;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Utils\StaticUtil\LogUtils;

class EggWordService extends EntityService {


    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * EggWordService constructor.
     * @param EggWordRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param LoggerInterface $logger
     */
    public function __construct(EggWordRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                LoggerInterface $logger
    ) {
        $this->repository = $repository;
        parent::__construct($repository, $authorizationChecker, $logger);
    }

}