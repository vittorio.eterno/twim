<?php

namespace ContestBundle\Service;


use ContestBundle\Repository\ContestRepository;
use Psr\Log\LoggerInterface;
use Schema\EntityService;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Utils\StaticUtil\LogUtils;

class ContestService extends EntityService {


    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * ContestService constructor.
     * @param ContestRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param LoggerInterface $logger
     */
    public function __construct(ContestRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                LoggerInterface $logger
    ) {
        $this->repository = $repository;
        parent::__construct($repository, $authorizationChecker, $logger);
    }

}