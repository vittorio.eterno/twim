<?php

namespace ContestBundle\Entity;

use Schema\Entity;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * EggWord
 *
 * @ORM\Table(name="egg_words",
 *     indexes={@ORM\Index(name="egg_word_idx", columns={"word"})}
 *     )
 * @ORM\Entity(repositoryClass="ContestBundle\Repository\EggWordRepository")
 */
class EggWord extends Entity {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Many EggWords have One Contest.
     * @ORM\ManyToOne(targetEntity="Contest", inversedBy="eggWords")
     * @ORM\JoinColumn(name="contest_id", referencedColumnName="id")
     */
    private $contest;

    /**
     * Many EggWords have One EggBonus.
     * @ORM\ManyToOne(targetEntity="EggBonus", inversedBy="eggWords")
     * @ORM\JoinColumn(name="egg_bonus_id", referencedColumnName="id")
     */
    private $bonus;

    /**
     * One EggWord has One UserEggWord.
     * @ORM\OneToOne(targetEntity="UserEggWord", mappedBy="eggWord")
     */
    private $userEggWord;

    /**
     * @var string
     *
     * @ORM\Column(name="word", type="string", length=255)
     */
    private $word;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_disabled", type="boolean", nullable=true, options={"default":0})
     */
    private $isDisabled;


    ################################################# SERIALIZER FUNCTIONS

    /**
     * Returns the array of fields to serialize in entity administration view.
     * @return array
     */
    public function adminSerializer() {
        $view_vars = $this->viewSerializer();

        $admin_vars = array();

        return array_merge($view_vars, $admin_vars);
    }

    /**
     * Returns the array of fields to serialize in entity view.
     * @return array
     */
    public function viewSerializer() {
        $list_vars = $this->listSerializer();

        $view_vars = array(
        );

        return array_merge($list_vars, $view_vars);
    }

    /**
     * Returns the array of fields to serialize in a list of this entity.
     * @return array
     */
    public function listSerializer() {
        $list_vars = array(
            'id'           => $this->serializedId()
        );
        return $list_vars;
    }

    /**
     * Returns the hash code unique identifier of the entity.
     * @return string
     */
    public function hashCode() {
        // TODO: Implement hashCode() method.
    }

    ################################################# SERIALIZED FUNCTIONS

    /**
     * Setting id
     * @JMS\VirtualProperty
     * @JMS\SerializedName("id")
     * @JMS\Type("integer")
     * @JMS\Groups({"list"})
     * @JMS\Since("1.0.x")
     */
    public function serializedId() {
        return (is_null($this->id)?null:$this->id);
    }


    ################################################# GETTERS AND SETTERS FUNCTIONS



    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set word.
     *
     * @param string $word
     *
     * @return EggWord
     */
    public function setWord($word)
    {
        $this->word = $word;

        return $this;
    }

    /**
     * Get word.
     *
     * @return string
     */
    public function getWord()
    {
        return $this->word;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return EggWord
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return EggWord
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set isDisabled.
     *
     * @param bool|null $isDisabled
     *
     * @return $this
     */
    public function setIsDisabled($isDisabled = null)
    {
        $this->isDisabled = $isDisabled;

        return $this;
    }

    /**
     * Get isDisabled.
     *
     * @return bool|null
     */
    public function getIsDisabled()
    {
        return $this->isDisabled;
    }

    /**
     * @return Contest
     */
    public function getContest()
    {
        return $this->contest;
    }

    /**
     * @param Contest $contest
     */
    public function setContest(Contest $contest): void
    {
        $this->contest = $contest;
    }

    /**
     * @return EggBonus
     */
    public function getBonus()
    {
        return $this->bonus;
    }

    /**
     * @param EggBonus $bonus
     */
    public function setBonus(EggBonus $bonus): void
    {
        $this->bonus = $bonus;
    }

    /**
     * @return UserEggWord
     */
    public function getUserEggWord()
    {
        return $this->userEggWord;
    }

    /**
     * @param UserEggWord $userEggWord
     */
    public function setUserEggWord(UserEggWord $userEggWord): void
    {
        $this->userEggWord = $userEggWord;
    }



}
