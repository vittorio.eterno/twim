<?php

namespace Schema;

use JsonSerializable;

abstract class Entity implements JsonSerializable
{

    ///////////////////////////////////////////
    /// SERIALIZER FUNCTIONS

    /**
     * Returns the array of fields to serialize in a list of this entity.
     * @return array
     */
    public function jsonSerialize()
    {
        return $this->listSerializer();
    }

    /**
     * Returns the array of fields to serialize in entity administration view.
     * @return array
     */
    abstract public function adminSerializer();

    /**
     * Returns the array of fields to serialize in entity view.
     * @return array
     */
    abstract public function viewSerializer();

    /**
     * Returns the array of fields to serialize in a list of this entity.
     * @return array
     */
    abstract public function listSerializer();

    ///////////////////////////////////////////
    /// UTILS FUNCTIONS

    /**
     * Returns the hash code unique identifier of the entity.
     * @return string
     */
    abstract public function hashCode();

    /**
     * Returns true if this entity is enabled
     * @return bool
     */
    public function isEnabled()
    {
        if(isset($this->enabled))
            return $this->enabled;

        return true;
    }

    /**
     * Returns true if this entity is locked
     * @return bool
     */
    public function isLocked()
    {
        if(isset($this->locked))
            return $this->locked;

        return false;
    }

    /**
     * Returns true if this entity is deleted
     * @return bool
     */
    public function isDeleted()
    {
        if(isset($this->deleted))
            return $this->deleted;

        return false;
    }

    /**
     * Returns true if this entity is valid
     * @return bool
     */
    public function isValid()
    {
        if($this->isEnabled() && !$this->isLocked() && !$this->isDeleted())
            return true;
        return false;
    }

    /**
     * If it is an ordered entity rises up the order field
     */
    public function orderUp()
    {
        if(isset($this->order)) {
            if($this->order > 1) {
                $this->order--;
            }
        }
    }

    /**
     * If it is an ordered entity brings down the order field
     */
    public function orderDown()
    {
        if(isset($this->order)) {
            $this->order++;
        }

    }

    /**
     * Returns true if the compared entity is the same
     * @param Entity $entity
     * @return bool
     */
    public function compare(Entity $entity)
    {
        if($this->hashCode() == $entity->hashCode())
            return true;

        return false;
    }

}