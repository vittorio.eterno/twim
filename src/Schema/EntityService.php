<?php

namespace Schema;

use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use UserBundle\Entity\User;
use Utils\StaticUtil\LogUtils;

abstract class EntityService {

    /** string $class */
    protected $entity = '';

    /** bool $entity_hashed */
    protected $entity_hashed = false;

    /** @var SchemaEntityRepository $repository */
    protected $repository;

    /** @var integer $pagination */
    protected $pagination = 20;

    /** @var AuthorizationCheckerInterface $authorization_checker */
    protected $authorization_checker;

    /** @var LoggerInterface $logger */
    protected $logger;

    ///////////////////////////////////////////
    /// CONSTRUCTOR

    /**
     * EntityService constructor.
     * @param SchemaEntityRepository $repository
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param LoggerInterface $logger
     */
    public function __construct(SchemaEntityRepository $repository,
                                AuthorizationCheckerInterface $authorizationChecker,
                                LoggerInterface $logger
    ) {
        $this->repository            = $repository;
        $this->authorization_checker = $authorizationChecker;
        $this->logger                = $logger;
    }

    ///////////////////////////////////////////
    /// SELECT FUNCTIONS

    /**
     * Returns the entity given its id.
     * NULL will be returned if the entity does not exist.
     * @param int $id
     * @return Entity|null
     */
    public function getById(int $id) {
        return $this->repository->findOne(array('id' => $id));
    }

    /**
     * Returns the entity given a unique attribute/s.
     * NULL will be returned if the entity does not exist.
     * @param array $attributes
     * @return Entity|null
     */
    public function getOneByAttributes(array $attributes) {
        return $this->repository->findOne($attributes);
    }

    /**
     * Returns the entity given its hash.
     * NULL will be returned if the entity does not exist.
     * @param string $hash
     * @return Entity|null
     */
    public function getByHash(string $hash) {
        return $this->repository->findOne(array('hash' => $hash));
    }

    ///////////////////////////////////////////
    /// LIST FUNCTIONS

    /**
     * Returns the list of this entity given filters.
     * @param string|null $method
     * @param array $criteria
     * @param int $page
     * @param int $limit
     * @param int|null $offset
     * @param array $ordering
     * @return array
     */
    public function getList($method = null, array $criteria = array(), int $page = 0, int $limit = null, int $offset = null, array $ordering = array()) {
        try {
            if(is_null($method)) {
                $method = 'listSerializer';
            }
            $list = $this->repository->findList($limit, $offset, $ordering, $criteria);
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            //TODO: add query.error log

            $list = array();
        }


        $response['list'] = array();

        if(!empty($list)){
            foreach ($list as $l){
                $response['list'][] = $l->{$method}();
            }
        }

        $response['total_items'] = count($list);

        $pagination = $this->pagination;
        if($page == 0) {
            $pagination = count($list);
            $response['current_page'] = 1;
            $response['total_pages'] = 1;
            return $response;
        }

        $response['current_page'] = $page;
        $response['total_pages'] = ceil($response['total_items'] / $pagination);

        return $response;
    }

    /**
     * Returns the list of this entity given a suggest.
     * @param   string|null $method
     * @param   array  $suggests
     * @param   int     $page
     * @param   int     $limit
     * @param   int     $offset
     * @param   array   $ordering
     * @return  array
     */
    public function getListBySuggest($method = null, array $suggests = array(), int $page = 0, int $limit = 0, int $offset = null, array $ordering = array(), array $criteria = array()) {
        try{
            if(is_null($method)) {
                $method = 'listSerializer';
            }
            $list = $this->repository->findListBySuggest($limit, $offset, $ordering, $suggests, $criteria);
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            //TODO: add query.error log

            $list = array();
        }

        if(!empty($list)){
            foreach ($list as $l){
                $response['list'][] = $l->{$method}();
            }
        }

        $response['total_items'] = count($list);

        $pagination = $this->pagination;
        if($page == 0) {
            $pagination = count($list);
            $response['current_page'] = 1;
            $response['total_pages'] = 1;
            return $response;
        }

        $response['current_page'] = $page;
        $response['total_pages'] = ceil($response['total_items'] / $pagination);

        return $response;
    }

    ///////////////////////////////////////////
    /// OBJECT FUNCTIONS

    /**
     * Finalize and save the creation of the entity.
     * Returns NULL if some error occurs otherwise it returns the persisted object.
     *
     * @param object $entity
     * @param bool $update
     * @return bool|object
     */
    public function save(object $entity, bool $update = false) {

        try {
            $this->repository->write($entity, $update);
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
//            print_r($exception);die("flush");
            $this->logger->error("Error on save", $exception);
            return false;
        }

        return $entity;
    }

    /**
     * Delete a given entity.
     *
     * @param object $entity
     * @return bool
     */
    public function forceDelete(object $entity) {
        try {
            $this->repository->remove($entity);
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            $this->logger->error("Error on force delete", $exception);
            return false;
        }

        return true;
    }

    /**
     * Delete a given entity.
     *
     * @param object $entity
     * @param User|null $user
     * @return bool
     */
    public function delete(object $entity, User $user = null) {
        try {
            $current_time = new \DateTime();
            $current_time->setTimestamp(time());
            if(!is_null($user)) {
                $entity->setDisabledBy($user->getId());
            }
            $entity->setDisabledOn($current_time);
            $entity->setIsDisable(1);

            $this->repository->write($entity, true);
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            $this->logger->error("Error on delete", $exception);
            return false;
        }

        return true;
    }

    /**
     * Restore a given entity.
     *
     * @param object $entity
     * @param User|null $user
     * @return bool
     */
    public function restore(object $entity, User $user = null) {
        try {
            if(!is_null($user)) {
                $current_time = new \DateTime();
                $current_time->setTimestamp(time());
                $entity->setEditedBy($user);
                $entity->setEditedOn($current_time);
                $entity->setIsDisabled(false);
            }

            $this->repository->write($entity, true);
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            $this->logger->error("Error on restore", $exception);
            return false;
        }

        return true;
    }

    /**
     * Lock/unlock a given entity.
     *
     * @param object $entity
     * @param User|null $user
     * @param string $locking
     * @return bool
     */
    public function locking(object $entity, User $user = null, string $locking) {
        try {
            if(!is_null($user)) {
                $current_time = new \DateTime();
                $current_time->setTimestamp(time());

                if ($locking == "lock") {
                    $entity->setLocked(true);
                } elseif ($locking == "unlock") {
                    $entity->setLocked(false);
                }
                $entity->setEditedBy($user);
                $entity->setEditedOn($current_time);
            }

            $this->repository->write($entity, true);
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            $this->logger->error("Error on locking", $exception);
            return false;
        }

        return true;
    }

    /**
     * Enable/disable a given entity.
     *
     * @param object $entity
     * @param User|null $user
     * @param string $enabling
     * @return bool
     */
    public function enabling(object $entity, User $user = null, string $enabling) {
        try {
            if(!is_null($user)) {
                $current_time = new \DateTime();
                $current_time->setTimestamp(time());

                if ($enabling == "enable") {
                    $entity->setLocked(true);
                } elseif ($enabling == "disable") {
                    $entity->setLocked(false);
                }
                $entity->setEditedBy($user);
                $entity->setEditedOn($current_time);
            }

            $this->repository->write($entity, true);
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            $this->logger->error("Error on enabling", $exception);
            return false;
        }

        return true;
    }


    /**
     * @param $results
     * @param int $page
     * @return mixed
     */
    public function getFormattedListToResponse(&$results, $page=0){

        $response['list']         = array();
        $response['current_page'] = 1;
        $response['total_pages']  = 1;
        $response['total_items']  = 0;

        if(!empty($results)) {

            $countResults = count($results);

            $response['list']        = $results;
            $response['total_items'] = $countResults;

            if($page > 0) {
                $response['current_page'] = $page;
                $response['total_pages'] = ceil($countResults / $this->pagination);
            }

        }

        return $response;
    }

    /**
     * Returns the list of this entity given filters.
     * @param array $criteria
     * @param int $page
     * @param int $limit
     * @param int|null $offset
     * @param array $ordering
     * @return array
     */
    public function getListByAttributes(array $criteria = array(), int $page = 0, int $limit = null, int $offset = null, array $ordering = array()) {
        try {
            $list = $this->repository->findList($limit, $offset, $ordering, $criteria);
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            //TODO: add query.error log

            $list = array();
        }

        return $list;
    }

    /**
     * @param object $entity
     * @return bool
     */
    public function doPersist(object $entity){
        try {
            $this->repository->doPersist($entity);
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            $this->logger->error("Error on do persist", $exception);
            return false;
        }
        return true;
    }

    /**
     * @param object $entity
     * @return bool|object
     */
    public function doMerge(object $entity){
        try {
            return $this->repository->doMerge($entity);
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            $this->logger->error("Error on do merge", $exception);
            return false;
        }
    }

    /**
     * @return bool
     */
    public function doFlush() {
        try {
            $this->repository->doFlush();
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            $this->logger->error("Error on do flush", $exception);
            return false;
        }
        return true;
    }

    /**
     * @param object $entity
     * @return bool
     */
    public function doRemove(object $entity) {
        try {
            $this->repository->doRemove($entity);
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            $this->logger->error("Error on do remove", $exception);
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function beginTransaction() {
        try {
            $this->repository->beginTransaction();
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            $this->logger->error("Error on begin transaction", $exception);
            return false;
        }
        return true;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function doCommit() {
        try {
            $this->repository->doCommit();
        } catch (\Exception $e) {
            throw $e;
        }
        return true;
    }

    /**
     * @return bool
     */
    public function doRollBack() {
        try {
            $this->repository->doRollBack();
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            $this->logger->error("Error on do rollback", $exception);
            return false;
        }
        return true;
    }

    /**
     * @param $ids
     * @param bool $checkIsDisabled
     * @return array|bool|mixed
     */
    public function getByIds ($ids, $checkIsDisabled=false) {
        try {
            return $this->repository->findByIds($ids, $checkIsDisabled);
        } catch (\Exception $e) {
            $exception = LogUtils::getFormattedExceptions($e);
            $this->logger->error("Error on get by ids", $exception);
            return false;
        }
    }

}