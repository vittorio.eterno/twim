<?php

namespace Schema;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Exception;
use Utils\StaticUtil\LogUtils;

abstract class SchemaEntityRepository extends ServiceEntityRepository {

    private $entity = '';

    public function __construct(ManagerRegistry $registry, $entity) {
        $this->entity = $entity;
        parent::__construct($registry, $entity);
    }

    ///////////////////////////////////////////
    /// FIND FUNCTIONS

    /**
     * Find one object by attributes.
     * @param array $criteria
     * @return Entity|null
     */
    public function findOne(array $criteria) {
        $object = null;

        if(count($criteria) > 0) {

            /** @var Entity $object */
            $object = $this->findOneBy(
                $criteria
            );
        }

        return $object;
    }

    /**
     * Find list objects by attributes given filters.
     * @param int|null $limit
     * @param int|null $offset
     * @param array $ordering
     * @param array $criteria
     * @return array
     */
    public function findList(int $limit = null, int $offset = null, array $ordering = array(), array $criteria = array()) {
        if(count($criteria) > 0) {
            $list = $this->findBy(
                $criteria, (count($ordering)>0?$ordering:null), $limit, $offset
            );
        } else {
            $qb = $this->createQueryBuilder('entity');

            if(count($ordering) > 0) {
                $first = true;
                foreach ($ordering as $field => $type) {
                    if($first) {
                        $qb->orderBy('entity.'.$field, $type);
                        $first = false;
                    } else {
                        $qb->addOrderBy('entity.'.$field, $type);
                    }
                }
            }

            if($limit > 0) {
                $qb->setMaxResults($limit);
            }

            if($offset > 0) {
                $qb->setFirstResult($offset);
            }

            $list = $qb->getQuery()->getResult();
        }

        return $list;
    }

    /**
     * Find list objects by attributes given filters.
     * @param int $limit
     * @param int|null $offset
     * @param array $ordering
     * @param array $suggests
     * @param array $criteria
     * @return array
     */
    public function findListBySuggest(int $limit = 0, int $offset = null, array $ordering = array(), array $suggests = array(), array $criteria = array()) {
        $qb = $this->createQueryBuilder('entity');

        if(count($suggests ) > 0) {
            foreach ($suggests as $field => $suggest) {
                $words = explode(' ', strtolower(trim($suggest)));
                $first = true;
                foreach ($words as $nr=>$word) {
                    if($first) {
                        $qb->where('entity.'.$field.' LIKE :str_'.$nr)->setParameter('str_'.$nr, '%'.$word.'%');
                        $first = false;
                    } else {
                        $qb->andWhere('entity.'.$field.' LIKE :str_'.$nr)->setParameter('str_'.$nr, '%'.$word.'%');
                    }
                }
            }
        }

        if(count($criteria ) > 0) {
            foreach ($criteria as $field => $value) {
                $qb->andWhere('entity.'.$field.' = :criteria_'.$field)->setParameter('criteria_'.$field, $value);
            }
        }


        if(count($ordering) > 0) {
            $first = true;
            foreach ($ordering as $field=>$type) {
                if($first) {
                    $qb->orderBy('entity.'.$field, $type);
                    $first = false;
                } else {
                    $qb->addOrderBy('entity.'.$field, $type);
                }
            }
        }


        if($limit > 0) {
            $qb->setMaxResults($limit);
        }

        if($offset > 0) {
            $qb->setFirstResult($offset);
        }
        $qb->distinct('entity.id');
        $list = $qb->getQuery()->getResult();
       
        return $list;
    }

    ///////////////////////////////////////////
    /// OBJECT FUNCTIONS

    /**
     * @param object $entity
     * @param bool $update
     * @return bool
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws Exception
     */
    public function write(object $entity, bool $update = false) {
        $this->getEntityManager()->getConnection()->beginTransaction();

        try {
            if(!$update) {
                $this->getEntityManager()->persist($entity);
            } else {
                $this->getEntityManager()->merge($entity);
            }
            $this->getEntityManager()->flush();
            $this->getEntityManager()->getConnection()->commit();
        } catch (Exception $e) {
            $this->getEntityManager()->getConnection()->rollBack();
            throw $e;
        }

        return true;
    }

    /**
     * Set as disabled a given entity.
     *
     * @param object $entity
     * @param bool $flush
     * @return bool
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws Exception
     */
    public function remove(object $entity, bool $flush = true) {
        $this->getEntityManager()->getConnection()->beginTransaction();
        try {
            $this->getEntityManager()->remove($entity);
            if($flush) {
                $this->getEntityManager()->flush();
                $this->getEntityManager()->getConnection()->commit();
            }
        } catch (Exception $e) {
            $this->getEntityManager()->getConnection()->rollBack();
            throw $e;
        }

        return true;
    }


    /**
     * @param object $entity
     * @return bool
     * @throws Exception
     */
    public function doPersist(object $entity) {
        try {
            $this->getEntityManager()->persist($entity);
        } catch (\Exception $e) {
            throw $e;
        }
        return true;
    }

    /**
     * @param object $entity
     * @return object
     * @throws Exception
     */
    public function doMerge(object $entity) {
        try {
            return $this->getEntityManager()->merge($entity);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function doFlush() {
        try {
            $this->getEntityManager()->flush();
        } catch (\Exception $e) {
            throw $e;
        }

        return true;
    }

    /**
     * @param object $entity
     * @return bool
     * @throws Exception
     */
    public function doRemove(object $entity) {
        try {
            $this->getEntityManager()->remove($entity);
        } catch (\Exception $e) {
            throw $e;
        }

        return true;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function beginTransaction() {
        try {
            $this->getEntityManager()->getConnection()->beginTransaction();
        } catch (\Exception $e) {
            throw $e;
        }

        return true;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function doCommit() {
        try {
            $this->getEntityManager()->getConnection()->commit();
        } catch (\Exception $e) {
            throw $e;
        }

        return true;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function doRollBack() {
        try {
            $this->getEntityManager()->getConnection()->rollBack();
        } catch (\Exception $e) {
            throw $e;
        }

        return true;
    }


    /**
     * @param $query
     * @param int $page
     * @param int $limit
     * @param bool $useOutputWalkers
     * @return Paginator
     */
    public function paginate ($query,$page=1,$limit=10,$useOutputWalkers=true) {
        $paginator = new Paginator($query);

        $paginator->getQuery()
            ->setFirstResult($limit * ($page - 1)) // Offset
            ->setMaxResults($limit); // Limit

        if (!$useOutputWalkers) {
            $paginator->setUseOutputWalkers(false);
        }

        return $paginator;
    }


    /**
     * @param $ids
     * @param bool $checkIsDisabled
     * @return array|mixed
     * @throws Exception
     */
    public function findByIds ($ids, $checkIsDisabled=false) {

        $qb = $this->createQueryBuilder('entity');

        $qb->select('entity');
        if ($checkIsDisabled) {
            $qb->where('entity.id IN (:ids) AND (entity.isDisabled IS NULL OR entity.isDisabled=0)');
        } else {
            $qb->where('entity.id IN (:ids)');
        }
        $qb->setParameter('ids', $ids);

        $results = array();

        try {
            $results = $qb->getQuery()->getResult();
        } catch (\Exception $e) {
            throw $e;
        }

        return $results;
    }


}