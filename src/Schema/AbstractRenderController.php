<?php

namespace Schema;

use ResourceBundle\Service\CalendarTimezoneService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Response;
use Utils\Constants\System;
use Utils\Service\CacheService;

class AbstractRenderController extends Controller {


    /**
     * @param $deviceName
     * @param $filePath
     * @param array $parameters
     * @param Response|null $response
     * @return Response
     */
    public function renderByDevice($deviceName, $filePath, $parameters = array(), Response $response = null) {

        $defaultParameters = array();

        $defaultParameters["deviceName"]   = $deviceName;
        $defaultParameters["listTimezone"] = null;

        /** @var CalendarTimezoneService $timezoneService */
        $timezoneService = $this->container->get("ResourceBundle\Service\CalendarTimezoneService");
        $listTimezone = $timezoneService->getFormattedListTimezone();

        $defaultParameters["listTimezone"] = $listTimezone;

        $parameters = array_merge($defaultParameters, $parameters);

        $viewName = $deviceName."/".$filePath;

        return $this->render($viewName, $parameters, $response);
    }


    /**
     * @param $filePath
     * @param array $parameters
     * @param Response|null $response
     * @return Response
     */
    public function renderByAdmin($filePath, $parameters = array(), Response $response = null) {

        $viewName = "Admin\\".$filePath;

        $defaultParameters = array();

        $defaultParameters["lockParameter"]     = System::LOCKING_LOCK;
        $defaultParameters["unlockParameter"]   = System::LOCKING_UNLOCK;
        $defaultParameters["enableParameter"]   = System::ENABLING_ENABLE;
        $defaultParameters["disableParameter"]  = System::ENABLING_DISABLE;

        $parameters = array_merge($defaultParameters, $parameters);

        return $this->render($viewName, $parameters, $response);
    }


    /**
     * @param array $parameters
     * @param Response|null $response
     * @return Response
     */
    public function renderError ($parameters = array(), Response $response = null) {
        $viewName = "Default\\error.html.twig";
        return $this->render($viewName, $parameters, $response);
    }

    /**
     * @param $path
     * @param array $parameters
     * @param Response|null $response
     * @return Response
     */
    public function renderCustom ($path, $parameters = array(), Response $response = null) {
        //TODO: add log
        return $this->render($path, $parameters, $response);
    }


    /**
     * @param FormInterface $form
     * @return array
     */
    private function getErrorsFromForm(FormInterface $form) {

        $errors = array();
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }
        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }

        return $errors;
    }

    /**
     * @param $prefix
     * @param FormInterface $form
     * @return array
     */
    protected function getFormattedErrors ($prefix, FormInterface $form) {

        $errors = $this->getErrorsFromForm($form);

        $formattedErrors = array();

        if (!empty($errors) && count($errors) > 0) {
            $i=0;
            foreach ($errors as $key => $value) {
                $formattedErrors[$key][$i]["keys"] = array();
                if (is_array($value)) {
                    foreach ($value as $k=>$v) {
                        if (is_array($v)) {
                            $formattedErrors[$key][$i]["keys"][] = $k;
                            foreach ($v as $v1) {
                                if (!is_array($v1)) {
                                    $formattedErrors[$key][$i]["value"] = $v1;
                                    $i++;
                                }
                            }
                        } else {
                            $formattedErrors[$key][$i]["value"] = $v;
                        }
                    }
                }
            }
        }

        return $this->getWellFormattedErrors($prefix, $formattedErrors);
    }


    /**
     * @param $prefix
     * @param $formattedErrors
     * @return array
     */
    private function getWellFormattedErrors ($prefix, &$formattedErrors) {
        $formatted = array();

        if (is_array($formattedErrors) && count($formattedErrors) > 0) {
            foreach ($formattedErrors as $key=>$childErrors) {

                $keyId      = $prefix."_".$key;
                $finalValue = "";

                if (is_array($childErrors) && count($childErrors) > 0) {
                    foreach ($childErrors as $error) {
                        if (isset($error["keys"]) && count($error["keys"])>0) {
                            $keys = implode("_",$error["keys"]);
                            $finalKey = $keyId."_".$keys;
                        } else {
                            $finalKey = $keyId;
                        }
                        if (isset($error["value"])) {
                            $finalValue = $error["value"];
                        }

                        $formatted[$finalKey] = $finalValue;
                    }
                }
            }
        }

        return $formatted;
    }


}
